var SystemConfigAPI = function(option) {
	this.option = $.extend({}, this.option);
	this.setOption(option);
};
SystemConfigAPI.prototype = {
	option: {
		urlPrefix : null
	},
	setOption: function(option) {
		$.extend(this.option, option);
	},
	changePassword: function(handler, data) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/ChangePasswordAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	}
};

var AttachmentAPI = function(option) {
	this.option = $.extend({}, this.option);
	this.setOption(option);
};
AttachmentAPI.prototype = {
	option: {
		urlPrefix : null
	},
	setOption: function(option) {
		$.extend(this.option, option);
	},
	loadImage: function(handler, data) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/ImageAttachmentSearchAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	},
	removeMemberAttachment: function(handler, data) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/AttachmentRemoveAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	},
	upload: function(handler, data) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/MaterialUploadAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	}
};

var MemberAPI = function(option) {
	this.option = $.extend({}, this.option);
	this.setOption(option);
};
MemberAPI.prototype = {
	option: {
		urlPrefix : null
	},
	setOption: function(option) {
		$.extend(this.option, option);
	},
	search: function(handler, data) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/MemberSearchAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	},
	info: function(handler, data) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/MemberInfoAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data.member);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	},
	sendPost: function(handler, data) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/SendPostAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	}
};

var AdvImageAPI = function(option) {
    this.option = $.extend({}, this.option);
    this.setOption(option);
};
AdvImageAPI.prototype = {
    option: {
        urlPrefix : null
    },
    setOption: function(option) {
        $.extend(this.option, option);
    },
    search: function(handler, data) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: this.option.urlPrefix + "api/AdvImageSearchAPI.json",
            param: {},
            data: data,
            cache: false,
            statusCode: {
                404 : function() {
                    alert("API not found.");
                }
            },
            success: function(data, textStatus, jqXHR) {
                if (data.status == '0') {
                    handler.onSuccess(data);
                } else {
                    var status = null;
                    if ('status' in data) {
                        status = data.status;
                    }
                    var errors = null;
                    if ('errors' in data) {
                        errors = data.errors;
                    }
                    handler.onAPIError(status, errors);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                handler.onServerError(jqXHR, textStatus, errorThrown);
            },
            complete: function() {
            }
        });
    }
};

var NewsAPI = function(option) {
    this.option = $.extend({}, this.option);
    this.setOption(option);
};
NewsAPI.prototype = {
    option: {
        urlPrefix : null
    },
    setOption: function(option) {
        $.extend(this.option, option);
    },
    searchSummary: function(handler, data) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: this.option.urlPrefix + "api/NewsSummaryAPI.json",
            param: {},
            data: data,
            cache: false,
            statusCode: {
                404 : function() {
                    alert("API not found.");
                }
            },
            success: function(data, textStatus, jqXHR) {
                if (data.status == '0') {
                    handler.onSuccess(data);
                } else {
                    var status = null;
                    if ('status' in data) {
                        status = data.status;
                    }
                    var errors = null;
                    if ('errors' in data) {
                        errors = data.errors;
                    }
                    handler.onAPIError(status, errors);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                handler.onServerError(jqXHR, textStatus, errorThrown);
            },
            complete: function() {
            }
        });
    }
};

var TenderAPI = function(option) {
    this.option = $.extend({}, this.option);
    this.setOption(option);
};
TenderAPI.prototype = {
    option: {
        urlPrefix : null
    },
    setOption: function(option) {
        $.extend(this.option, option);
    },
    search: function(handler, data) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: this.option.urlPrefix + "api/TenderSearchAPI.json",
            param: {},
            data: data,
            cache: false,
            statusCode: {
                404 : function() {
                    alert("API not found.");
                }
            },
            success: function(data, textStatus, jqXHR) {
                if (data.status == '0') {
                    handler.onSuccess(data);
                } else {
                    var status = null;
                    if ('status' in data) {
                        status = data.status;
                    }
                    var errors = null;
                    if ('errors' in data) {
                        errors = data.errors;
                    }
                    handler.onAPIError(status, errors);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                handler.onServerError(jqXHR, textStatus, errorThrown);
            },
            complete: function() {
            }
        });
    }
};