(function($) {
	
	$(document).bind("contextmenu", function(e) {   
    	return false;   
    });
	
	$.format = function() {
		var s = arguments[0];
		for (var i = 0; i < arguments.length - 1; i++) {
			var reg = new RegExp("\\{" + i + "\\}", "gm");
			s = s.replace(reg, arguments[i + 1]);
		}
		return s;
	};
	
	$.bytes = function() {
		var str = arguments[0];
		return str.replace(/[^\x00-\xff]/g, "**").length;
	};
	
	$.alert = function(title, message, callback) {
		$('#dialog-alert').remove();
		var div = $(document.createElement('div'));
		div.attr('id', 'dialog-alert').attr('title', title);
		var p = $(document.createElement('p'));
		$(p).html(message);
		div.append(p);
		$("body").append(div);
		
		$('#dialog-alert').dialog({
			modal: true,
			buttons: {
				OK: function() {
					if (callback && typeof(callback) == "function") {
						callback.call();
					}
					$(this).dialog("close");
				}
			},
			close: function(event, ui) {
				$('#dialog-alert').remove();
			}
	    });
	};
	
	$.confirm = function(title, message, callback) {
		$('#dialog-confirm').remove();
		var div = $(document.createElement('div'));
		div.attr('id', 'dialog-confirm').attr('title', title);
		var p = $(document.createElement('p'));
		$(p).html(message);
		div.append(p);
		$("body").append(div);
		
		$('#dialog-confirm').dialog({
			modal: true,
			buttons: {
				OK: function() {
					callback.onOk();
					$(this).dialog("close");
				},
				Cancel: function() {
					callback.onCancel();
					$(this).dialog("close");
				}
			},
			close: function(event, ui) {
				$('#dialog-confirm').remove();
			}
	    });
	};
	
	$.loadImage = function(attachmentId, urlPrefix, callback) {
		var attachmentAPI = new AttachmentAPI({
			urlPrefix : urlPrefix
		});
		
		var paramData = {};
		paramData.attachment_id = attachmentId;
		attachmentAPI.loadImage({
			onSuccess : function(imageData) {
				if (imageData.image) {
					callback.onSuccess(imageData.image);
				} else {
					callback.onNoImage();
				}
			},
			onAPIError : function(status, errors) {
				callback.onError(errors);
			},
			onServerError : function(jqXHR, textStatus, errorThrown) {
				callback.onError(errorThrown);
			}
		},
		{
			param: JSON.stringify(paramData)
		});
	}
	
})(jQuery);