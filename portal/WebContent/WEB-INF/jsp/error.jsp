<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="title.error" /></title>
<link rel="shortcut icon" href="${pageContext.request.contextPath}/favicon.ico">
<link rel="stylesheet" href="${pageContext.request.contextPath}/themes/bootstrap/easyui.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/themes/icon.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css?" type="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
<script type="text/javascript">
$(function() {
	$("#logout_button").bind('click', function() {
		window.location.href = "logout";
	});
	if ($("#message").text() == "") {
		$("#message").text('<s:text name="messages.error.9004"/>');
	}
});
</script>
</head>
<body>
  <div id="container">
    <div id="header">
      <div id="logo"></div>
      <div id="header_container">
        <a href="#" id="logout_button" class="easyui-linkbutton"><s:text name="button.logout" /></a>
        <span id="current_user"><s:text name="label.current_user" /> <s:property value='userInfo.fullName' /></span>
      </div>
    </div>
    <div id="main">
      <div class="common_panel" id="login_form">
        <div class="common_panel_container">
          <div class="error">
            <ul>
              <li id="message"><s:actionerror /></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div id="footer">
      <s:text name="copyright" />
    </div>
  </div>
</body>
</html>
