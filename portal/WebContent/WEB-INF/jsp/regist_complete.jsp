<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<title>完成注册</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/upload.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/core.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/upload.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/api.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript">
$(function() {
	var urlPrefix = "${pageContext.request.contextPath}/api/";
	var memberAPI = new MemberAPI({urlPrefix : urlPrefix});
	var memberLoginName = "<s:property value='name' />";
	var memberData = {};
	
	loadMemberData({
		onSuccess: function(data) {
			memberData = data;
			
			initialize();
		},
		onError: function() {
			$.alert("错误", "会员信息不存在，请重新注册！");
			window.location.href = "regist";
		}
	});
	
	function initialize() {
		
		$('#button_home').unbind('click');
		$('#button_home').bind('click', function() {
			window.location.href = "${pageContext.request.contextPath}";
		});
	}
	
	function loadMemberData(callback) {
		var paramData = {};
		paramData.member_login_name = memberLoginName;
		memberAPI.search({
	        onSuccess : function(data) {
	        	if (data && data.total == 1) {
	        		callback.onSuccess(data.members[0]);
	        	} else {
	        		callback.onError();
	        	}
	        },
	        onAPIError : function(status, errors) {
	        	callback.onError();
	        },
	        onServerError : function(jqXHR, textStatus, errorThrown) {
	        	callback.onError();
	        }
	    },
	    {
	        param: JSON.stringify(paramData)
	    });
	}
	
});
</script>
</head>
<body class="regist_body">
  <div style="text-align: center;" align="center">
    <div id="regist_header"></div>
    <div id="regist_content">
      <img id="step_icon" src="${pageContext.request.contextPath}/img/regist_step_three.png">
      <div id="regist_form_container">
        <div class="information_confirm">
          <div style="margin: 0 auto; width:450px; padding:12px;">
            <img src="${pageContext.request.contextPath}/img/id_card.png" style="float: left;">
            <div style="font-size: 16px; font-weight: bold; text-align: left; text-indent: 20px; margin-top: 15px;">感谢您注册会员！</div>
            <div style="font-size: 15px; font-weight: bold; color: #aaa; text-align: left; text-indent: 20px;">现在，您的账户可以浏览本网站的免费信息。</div>
          </div>
          <div style="margin: 0 auto; width:450px; padding:12px;">
            <div style="font-size: 15px; text-align: left;">如果您已上传电子资质材料，我们将在72小时内进行材料审核，审核通过后您就可以浏览本站所有信息！</div>
          </div>
        </div>
        <div align="center" style="width: 100%;"><input type="button" id="button_home"></div>
      </div>
    </div>
    <div id="login_footer">
      <s:text name="copyright" />
    </div>
  </div>
</body>
</html>