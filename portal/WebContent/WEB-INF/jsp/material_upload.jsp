<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<title>会员注册-资料上传</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/magnific-popup.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/api.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript">
$(function() {
	var urlPrefix = "${pageContext.request.contextPath}/api/";
	var memberAPI = new MemberAPI({
		urlPrefix: urlPrefix
	});
	var attachmentAPI = new AttachmentAPI({
		urlPrefix: urlPrefix
	});
	var memberLoginName = "<s:property value='session.regist_name' />";
	if (memberLoginName.length == 0) {
		memberLoginName = "<s:property value='memberInfo.memberLoginName' />";
	}
	if (memberLoginName.length == 0) {
		window.location.href = "login";
	}
	var auditPhase;
	var memberData = {};
	var uploadData = {};
	var imageData = {};

	var dialog, form;

	loadMemberData({
		onSuccess: function(data) {
			memberData = data;
			auditPhase = data.lastest_audit_phase;

			initialize();

			if (data.image_contact_certificates_front > 0) {
				showImage('image_contact_front', data.image_contact_certificates_front);
			}
			if (data.image_contact_certificates_back > 0) {
				showImage('image_contact_back', data.image_contact_certificates_back);
			}
			if (data.image_legal_certificates_front > 0) {
				showImage('image_legal_front', data.image_legal_certificates_front);
			}
			if (data.image_legal_certificates_back > 0) {
				showImage('image_legal_back', data.image_legal_certificates_back);
			}
			if (data.image_license > 0) {
				showImage('image_license', data.image_license);
			}
		},
		onError: function() {
			$.alert("错误", "会员信息不存在，请重新注册！");
			window.location.href = "${pageContext.request.contextPath}/forward/regist";
		}
	});

	function initialize() {

		dialog = $("#dialog_upload").dialog({
			autoOpen: false,
			height: 100,
			width: 300,
			modal: true,
			close: function() {
				form[0].reset();
			}
		});

		var btnUpload = {
			icons: {
				primary: "ui-icon-folder-open"
			},
			label: "上传",
			text: false,
			disabled: false
		};

		form = dialog.find("form").on("submit", function(event) {
			event.preventDefault();
		});

		$("#upload_contact_front").button(btnUpload).on("click", function() {
			dialog.dialog("open");
			uploadImage(this.id);
		});

		$("#upload_contact_back").button(btnUpload).on("click", function() {
			dialog.dialog("open");
			uploadImage(this.id);
		});

		$("#upload_license").button(btnUpload).on("click", function() {
			dialog.dialog("open");
			uploadImage(this.id);
		});

		$("#upload_legal_front").button(btnUpload).on("click", function() {
			dialog.dialog("open");
			uploadImage(this.id);
		});

		$("#upload_legal_back").button(btnUpload).on("click", function() {
			dialog.dialog("open");
			uploadImage(this.id);
		});

		var btnZoomIn = {
			icons: {
				primary: "ui-icon-zoomin"
			},
			label: "放大",
			text: false,
			disabled: true
		};

		$("#zoomin_contact_front").button(btnZoomIn);
		$("#zoomin_contact_back").button(btnZoomIn);
		$("#zoomin_license").button(btnZoomIn);
		$("#zoomin_legal_front").button(btnZoomIn);
		$("#zoomin_legal_back").button(btnZoomIn);

		var btnRemove = {
			icons: {
				primary: "ui-icon-trash"
			},
			label: "删除",
			text: false,
			disabled: true
		};
		$("#remove_contact_front").button(btnRemove).click(removeImage);
		$("#remove_contact_back").button(btnRemove).click(removeImage);
		$("#remove_license").button(btnRemove).click(removeImage);
		$("#remove_legal_front").button(btnRemove).click(removeImage);
		$("#remove_legal_back").button(btnRemove).click(removeImage);

		$(".progress_bar").progressbar({
			value: 0
		});
		$(".progress_bar").hide();

		$('#button_confirm_upload').unbind('click');
		$('#button_confirm_upload').bind('click', function() {
			var paramData = {};
			paramData.member_login_name = memberLoginName;
			paramData.upload = uploadData;
			paramData.image = imageData;
			attachmentAPI.upload({
				onSuccess: function(data) {
					if (data.status == 0) {
						window.location.href = "${pageContext.request.contextPath}/forward/audit_status";
					}
				},
				onAPIError: function(status, errors) {
				},
				onServerError: function(jqXHR, textStatus, errorThrown) {
				}
			},
			{
				param: JSON.stringify(paramData)
			});
		});

		$('#button_skip_upload').unbind('click');
		$('#button_skip_upload').bind('click', function() {
			$.confirm('警告', '资质材料上传将终止，已上传的资料将不会被保存，您确认要继续吗？', {
				onOk: function() {
					window.location.href = "${pageContext.request.contextPath}/forward/audit_status";
				},
				onCancel: function() {
				}
			});
		});

		if (auditPhase == "3") {
			$('#step_icon').attr('src', '${pageContext.request.contextPath}/img/step_upload.png');
		} else if (auditPhase == "4") {
			$('#step_icon').attr('src', '${pageContext.request.contextPath}/img/step_audit_first.png');
		} else if (auditPhase == "5") {
			$('#step_icon').attr('src', '${pageContext.request.contextPath}/img/step_post.png');
		} else if (auditPhase == "6") {
			$('#step_icon').attr('src', '${pageContext.request.contextPath}/img/step_audit_last.png');
		}
	}

	function removeImage(event) {
		var targetId = this.id.replace('remove_', 'image_');

		uploadData[targetId] = {};
		if (imageData[targetId] != undefined) {
			imageData[targetId].delete_flag = true;
		}

		$('#' + targetId).attr('src', '');
		$('#' + targetId).attr('class', 'preview_no_image');
		changeButtonStatus(targetId, false);
	}

	function uploadImage(id) {
		var targetId = id.replace('upload_', 'file_');
		var uploadParam = {};
		uploadParam.person = memberLoginName;
		uploadParam.person_type = 1;
		uploadParam.purpose = id;
		uploadParam.file_type = 1;
		$('#upload_param').val(JSON.stringify(uploadParam));
	}

	function loadMemberData(callback) {
		var paramData = {};
		paramData.member_login_name = memberLoginName;
		memberAPI.search({
			onSuccess: function(data) {
				if (data && data.total == 1) {
					callback.onSuccess(data.members[0]);
				} else {
					callback.onError();
				}
			},
			onAPIError: function(status, errors) {
				callback.onError();
			},
			onServerError: function(jqXHR, textStatus, errorThrown) {
				callback.onError();
			}
		}, {
			param: JSON.stringify(paramData)
		});
	}

	function changeButtonStatus(targetId, enabled) {
		var zoominId = targetId.replace('image_', 'zoomin_');
		if (enabled) {
			$('#' + zoominId).button('enable');
		} else {
			$('#' + zoominId).button('disable');
		}

		var removeId = targetId.replace('image_', 'remove_');
		if (enabled) {
			$('#' + removeId).button('enable');
		} else {
			$('#' + removeId).button('disable');
		}
	}

	function showImage(targetId, imageId) {
		$.loadImage(imageId, urlPrefix, {
			onSuccess: function(image) {
				$('#' + targetId).attr('src', image);
				$('#' + targetId).attr('class', 'preview');
				changeButtonStatus(targetId, true);
				$('#' + targetId.replace('image_', 'a_')).attr('href', image);
				$('#' + targetId.replace('image_', 'a_')).magnificPopup({
					type: 'image',
					closeOnContentClick: true,
					image: {
						verticalFit: true
					}
				});

				$('#' + targetId.replace('image_', 'zoomin_')).attr('href', image);
				$('#' + targetId.replace('image_', 'zoomin_')).magnificPopup({
    				type: 'image',
    				closeOnContentClick: true,
    				image: {
    					verticalFit: true
    				}
    			});

				imageData[targetId] = {};
				imageData[targetId].attachment_id = imageId;
			},
			onNoImage: function() {
				$('#' + targetId).attr('src', '');
				$('#' + targetId).attr('class', 'preview_no_image');
				changeButtonStatus(targetId, false);

				imageData[targetId] = {};
			},
			onError: function(error) {
				$('#' + targetId).attr('src', '');
				$('#' + targetId).attr('class', 'preview_no_image');
				changeButtonStatus(targetId, false);

				imageData[targetId] = {};
			}
		});
	}

	if (memberLoginName.length > 0) {
		$('#link_login').hide();
		$('#link_logout').show();
		$('#link_center').show();
		$('#link_regist').hide();
	} else {
		$('#link_login').show();
		$('#link_logout').hide();
		$('#link_center').hide();
		$('#link_regist').show();
	}
	$('#member_name').html(memberLoginName + "&nbsp;&nbsp;");

	$("#image_frame").load(function() {
		var uploadResultJson = $(this).contents().find("*").first().text();
		var uploadResult = $.parseJSON(uploadResultJson);
		if (uploadResult.status == 0) {
			var targetId = uploadResult.upload_info.upload_purpose.replace('upload_', 'image_');

			$('#' + targetId).attr('src', uploadResult.image);
			$('#' + targetId).attr('class', 'preview');
			changeButtonStatus(targetId, true);
			$('#' + targetId.replace('image_', 'a_')).attr('href', uploadResult.image);
			$('#' + targetId.replace('image_', 'a_')).magnificPopup({
				type: 'image',
				closeOnContentClick: true,
				image: {
					verticalFit: true
				}
			});

			$('#' + targetId.replace('image_', 'zoomin_')).attr('href', uploadResult.image);
			$('#' + targetId.replace('image_', 'zoomin_')).magnificPopup({
				type: 'image',
				closeOnContentClick: true,
				image: {
					verticalFit: true
				}
			});

			uploadData[targetId] = uploadResult.upload_info;

			if (imageData[targetId] && imageData[targetId].attachment_id) {
				imageData[targetId].delete_flag = true;
			}
		} else if (uploadResult.errors) {
			$.alert("错误", uploadResult.errors[0].msg);
		}
		dialog.dialog('close');
	});

	$('#file_upload').change(function() {
		this.form.submit();
	});

});
</script>
</head>
<body class="regist_body">
  <div style="text-align: center;" align="center">
    <div id="topbar">
      <div class="wp">
        <div class="left">
          <span id="member_name" style="font-weight: bold;"></span> <span>欢迎来到辽地集团网络集中采购平台！</span>
          <a id="link_login" href="${pageContext.request.contextPath}/forward/login" rel="nofollow" class="smart">登录</a>
          <a id="link_logout" href="${pageContext.request.contextPath}/forward/logout" rel="nofollow" class="smart">退出</a>
          <a id="link_center" href="${pageContext.request.contextPath}/forward/member_center" rel="nofollow" class="smart">会员中心</a>
          <a id="link_regist" href="${pageContext.request.contextPath}/forward/regist" rel="nofollow" class="smart">免费注册</a>
        </div>
        <div class="right">
          <span class="topbar_tel">服务热线：<span class="topbar_tel_no">XXXX-XXXXXXXX</span></span>
        </div>
      </div>
    </div>
    <div id="regist_content">
      <img id="step_icon" src="${pageContext.request.contextPath}/img/step_upload.png">
      <div id="regist_form_container">
        <div class="information_confirm">
          <div style="margin: 0 auto; width: 450px; padding: 12px;">
            <img src="${pageContext.request.contextPath}/img/id_card.png" style="float: left;">
            <div style="font-size: 16px; font-weight: bold; text-align: left; text-indent: 20px; margin-top: 15px;">感谢您注册会员！</div>
            <div style="font-size: 15px; font-weight: bold; color: #aaa; text-align: left; text-indent: 20px;">现在，您的账户可以浏览本网站的免费信息。</div>
          </div>
          <div style="margin: 0 auto; width: 450px; padding: 12px;">
            <div style="font-size: 15px; text-align: left;">如果您想浏览最新、最及时的招标采购信息，请上传电子资质材料，申请成为认证会员！</div>
          </div>
        </div>
        <div class="material_upload">
          <table class="upload_table">
            <thead>
              <tr>
                <th>联系人证件正面(必须)</th>
                <th>联系人证件背面(必须)</th>
                <th>工商营业执照(必须)</th>
                <th>法人证件正面(可选)</th>
                <th>法人证件背面(可选)</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td width="165" height="165" style="padding: 5px;">
                  <a id="a_contact_front" title="联系人证件正面"><img id="image_contact_front" class="preview_no_image"></a>
                  <div id="progress_upload_contact_front" style="position: absolute; bottom; 0 px; width: 163px; height: 5px;" class="progress_bar"></div>
                </td>
                <td width="165" height="165" style="padding: 5px;">
                  <a id="a_contact_back" title="联系人证件背面"><img id="image_contact_back" class="preview_no_image"></a>
                  <div id="progress_upload_contact_back" style="position: absolute; bottom; 0 px; width: 163px; height: 5px;" class="progress_bar"></div>
                </td>
                <td width="165" height="165" style="padding: 5px;">
                  <a id="a_license" title="工商营业执照"><img id="image_license" class="preview_no_image"></a>
                  <div id="progress_upload_license" style="position: absolute; bottom; 0 px; width: 163px; height: 5px;" class="progress_bar"></div>
                </td>
                <td width="165" height="165" style="padding: 5px;">
                  <a id="a_legal_front" title="法人证件正面"><img id="image_legal_front" class="preview_no_image"></a>
                  <div id="progress_upload_legal_front" style="position: absolute; bottom; 0 px; width: 163px; height: 5px;" class="progress_bar"></div>
                </td>
                <td width="165" height="165" style="padding: 5px;">
                  <a id="a_legal_back" title="法人证件背面"><img id="image_legal_back" class="preview_no_image"></a>
                  <div id="progress_upload_legal_back" style="position: absolute; bottom; 0 px; width: 163px; height: 5px;" class="progress_bar"></div>
                </td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <th>
                  <a href="#" id="upload_contact_front"></a>
                  <a href="#" id="zoomin_contact_front"></a>
                  <a href="#" id="remove_contact_front"></a>
                </th>
                <th>
                  <a href="#" id="upload_contact_back"></a>
                  <a href="#" id="zoomin_contact_back"></a>
                  <a href="#" id="remove_contact_back"></a>
                </th>
                <th>
                  <a href="#" id="upload_license"></a>
                  <a href="#" id="zoomin_license"></a>
                  <a href="#" id="remove_license"></a>
                </th>
                <th>
                  <a href="#" id="upload_legal_front"></a>
                  <a href="#" id="zoomin_legal_front"></a>
                  <a href="#" id="remove_legal_front"></a>
                </th>
                <th>
                  <a href="#" id="upload_legal_back"></a>
                  <a href="#" id="zoomin_legal_back"></a>
                  <a href="#" id="remove_legal_back"></a>
                </th>
              </tr>
              <tr>
                <td colspan="5" align="center" style="padding-top: 20px;">
                  <input type="button" id="button_skip_upload">
                  <input type="button" id="button_confirm_upload">
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
      <div id="dialog_upload" title="请选择要上传的文件">
        <form id="upload_form" name="upload_form" action="${pageContext.request.contextPath}/file/UploadAction" method="post" enctype="multipart/form-data" target="image_frame">
          <input type="file" name="material" id="file_upload" accept="image/*">
          <iframe width="0" height="0" id="image_frame" name="image_frame" style="border: none;"></iframe>
          <input type="hidden" id="upload_param" name="param" />
        </form>
      </div>
    </div>
    <div id="login_footer">
      <s:text name="copyright" />
    </div>
  </div>
</body>
</html>