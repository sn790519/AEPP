<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<title>会员注册</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery.idcode.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.idcode.js"></script>
<script type="text/javascript">
$(function() {
	var validate = $("#regist_form").validate({
		focusInvalid: true,
		groups: {
			tel: "area_code telephone mobile"
		},
		errorPlacement: function(error, element) {
			if (element.attr("name") == "area_code" || element.attr("name") == "telephone" || element.attr("name") == "mobile") {
				error.insertAfter("#mobile");
			} else if (element.attr("id") == "captcha_code_input") {
				error.insertAfter("#captcha_code_tip_ck");
			} else {
				error.insertAfter(element);
			}
		},
		rules: {
			member_login_name: {
				required: true,
				remote: {
					type: "POST",
					url: "${pageContext.request.contextPath}/api/MemberExistsAPI.json",
					dataType: "json",
					data : {
						param: function() {
							return JSON.stringify({member_login_name: $("#member_login_name").val()	});
						}
					},
					dataFilter: function(data, type) {
						return !$.parseJSON(data).result;
					}
				},
				minlength: 6,
				maxlength: 16
			},
			member_login_password: {
				required: true,
				minlength:6,
				maxlength:16
			},
			confirm_password: {
				required: true,
				minlength:6,
				maxlength:16,
				equalTo: "#member_login_password"
			},
			license_number: {
				required: true,
				maxlength: 15,
			},
			enterprise_name: {
				required: true,
				maxlength: 200
			},
			contact_name: {
				required: true,
				maxlength: 50
			},
			contact_certificates_no: {
				required: true,
				maxlength: 20
			},
			area_code: {
				required: {
					depends: function() {
						return ($("#mobile").val().length <= 0);
					}
				},
				maxlength: 4,
				number: true
			},
			telephone: {
				required: {
					depends: function() {
						return ($("#mobile").val().length <= 0);
					}
				},
				maxlength: 10,
				number: true
			},
			mobile: {
				required: {
					depends: function() {
						return ($("#area_code").val().length <= 0 || $("#telephone").val().length <= 0);
					}
				},
				maxlength: 11,
				number: true
			},
			email: {
				required: true,
				maxlength: 50,
				email: true
			},
			qq_number: {
				maxlength: 20,
				number: true
			},
			weixin_number: {
				maxlength: 50
			},
			captcha_code_input: {
				required: true,
				captcha: true
			}
		},
		messages: {
			member_login_name: {
				required: "请输入用户名！",
				remote: "此用户名已被他人注册，请输入其他用户名！",
				minlength: $.validator.format("用户名不能小于{0}个字符！"),
				maxlength: $.validator.format("用户名不能大于{0}个字符！")
			},
			member_login_password: {
				required: "请输入密码！",
				minlength: $.validator.format("密码不能小于{0}个字符！"),
				maxlength: $.validator.format("密码不能大于{0}个字符！")
			},
			confirm_password: {
				required: "请输入确认密码！",
				minlength: $.validator.format("确认不能小于{0}个字符！"),
				maxlength: $.validator.format("确认密码不能大于{0}个字符！"),
				equalTo: "两次输入密码不一致！"
			},
			license_number: {
				required: "请输入工商营业执照编号！",
				maxlength: $.validator.format("营业执照编号不能大于{0}个字符！")
			},
			enterprise_name: {
				required: "请输入企业名称！",
				maxlength: $.validator.format("企业名称不能大于{0}个字符！")
			},
			contact_name: {
				required: "请输入联系人姓名！",
				maxlength: $.validator.format("联系人姓名不能大于{0}个字符！")
			},
			contact_certificates_no: {
				required: "请输入联系人证件号码！",
				maxlength: $.validator.format("联系人证件号码不能大于{0}个字符！")
			},
			area_code: {
				maxlength: $.validator.format("区号不能大于{0}个字符！"),
				number: "请输入数字！"
			},
			telephone: {
				maxlength: $.validator.format("固定电话号码不能大于{0}个字符！"),
				number: "请输入数字！"
			},
			mobile: {
				required: "请输入区号+固定电话号码或者手机号码！",
				maxlength: $.validator.format("手机号码不能大于{0}个字符！"),
				number: "请输入数字！"
			},
			email: {
				required: "请输入电子邮件地址！",
				maxlength: $.validator.format("电子邮件地址不能大于{0}个字符！"),
				email: "请输入合法邮件地址！"
			},
			qq_number: {
				maxlength: $.validator.format("QQ号码不能大于{0}个字符！"),
				number: "请输入数字！"
			},
			weixin_number: {
				maxlength: $.validator.format("微信号码不能大于{0}个字符！")
			},
			captcha_code_input: {
				required: "请输入验证码！",
				captcha: "输入的验证码不正确！"
			}
		}
	});
	
	jQuery.validator.addMethod("captcha", function(value, element) {
	    return value.toLowerCase() == $.idcode.getCode().toLowerCase();
	}, "");
	
 	$("#enterprise_property").selectmenu();
	$("#enterprise_category").selectmenu();
	$("#contact_certificates_type").selectmenu();
	
	$("#button_regist").bind("click", function() {
		if (validate.form()) {
			var paramData = {
				member_login_name: $("#member_login_name").val(),
				member_login_password: $("#member_login_password").val(),
				license_number: $("#license_number").val(),
				enterprise_name: $("#enterprise_name").val(),
				enterprise_property: $("#enterprise_property").val(),
				enterprise_category: $("#enterprise_category").val(),
				contact_name: $("#contact_name").val(),
				contact_certificates_type: $("#contact_certificates_type").val(),
				contact_certificates_no: $("#contact_certificates_no").val(),
				area_code: $("#area_code").val(),
				telephone: $("#telephone").val(),
				mobile: $("#mobile").val(),
				email: $("#email").val(),
				qq_no: $("#qq_no").val(),
				weixin_no: $("#weixin_no").val()
			};
	        var url = "${pageContext.request.contextPath}/api/MemberRegistAPI.json";
	        $.ajax({
	            type: "POST",
	            dataType: "json",
	            url: url,
	            param: {},
	            data: {
	            	param: JSON.stringify(paramData)
	            },
	            cache: false,
	            success: function(data, textStatus, jqXHR){
	            	window.location.href = "${pageContext.request.contextPath}/forward/login";
	            },
				error: function(jqXHR, textStatus, errorThrown) {
				},
	            complete: function(){
	            }
	        });
		}
	});
	
	$.idcode.setCode();
	
	$('#enterprise_category-menu').css('height', '200px');
});
</script>
</head>
<body class="regist_body">
  <div style="text-align: center;" align="center">
    <div id="topbar">
      <div class="wp">
        <div class="left">
          <span id="member_name" style="font-weight: bold;"></span>
          <span>欢迎来到辽地集团网络集中采购平台！</span>
          <a href="home" rel="nofollow" class="smart">首页</a>
          已是会员？立即
          <a href="login" rel="nofollow" class="smart">登录</a>
        </div>
        <div class="right">
          <span class="topbar_tel">
            服务热线：
            <span class="topbar_tel_no">XXXX-XXXXXXXX</span>
          </span>
        </div>
      </div>
    </div>
    <div id="regist_content">
      <img id="step_icon" src="${pageContext.request.contextPath}/img/step_regist.png">
      <div id="regist_form_container">
        <form id="regist_form">
          <div style="padding: 5px; font-size: 16px; color: #ff7117; border-bottom: 1px solid #DDD;">账户信息</div>
          <div style="padding-top: 5px; padding-bottom: 5px;">
            <table class="regist_table">
              <tbody>
                <tr>
                  <th width="150" align="right">用户名</th>
                  <td width="750" style="padding-left: 10px;">
                    <input type="text" id="member_login_name" name="member_login_name" style="width: 300px;" placeholder="6-16位小写字母或数字">
                  </td>
                </tr>
                <tr>
                  <th align="right">密码</th>
                  <td style="padding-left: 10px;">
                    <input type="password" id="member_login_password" name="member_login_password" style="width: 300px;" placeholder="6-16位字符，可使用字母、数字和符号的组合">
                  </td>
                </tr>
                <tr>
                  <th align="right">确认密码</th>
                  <td style="padding-left: 10px;">
                    <input type="password" id="confirm_password" name="confirm_password" style="width: 300px;" placeholder="请再次输入密码">
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div style="padding: 5px; font-size: 16px; color: #ff7117; border-bottom: 1px solid #DDD;">企业信息</div>
          <div style="padding-top: 5px; padding-bottom: 5px;">
            <table class="regist_table">
              <tbody>
                <tr>
                  <th width="150" align="right">营业执照编号</th>
                  <td width="750" style="padding-left: 10px;">
                    <input type="text" id="license_number" name="license_number" style="width: 300px;" placeholder="工商营业执照编号">
                  </td>
                </tr>
                <tr>
                  <th align="right">企业名称</th>
                  <td style="padding-left: 10px;">
                    <input type="text" id="enterprise_name" name="enterprise_name" style="width: 300px;" placeholder="工商营业执照中注册的企业名称">
                  </td>
                </tr>
                <tr>
                  <th align="right">企业性质/行业分类</th>
                  <td style="padding-left: 10px; font-siza: 12px;">
                    <select id="enterprise_property" style="height: 27px; width: 162px;">
                      <option value="1" selected="selected">国有企业</option>
                      <option value="2">中外合作企业</option>
                      <option value="3">中外合资企业</option>
                      <option value="4">外商独资企业</option>
                      <option value="5">集体企业</option>
                      <option value="6">私营企业</option>
                    </select>
                    <select id="enterprise_category" style="height: 27px; width: 162px;">
                      <option value="1" selected="selected">机构组织</option>
                      <option value="2">农林牧渔</option>
                      <option value="3">医药卫生</option>
                      <option value="4">建筑建材</option>
                      <option value="5">冶金矿产</option>
                      <option value="6">石油化工</option>
                      <option value="7">水利水电</option>
                      <option value="8">交通运输</option>
                      <option value="9">信息产业</option>
                      <option value="10">机械机电</option>
                      <option value="11">轻工食品</option>
                      <option value="12">服装纺织</option>
                      <option value="13">专业服务</option>
                      <option value="14">安全防护</option>
                      <option value="15">环保绿化</option>
                      <option value="16">旅游休闲</option>
                      <option value="17">办公文教</option>
                      <option value="18">电子电工</option>
                      <option value="19">玩具礼品</option>
                      <option value="20">家居用品</option>
                      <option value="21">物资专材</option>
                      <option value="22">包装用品</option>
                      <option value="23">体育用品</option>
                      <option value="24">办公家具</option>
                      <option value="99">其他分类</option>
                    </select>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div style="padding: 5px; font-size: 16px; color: #ff7117; border-bottom: 1px solid #DDD;">联系人信息</div>
          <div style="padding-top: 5px; padding-bottom: 5px;">
            <table class="regist_table">
              <tbody>
                <tr>
                  <th width="150" align="right">联系人姓名</th>
                  <td width="750" style="padding-left: 10px;"><input type="text" id="contact_name" name="contact_name" style="width: 300px;" placeholder="主要联系人姓名"></td>
                </tr>
                <tr>
                  <th align="right">联系人证件</th>
                  <td style="padding-left: 10px;">
                    <select id="contact_certificates_type" style="height: 27px; width: 325px;">
                      <option value="1" selected="selected">身份证</option>
                      <option value="2">护照</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <th align="right">联系人证件号码</th>
                  <td style="padding-left: 10px;"><input type="text" id="contact_certificates_no" name="contact_certificates_no" style="width: 300px;" placeholder="主要联系人证件号码"></td>
                </tr>
                <tr>
                  <th align="right">固定电话</th>
                  <td style="padding-left: 10px;">
                    <input type="text" id="area_code" name="area_code" style="width: 60px;" placeholder="区号" maxlength="4">
                    -
                    <input type="text" id="telephone" name="telephone" style="width: 202px;" placeholder="固定电话号码" maxlength="8">
                  </td>
                </tr>
                <tr>
                  <th align="right">手机</th>
                  <td style="padding-left: 10px;"><input type="text" id="mobile" name="mobile" style="width: 300px;" placeholder="手机号码" maxlength="11"></td>
                </tr>
                <tr>
                  <th align="right">电子邮件</th>
                  <td style="padding-left: 10px;"><input type="email" id="email" name="email" style="width: 300px;" placeholder="电子邮件地址" maxlength="50"></td>
                </tr>
                <tr>
                  <th align="right">QQ</th>
                  <td style="padding-left: 10px;"><input type="text" id="qq_no" name="qq_no" style="width: 300px;" placeholder="QQ号码" maxlength="20"></td>
                </tr>
                <tr>
                  <th align="right">微信号</th>
                  <td style="padding-left: 10px;"><input type="text" id="weixin_no" name="weixin_no" style="width: 300px;" placeholder="微信号" maxlength="50"></td>
                </tr>
                <tr>
                  <th align="right">验证码</th>
                  <td style="padding-left: 10px;"><span id="idcode"></span></td>
                </tr>
                <tr>
                  <th align="right">&nbsp;</th>
                  <td style="padding-left: 10px;"><input type="button" id="button_regist"></td>
                </tr>
              </tbody>
            </table>
          </div>
        </form>
      </div>
    </div>
    <div id="login_footer">
      <s:text name="copyright" />
    </div>
  </div>
</body>
</html>