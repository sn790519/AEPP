<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<title>AEPP采购信息发布平台</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/api.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.slides.min.js"></script>
<script type="text/javascript">
$(function() {
    
    var urlPrefix = "${pageContext.request.contextPath}/api/";
	var advImageAPI = new AdvImageAPI({
		urlPrefix : urlPrefix
	});
	var tenderAPI = new TenderAPI({
		urlPrefix : urlPrefix
	});
	
	var memberLoginName = "<s:property value='memberInfo.memberLoginName' />";
	if (memberLoginName.length > 0) {
		$('#link_login').hide();
		$('#link_logout').show();
		$('#link_center').show();
		$('#link_regist').hide();
	} else {
		$('#link_login').show();
		$('#link_logout').hide();
		$('#link_center').hide();
		$('#link_regist').show();
	}
	$('#member_name').html(memberLoginName + "&nbsp;&nbsp;");
	
	getBannarImages({
	    onSuccess: function(images) {
	        $('#image_slider').remove();
        	$('#banner').append('<div id="image_slider"></div>');
        	$.each(images, function(index, row) {
        	    $('#image_slider').append('<img id="image' + (index + 1) + '" src="' + row.image + '" title="' + row.title + '" alt="' + row.description + '">');
            });

            initializePreview();
	    }
	});
	
	getTenderInformation({
	    onSuccess: function(tenders) {
	        $('#tender_list').children('li').remove();
	        $.each(tenders, function(index, row) {
	            var title = '';
	            if (row.status == 1) {
	                title = '招标预告';
	            } else {
	                title = '招标公告';
	            }
	            $('#tender_list').append('<li><a href="#" style="color: red;">[' + title + ']</a>&nbsp;<a href="#" title="' + row.tender_title + '">' + row.tender_title + '</a></li>');
	        });
	    }
	});
	
	function getBannarImages(callback) {
		var paramData = {};
		paramData.use_flag = true;
		advImageAPI.search({
			onSuccess : function(data) {
			    if (callback) {
			        if (data.images) {
			            if (callback.onSuccess && typeof(callback.onSuccess) == "function") {
			                callback.onSuccess(data.images);
			            }
					} else {
					    if (callback.onError && typeof(callback.onError) == "function") {
			                callback.onError();
			            }
					}
			    }
			},
			onAPIError : function(status, errors) {
			},
			onServerError : function(jqXHR, textStatus, errorThrown) {
			}
		}, {
			param : JSON.stringify(paramData)
		});
	}
	
	function getTenderInformation(callback) {
		var paramData = {};
		paramData.status_from = 1;
		paramData.status_to = 4;
		tenderAPI.search({
			onSuccess : function(data) {
			    if (callback) {
			        if (data.total > 0) {
			            if (callback.onSuccess && typeof(callback.onSuccess) == "function") {
			                callback.onSuccess(data.tenders);
			            }
					} else {
					    if (callback.onError && typeof(callback.onError) == "function") {
			                callback.onError();
			            }
					}
			    }
			},
			onAPIError : function(status, errors) {
			},
			onServerError : function(jqXHR, textStatus, errorThrown) {
			}
		}, {
			param : JSON.stringify(paramData),
			page : 1,
			rows : 25
		});
	}
	
	function getNews(callback) {
		var paramData = {};
		tenderAPI.search({
			onSuccess : function(data) {
			    if (callback) {
			        if (data.total > 0) {
			            if (callback.onSuccess && typeof(callback.onSuccess) == "function") {
			                callback.onSuccess(data.tenders);
			            }
					} else {
					    if (callback.onError && typeof(callback.onError) == "function") {
			                callback.onError();
			            }
					}
			    }
			},
			onAPIError : function(status, errors) {
			},
			onServerError : function(jqXHR, textStatus, errorThrown) {
			}
		}, {
			param : JSON.stringify(paramData)
		});
	}
	
	function initializePreview() {
    	$("#image_slider").slidesjs({
            width: 1000,
            height: 200,
            start: 1,
            play: {
                active: false,
                effect: "slide",
                interval: 5000,
                auto: true,
                swap: false,
                pauseOnHover: true,
                restartDelay: 2500
            },
            navigation: {
                active: false
            },
            pagination: {
                active: false
            },
            effect: {
            	slide: {
            		speed: 1000
            	}
            }
        });
    }
});
</script>
</head>
<body>
  <div style="text-align: center;" align="center">
    <div id="topbar">
      <div class="wp">
        <div class="left">
          <span id="member_name" style="font-weight:bold;"></span>
          <span>欢迎来到辽地集团网络集中采购平台！</span>
          <a id="link_login" href="${pageContext.request.contextPath}/forward/login" rel="nofollow" class="smart">登录</a>
          <a id="link_logout" href="${pageContext.request.contextPath}/forward/logout" rel="nofollow" class="smart">退出</a>
          <a id="link_center" href="${pageContext.request.contextPath}/forward/member_center" rel="nofollow" class="smart">会员中心</a>
          <a id="link_regist" href="${pageContext.request.contextPath}/forward/regist" rel="nofollow" class="smart">免费注册</a>
        </div>
        <div class="right">
          <span class="topbar_tel">服务热线：<span class="topbar_tel_no">XXXX-XXXXXXXX</span></span>
        </div>
      </div>
    </div>
    <div id="banner" style="margin-top: 5px; margin-bottom: 5px;">
      <div id="image_slider"></div>
    </div>
    <div class="navibar">
      <ul>
        <li><a href="home">网站首页</a></li>
        <li><a href="#">招标公告</a></li>
        <li><a href="#">招标预告</a></li>
        <li><a href="#">入围公告</a></li>
        <li><a href="#">中标公告</a></li>
        <li><a href="#">招标文件</a></li>
        <li><a href="#">新闻通告</a></li>
        <li><a href="#">会员一览</a></li>
      </ul>
    </div>
    <div id="content" style="width: 1000px; height: 600px; margin: 0 auto; margin-top: 10px;">
      <div id="left" style="border: 1px solid #eee; float: left; height: 590px; width: 358px; text-align: left; padding: 5px;">
        <label style="font-size: 15px; font-weight: bold; width: 358px; height:35px; border-bottom: 1px double #ccc; text-shadow: 2px 2px 1px #ccc;">最新招标信息</label>
        <a href="#" style="float: right;">更多&gt;&gt;</a>
        <ul id="tender_list" style="list-style: none; width: 358px; margin-top: 5px; font-size: 13px;"></ul>
      </div>
      <div id="center-top" style="border: 1px solid #eee; float: left; height: 285px; width: 358px; text-align: left; margin-left: 4px; padding: 5px;">
        <label style="font-size: 15px; font-weight: bold; width: 358px; height:35px; border-bottom: 1px double #ccc; text-shadow: 2px 2px 1px #ccc;">中标公告</label>
        <a href="#" style="float: right;">更多&gt;&gt;</a>
      </div>
      <div id="right-top" style="border: 1px solid #eee; float: right; height: 285px; width: 240px; text-align: left; padding: 5px;">
        <label style="font-size: 15px; font-weight: bold; width: 240px; height:35px; border-bottom: 1px double #ccc; text-shadow: 2px 2px 1px #ccc;">新闻通告</label>
        <a href="#" style="float: right;">更多&gt;&gt;</a>
        <ul id="news_list" style="list-style: none; width: 358px; margin-top: 5px; font-size: 13px;"></ul>
      </div>
      <div id="center-bottom" style="border: 1px solid #eee; float: left; height: 285px; width: 358px; text-align: left; margin-left: 4px; margin-top: 8px; padding: 5px;">
        <label style="font-size: 15px; font-weight: bold; width: 358px; height:35px; border-bottom: 1px double #ccc; text-shadow: 2px 2px 1px #ccc;">入围公告</label>
        <a href="#" style="float: right;">更多&gt;&gt;</a>
      </div>
      <div id="right-bottom" style="border: 1px solid #eee; float: right; height: 285px; width: 240px; text-align: left; margin-top: 8px; padding: 5px;">
        <label style="font-size: 15px; font-weight: bold; width: 240px; height:35px; border-bottom: 1px double #ccc; text-shadow: 2px 2px 1px #ccc;">会员展示</label>
      </div>
    </div>
    <div id="footer"><s:text name="copyright" /></div>
  </div>
</body>
</html>
