<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<title>AEPP采购信息发布平台</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery.idcode.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.idcode.js"></script>
<script type="text/javascript">
$(function() {
	
	var authFail = "<s:property value='authFail' />";
	var auditPhase = "<s:property value='memberInfo.lastestAuditPhase' />";
	
 	var validate = $("#login_form").validate({
		focusInvalid: true,
		errorPlacement: function(error, element) {
			if (element.attr("id") == "captcha_code_input") {
				error.insertAfter("#captcha_code");
			} else {
				error.insertAfter(element);
			}
		},
		rules: {
			captcha_code_input: {
				required: true,
				captcha: true
			}
		},
		messages: {
			captcha_code_input: {
				required: "验证码错误",
				captcha: "验证码错误"
			}
		}
	});
	
	jQuery.validator.addMethod("captcha", function(value, element) {
	    return value.toLowerCase() == $.idcode.getCode().toLowerCase();
	}, "");

	$.idcode.setCode({
		codeTip: ""
	});
	
	$("#btn_login").button().click(function(event) {
		if ($('#member_login_name').val().length == 0 && $('#member_login_password').val().length == 0) {
			showErrorMsg([$('#member_login_name'), $('#member_login_password')], "请输入登录名和密码！");
		} else if ($('#member_login_name').val().length == 0) {
        	showErrorMsg($('#member_login_name'), "请输入登录名！");
        } else if ($('#member_login_password').val().length == 0) {
        	showErrorMsg($('#member_login_password'), "请输入密码！");
        } else {
        	clearErrorMsg();
        	if (validate.form()) {
        		$('#login_form').submit();
        	}
        }
    });
	
	clearErrorMsg();
	
	if (authFail == "true") {
		showErrorMsg($('#member_login_password'), "登录名与密码不匹配，请重新输入！");
	}
	
	function showErrorMsg(target, msg) {
		if (target) {
			if ($.isArray(target)) {
				$.each(target, function(index, row) {
					row.addClass('error');
				});
			} else {
				target.addClass('error');
			}
		}
		if (msg) {
			$('#msg').text(msg);
			$('.msg-wrap').css('visibility', 'visible');
		}
	}
	
	function clearErrorMsg() {
		$('#msg').text('');
		$('.msg-wrap').css('visibility', 'hidden');
	}

});
</script>
</head>
<body>
  <div style="text-align: center;" align="center">
    <div id="topbar">
      <div class="wp">
        <div class="left">
          <span>欢迎来到辽地集团网络集中采购平台！</span>
          <a href="${pageContext.request.contextPath}/forward/home" rel="nofollow" class="smart">首页</a>
          <a href="${pageContext.request.contextPath}/forward/regist" rel="nofollow" class="smart">免费注册</a>
        </div>
        <div class="right">
          <span class="topbar_tel">服务热线：<span class="topbar_tel_no">XXXX-XXXXXXXX</span></span>
        </div>
      </div>
    </div>
    <div id="content">
      <div id="login_container">
        <div id="login_box">
          <s:form action="auth" namespace="/forward" id="login_form" autocomplete="off">
            <h1 style="margin-top: 0;">会员登录</h1>
            <div class="msg-wrap" style="visibility: hidden;">
              <div class="msg-error"><b></b><span id="msg"></span></div>
            </div>
            <p><s:textfield name="memberLoginName" id="member_login_name" autocomplete="off" /></p>
            <p><s:password name="memberLoginPassword" id="member_login_password" autocomplete="off" /></p>
            <p><span id="idcode"></span></p>
            <p><a id="btn_login" href="#">登录</a></p>
            <p>还不是会员？<a href="${pageContext.request.contextPath}/forward/regist" rel="nofollow" class="smart">免费注册</a></p>
          </s:form>
        </div>
      </div>
    </div>
    <div id="footer">
      <s:text name="copyright" />
    </div>
  </div>
</body>
</html>
