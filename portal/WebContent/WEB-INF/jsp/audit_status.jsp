<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<title>审核结果</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery.dataTables.min.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/api.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(function() {
	var urlPrefix = "${pageContext.request.contextPath}/api/";
	var memberAPI = new MemberAPI({urlPrefix : urlPrefix});
	var memberLoginName = "<s:property value='session.member_info.memberLoginName' />";
	var auditPhase;
	var auditResult;
	
	function initialize() {
		
		if (memberLoginName == "") {
			window.location.href = "${pageContext.request.contextPath}/forward/login";
		}
		
		$('#post_info').hide();
		
		if (auditPhase == "3") {
			$('#step_icon').attr('src', '${pageContext.request.contextPath}/img/step_upload.png');
			$('#message').html('如果您想浏览和投标本站的采购信息，请您立即上传电子资质文件，申请成为认证会员！');
			$('#button_upload_now').show();
			$('#button_edit').hide();
			$('#button_reupload').hide();
			$('#button_post').hide();
		} else if (auditPhase == "4") {
			$('#step_icon').attr('src', '${pageContext.request.contextPath}/img/step_audit_first.png');
			$('#message').html('您的注册信息和上传的电子资质文件我们正在进行审核！');
			$('#button_upload_now').hide();
			if (auditResult == 2) {
				$('#button_edit').show();
				$('#button_reupload').show();
			} else {
				$('#button_edit').hide();
				$('#button_reupload').hide();
			}
			$('#button_post').hide();
		} else if (auditPhase == "5") {
			$('#step_icon').attr('src', '${pageContext.request.contextPath}/img/step_post.png');
			$('#message').html('您已通过初级审核，我们还需要进行纸质资质文件的审核！请您将资质文件的复印副本通过快递邮寄给我们！我们将在收到资质文件的三个工作日内完成最终的审核！<br><br>邮寄地址：辽宁省沈阳市XX区XX大街XX大厦XXX室，100000<br>收件人：XXX，130XXXXXXXX');
			$('#button_upload_now').hide();
			$('#button_edit').hide();
			$('#button_reupload').hide();
			$('#button_post').show();
			$('#post_info').show();
		} else if (auditPhase == "6") {
			$('#step_icon').attr('src', '${pageContext.request.contextPath}/img/step_audit_last.png');
			$('#button_upload_now').hide();
			$('#button_edit').hide();
			$('#button_reupload').hide();
			$('#button_post').hide();
		} else if (auditPhase == "9") {
			window.location.href = "${pageContext.request.contextPath}/forward/member_center";
		}
		
		$('#button_home').unbind('click');
		$('#button_home').bind('click', function() {
			window.location.href = "${pageContext.request.contextPath}/forward/home";
		});
		
		$('#button_upload_now').unbind('click');
		$('#button_upload_now').bind('click', function() {
			window.location.href = "${pageContext.request.contextPath}/forward/material_upload";
		});
		
		$('#button_edit').unbind('click');
		$('#button_edit').bind('click', function() {
			window.location.href = "${pageContext.request.contextPath}/forward/member_edit";
		});
		
		$('#button_reupload').unbind('click');
		$('#button_reupload').bind('click', function() {
			window.location.href = "${pageContext.request.contextPath}/forward/material_upload";
		});
		
		$('#button_post').unbind('click');
		$('#button_post').bind('click', function() {
			if ($('#post_no').val() == "") {
				$.alert("错误", "请填写快递单号！");
			} else {
				sendPost({
					onSuccess: function(data) {
						window.location.href = "${pageContext.request.contextPath}/forward/audit_status";
					},
					onError: function() {
					}
				});
			}
		});
		
		var paramData = {};
		paramData.member_login_name = memberLoginName;
		var table = $('#audit_flow_table').DataTable({
			paging: false,
			searching: false,
			info: false,
			jQueryUI: false,
			ordering: false,
			autoWidth: false,
			columns: [
				{data:'audit_phase_name', width:'15%'},
				{data:'audit_date', width:'20%'},
				{data:'audit_result_name', width:'10%'},
				{data:'audit_comment', width:'55%', className: 'dt-body-left'}
			],
			serverSide: true,
			processing: true,
			ajax: {
				url: '${pageContext.request.contextPath}/api/AuditFlowSearchAPI.json',
				type: 'POST',
				data: {
					param: JSON.stringify(paramData)
				},
				dataSrc: 'audit_flow'
			}
		});
		
		if (memberLoginName.length > 0) {
			$('#link_login').hide();
			$('#link_logout').show();
			$('#link_center').show();
			$('#link_regist').hide();
		} else {
			$('#link_login').show();
			$('#link_logout').hide();
			$('#link_center').hide();
			$('#link_regist').show();
		}
		$('#member_name').html(memberLoginName + "&nbsp;&nbsp;");
	}
	
	function loadMemberData(callback) {
		var paramData = {};
		paramData.member_login_name = memberLoginName;
		memberAPI.search({
	        onSuccess : function(data) {
	        	if (data && data.total == 1) {
	        		callback.onSuccess(data.members[0]);
	        	} else {
	        		callback.onError();
	        	}
	        },
	        onAPIError : function(status, errors) {
	        	callback.onError();
	        },
	        onServerError : function(jqXHR, textStatus, errorThrown) {
	        	callback.onError();
	        }
	    },
	    {
	        param: JSON.stringify(paramData)
	    });
	}
	
	function sendPost(callback) {
		var paramData = {};
		paramData.member_login_name = memberLoginName;
		paramData.post_no = $('#post_no').val();
		memberAPI.sendPost({
	        onSuccess : function(data) {
	        	callback.onSuccess(data);
	        },
	        onAPIError : function(status, errors) {
	        	callback.onError();
	        },
	        onServerError : function(jqXHR, textStatus, errorThrown) {
	        	callback.onError();
	        }
	    },
	    {
	        param: JSON.stringify(paramData)
	    });
	}
	
	loadMemberData({
		onSuccess: function(data) {
			if (data) {
				auditPhase = data.lastest_audit_phase;
				auditResult = data.lastest_audit_result;
				initialize();
			} else {
				window.location.href = "${pageContext.request.contextPath}/forward/login";
			}
		},
		onError: function() {
			window.location.href = "${pageContext.request.contextPath}/forward/login";
		}
	});
});
</script>
</head>
<body class="regist_body">
  <div style="text-align: center;" align="center">
    <div id="topbar">
      <div class="wp">
        <div class="left">
          <span id="member_name" style="font-weight: bold;"></span>
          <span>欢迎来到辽地集团网络集中采购平台！</span>
          <a id="link_login" href="${pageContext.request.contextPath}/forward/login" rel="nofollow" class="smart">登录</a>
          <a id="link_logout" href="${pageContext.request.contextPath}/forward/logout" rel="nofollow" class="smart">退出</a>
          <a id="link_center" href="${pageContext.request.contextPath}/forward/member_center" rel="nofollow" class="smart">会员中心</a>
          <a id="link_regist" href="${pageContext.request.contextPath}/forward/regist" rel="nofollow" class="smart">免费注册</a>
        </div>
        <div class="right">
          <span class="topbar_tel">
            服务热线：
            <span class="topbar_tel_no">XXXX-XXXXXXXX</span>
          </span>
        </div>
      </div>
    </div>
    <div id="regist_content">
      <img id="step_icon" src="${pageContext.request.contextPath}/img/step_audit_first.png">
      <div id="regist_form_container">
        <div class="information_confirm">
          <div style="margin: 0 auto; width:450px; padding:12px;">
            <img src="${pageContext.request.contextPath}/img/id_card.png" style="float: left;">
            <div style="font-size: 16px; font-weight: bold; text-align: left; text-indent: 20px; margin-top: 15px;">感谢您注册本站会员！</div>
            <div style="font-size: 15px; font-weight: bold; color: #aaa; text-align: left; text-indent: 20px;">现在，您的账户可以浏览本网站的免费信息。</div>
          </div>
          <div style="margin: 0 auto; width:450px; padding:12px;">
            <div id="message" style="font-size: 15px; text-align: left;"></div>
            <div id="post_info" style="display: none; margin-top: 10px;">
              <input type="text" id="post_no" name="post_no" style="width: 300px;" placeholder="请填写运单信息，例：[申通快递：123456789]" required>
            </div>
          </div>
        </div>
        <div class="material_upload">
          <table id="audit_flow_table" class="display">
            <thead>
              <tr>
                <th>审核阶段</th>
                <th>处理时间</th>
                <th>状态</th>
                <th align="left">备注</th>
              </tr>
            </thead>
          </table>
        </div>
        <div align="center" style="width: 100%;">
          <input type="button" id="button_upload_now" style="display: none;">
          <input type="button" id="button_edit" style="display: none;">
          <input type="button" id="button_reupload" style="display: none;">
          <input type="button" id="button_post" style="display: none;">
          <input type="button" id="button_home">
        </div>
      </div>
    </div>
    <div id="login_footer">
      <s:text name="copyright" />
    </div>
  </div>
</body>
</html>