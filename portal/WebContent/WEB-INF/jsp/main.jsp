<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><s:text name="system_name" /></title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/themes/bootstrap/easyui.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/themes/icon.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/api.js"></script>
<script type="text/javascript">
$(function() {
	var urlPrefix = "${pageContext.request.contextPath}/api/";
    var systemConfigAPI = new SystemConfigAPI({urlPrefix : urlPrefix});

	$.extend($.fn.validatebox.defaults.rules, {
        length: {
            validator: function(value, param) {
                var len = $.trim(value).length;
                return len >= param[0] && len <= param[1];
            },
            message: '<s:text name="messages.error.1006" />'
        },
        same: {
            validator: function (value, param) {
                if ($(param[0]).val() != "" && value != "") {
                    return $(param[0]).val() == value;
                } else {
                    return true;
                }
            }, 
            message: '<s:text name="messages.error.1007" />'
        }
    });

	var tabs = new Array('tab_task');
	var changePassword = <s:property value='systemConfig.changePassword' />;
	
    $('#logout_button').bind('click', function() {
        window.location.href = "logout";
    });
    
    $("#tabs").tabs({
    	onClose: function(title, index) {
    		tabs.splice(index, 1);
    	},
    	onLoad: function(panel) {
    		if ($(panel).find('div#login').length > 0) {
    			$.getTopWindow().location.href = "logout";
    		}
    	}
    });
    
    $('#button_system_config').bind('click', function() {
    	if (tabs.indexOf("tab_system_config") > 0) {
    		$("#tabs").tabs("select", tabs.indexOf("tab_system_config"))
    	} else {
	    	$('#tabs').tabs('add', {
	    		id: 'tab_system_config',
	    		title: '<s:text name="tab.title.system_config" />',
	    		selected: true,
	    		closable: true,
	    		style: {padding:10},
	    		href: 'system_config.action'
	    	});
	    	tabs.push("tab_system_config");
    	}
    });
    
    $('#button_user_manage').bind('click', function() {
    	if (tabs.indexOf("tab_user_manage") > 0) {
    		$("#tabs").tabs("select", tabs.indexOf("tab_user_manage"))
    	} else {
	    	$('#tabs').tabs('add', {
	    		id: 'tab_user_manage',
	    		title: '<s:text name="tab.title.user_manage" />',
	    		selected: true,
	    		closable: true,
	    		style: {padding:10},
	    		content: 'tab3'
	    	});
	    	tabs.push("tab_user_manage");
    	}
    });
    
    $('#button_tender_publish').bind('click', function() {
    	if (tabs.indexOf("tab_tender_publish") > 0) {
    		$("#tabs").tabs("select", tabs.indexOf("tab_tender_publish"))
    	} else {
	    	$('#tabs').tabs('add', {
	    		id: 'tab_tender_publish',
	    		title: '<s:text name="tab.title.tender_publish" />',
	    		selected: true,
	    		closable: true,
	    		style: {padding:10},
	    		content: 'tab4'
	    	});
	    	tabs.push("tab_tender_publish");
    	}
    });
    
    if (changePassword) {
    	$('#win_change_password').window('open');
    	$('#win_change_password #btn_save').bind('click', function() {
    		if ($('#change_password_form').form('validate')) {
                var oldPassword = $('#change_password_form #old_password').val();
                var newPassword = $('#change_password_form #new_password').val();
                funcChangePassword(oldPassword, newPassword, {
                    onSuccess: function() {
                        $.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.success" />', '<s:text name="messages.function.change_password" />'), 'info', function() {
                            window.location.href = "logout";
                        });
                    },
                    onError: function() {
                        $.messager.alert('<s:text name="title.error" />', '<s:text name="messages.error.1008" />');
                    }
                });
            } else {
                $.messager.alert('<s:text name="title.error" />', '<s:text name="messages.error.9001" />');
            }
    	});
    }
    
    function funcChangePassword(oldPassword, newPassword, handler) {
        var paramData = {};
        paramData.login_name = "admin";
        paramData.login_password = oldPassword;
        paramData.new_password = newPassword;
        systemConfigAPI.changePassword({
            onSuccess : function(data) {
                if (data.result) {
                    handler.onSuccess();
                } else {
                    handler.onError();
                }
            },
            onAPIError : function(status, errors) {
                $.messager.alert('<s:text name="title.error" />', '<s:text name="messages.error.9004" />');
            },
            onServerError : function(jqXHR, textStatus, errorThrown) {
                $.messager.alert('<s:text name="title.error" />', '<s:text name="messages.error.9004" />');
            }
        },
        {
            param: JSON.stringify(paramData)
        });
    }
});
</script>
</head>
<body>
  	<div class="easyui-layout" data-options="fit:true">
		<div data-options="region:'north'" style="height:50px; padding-left: 0px; padding-right: 10px; vertical-align: bottom;">
			<div style="float: left;"><img src="<%=request.getContextPath()%>/img/logo.gif" /></div>
			<div style="float: right; padding-top: 20px;">
				<s:text name="label.current_user" />
				<s:property value="userInfo.loginName" />
				<a href="#" id="logout_button" class="easyui-linkbutton" data-options="iconCls:'icon-logout', plain:true"><s:text name="button.logout" /></a>
			</div>
		</div>
		<div data-options="region:'south', split:false" style="height:30px;">
			<div class="blank"></div>
    		<div id="footer"><s:text name="copyright" /></div>
		</div>
		<div data-options="region:'west', split:false" title="<s:text name='title.function' />" style="width:200px;">
			<div class="easyui-accordion" data-options="multiple:true, border:false">
				<div title="<s:text name='menu.member_manage' />" data-options="iconCls:'icon-man', expand:true" style="padding:10px; text-align: center;">
					<div id="button_member_audit" class="image_button">
						<div class="image_button_image"><img src="${pageContext.request.contextPath}/img/member_audit.png"></div>
						<div class="image_button_text"><s:text name="button.member_audit" /></div>
					</div>
				</div>
				<div title="<s:text name='menu.tender_manage' />" data-options="iconCls:'icon-bid', expand:true" style="padding:10px;">
					<div id="button_tender_publish" class="image_button">
						<div class="image_button_image"><img src="${pageContext.request.contextPath}/img/tender_publish.png"></div>
						<div class="image_button_text"><s:text name="button.tender_publish" /></div>
					</div>
				</div>
				<div title="<s:text name='menu.invitation_manage' />" style="padding:10px;">
					
				</div>
				<div title="<s:text name='menu.system_config' />" style="padding:10px;">
					<div id="button_system_config" class="image_button">
						<div class="image_button_image"><img src="${pageContext.request.contextPath}/img/system_config.png"></div>
						<div class="image_button_text"><s:text name="button.system_config" /></div>
					</div>
					<div id="button_user_manage" class="image_button">
						<div class="image_button_image"><img src="${pageContext.request.contextPath}/img/user_manage.png"></div>
						<div class="image_button_text"><s:text name="button.user_manage" /></div>
					</div>
				</div>
			</div>
		</div>
		<div data-options="region:'center', title:'<s:text name="title.location" />'">
			<div id="tabs" class="easyui-tabs" data-options="fit:true, border:false">
			    <div title="<s:text name='tab.title.task' />" data-options="id:'tab_task', closable:false" style="padding:10px;">
					tab1
			    </div>
			</div>
		</div>
	</div>
	<div id="win_change_password" class="easyui-window" title="<s:text name='window.title.change_password' />" style="width:400px;" data-options="modal:true, closable:false, closed:true, collapsible:false, minimizable:false, maximizable:false, resizable:false">
		<div style="padding: 10px;">
			<p class="alert_message"><s:text name="messages.alert.change_password" /></p>
			<form id="change_password_form">
				<table id="user_table" class="edit_table" style="margin-top: 10px;">
					<tr>
						<th width="100"><s:text name="label.change_password.old_password" /></th>
						<td width="280"><input id="old_password" type="password" class="easyui-validatebox" data-options="required:true, validType:length[4, 16]" style="width: 100%;" placeholder="<s:text name='placeholder.change_password.old_password' />" /></td>
					</tr>
					<tr>
						<th><s:text name="label.change_password.new_password" /></th>
						<td><input id="new_password" type="password" class="easyui-validatebox" data-options="required:true, validType:['length[4, 16]', 'same[\'#win_change_password #confirm_password\']']" style="width: 100%;" placeholder="<s:text name='placeholder.change_password.new_password' />" /></td>
					</tr>
					<tr>
						<th><s:text name="label.change_password.confirm_password" /></th>
						<td><input id="confirm_password" type="password" class="easyui-validatebox" data-options="required:true, validType:['length[4, 16]', 'same[\'#win_change_password #new_password\']']" style="width: 100%;" placeholder="<s:text name='placeholder.change_password.confirm_password' />" /></td>
					</tr>
				</table>
				<div style="text-align: right; padding-top: 20px;">
					<a id="btn_save" class="easyui-linkbutton" data-options="iconCls:'icon-save', plain:true"><s:text name="button.save" /></a>
				</div>
			</form>
		</div>
	</div>
</body>
</html>
