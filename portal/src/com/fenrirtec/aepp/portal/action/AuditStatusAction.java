package com.fenrirtec.aepp.portal.action;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.MemberDto;
import com.fenrirtec.aepp.portal.common.action.CommonAction;

public class AuditStatusAction extends CommonAction {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(AuditStatusAction.class);

	public String init() {
		if (logger.isInfoEnabled()) {
			logger.info("[AuditStatusAction#init] start.");
		}
		
		MemberDto loginMember = getMemberInfo();
		if (loginMember == null) {
			return "login";
		}
		
		String memberLoginName = getMemberInfo().getMemberLoginName();
		if (StringUtils.isEmpty(memberLoginName)) {
			return "login";
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("[AuditStatusAction#init] finish.");
		}
		return SUCCESS;
	}
}
