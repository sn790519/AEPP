package com.fenrirtec.aepp.portal.action;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.MemberDto;
import com.fenrirtec.aepp.common.service.MemberManageService;
import com.fenrirtec.aepp.common.service.impl.MemberManageServiceImpl;
import com.fenrirtec.aepp.portal.common.action.CommonAction;

public class MemberCenterAction extends CommonAction {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(MemberCenterAction.class);

	public String init() {
		if (logger.isInfoEnabled()) {
			logger.info("[MemberCenterAction#init] start.");
		}
		
		MemberManageService service = new MemberManageServiceImpl();
		MemberDto loginMember = getMemberInfo();
		if (loginMember == null) {
			return "login";
		}
		
		String memberLoginName = getMemberInfo().getMemberLoginName();
		if (StringUtils.isEmpty(memberLoginName)) {
			return "login";
		}
		
		MemberDto memberDto = service.memberInfo(memberLoginName);
		if (memberDto.getLastestAuditPhase() < 9) {
			return "audit_status";
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("[MemberCenterAction#init] finish.");
		}
		return SUCCESS;
	}
}
