package com.fenrirtec.aepp.portal.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.portal.common.action.CommonAction;

public class MaterialUploadAction extends CommonAction {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(MaterialUploadAction.class);
	
	public String init() {
		if (logger.isInfoEnabled()) {
			logger.info("[MaterialUploadAction#init] start.");
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("[MaterialUploadAction#init] finish.");
		}
		return SUCCESS;
	}

}
