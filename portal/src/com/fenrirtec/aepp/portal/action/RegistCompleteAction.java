package com.fenrirtec.aepp.portal.action;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.portal.common.action.CommonAction;

public class RegistCompleteAction extends CommonAction {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(RegistCompleteAction.class);

	public String init() {
		if (logger.isInfoEnabled()) {
			logger.info("[RegistCompleteAction#init] start.");
		}
		
		if (!getSession().containsKey("regist_name") || getSession().get("regist_name") == null || StringUtils.isEmpty(getSession().get("regist_name").toString())) {
			return INPUT;
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("[RegistCompleteAction#init] finish.");
		}
		return SUCCESS;
	}
	
	public String getName() {
		return (String) getSession().get("regist_name");
	}
}
