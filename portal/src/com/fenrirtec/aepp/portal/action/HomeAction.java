package com.fenrirtec.aepp.portal.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.portal.common.action.CommonAction;

public class HomeAction extends CommonAction {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(HomeAction.class);

	public String init() {
		if (logger.isInfoEnabled()) {
			logger.info("[IndexAction#init] start.");
		}
		if (logger.isInfoEnabled()) {
			logger.info("[IndexAction#init] finish.");
		}
		return SUCCESS;
	}
}
