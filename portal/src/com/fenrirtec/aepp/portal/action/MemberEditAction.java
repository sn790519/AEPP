package com.fenrirtec.aepp.portal.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.portal.common.action.CommonAction;

public class MemberEditAction extends CommonAction {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(MemberEditAction.class);

	public String init() {
		if (logger.isInfoEnabled()) {
			logger.info("[MainAction#init] start.");
		}
		
		if (logger.isInfoEnabled()) {
			logger.info("[MainAction#init] finish.");
		}
		return SUCCESS;
	}
}
