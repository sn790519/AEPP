package com.fenrirtec.aepp.portal.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.MemberDto;
import com.fenrirtec.aepp.common.service.MemberManageService;
import com.fenrirtec.aepp.common.service.impl.MemberManageServiceImpl;
import com.fenrirtec.aepp.portal.common.action.CommonAction;
import com.fenrirtec.aepp.portal.common.definition.Definition;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;

public class LoginAction extends CommonAction {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(LoginAction.class);

	private String mMemberLoginName;

	private String mMemberLoginPassword;
	
	private boolean mAuthFail = false;

	public String getMemberLoginName() {
		return mMemberLoginName;
	}

	public void setMemberLoginName(String memberLoginName) {
		this.mMemberLoginName = memberLoginName;
	}

	public String getMemberLoginPassword() {
		return mMemberLoginPassword;
	}

	public void setMemberLoginPassword(String memberLoginPassword) {
		this.mMemberLoginPassword = memberLoginPassword;
	}

	public boolean getAuthFail() {
		return mAuthFail;
	}

	public void setAuthFail(boolean mAuthFail) {
		this.mAuthFail = mAuthFail;
	}

	public String init() {
		if (logger.isInfoEnabled()) {
			logger.info("[LoginAction#init] start.");
		}
		if (logger.isInfoEnabled()) {
			logger.info("[LoginAction#init] finish.");
		}
		return SUCCESS;
	}

	public String login() {
		try {
			if (logger.isInfoEnabled()) {
				logger.info("[LoginAction#login] start.");
			}
			
			MemberManageService memberManageService = new MemberManageServiceImpl();
			MemberDto memberDto = memberManageService.login(mMemberLoginName, mMemberLoginPassword);
			if (memberDto == null) {
				//addActionError(MessageUtils.getLocalizedErrorMessage(this, Definition.ErrorCode.ERROR_AUTHORITY, null));
				mAuthFail = true;
				return INPUT;
			} else {
				getSession().put(Definition.SessionKey.MEMBER_INFO, memberDto);
				getSession().put("regist_name", mMemberLoginName);
				mAuthFail = false;
				if (memberDto.getLastestAuditPhase() < 3) {
					return "upload";
				}
				return SUCCESS;
			}
			
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("[LoginAction#login] database error occurred.", e);
			}
			throw e;
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("[LoginAction#login] unexpected error occurred.", e);
			}
			throw e;
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[LoginAction#login] finish.");
			}
		}
	}
}
