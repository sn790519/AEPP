package com.fenrirtec.aepp.portal.common.action;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.MemberDto;
import com.fenrirtec.aepp.common.dto.SystemConfigDto;
import com.fenrirtec.aepp.common.service.impl.SystemConfigServiceImpl;
import com.fenrirtec.aepp.portal.action.LoginAction;
import com.fenrirtec.aepp.portal.common.definition.Definition;
import com.fenrirtec.core.action.BaseAction;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;

public class CommonAction extends BaseAction implements SessionAware {

	private static final long serialVersionUID = 1L;

	private static SystemConfigDto mSystemConfig;
	
	private MemberDto mMemberInfo;
	
	private Map<String, Object> mSession = null;
	
	private static final Logger logger = LoggerFactory.getLogger(LoginAction.class);
	
	protected CommonAction() {
		
		if (mSystemConfig == null) {
			try {
				mSystemConfig = new SystemConfigServiceImpl().loadSystemConfig();
			} catch (DatabaseException e) {
				if (logger.isErrorEnabled()) {
					logger.error("[CommonAction#CommonAction] database error occurred.", e);
				}
				throw e;
			} catch (UnexpectedException e) {
				if (logger.isErrorEnabled()) {
					logger.error("[CommonAction#CommonAction] unexpected error occurred.", e);
				}
				throw e;
			} finally {
				if (logger.isInfoEnabled()) {
					logger.info("[CommonAction#CommonAction] finish.");
				}
			}
		}
	}

	public SystemConfigDto getSystemConfig() {
		return mSystemConfig;
	}

	public MemberDto getMemberInfo() {
		mMemberInfo = (MemberDto) getSession().get(Definition.SessionKey.MEMBER_INFO);
		return mMemberInfo;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.mSession = session;
	}

	public Map<String, Object> getSession() {
		return this.mSession;
	}
	
	public String forward() {
		if (logger.isInfoEnabled()) {
			logger.info("[CommonAction#forward] start.");
		}
		if (logger.isInfoEnabled()) {
			logger.info("[CommonAction#forward] finish.");
		}
		return SUCCESS;
	}
}
