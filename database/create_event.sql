drop event if exists e_news_notice_clean;

create event e_news_notice_clean
on schedule every 1 day
do
  update
    t_news_notice
  set
    delete_flag = true,
    update_user = 'database',
    update_date = current_timestamp
  where
    delete_flag = false
  and
    not isnull(valid_end_date)
  and
    date_format(valid_end_date, '%Y%m%d') <= date_format(current_date - INTERVAL 1 DAY, '%Y%m%d');