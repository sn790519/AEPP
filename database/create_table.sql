SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */
DROP TABLE IF EXISTS m_code_name;
DROP TABLE IF EXISTS t_adv_image;
DROP TABLE IF EXISTS t_attachment;
DROP TABLE IF EXISTS t_audit_flow;
DROP TABLE IF EXISTS t_function;
DROP TABLE IF EXISTS t_member;
DROP TABLE IF EXISTS t_news_notice;
DROP TABLE IF EXISTS t_product_attachment;
DROP TABLE IF EXISTS t_system_config;
DROP TABLE IF EXISTS t_tender_bid;
DROP TABLE IF EXISTS t_tender_bid_doc;
DROP TABLE IF EXISTS t_tender_enroll;
DROP TABLE IF EXISTS t_tender_enroll_file;
DROP TABLE IF EXISTS t_tender_finalist;
DROP TABLE IF EXISTS t_tender_information;
DROP TABLE IF EXISTS t_tender_product;
DROP TABLE IF EXISTS t_tender_quote;
DROP TABLE IF EXISTS t_tender_quote_detail;
DROP TABLE IF EXISTS t_upload;
DROP TABLE IF EXISTS t_user;
DROP TABLE IF EXISTS t_user_permission;
DROP TABLE IF EXISTS t_group;
DROP TABLE IF EXISTS t_group_permission;
DROP TABLE IF EXISTS t_user_group;

/* Create Tables */

-- 数据字典表
CREATE TABLE m_code_name
(
	-- 分类ID
	type_id varchar(2) NOT NULL COMMENT '分类ID',
	-- 代码
	code int NOT NULL COMMENT '代码',
	-- 名称
	name varchar(100) NOT NULL COMMENT '名称',
	-- 创建者
	create_user varchar(20) NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (type_id, code)
) COMMENT = '数据字典表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 广告图片表
CREATE TABLE t_adv_image
(
	-- 图片ID
	image_id int NOT NULL AUTO_INCREMENT COMMENT '图片ID',
	-- 表示顺序
	display_order int NOT NULL COMMENT '表示顺序',
	-- 使用识别子
	use_flag boolean DEFAULT true NOT NULL COMMENT '使用识别子',
	-- 标题
	title varchar(20) NOT NULL COMMENT '标题',
	-- 描述
	description varchar(50) NOT NULL COMMENT '描述',
	-- 路径
	path varchar(255) NOT NULL COMMENT '路径',
	-- 宽度
	width int NOT NULL COMMENT '宽度',
	-- 高度
	height int NOT NULL COMMENT '高度',
	-- 文件大小
	size int NOT NULL COMMENT '文件大小',
	-- 创建者
	create_user varchar(20) DEFAULT 'system' NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false COMMENT '逻辑删除识别子',
	PRIMARY KEY (image_id)
) COMMENT = '广告图片表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 附件表
CREATE TABLE t_attachment
(
	-- 附件ID
	attachment_id int NOT NULL AUTO_INCREMENT COMMENT '附件ID',
	-- 1：图片
	-- 2：EXCEL
	-- 3：WORD
	-- 4：PPT
	-- 5：PDF
	-- 6：ZIP
	file_type int DEFAULT 1 NOT NULL COMMENT '文件类型',
	-- 1：会员附件
	-- 2：产品附件
	-- 3：标书
	-- 4：表明文件
	-- 5：其他
	file_category int DEFAULT 1 NOT NULL COMMENT '文件所属',
	-- 路径
	path varchar(255) NOT NULL COMMENT '路径',
	-- 说明
	comment varchar(200) COMMENT '说明',
	-- 上传者
	upload_user varchar(20) NOT NULL COMMENT '上传者',
	-- 上传日期
	upload_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '上传日期',
	-- 创建者
	create_user varchar(20) NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (attachment_id)
) COMMENT = '附件表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 审批流程表
CREATE TABLE t_audit_flow
(
	-- 会员ID
	member_login_name varchar(20) NOT NULL COMMENT '会员ID',
	-- 审核回数
	audit_sequence int DEFAULT 1 NOT NULL COMMENT '审核回数',
	-- 审核日期
	audit_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '审核日期',
	-- 1：用户注册
	-- 2：等待上传电子资质文件
	-- 3：初审阶段
	-- 4：等待邮寄纸质资质文件
	-- 5：二次审核阶段
	-- 6：审核结束
	audit_phase int DEFAULT 1 NOT NULL COMMENT '审核阶段',
	-- 1：审核中
	-- 2：通过
	-- 3：未通过
	-- 4：取消
	-- 
	audit_result int DEFAULT 1 NOT NULL COMMENT '审核结果',
	-- 审批意见
	audit_comment varchar(100) COMMENT '审批意见',
	-- 审核操作人用户名
	login_name varchar(20) DEFAULT 'system' NOT NULL COMMENT '审核操作人用户名',
	-- 创建者
	create_user varchar(20) DEFAULT 'system' NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) DEFAULT 'system' COMMENT '更新者',
	-- 最终更新日期
	update_date datetime DEFAULT CURRENT_TIMESTAMP COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (member_login_name, audit_sequence)
) COMMENT = '审批流程表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 功能表
CREATE TABLE t_function
(
	-- 功能名
	function_name varchar(20) NOT NULL COMMENT '功能名',
	-- 表示名
	name varchar(50) NOT NULL COMMENT '表示名',
	-- URL
	url varchar(255) NOT NULL COMMENT 'URL',
	-- 创建者
	create_user varchar(20) NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (function_name)
) COMMENT = '功能表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 会员表
CREATE TABLE t_member
(
	-- 登录名
	member_login_name varchar(20) NOT NULL COMMENT '登录名',
	-- 登录密码
	member_login_password varchar(200) NOT NULL COMMENT '登录密码',
	-- 营业执照编号
	license_number varchar(15) NOT NULL COMMENT '营业执照编号',
	-- 企业名称
	enterprise_name varchar(200) NOT NULL COMMENT '企业名称',
	-- 1：国有企业
	-- 2：中外合作企业
	-- 3：中外合资企业
	-- 4：外商独资企业
	-- 5：集体企业
	-- 6：私营企业
	enterprise_property int DEFAULT 1 NOT NULL COMMENT '企业性质',
	-- 1：机构组织
	-- 2：农林牧渔
	-- 3：医药卫生
	-- 4：建筑建材
	-- 5：冶金矿产
	-- 6：石油化工
	-- 7：水利水电
	-- 8：交通运输
	-- 9：信息产业
	-- 10：机械机电
	-- 11：轻工食品
	-- 12：服装纺织
	-- 13：专业服务
	-- 14：安全防护
	-- 15：环保绿化
	-- 16：旅游休闲
	-- 17：办公文教
	-- 18：电子电工
	-- 19：玩具礼品
	-- 20：家居用品
	-- 21：物资专材
	-- 22：包装用品
	-- 23：体育用品
	-- 24：办公家具
	-- 99：其他分类
	enterprise_category int DEFAULT 99 NOT NULL COMMENT '行业分类',
	-- 联系人姓名
	contact_name varchar(50) NOT NULL COMMENT '联系人姓名',
	-- 1：身份证
	-- 2：护照
	contact_certificates_type int NOT NULL COMMENT '联系人证件类型',
	-- 联系人证件号码
	contact_certificates_no varchar(50) NOT NULL COMMENT '联系人证件号码',
	-- 固定电话区号
	area_code varchar(4) COMMENT '固定电话区号',
	-- 固定电话号码
	telephone varchar(10) COMMENT '固定电话号码',
	-- 手机号码
	mobile varchar(11) COMMENT '手机号码',
	-- 手机认证
	mobile_certificated boolean DEFAULT false NOT NULL COMMENT '手机认证',
	-- 电子邮件
	email varchar(100) NOT NULL COMMENT '电子邮件',
	-- 邮箱认证
	email_certificated boolean DEFAULT false NOT NULL COMMENT '邮箱认证',
	-- QQ号码
	qq_no varchar(20) COMMENT 'QQ号码',
	-- 微信号码
	weixin_no varchar(50) COMMENT '微信号码',
	-- 法人姓名
	legal_name varchar(50) COMMENT '法人姓名',
	-- 1：身份证
	-- 2：护照
	legal_certificates_type int COMMENT '法人证件类型',
	-- 法人证件号码
	legal_certificates_no varchar(50) COMMENT '法人证件号码',
	-- 联系人身份证复印件（正面）附件ID
	image_contact_certificates_front int COMMENT '联系人身份证复印件（正面）附件ID',
	-- 联系人身份证复印件（背面）附件ID
	image_contact_certificates_back int COMMENT '联系人身份证复印件（背面）附件ID',
	-- 法人身份证复印件（正面）附件ID
	image_legal_certificates_front int COMMENT '法人身份证复印件（正面）附件ID',
	-- 法人身份证复印件（背面）附件ID
	image_legal_certificates_back int COMMENT '法人身份证复印件（背面）附件ID',
	-- 营业执照复印件附件ID
	image_license int COMMENT '营业执照复印件附件ID',
	-- 会员积分
	points int DEFAULT 0 NOT NULL COMMENT '会员积分',
	-- 会员等级
	rank int DEFAULT 1 NOT NULL COMMENT '会员等级',
	-- 创建者
	create_user varchar(20) NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (member_login_name)
) COMMENT = '会员表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 新闻通告表
CREATE TABLE t_news_notice
(
	-- 新闻通告ID
	news_notice_id int NOT NULL AUTO_INCREMENT COMMENT '新闻通告ID',
	-- 类别
	-- 1：新闻
	-- 2：通告
	category int(1) DEFAULT 1 NOT NULL COMMENT '类别',
	-- 标题
	title varchar(30) NOT NULL COMMENT '标题',
	-- 正文（HTML格式）
	body varchar(2000) NOT NULL COMMENT '正文',
	-- 有效开始日
	valid_start_date date NOT NULL COMMENT '有效开始日',
	-- 有效结束日
	valid_end_date date COMMENT '有效结束日',
	-- 创建者
	create_user varchar(20) DEFAULT 'system' NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (news_notice_id)
) COMMENT = '新闻通告表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 招标产品附件表
CREATE TABLE t_product_attachment
(
	-- 产品ID
	product_id int NOT NULL COMMENT '产品ID',
	-- 附件ID
	attachment_id int NOT NULL COMMENT '附件ID',
	-- 创建者
	create_user varchar(20) DEFAULT 'system' NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (product_id)
) COMMENT = '招标产品附件表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 系统设置表
CREATE TABLE t_system_config
(
	-- 登录密码
	password varchar(200) DEFAULT '352134df9b109da0fd9ded06d7931201f84457ce0508e7b6b9b587390e3da899' NOT NULL COMMENT '登录密码',
	-- 修改密码
	change_password boolean DEFAULT true NOT NULL COMMENT '修改密码',
	-- 允许注册
	regist_enabled boolean DEFAULT true NOT NULL COMMENT '允许注册',
	-- 首页新闻表示件数
	news_count int DEFAULT 10 NOT NULL COMMENT '首页新闻表示件数',
	-- 首页招标信息表示件数
	tender_count int DEFAULT 10 NOT NULL COMMENT '首页招标信息表示件数',
	-- 1：时间降序
	-- 2：时间升序
	-- 3：浏览量降序
	-- 4：关注度降序
	tender_sort int DEFAULT 1 NOT NULL COMMENT '招标信息排序规则',
	-- 首页会员表示件数
	member_count int DEFAULT 10 NOT NULL COMMENT '首页会员表示件数',
	-- 必须上传法人身份证复印件
	contact_copy_required boolean DEFAULT true NOT NULL COMMENT '必须上传法人身份证复印件',
	-- 必须上传营业执照复印件
	license_copy_required boolean DEFAULT true NOT NULL COMMENT '必须上传营业执照复印件',
	-- 必须上传法人身份证复印件
	legal_copy_required boolean DEFAULT false NOT NULL COMMENT '必须上传法人身份证复印件',
	-- 0：逻辑删除
	-- 1：物理删除
	physical_delete tinyint DEFAULT 0 NOT NULL COMMENT '物理删除识别子',
	-- 文件上传根目录
	upload_root_folder varchar(255) DEFAULT '/usr/aepp/upload' NOT NULL COMMENT '文件上传根目录',
	-- 资质文件保存目录
	qualifications_save_folder varchar(255) DEFAULT '/qualifications' NOT NULL COMMENT '资质文件保存目录',
	-- 图片保存路径
	images_save_folder varchar(255) DEFAULT '/images' NOT NULL COMMENT '图片保存路径',
	-- 上传临时目录
	upload_temp_folder varchar(255) DEFAULT '/temp' NOT NULL COMMENT '上传临时目录',
	-- 允许上传的图像文件扩展名
	allowed_image_extension varchar(200) DEFAULT 'jpg|png|bmp|gif' NOT NULL COMMENT '允许上传的图像文件扩展名',
	-- 允许上传的图像文件大小（字节数）
	allowed_image_size int DEFAULT 524288 NOT NULL COMMENT '允许上传的图像文件大小（字节数）',
	-- 允许上传的文档文件扩展名
	allowed_doc_extension varchar(200) DEFAULT 'doc|docx|xls|xlsx|ppt|pptx|pdf|txt' NOT NULL COMMENT '允许上传的文档文件扩展名',
	-- 允许上传的文档文件大小（字节数）
	allowed_doc_size int DEFAULT 10485760 NOT NULL COMMENT '允许上传的文档文件大小（字节数）',
	-- 允许上传的压缩文件扩展名
	allowed_compress_extension varchar(200) DEFAULT 'zip|rar|7z' NOT NULL COMMENT '允许上传的压缩文件扩展名',
	-- 允许上传的压缩文件大小（字节数）
	allowed_compress_size int DEFAULT 20971520 NOT NULL COMMENT '允许上传的压缩文件大小（字节数）',
	-- 创建者
	create_user varchar(20) NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) DEFAULT 'admin' COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子'
) COMMENT = '系统设置表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 中标供应商表
CREATE TABLE t_tender_bid
(
	-- 招标信息ID
	tender_id int NOT NULL COMMENT '招标信息ID',
	-- 会员名
	member_login_name varchar(20) NOT NULL COMMENT '会员名',
	-- 中标时间
	bid_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '中标时间',
	-- 评标说明
	evaluation varchar(500) COMMENT '评标说明',
	-- 创建者
	create_user varchar(20) DEFAULT 'system' NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (tender_id, member_login_name)
) COMMENT = '中标供应商表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 中标标书表
CREATE TABLE t_tender_bid_doc
(
	-- 标书文件ID
	bid_doc_file_id int NOT NULL AUTO_INCREMENT COMMENT '标书文件ID',
	-- 招标信息ID
	tender_id int NOT NULL COMMENT '招标信息ID',
	-- 会员名
	member_login_name varchar(20) NOT NULL COMMENT '会员名',
	-- 文件路径
	path varchar(255) NOT NULL COMMENT '文件路径',
	-- 创建者
	create_user varchar(20) DEFAULT 'system' NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (bid_doc_file_id)
) COMMENT = '中标标书表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 报名供应商表
CREATE TABLE t_tender_enroll
(
	-- 招标信息ID
	tender_id int NOT NULL COMMENT '招标信息ID',
	-- 会员名
	member_login_name varchar(20) NOT NULL COMMENT '会员名',
	-- 报名时间
	enroll_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '报名时间',
	-- 报名说明
	description varchar(500) COMMENT '报名说明',
	-- 入围标识
	finalist_flag boolean DEFAULT false NOT NULL COMMENT '入围标识',
	-- 邀标标识
	-- true：定向邀标
	-- false：自己报名
	invite_flag boolean DEFAULT false NOT NULL COMMENT '邀标标识',
	-- 创建者
	create_user varchar(20) DEFAULT 'system' NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (tender_id, member_login_name)
) COMMENT = '报名供应商表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 报名文件表
CREATE TABLE t_tender_enroll_file
(
	-- 文件ID
	enroll_file_id int NOT NULL AUTO_INCREMENT COMMENT '文件ID',
	-- 招标信息ID
	tender_id int NOT NULL COMMENT '招标信息ID',
	-- 会员名
	member_login_name varchar(20) NOT NULL COMMENT '会员名',
	-- 文件路径
	path varchar(255) NOT NULL COMMENT '文件路径',
	-- 创建者
	create_user varchar(20) DEFAULT 'system' NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (enroll_file_id)
) COMMENT = '报名文件表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 入围供应商表
CREATE TABLE t_tender_finalist
(
	-- 招标信息ID
	tender_id int NOT NULL COMMENT '招标信息ID',
	-- 会员名
	member_login_name varchar(20) NOT NULL COMMENT '会员名',
	-- 入围时间
	finalist_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '入围时间',
	-- 评价说明
	evaluation varchar(500) COMMENT '评价说明',
	-- 投标标识
	quote_flag boolean DEFAULT false NOT NULL COMMENT '投标标识',
	-- 邀标标识
	-- true：定向邀标
	-- false：自己报名
	invite_flag boolean DEFAULT false NOT NULL COMMENT '邀标标识',
	-- 创建者
	create_user varchar(20) DEFAULT 'system' NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (tender_id, member_login_name)
) COMMENT = '入围供应商表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 招标信息表
CREATE TABLE t_tender_information
(
	-- 招标信息ID
	tender_id int NOT NULL AUTO_INCREMENT COMMENT '招标信息ID',
	-- 招标编号
	tender_no varchar(12) NOT NULL COMMENT '招标编号',
	-- 1：报名中
	-- 2：投标中（入围截止）
	-- 3：评标中（投标截止）
	-- 4：已授标
	status int DEFAULT 1 NOT NULL COMMENT '状态',
	-- 标题
	tender_title varchar(50) NOT NULL COMMENT '标题',
	-- 报名开始日
	enroll_start_date date NOT NULL COMMENT '报名开始日',
	-- 报名截止时间
	enroll_end_date date NOT NULL COMMENT '报名截止时间',
	-- 投标截止时间
	tender_end_date date NOT NULL COMMENT '投标截止时间',
	-- 结果公布时间
	publish_result_date date NOT NULL COMMENT '结果公布时间',
	-- 内容说明
	content_description varchar(500) COMMENT '内容说明',
	-- 供应商要求
	vendor_requirement varchar(200) COMMENT '供应商要求',
	-- 1：公开招标
	-- 2：定向邀标
	tender_pattern int DEFAULT 1 NOT NULL COMMENT '招标方式',
	-- 标书模板附件ID
	tender_template int NOT NULL COMMENT '标书模板附件ID',
	-- 联系人姓名
	contact_name varchar(20) COMMENT '联系人姓名',
	-- 联系电话
	contact_tel varchar(15) COMMENT '联系电话',
	-- 创建者
	create_user varchar(20) DEFAULT 'system' NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (tender_id)
) COMMENT = '招标信息表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 招标产品表
CREATE TABLE t_tender_product
(
	-- 产品ID
	product_id int NOT NULL AUTO_INCREMENT COMMENT '产品ID',
	-- 招标信息ID
	tender_id int NOT NULL COMMENT '招标信息ID',
	-- 产品名称
	product_name varchar(100) NOT NULL COMMENT '产品名称',
	-- 数量
	quantity int NOT NULL COMMENT '数量',
	-- 单位
	unit varchar(5) NOT NULL COMMENT '单位',
	-- 产品描述
	product_description varchar(100) COMMENT '产品描述',
	-- 创建者
	create_user varchar(20) DEFAULT 'system' NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (product_id)
) COMMENT = '招标产品表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 投标供应商表
CREATE TABLE t_tender_quote
(
	-- 招标信息ID
	tender_id int NOT NULL COMMENT '招标信息ID',
	-- 会员名
	member_login_name varchar(20) NOT NULL COMMENT '会员名',
	-- 投标时间
	quote_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '投标时间',
	-- 报价总价
	total_price float(12,2) NOT NULL COMMENT '报价总价',
	-- 中标标识
	bid_flag boolean DEFAULT '48' NOT NULL COMMENT '中标标识',
	-- 报价说明
	comment varchar(200) COMMENT '报价说明',
	-- 创建者
	create_user varchar(20) DEFAULT 'system' NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT '48' NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (tender_id, member_login_name)
) COMMENT = '投标供应商表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 报价明细表
CREATE TABLE t_tender_quote_detail
(
	-- 招标信息ID
	tender_id int NOT NULL COMMENT '招标信息ID',
	-- 产品ID
	product_id int NOT NULL COMMENT '产品ID',
	-- 会员名
	member_login_name varchar(20) NOT NULL COMMENT '会员名',
	-- 单价
	unit_price float(12,2) NOT NULL COMMENT '单价',
	-- 总价
	total_price float(12,2) NOT NULL COMMENT '总价',
	-- 创建者
	create_user varchar(20) DEFAULT 'system' NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT '48' NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (tender_id, product_id, member_login_name)
) COMMENT = '报价明细表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 上传管理表
CREATE TABLE t_upload
(
	-- 上传动作ID
	upload_id int NOT NULL AUTO_INCREMENT COMMENT '上传动作ID',
	-- 上传者登录名
	upload_person varchar(20) NOT NULL COMMENT '上传者登录名',
	-- 1：前台会员
	-- 2：后台用户
	upload_person_type int NOT NULL COMMENT '上传者类别',
	-- 用途
	upload_purpose varchar(50) NOT NULL COMMENT '用途',
	-- 上传时间
	upload_datetime datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '上传时间',
	-- 文件路径
	upload_file_path varchar(255) NOT NULL COMMENT '文件路径',
	-- 1：图片
	-- 2：文档（Excel，Word，PPT，PDF，Text）
	-- 3：压缩文件
	upload_file_type int DEFAULT 1 NOT NULL COMMENT '文件类型',
	PRIMARY KEY (upload_id)
) COMMENT = '上传管理表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 用户表
CREATE TABLE t_user
(
	-- 登录名
	login_name varchar(20) NOT NULL COMMENT '登录名',
	-- 登录密码
	login_password varchar(200) NOT NULL COMMENT '登录密码',
	-- 姓名
	name varchar(20) NOT NULL COMMENT '姓名',
	-- 创建者
	create_user varchar(20) NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (login_name)
) COMMENT = '用户表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 用户权限表
CREATE TABLE t_user_permission
(
	-- 用户ID
	login_name varchar(20) NOT NULL COMMENT '登录名',
	-- 功能名
	function_name varchar(10) NOT NULL COMMENT '功能名',
	-- true：可使用
	-- false：不可使用
	readable boolean DEFAULT false NOT NULL COMMENT '浏览权限',
	-- true：可使用
	-- false：不可使用
	addable boolean DEFAULT false NOT NULL COMMENT '追加权限',
	-- true：可使用
	-- false：不可使用
	editable boolean DEFAULT false NOT NULL COMMENT '编辑权限',
	-- true：可使用
	-- false：不可使用
	deleteable boolean DEFAULT false NOT NULL COMMENT '删除权限',
	-- 创建者
	create_user varchar(20) NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (login_name, function_name)
) COMMENT = '用户权限表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

-- 用户组表
CREATE TABLE t_group
(
	-- 用户组名
	group_name varchar(20) NOT NULL COMMENT '用户组名',
	-- 表示名
	name varchar(20) NOT NULL COMMENT '表示名',
	-- 创建者
	create_user varchar(20) NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (group_name)
) COMMENT = '用户组表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;


-- 用户组权限表
CREATE TABLE t_group_permission
(
	-- 用户组名
	group_name varchar(20) NOT NULL COMMENT '用户组名',
	-- 功能名
	function_name varchar(10) NOT NULL COMMENT '功能名',
	-- true：可
	-- false：不可
	readable boolean DEFAULT false NOT NULL COMMENT '浏览权限',
	-- true：可
	-- false：不可
	addable boolean DEFAULT false COMMENT '追加权限',
	-- true：可
	-- false：不可
	editable boolean DEFAULT false COMMENT '编辑权限',
	-- true：可
	-- false：不可
	deleteable boolean DEFAULT false COMMENT '删除权限',
	-- 创建者
	create_user varchar(20) NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (group_name, function_name)
) COMMENT = '用户组权限表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

-- 用户用户组映射表
CREATE TABLE t_user_group
(
	-- 登录名
	login_name varchar(20) NOT NULL COMMENT '登录名',
	-- 用户组名
	group_name varchar(20) NOT NULL COMMENT '用户组名',
	-- 创建者
	create_user varchar(20) DEFAULT 'system' NOT NULL COMMENT '创建者',
	-- 创建日期
	create_date datetime DEFAULT current_timestamp NOT NULL COMMENT '创建日期',
	-- 更新者
	update_user varchar(20) COMMENT '更新者',
	-- 最终更新日期
	update_date datetime COMMENT '最终更新日期',
	-- 逻辑删除识别子
	delete_flag boolean DEFAULT false NOT NULL COMMENT '逻辑删除识别子',
	PRIMARY KEY (login_name, group_name)
) COMMENT = '用户用户组映射表' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
