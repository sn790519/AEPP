SET SESSION FOREIGN_KEY_CHECKS=0;

drop function if exists get_lastest_audit_seq;
drop function if exists get_next_audit_phase;
drop function if exists get_random;
drop function if exists get_user_group;
drop function if exists get_user_group_name;
drop function if exists get_group_user;
drop function if exists get_group_user_name;

DELIMITER $$
CREATE FUNCTION `get_lastest_audit_seq`(in_login_name varchar(20)) RETURNS int(2)
    DETERMINISTIC
BEGIN
	DECLARE audit_seq int(2) default 0;
    select audit_sequence into audit_seq from v_member_audit where member_login_name = in_login_name;
	RETURN audit_seq + 1;
END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `get_next_audit_phase`(in_login_name varchar(20), in_audit_result int(2)) RETURNS int(1)
    DETERMINISTIC
BEGIN
	DECLARE audit_phase int(1) default 1;
    select audit_phase into audit_phase from v_member_audit_status where member_login_name = in_login_name;
    if (in_audit_result = 9) then
		set audit_phase = audit_phase + 1;
    end if;
	RETURN audit_phase;
END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `get_random`(in_size LONG) RETURNS varchar(50) CHARSET utf8
    DETERMINISTIC
BEGIN
	RETURN LPAD(FLOOR(RAND() * POWER(10, in_size)), in_size, '0');
END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `get_user_group`(in_login_name varchar(20)) RETURNS varchar(1000) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE result varchar(1000) default '';
	declare user_group varchar(20);
	declare done boolean default false;
	declare cur cursor for select group_name from v_user_group where login_name = in_login_name and delete_flag = false;
	declare continue handler for not found set done = true;
	set done = false;
	
	open cur;
	repeat
	fetch cur into user_group;
    if (not done) then
		if (result = '') then
			set result = user_group;
		else
			set result = concat(result, ',', user_group);
		end if;
    end if;
	
	until done
	end repeat;
	close cur;

	RETURN result;
END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `get_user_group_name`(in_login_name varchar(20)) RETURNS varchar(1000) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE result varchar(1000) default '';
	declare user_group_name varchar(20);
	declare done boolean default false;
	declare cur cursor for select gname from v_user_group where login_name = in_login_name and delete_flag = false;
	declare continue handler for not found set done = true;
	set done = false;
	
	open cur;
	repeat
	fetch cur into user_group_name;
    if (not done) then
		if (result = '') then
			set result = user_group_name;
		else
			set result = concat(result, ',', user_group_name);
		end if;
    end if;
	
	until done
	end repeat;
	close cur;

	RETURN result;
END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `get_group_user`(in_group_name varchar(20)) RETURNS varchar(1000) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE result varchar(1000) default '';
	declare group_user varchar(20);
	declare done boolean default false;
	declare cur cursor for select login_name from v_user_group where group_name = in_group_name and delete_flag = false;
	declare continue handler for not found set done = true;
	set done = false;
	
	open cur;
	repeat
	fetch cur into group_user;
    if (not done) then
		if (result = '') then
			set result = group_user;
		else
			set result = concat(result, ',', group_user);
		end if;
    end if;
	
	until done
	end repeat;
	close cur;

	RETURN result;
END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `get_group_user_name`(in_group_name varchar(20)) RETURNS varchar(1000) CHARSET utf8
    DETERMINISTIC
BEGIN
	DECLARE result varchar(1000) default '';
	declare group_user_name varchar(20);
	declare done boolean default false;
	declare cur cursor for select uname from v_user_group where group_name = in_group_name and delete_flag = false;
	declare continue handler for not found set done = true;
	set done = false;
	
	open cur;
	repeat
	fetch cur into group_user_name;
    if (not done) then
		if (result = '') then
			set result = group_user_name;
		else
			set result = concat(result, ',', group_user_name);
		end if;
    end if;
	
	until done
	end repeat;
	close cur;

	RETURN result;
END$$
DELIMITER ;