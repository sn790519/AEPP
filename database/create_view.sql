SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Views */
DROP VIEW IF EXISTS v_audit_flow;
DROP VIEW IF EXISTS v_member;
DROP VIEW IF EXISTS v_member_audit;
DROP VIEW IF EXISTS v_member_audit_status;
DROP VIEW IF EXISTS v_tender_enroll_count;
DROP VIEW IF EXISTS v_tender_finalist_count;
DROP VIEW IF EXISTS v_tender_information;
DROP VIEW IF EXISTS v_tender_quote_count;
DROP VIEW IF EXISTS v_user;
DROP VIEW IF EXISTS v_group;
DROP VIEW IF EXISTS v_user_group;

/* Create Views */

-- 会员审批流程视图
CREATE VIEW v_audit_flow AS select
  taf.member_login_name AS member_login_name,
  taf.audit_sequence AS audit_sequence,
  taf.audit_date AS audit_date,
  taf.audit_phase AS audit_phase,
  taf.audit_result AS audit_result,
  taf.audit_comment AS audit_comment,
  taf.login_name AS login_name,
  taf.create_user AS create_user,
  taf.create_date AS create_date,
  taf.update_user AS update_user,
  taf.update_date AS update_date,
  taf.delete_flag AS delete_flag,
  mcn1.name AS audit_phase_name,
  mcn2.name AS audit_result_name
from
  aepp.t_audit_flow taf
left join
  aepp.m_code_name mcn1
on (mcn1.type_id = '04' and mcn1.code = taf.audit_phase)
left join
  aepp.m_code_name mcn2
on (mcn2.type_id = '05' and mcn2.code = taf.audit_result);

-- 会员审核视图
CREATE VIEW v_member_audit AS SELECT
  taf.member_login_name AS member_login_name,
  MAX(taf.audit_sequence) AS audit_sequence
FROM
  aepp.t_audit_flow taf
WHERE
  taf.delete_flag = 0
GROUP BY
  taf.member_login_name;

-- 会员审核状态视图
CREATE VIEW v_member_audit_status AS select
  taf.member_login_name AS member_login_name,
  taf.audit_sequence AS audit_sequence,
  taf.audit_date AS audit_date,
  taf.audit_phase AS audit_phase,
  mcn1.name AS audit_phase_name,
  taf.audit_result AS audit_result,
  mcn2.name AS audit_result_name,
  taf.audit_comment AS audit_comment,
  taf.login_name AS login_name,
  taf.create_user AS create_user,
  taf.create_date AS create_date,
  taf.update_user AS update_user,
  taf.update_date AS update_date,
  taf.delete_flag AS delete_flag
from
  aepp.t_audit_flow taf
join
  aepp.v_member_audit vma
on (taf.member_login_name = vma.member_login_name and taf.audit_sequence = vma.audit_sequence)
left join
  aepp.m_code_name mcn1
on (mcn1.type_id = '04' and mcn1.code = taf.audit_phase)
left join
  aepp.m_code_name mcn2
on (mcn2.type_id = '05' and mcn2.code = taf.audit_result);

-- 会员视图
CREATE VIEW v_member AS select
  tm.member_login_name AS member_login_name,
  tm.member_login_password AS member_login_password,
  tm.license_number AS license_number,
  tm.enterprise_name AS enterprise_name,
  tm.enterprise_property AS enterprise_property,
  mcn1.name AS enterprise_property_name,
  tm.enterprise_category AS enterprise_category,
  mcn2.name AS enterprise_category_name,
  tm.contact_name AS contact_name,
  tm.contact_certificates_type AS contact_certificates_type,
  tm.contact_certificates_no AS contact_certificates_no,
  tm.area_code AS area_code,
  tm.telephone AS telephone,
  tm.mobile AS mobile,
  tm.mobile_certificated AS mobile_certificated,
  tm.email AS email,
  tm.email_certificated AS email_certificated,
  tm.qq_no AS qq_no,
  tm.weixin_no AS weixin_no,
  tm.legal_name AS legal_name,
  tm.legal_certificates_type AS legal_certificates_type,
  tm.legal_certificates_no AS legal_certificates_no,
  tm.image_contact_certificates_front AS image_contact_certificates_front,
  tm.image_contact_certificates_back AS image_contact_certificates_back,
  tm.image_legal_certificates_front AS image_legal_certificates_front,
  tm.image_legal_certificates_back AS image_legal_certificates_back,
  tm.image_license AS image_license,
  tm.points AS points,
  tm.rank AS rank,
  mcn3.name AS rank_name,
  tm.create_user AS create_user,
  tm.create_date AS create_date,
  tm.update_user AS update_user,
  tm.update_date AS update_date,
  tm.delete_flag AS delete_flag,
  vmas.audit_sequence AS lastest_audit_sequence,
  vmas.audit_date AS lastest_audit_date,
  vmas.audit_phase AS lastest_audit_phase,
  vmas.audit_phase_name AS lastest_audit_phase_name,
  vmas.audit_result AS lastest_audit_result,
  vmas.audit_result_name AS lastest_audit_result_name,
  vmas.audit_comment AS lastest_audit_comment,
  vmas.login_name AS lastest_audit_login_name
from
  aepp.t_member tm
left join
  aepp.m_code_name mcn1
on (tm.enterprise_property = mcn1.code and mcn1.type_id = '02')
left join
  aepp.m_code_name mcn2
on (tm.enterprise_category = mcn2.code and mcn2.type_id = '03')
left join
  aepp.m_code_name mcn3
on (tm.rank = mcn3.code and mcn3.type_id = '06')
left join
  aepp.v_member_audit_status vmas
on (tm.member_login_name = vmas.member_login_name);

-- 报名供应商数量视图
CREATE VIEW v_tender_enroll_count AS SELECT 
    tte.tender_id AS tender_id,
    COUNT(tte.member_login_name) AS enroll_count
FROM
    aepp.t_tender_enroll tte
WHERE
    (tte.delete_flag = 0)
GROUP BY tte.tender_id;
-- 入围供应商数量视图
CREATE VIEW v_tender_finalist_count AS SELECT 
    ttf.tender_id AS tender_id,
    COUNT(ttf.member_login_name) AS finalist_count
FROM
    aepp.t_tender_finalist ttf
WHERE
    (ttf.delete_flag = 0)
GROUP BY ttf.tender_id;
    
-- 投标供应商数量视图
CREATE VIEW v_tender_quote_count AS SELECT 
    ttq.tender_id AS tender_id,
    COUNT(ttq.member_login_name) AS quote_count
FROM
    aepp.t_tender_quote ttq
WHERE
    (ttq.delete_flag = 0)
GROUP BY ttq.tender_id;

-- 招标信息视图
CREATE VIEW v_tender_information AS SELECT 
    tti.tender_id AS tender_id,
    tti.tender_no AS tender_no,
    tti.status AS status,
    mcn1.name AS status_name,
    tti.tender_title AS tender_title,
    tti.enroll_start_date AS enroll_start_date,
    tti.enroll_end_date AS enroll_end_date,
    tti.tender_end_date AS tender_end_date,
    tti.publish_result_date AS publish_result_date,
    tti.content_description AS content_description,
    tti.vendor_requirement AS vendor_requirement,
    tti.tender_pattern AS tender_pattern,
    mcn2.name AS tender_pattern_name,
    tti.tender_template AS tender_template,
    tti.contact_name AS contact_name,
    tti.contact_tel AS contact_tel,
    IF(ISNULL(vtec.enroll_count),
        0,
        vtec.enroll_count) AS enroll_count,
    IF(ISNULL(vtfc.finalist_count),
        0,
        vtfc.finalist_count) AS finalist_count,
    IF(ISNULL(vtqc.quote_count),
        0,
        vtqc.quote_count) AS quote_count,
    tti.create_user AS create_user,
    tti.create_date AS create_date,
    tti.update_user AS update_user,
    tti.update_date AS update_date,
    tti.delete_flag AS delete_flag
FROM
    (((((aepp.t_tender_information tti
    LEFT JOIN aepp.m_code_name mcn1 ON (((mcn1.type_id = '08')
        AND (mcn1.code = tti.status))))
    LEFT JOIN aepp.m_code_name mcn2 ON (((mcn2.type_id = '09')
        AND (mcn2.code = tti.tender_pattern))))
    LEFT JOIN aepp.v_tender_enroll_count vtec ON ((vtec.tender_id = tti.tender_id)))
    LEFT JOIN aepp.v_tender_finalist_count vtfc ON ((vtfc.tender_id = tti.tender_id)))
    LEFT JOIN aepp.v_tender_quote_count vtqc ON ((vtqc.tender_id = tti.tender_id)));
    
-- 用户视图
CREATE VIEW `v_user` AS
    SELECT 
        `tu`.`login_name` AS `login_name`,
        `tu`.`login_password` AS `login_password`,
        `tu`.`name` AS `name`,
        `tu`.`create_user` AS `create_user`,
        `tu`.`create_date` AS `create_date`,
        `tu`.`update_user` AS `update_user`,
        `tu`.`update_date` AS `update_date`,
        `tu`.`delete_flag` AS `delete_flag`,
        GET_USER_GROUP(`tu`.`login_name`) AS `user_group`,
        GET_USER_GROUP_NAME(`tu`.`login_name`) AS `user_group_name`
    FROM
        `t_user` `tu`
    WHERE
        (`tu`.`delete_flag` = 0);

-- 用户组视图
CREATE VIEW `v_group` AS
    SELECT 
        `tg`.`group_name` AS `group_name`,
        `tg`.`name` AS `name`,
        `tg`.`create_user` AS `create_user`,
        `tg`.`create_date` AS `create_date`,
        `tg`.`update_user` AS `update_user`,
        `tg`.`update_date` AS `update_date`,
        `tg`.`delete_flag` AS `delete_flag`,
        GET_GROUP_USER(`tg`.`group_name`) AS `group_user`,
        GET_GROUP_USER_NAME(`tg`.`group_name`) AS `group_user_name`
    FROM
        `t_group` `tg`
    WHERE
        (`tg`.`delete_flag` = 0);
        
-- 用户用户组映射视图
CREATE VIEW `v_user_group` AS
    SELECT 
        `tug`.`login_name` AS `login_name`,
        `tug`.`group_name` AS `group_name`,
        `tug`.`create_user` AS `create_user`,
        `tug`.`create_date` AS `create_date`,
        `tug`.`update_user` AS `update_user`,
        `tug`.`update_date` AS `update_date`,
        `tug`.`delete_flag` AS `delete_flag`,
        `tu`.`name` AS `uname`,
        `tg`.`name` AS `gname`
    FROM
        ((`t_user_group` `tug`
        JOIN `t_user` `tu` ON (((`tu`.`login_name` = `tug`.`login_name`)
            AND (`tu`.`delete_flag` = 0))))
        JOIN `t_group` `tg` ON (((`tg`.`group_name` = `tug`.`group_name`)
            AND (`tg`.`delete_flag` = 0))))
    WHERE
        (`tug`.`delete_flag` = 0);