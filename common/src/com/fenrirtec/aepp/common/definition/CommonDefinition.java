package com.fenrirtec.aepp.common.definition;

import com.fenrirtec.core.definition.BaseDefinition;

public class CommonDefinition extends BaseDefinition {
	
	public static final String ADMIN = "admin";

	public final class SessionKey {
		public static final String SYSTEM_CONFIG = "system_config";
		public static final String USER_INFO = "user_info";
		public static final String MEMBER_INFO = "member_info";
	}

	public final class ErrorCode extends BaseErrorCode {
		public static final int ERROR_REQUIRED = 1001;
		public static final int ERROR_AUTHORITY = 1002;
		public static final int ERROR_VERIFICATION = 1003;
		public static final int ERROR_UNIQUE = 1004;
		public static final int ERROR_DOWNLOAD_FAIL = 1005;
	}

	public final class SQLState {
		public static final String UNIQUE_VIOLATION = "23505";
	}
}
