package com.fenrirtec.aepp.common.definition;

public class Enum {
	
	/**
	 * 证件类�?
	 */
	public enum CertificatesType {
		
		ID(1),						//身份�?
		PASSPORT(2);				//护照
		
		private int mValue;
		
		private CertificatesType(int value) {
			mValue = value;
		}
		
		public int getValue() {
			return mValue;
		}
		
		public static CertificatesType valueOf(int intVal) {
			for (CertificatesType enumVal : values()) {
				if (enumVal.getValue() == intVal) {
					return enumVal;
				}
			}
			return ID;
		}
	}
	
	/** 
	 * 企业�?质
	 */
	public enum EnterpriseProperty {
		
		STATE(1),
		COOPERATION(2),
		JOINT(3),
		SOLE(4),
		COLLECTIVE(5),
		PRIVATE(6);
		
		private int mValue;
		
		private EnterpriseProperty(int value) {
			mValue = value;
		}
		
		public int getValue() {
			return mValue;
		}
		
		public static EnterpriseProperty valueOf(int intVal) {
			for (EnterpriseProperty enumVal : values()) {
				if (enumVal.getValue() == intVal) {
					return enumVal;
				}
			}
			return STATE;
		}
	}
	
	/**
	 * 行业�?类
	 */
	public enum EnterpriseCategory {
		
		ORGANIZATION(1),			//机�?�?�?
		AGRICULTURE(2),			//农林牧�?
		MEDICAL(3),				//医药卫�?
		ARCHITECTURE(4),			//建筑建�?
		MINERAL(5),				//冶金矿产
		PETRIFACTION(6),			//石油化工
		WATER(7),					//水利水电
		TRANSPORTATION(8),		//交通运�?
		IT(9),						//信息产�?
		MECHANICAL(10),			//机械机电
		FOOD(11),					//轻工食品
		TEXTILE(12),				//服�?���?
		SERVICES(13),				//专业服务
		SAFETY(14),					//安�?防护
		ENVIRONMENTAL(15),			//环保绿�?
		TOURISM(16),				//�?��休闲
		OFFICE(17),					//办�?�?��
		ELECTRONIC(18),			//电子电工
		TOYS(19),					//玩具礼�?
		HOUSEWEAR(20),				//家�?���?
		MATERIAL(21),				//物�?��材
		PACKAGING(22),				//�?�?���?
		SPORTS(23),					//体育用�?
		FURNITURE(24),				//办�?家具
		OTHER(99);					//其他�?类
		
		private int mValue;
		
		private EnterpriseCategory(int value) {
			mValue = value;
		}
		
		public int getValue() {
			return mValue;
		}
		
		public static EnterpriseCategory valueOf(int intVal) {
			for (EnterpriseCategory enumVal : values()) {
				if (enumVal.getValue() == intVal) {
					return enumVal;
				}
			}
			return OTHER;
		}
	}
	
	/**
	 * 审核阶段
	 */
	public enum AuditPhase {
		
		REGIST(1),					//会员注�?
		UPLOAD(2),					//�?��上�?
		TRIAL(3),					//初级审核
		SUBMIT(4),					//�?��材料提交
		SECOND(5),					//二级审核
		FINISH(6);					//审核结束
		
		private int mValue;
		
		private AuditPhase(int value) {
			mValue = value;
		}
		
		public int getValue() {
			return mValue;
		}
		
		public static AuditPhase valueOf(int intVal) {
			for (AuditPhase enumVal : values()) {
				if (enumVal.getValue() == intVal) {
					return enumVal;
				}
			}
			return REGIST;
		}
	}
	
	/**
	 * 审核阶段
	 */
	public enum AuditResult {
		
		PROCESSING(1),				//审核中
		PASS(2),					//通�?
		REFUSE(3),					//未通�?
		CANCEL(4);					//取�?
		
		private int mValue;
		
		private AuditResult(int value) {
			mValue = value;
		}
		
		public int getValue() {
			return mValue;
		}
		
		public static AuditResult valueOf(int intVal) {
			for (AuditResult enumVal : values()) {
				if (enumVal.getValue() == intVal) {
					return enumVal;
				}
			}
			return PROCESSING;
		}
	}
	
	/**
	 * 会员等级
	 */
	public enum Rank {
		
		REGIST(1),					//注册会员
		CERTIFICATED(2),			//认证会员
		IRON(3),					//铁牌会员
		COPPER(4),					//铜牌会员
		SILVER(5),					//银牌会员
		GOLD(6),					//金牌会员
		DIAMONDS(7);				//钻石会员
		
		private int mValue;
		
		private Rank(int value) {
			mValue = value;
		}
		
		public int getValue() {
			return mValue;
		}
		
		public static Rank valueOf(int intVal) {
			for (Rank enumVal : values()) {
				if (enumVal.getValue() == intVal) {
					return enumVal;
				}
			}
			return REGIST;
		}
	}
	
	/**
	 * 首页招�?信息排序�??
	 */
	public enum TenderSort {
		
		TIME_DESC(1),				//时间降�?
		TIME_ASC(2),				//时间�?�?
		BROWSE_DESC(3),			//浏览量降�?
		PRICE_DESC(4),				//合同额降�?
		PRICE_ASC(5);				//合同额升�?
		
		private int mValue;
		
		private TenderSort(int value) {
			mValue = value;
		}
		
		public int getValue() {
			return mValue;
		}
		
		public static TenderSort valueOf(int intVal) {
			for (TenderSort enumVal : values()) {
				if (enumVal.getValue() == intVal) {
					return enumVal;
				}
			}
			return TIME_DESC;
		}
	}
}
