package com.fenrirtec.aepp.common.dao;

import java.util.List;

import com.fenrirtec.aepp.common.condition.TenderCondition;
import com.fenrirtec.aepp.common.model.TenderInformation;

public interface InformationDao {
	Integer countByCnd(TenderCondition condition);
	List<TenderInformation> searchByCnd(TenderCondition condition);
}
