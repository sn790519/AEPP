package com.fenrirtec.aepp.common.dao;

import java.util.List;

import com.fenrirtec.aepp.common.dto.TenderBidDto;
import com.fenrirtec.aepp.common.model.TenderBidlist;


public interface TenderBidDao {
	//根据传入的ID显示所对应的投标供应商
	List<TenderBidlist> findTenderBidlsit(Integer tenderid);
	//授标确定在中标供应商表追加记录
	void insertTenderBid(List<TenderBidDto> tenderBid);
	//授标时更新招标入围表的入围标识为true
    void updateTenderFinalist(Integer tenderid);
    //中标供应商：
    List<TenderBidlist> findTenderBidSuppliers(Integer tenderid);
    //中标供应商：点击公示按钮更新招标信息表中阶段为公示
    void updateTenderInformation(Integer tenderid);
}