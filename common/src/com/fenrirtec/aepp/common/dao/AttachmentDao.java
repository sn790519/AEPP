package com.fenrirtec.aepp.common.dao;

import com.fenrirtec.aepp.common.model.Attachment;
import com.fenrirtec.aepp.common.model.Upload;

public interface AttachmentDao {
	Integer saveAttachment(Integer fileType, String path, String comment, String uploadUser);
	Attachment searchAttachment(Integer attachmentId);
	void relateMemberMaterial(String memberLoginName, String uploadFileCategory, Integer attachmentId);
	void removeAttachment(Integer attachmentId);
	Integer upload(String person, Integer personType, String purpose, String path, Integer fileType);
	Upload uploadInfo(Integer uploadId);
	void removeUpload(Integer uploadId);
}
