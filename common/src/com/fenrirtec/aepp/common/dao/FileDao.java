package com.fenrirtec.aepp.common.dao;

public interface FileDao {
	void moveFile(String srcFile, String destFile);
    void copyFile(String srcFile, String destFile);
	void deleteFile(String path);
	void renameFolder(String oldPath, String newPath);
	void deleteFolder(String path);
}
