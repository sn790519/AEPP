package com.fenrirtec.aepp.common.dao;

import java.util.List;

import com.fenrirtec.aepp.common.model.TenderInformation;

public interface TenderInformationDao {
	int insertTenderInformation(TenderInformation tenderInformation);
	List<TenderInformation> findTenderInformation(Integer tenderid);
}
