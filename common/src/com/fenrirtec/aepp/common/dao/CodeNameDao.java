package com.fenrirtec.aepp.common.dao;

import java.util.List;

import com.fenrirtec.aepp.common.model.CodeName;

public interface CodeNameDao {

	List<CodeName> findByTypeId(String typeId);

}
