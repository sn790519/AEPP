package com.fenrirtec.aepp.common.dao;

import java.util.List;

import com.fenrirtec.aepp.common.model.Group;
import com.fenrirtec.aepp.common.model.User;
import com.fenrirtec.aepp.common.model.UserGroup;

public interface UserDao {
	User find(String loginName);
    List<User> findAll();
    List<Group> findAllGroup();
    List<User> find(String keyword, String[] groups);
    List<Group> findGroup(String keyword);
    Boolean insert(User user);
    Boolean insert(UserGroup userGroup);
    Boolean update(User user);
    void deleteUserGroup(String loginName);
    Boolean delete(String loginName);
    Group findGroupByGroupName(String groupName);
    Boolean insert(Group group);
    Boolean update(Group group);
    Boolean deleteGroup(String groupName);
    void deleteGroupUser(String groupName);
}
