package com.fenrirtec.aepp.common.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dao.TenderFinalistDao;
import com.fenrirtec.aepp.common.dto.TenderFinalistDto;
import com.fenrirtec.aepp.common.model.TenderEnroll;
import com.fenrirtec.core.database.DatabaseSessionManager;
import com.fenrirtec.core.exception.DatabaseException;

public class TenderFinalistDao_JDBCImpl implements TenderFinalistDao {
	private static final Logger logger = LoggerFactory
			.getLogger(TenderFinalistDao_JDBCImpl.class);

	@Override
	public void insertTenderFinalist(List<TenderFinalistDto> tenderFinaList) {
		if(logger.isInfoEnabled()){
			logger.info("[TenderFinalistDao_JDBCImpl#insertTenderFinalist] start.");
		}
		PreparedStatement statement=null;
		try{
			for(int i = 0;i < tenderFinaList.size();i++){
			
			StringBuffer sql=new StringBuffer()
			.append("insert into t_tender_finalist( \n")
			.append(" tender_id,\n")
			.append(" member_login_name,\n")
			.append(" finalist_date,\n")
			.append(" evaluation,\n")
			.append(" create_user,\n")
			.append(" create_date,\n")
			.append(" update_user,\n")
			.append(" update_date)\n")
			.append("values( \n")
			.append(" ? ,\n")
			.append(" ? ,\n")
			.append(" ? ,\n")
			.append(" ? ,\n")
			.append(" ? ,\n")
			.append(" ? ,\n")
			.append(" ? ,\n")
			.append(" ? )\n");
			if(logger.isTraceEnabled()){
				logger.trace("[TenderFinalistDao_JDBCImpl#insertTenderFinalist] sql={}",sql);
			}
			Connection connection = DatabaseSessionManager.getInstance()
					.getSession();
			statement = connection.prepareStatement(sql.toString());

			/*Date nowTime = new Date(System.currentTimeMillis());
			SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyyMMdd");
			String retStrFormatNowDate = sdFormatter.format(nowTime);*/
			
			statement.setInt(1, tenderFinaList.get(i).getTenderId());
			statement.setString(2, tenderFinaList.get(i).getMemberLoginName());
			String finalistDatestr = tenderFinaList.get(i).getFinalistDate();
			String createDatestr = tenderFinaList.get(i).getFinalistDate();
			String updateDatestr = tenderFinaList.get(i).getFinalistDate();
			SimpleDateFormat sim=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");	
			Date finalistDate=null;
			Date createDate=null;
			Date updateDate=null;
			try {
				finalistDate=sim.parse(finalistDatestr);
				createDate=sim.parse(createDatestr);
				updateDate=sim.parse(updateDatestr);
			} catch (ParseException e) {
				
				e.printStackTrace();
			}
			
			
			statement.setDate(3,  new java.sql.Date(finalistDate.getTime()));
			statement.setString(4, tenderFinaList.get(i).getEvaluation());
			statement.setString(5, "system");
			statement.setDate(6, new java.sql.Date(createDate.getTime()));
			statement.setString(7, "system");
			statement.setDate(8, new java.sql.Date(updateDate.getTime()));
			statement.execute();
			}
		}catch(SQLException e){
			if(logger.isTraceEnabled()){
				logger.trace("[TenderFinalistDao_JDBCImpl#insertTenderFinalist] sql error occurred.",
						e);
			}
			throw new DatabaseException("database error occurred.", e);
		}finally{
			if (logger.isInfoEnabled()) {
				logger.info("[TenderFinalistDao_JDBCImpl#insertTenderFinalist] finish.");
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
				}
			}
		}

	}

	@Override
	public void updateTenderFinalist(Integer tenderid) {
		if(logger.isInfoEnabled()){
			logger.info("[TenderFinalistDao_JDBCImpl#updateTenderFinalist] start.");
		}
		PreparedStatement statement=null;
		try{
	
			StringBuffer sql=new StringBuffer()
			.append("update \n")
			.append(" t_tender_enroll \n")
			.append(" set \n")
			.append(" finalist_flag = ? \n")
			.append(" where \n")
			.append(" tender_id = ?\n");
			
			if(logger.isTraceEnabled()){
				logger.trace("[TenderFinalistDao_JDBCImpl#updateTenderFinalist] sql={}",sql);
			}
			Connection connection = DatabaseSessionManager.getInstance()
					.getSession();
			statement = connection.prepareStatement(sql.toString());
			statement.setBoolean(1, true);
			statement.setInt(2, tenderid);
			statement.executeUpdate();
			
		}catch(SQLException e){
			if(logger.isTraceEnabled()){
				logger.trace("[TenderFinalistDao_JDBCImpl#updateTenderFinalist] sql error occurred.",
						e);
			}
			throw new DatabaseException("database error occurred.", e);
		}finally{
			if (logger.isInfoEnabled()) {
				logger.info("[TenderFinalistDao_JDBCImpl#updateTenderFinalist] finish.");
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	@Override
	public void updateTenderFinalistflag(TenderEnroll tenderEnroll) {
		if(logger.isInfoEnabled()){
			logger.info("[TenderFinalistDao_JDBCImpl#updateTenderFinalistflag] start.");
		}
		PreparedStatement statement=null;
		try{
	
			StringBuffer sql=new StringBuffer()
			.append("update \n")
			.append(" t_tender_enroll \n")
			.append(" set \n")
			.append(" update_user = ? , \n")
			.append(" update_date = current_timestamp , \n")
			.append(" delete_flag = ? \n")
			.append(" where \n")
			.append(" tender_id = ?\n");
			
			if(logger.isTraceEnabled()){
				logger.trace("[TenderFinalistDao_JDBCImpl#updateTenderFinalistflag] sql={}",sql);
			}
			Connection connection = DatabaseSessionManager.getInstance()
					.getSession();
			statement = connection.prepareStatement(sql.toString());
			statement.setString(1, tenderEnroll.getUpdateUser());		
			statement.setBoolean(2, true);
			statement.setInt(3, tenderEnroll.getTenderId());
			statement.executeUpdate();
			
		}catch(SQLException e){
			if(logger.isTraceEnabled()){
				logger.trace("[TenderFinalistDao_JDBCImpl#updateTenderFinalistflag] sql error occurred.",
						e);
			}
			throw new DatabaseException("database error occurred.", e);
		}finally{
			if (logger.isInfoEnabled()) {
				logger.info("[TenderFinalistDao_JDBCImpl#updateTenderFinalistflag] finish.");
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
				}
			}
		}
		
	}
}
