package com.fenrirtec.aepp.common.dao.impl;

import java.sql.SQLException;

import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fenrirtec.aepp.common.dao.TenderProductDao;
import com.fenrirtec.aepp.common.dto.TenderProductDto;
import com.fenrirtec.core.database.DatabaseSessionManager;
import com.fenrirtec.core.exception.DatabaseException;

public class TenderProductDao_JDBCImpl implements TenderProductDao {

	private static final Logger logger = LoggerFactory
			.getLogger(TenderProductDao_JDBCImpl.class);

	@Override
	public void insertTenderProduct(List<TenderProductDto> tenderproductdtoList,int maxId) {
		if (logger.isInfoEnabled()) {
			logger.info("[TenderProductDao_JDBCImpl#insertTenderProduct] start.");
		}
		
		PreparedStatement statement = null;
		try {
			for (int i = 0; i < tenderproductdtoList.size(); i++) {
				StringBuilder sql = new StringBuilder()
						.append("insert into t_tender_product ( \n")
						.append("  tender_id, \n").append("  product_name, \n")
						.append("  quantity, \n").append("  unit, \n")
						.append("  product_description, \n")
						.append("  create_date )\n").append("values ( \n")
						.append("  ? ,\n").append("  ? ,\n").append("  ? ,\n")
						.append("  ? ,\n").append("  ? ,\n")
						.append("  NOW() )\n");
				if (logger.isTraceEnabled()) {
					logger.trace(
							"[TenderProductDao_JDBCImpl#insertTenderProduct] sql={}",
							sql);
				}
				Connection connection = DatabaseSessionManager.getInstance()
						.getSession();
				statement = connection.prepareStatement(sql.toString());
				statement.setLong(1, maxId);
				statement.setString(2, tenderproductdtoList.get(i)
						.getProductName());
				statement.setLong(3, tenderproductdtoList.get(i).getQuantity());
				statement.setString(4, tenderproductdtoList.get(i).getUnit());
				statement.setString(5, tenderproductdtoList.get(i)
						.getProductDescription());
				statement.execute();
			}
		} catch (SQLException e) {
			if (logger.isTraceEnabled()) {
				logger.trace(
						"[TenderProductDao_JDBCImpl#insertTenderInformation] sql error occurred.",
						e);
			}
			throw new DatabaseException("database error occurred.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[TenderProductDao_JDBCImpl#insertTenderInformation] finish.");
			}
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
				}
			}
		}
	}
}