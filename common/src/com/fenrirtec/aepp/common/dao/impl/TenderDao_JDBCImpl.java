package com.fenrirtec.aepp.common.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.condition.TenderCondition;
import com.fenrirtec.aepp.common.dao.TenderDao;
import com.fenrirtec.aepp.common.model.TenderDetail;
import com.fenrirtec.aepp.common.model.TenderEnroll;
import com.fenrirtec.aepp.common.model.TenderFinalist;
import com.fenrirtec.aepp.common.model.TenderProduct;
import com.fenrirtec.core.database.DatabaseSessionManager;
import com.fenrirtec.core.database.ResultSetMapper;
import com.fenrirtec.core.exception.DatabaseException;

public class TenderDao_JDBCImpl implements TenderDao {
    
	private static final Logger logger = LoggerFactory.getLogger(TenderDao_JDBCImpl.class);

    @Override
    public Integer countByCnd(TenderCondition condition) {
        if (logger.isInfoEnabled()) {
            logger.info("[TenderDao_JDBCImpl#countByCnd] start.");
        }

        PreparedStatement statement = null;
        
        int count = 0;
        
        try {
            
            StringBuilder sql = new StringBuilder()
                .append("select \n")
                .append("  count(tender_id) as count \n")
                .append("from \n")
                .append("  v_tender_information \n")
                .append("where \n")
                .append("  delete_flag = false \n");
            
            if (condition != null) {
                if (StringUtils.isNotEmpty(condition.getTenderTitle())) {
                    sql.append("and \n");
                    sql.append("  tender_title like ? \n");
                }
                if (StringUtils.isNotEmpty(condition.getEnrollStartDateFrom())) {
                    sql.append("and \n");
                    sql.append("  enroll_start_date >= ? \n");
                }
                if (StringUtils.isNotEmpty(condition.getEnrollStartDateTo())) {
                    sql.append("and \n");
                    sql.append("  enroll_start_date <= ? \n");
                }
                if (StringUtils.isNotEmpty(condition.getEnrollEndDateFrom())) {
                    sql.append("and \n");
                    sql.append("  enroll_end_date >= ? \n");
                }
                if (StringUtils.isNotEmpty(condition.getEnrollEndDateTo())) {
                    sql.append("and \n");
                    sql.append("  enroll_end_date <= ? \n");
                }
                if (StringUtils.isNotEmpty(condition.getTenderEndDateFrom())) {
                    sql.append("and \n");
                    sql.append("  tender_end_date >= ? \n");
                }
                if (StringUtils.isNotEmpty(condition.getTenderEndDateTo())) {
                    sql.append("and \n");
                    sql.append("  tender_end_date <= ? \n");
                }
                if (StringUtils.isNotEmpty(condition.getPublishResultDateFrom())) {
                    sql.append("and \n");
                    sql.append("  publish_result_date >= ? \n");
                }
                if (StringUtils.isNotEmpty(condition.getPublishResultDateTo())) {
                    sql.append("and \n");
                    sql.append("  publish_result_date <= ? \n");
                }
                if (condition.getTenderPattern() != null && condition.getTenderPattern().intValue() > 0) {
                    sql.append("and \n");
                    sql.append("  tender_pattern = ? \n");
                }
                if (condition.getStatusFrom() != null && condition.getStatusFrom().intValue() > 0) {
                    sql.append("and \n");
                    sql.append("  status >= ? \n");
                }
                if (condition.getStatusTo() != null && condition.getStatusTo().intValue() > 0) {
                    sql.append("and \n");
                    sql.append("  status <= ? \n");
                }
            }
            
            if (logger.isTraceEnabled()) {
                logger.trace("[TenderDao_JDBCImpl#countByCnd] sql={}", sql);
            }
            
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            
            if (condition != null) {
                int index = 0;
                if (StringUtils.isNotEmpty(condition.getTenderTitle())) {
                    statement.setString(++index, "%" + condition.getTenderTitle() + "%");
                }
                if (StringUtils.isNotEmpty(condition.getEnrollStartDateFrom())) {
                    statement.setString(++index, condition.getEnrollStartDateFrom());
                }
                if (StringUtils.isNotEmpty(condition.getEnrollStartDateTo())) {
                    statement.setString(++index, condition.getEnrollStartDateTo());
                }
                if (StringUtils.isNotEmpty(condition.getEnrollEndDateFrom())) {
                    statement.setString(++index, condition.getEnrollEndDateFrom());
                }
                if (StringUtils.isNotEmpty(condition.getEnrollEndDateTo())) {
                    statement.setString(++index, condition.getEnrollEndDateTo());
                }
                if (StringUtils.isNotEmpty(condition.getTenderEndDateFrom())) {
                    statement.setString(++index, condition.getTenderEndDateFrom());
                }
                if (StringUtils.isNotEmpty(condition.getTenderEndDateTo())) {
                    statement.setString(++index, condition.getTenderEndDateTo());
                }
                if (StringUtils.isNotEmpty(condition.getPublishResultDateFrom())) {
                    statement.setString(++index, condition.getPublishResultDateFrom());
                }
                if (StringUtils.isNotEmpty(condition.getPublishResultDateTo())) {
                    statement.setString(++index, condition.getPublishResultDateTo());
                }
                if (condition.getTenderPattern() != null && condition.getTenderPattern().intValue() > 0) {
                    statement.setInt(++index, condition.getTenderPattern());
                }
                if (condition.getStatusFrom() != null && condition.getStatusFrom().intValue() > 0) {
                    statement.setInt(++index, condition.getStatusFrom());
                }
                if (condition.getStatusTo() != null && condition.getStatusTo().intValue() > 0) {
                    statement.setInt(++index, condition.getStatusTo());
                }
            }
            
            ResultSet resultSet = statement.executeQuery();
            
            if (resultSet.next()) {
                count = resultSet.getInt("count");
            }
            
            return count;
            
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[TenderDao_JDBCImpl#countByCnd] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[TenderDao_JDBCImpl#countByCnd] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
    }

    @Override
    public List<TenderDetail> searchByCnd(TenderCondition condition) {
        if (logger.isInfoEnabled()) {
            logger.info("[TenderDao_JDBCImpl#searchByCnd] start.");
        }

        PreparedStatement statement = null;
        
        try {
            
            StringBuilder sql = new StringBuilder()
                .append("select \n")
                .append("  tender_id, \n")
                .append("  tender_no, \n")
                .append("  status, \n")
                .append("  status_name, \n")
                .append("  tender_title, \n")
                .append("  enroll_start_date, \n")
                .append("  enroll_end_date, \n")
                .append("  tender_end_date, \n")
                .append("  publish_result_date, \n")
                .append("  content_description, \n")
                .append("  vendor_requirement, \n")
                .append("  tender_pattern, \n")
                .append("  tender_pattern_name, \n")
                .append("  tender_template, \n")
                .append("  contact_name, \n")
                .append("  contact_tel, \n")
                .append("  enroll_count, \n")
                .append("  finalist_count, \n")
                .append("  quote_count, \n")
                .append("  create_user, \n")
                .append("  create_date, \n")
                .append("  update_user, \n")
                .append("  update_date, \n")
                .append("  delete_flag \n")
                .append("from \n")
                .append("  v_tender_information \n")
                .append("where \n")
                .append("  delete_flag = false \n");
            
            if (condition != null) {
                if (StringUtils.isNotEmpty(condition.getTenderTitle())) {
                    sql.append("and \n");
                    sql.append("  tender_title like ? \n");
                }
                if (StringUtils.isNotEmpty(condition.getEnrollStartDateFrom())) {
                    sql.append("and \n");
                    sql.append("  enroll_start_date >= ? \n");
                }
                if (StringUtils.isNotEmpty(condition.getEnrollStartDateTo())) {
                    sql.append("and \n");
                    sql.append("  enroll_start_date <= ? \n");
                }
                if (StringUtils.isNotEmpty(condition.getEnrollEndDateFrom())) {
                    sql.append("and \n");
                    sql.append("  enroll_end_date >= ? \n");
                }
                if (StringUtils.isNotEmpty(condition.getEnrollEndDateTo())) {
                    sql.append("and \n");
                    sql.append("  enroll_end_date <= ? \n");
                }
                if (StringUtils.isNotEmpty(condition.getTenderEndDateFrom())) {
                    sql.append("and \n");
                    sql.append("  tender_end_date >= ? \n");
                }
                if (StringUtils.isNotEmpty(condition.getTenderEndDateTo())) {
                    sql.append("and \n");
                    sql.append("  tender_end_date <= ? \n");
                }
                if (StringUtils.isNotEmpty(condition.getPublishResultDateFrom())) {
                    sql.append("and \n");
                    sql.append("  publish_result_date >= ? \n");
                }
                if (StringUtils.isNotEmpty(condition.getPublishResultDateTo())) {
                    sql.append("and \n");
                    sql.append("  publish_result_date <= ? \n");
                }
                if (condition.getTenderPattern() != null && condition.getTenderPattern().intValue() > 0) {
                    sql.append("and \n");
                    sql.append("  tender_pattern = ? \n");
                }
                if (condition.getStatusFrom() != null && condition.getStatusFrom().intValue() > 0) {
                    sql.append("and \n");
                    sql.append("  status >= ? \n");
                }
                if (condition.getStatusTo() != null && condition.getStatusTo().intValue() > 0) {
                    sql.append("and \n");
                    sql.append("  status <= ? \n");
                }
            }
            
            sql.append("order by \n");
            sql.append("  tender_id desc \n");
            
            if (condition != null) {
                if (condition.getPage() != null && condition.getRows() != null && condition.getPage() > 0 && condition.getRows() > 0) {
                    sql.append("limit ?, ? \n");
                }
            }
            
            if (logger.isTraceEnabled()) {
                logger.trace("[TenderDao_JDBCImpl#searchByCnd] sql={}", sql);
            }
            
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            
            if (condition != null) {
                int index = 0;
                if (StringUtils.isNotEmpty(condition.getTenderTitle())) {
                    statement.setString(++index, "%" + condition.getTenderTitle() + "%");
                }
                if (StringUtils.isNotEmpty(condition.getEnrollStartDateFrom())) {
                    statement.setString(++index, condition.getEnrollStartDateFrom());
                }
                if (StringUtils.isNotEmpty(condition.getEnrollStartDateTo())) {
                    statement.setString(++index, condition.getEnrollStartDateTo());
                }
                if (StringUtils.isNotEmpty(condition.getEnrollEndDateFrom())) {
                    statement.setString(++index, condition.getEnrollEndDateFrom());
                }
                if (StringUtils.isNotEmpty(condition.getEnrollEndDateTo())) {
                    statement.setString(++index, condition.getEnrollEndDateTo());
                }
                if (StringUtils.isNotEmpty(condition.getTenderEndDateFrom())) {
                    statement.setString(++index, condition.getTenderEndDateFrom());
                }
                if (StringUtils.isNotEmpty(condition.getTenderEndDateTo())) {
                    statement.setString(++index, condition.getTenderEndDateTo());
                }
                if (StringUtils.isNotEmpty(condition.getPublishResultDateFrom())) {
                    statement.setString(++index, condition.getPublishResultDateFrom());
                }
                if (StringUtils.isNotEmpty(condition.getPublishResultDateTo())) {
                    statement.setString(++index, condition.getPublishResultDateTo());
                }
                if (condition.getTenderPattern() != null && condition.getTenderPattern().intValue() > 0) {
                    statement.setInt(++index, condition.getTenderPattern());
                }
                if (condition.getStatusFrom() != null && condition.getStatusFrom().intValue() > 0) {
                    statement.setInt(++index, condition.getStatusFrom());
                }
                if (condition.getStatusTo() != null && condition.getStatusTo().intValue() > 0) {
                    statement.setInt(++index, condition.getStatusTo());
                }
                if (condition.getPage() != null && condition.getRows() != null && condition.getPage() > 0 && condition.getRows() > 0) {
                    statement.setInt(++index, (condition.getPage() - 1) * condition.getRows());
                    statement.setInt(++index, condition.getRows());
                }
            }
            
            ResultSet resultSet = statement.executeQuery();
            ResultSetMapper<TenderDetail> resultSetMapper = new ResultSetMapper<TenderDetail>();
            List<TenderDetail> tenderDetailList = resultSetMapper.mapResultSetToList(resultSet, TenderDetail.class);
            
            return tenderDetailList;
            
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[TenderDao_JDBCImpl#searchByCnd] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[TenderDao_JDBCImpl#searchByCnd] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
    }

    @Override
    public Boolean insertTenderInformation(TenderDetail tenderDetail, String createUser) {
        if (logger.isInfoEnabled()) {
            logger.info("[TenderDao_JDBCImpl#insertTenderInformation] start.");
        }

        PreparedStatement statement = null;
        Boolean result = false;
        
        try {
            
            StringBuilder sql = new StringBuilder()
                .append("insert into t_tender_information ( \n")
                .append("  tender_no, \n")
                .append("  status, \n")
                .append("  tender_title, \n")
                .append("  enroll_start_date, \n")
                .append("  enroll_end_date, \n")
                .append("  tender_end_date, \n")
                .append("  publish_result_date, \n")
                .append("  content_description, \n")
                .append("  vendor_requirement, \n")
                .append("  tender_pattern, \n")
                .append("  tender_template, \n")
                .append("  contact_name, \n")
                .append("  contact_tel, \n")
                .append("  create_user) \n")
                .append("values ( \n")
                .append("  concat(date_format(now(), '%Y%m%d'), get_random(4)), \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?) \n");
            
            if (logger.isTraceEnabled()) {
                logger.trace("[TenderDao_JDBCImpl#insertTenderInformation] sql={}", sql);
            }
            
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            statement.setInt(1, tenderDetail.getStatus());
            statement.setString(2, tenderDetail.getTenderTitle());
            statement.setString(3, tenderDetail.getEnrollStartDate());
            statement.setString(4, tenderDetail.getEnrollEndDate());
            statement.setString(5, tenderDetail.getTenderEndDate());
            statement.setString(6, tenderDetail.getPublishResultDate());
            statement.setString(7, tenderDetail.getContentDescription());
            statement.setString(8, tenderDetail.getVendorRequirement());
            statement.setInt(9, tenderDetail.getTenderPattern());
            statement.setInt(10, 0);
            statement.setString(11, tenderDetail.getContactName());
            statement.setString(12, tenderDetail.getContactTel());
            statement.setString(13, createUser);
            
            int count = statement.executeUpdate();
            
            if (count == 1) {
                result = true;
            }
            
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[TenderDao_JDBCImpl#insertTenderInformation] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[TenderDao_JDBCImpl#insertTenderInformation] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
        return result;
    }

    @Override
    public Boolean insertTenderProduct(TenderProduct tenderProduct, String createUser) {
        if (logger.isInfoEnabled()) {
            logger.info("[TenderDao_JDBCImpl#insertTenderProduct] start.");
        }

        PreparedStatement statement = null;
        Boolean result = false;
        
        try {
            
            StringBuilder sql = new StringBuilder()
                .append("insert into t_tender_product ( \n")
                .append("  tender_id, \n")
                .append("  product_name, \n")
                .append("  quantity, \n")
                .append("  unit, \n")
                .append("  product_description, \n")
                .append("  create_user) \n")
                .append("values ( \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?) \n");
            
            if (logger.isTraceEnabled()) {
                logger.trace("[TenderDao_JDBCImpl#insertTenderProduct] sql={}", sql);
            }
            
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            statement.setInt(1, tenderProduct.getTenderId());
            statement.setString(2, tenderProduct.getProductName());
            statement.setInt(3, tenderProduct.getQuantity());
            statement.setString(4, tenderProduct.getUnit());
            statement.setString(5, tenderProduct.getProductDescription());
            statement.setString(6, createUser);
            
            int count = statement.executeUpdate();
            
            if (count == 1) {
                result = true;
            }
            
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[TenderDao_JDBCImpl#insertTenderProduct] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[TenderDao_JDBCImpl#insertTenderProduct] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
        return result;
    }

    @Override
    public Boolean insertTenderEnroll(TenderEnroll tenderEnroll, String createUser) {
        if (logger.isInfoEnabled()) {
            logger.info("[TenderDao_JDBCImpl#insertTenderEnroll] start.");
        }

        PreparedStatement statement = null;
        Boolean result = false;
        
        try {
            
            StringBuilder sql = new StringBuilder()
                .append("insert into t_tender_enroll ( \n")
                .append("  tender_id, \n")
                .append("  member_login_name, \n")
                .append("  finalist_flag, \n")
                .append("  invite_flag, \n")
                .append("  create_user) \n")
                .append("values ( \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?) \n");
            
            if (logger.isTraceEnabled()) {
                logger.trace("[TenderDao_JDBCImpl#insertTenderEnroll] sql={}", sql);
            }
            
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            statement.setInt(1, tenderEnroll.getTenderId());
            statement.setString(2, tenderEnroll.getMemberLoginName());
            statement.setBoolean(3, tenderEnroll.getFinalistFlag());
            statement.setBoolean(4, tenderEnroll.getInviteFlag());
            statement.setString(5, createUser);
            
            int count = statement.executeUpdate();
            
            if (count == 1) {
                result = true;
            }
            
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[TenderDao_JDBCImpl#insertTenderEnroll] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[TenderDao_JDBCImpl#insertTenderEnroll] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
        return result;
    }

    @Override
    public Boolean insertTenderFinalist(TenderFinalist tenderFinalist, String createUser) {
        if (logger.isInfoEnabled()) {
            logger.info("[TenderDao_JDBCImpl#insertTenderFinalist] start.");
        }

        PreparedStatement statement = null;
        Boolean result = false;
        
        try {
            
            StringBuilder sql = new StringBuilder()
                .append("insert into t_tender_finalist ( \n")
                .append("  tender_id, \n")
                .append("  member_login_name, \n")
                .append("  quote_flag, \n")
                .append("  invite_flag, \n")
                .append("  create_user) \n")
                .append("values ( \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?) \n");
            
            if (logger.isTraceEnabled()) {
                logger.trace("[TenderDao_JDBCImpl#insertTenderFinalist] sql={}", sql);
            }
            
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            statement.setInt(1, tenderFinalist.getTenderId());
            statement.setString(2, tenderFinalist.getMemberLoginName());
            statement.setBoolean(3, tenderFinalist.getQuoteFlag());
            statement.setBoolean(4, tenderFinalist.getInviteFlag());
            statement.setString(5, createUser);
            
            int count = statement.executeUpdate();
            
            if (count == 1) {
                result = true;
            }
            
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[TenderDao_JDBCImpl#insertTenderFinalist] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[TenderDao_JDBCImpl#insertTenderFinalist] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
        return result;
    }

}
