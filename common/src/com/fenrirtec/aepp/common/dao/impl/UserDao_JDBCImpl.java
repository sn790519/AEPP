package com.fenrirtec.aepp.common.dao.impl;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dao.UserDao;
import com.fenrirtec.aepp.common.model.Group;
import com.fenrirtec.aepp.common.model.User;
import com.fenrirtec.aepp.common.model.UserGroup;
import com.fenrirtec.core.database.DatabaseSessionManager;
import com.fenrirtec.core.database.ResultSetMapper;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.utils.DigestUtils;

public class UserDao_JDBCImpl implements UserDao {

	private static final Logger logger = LoggerFactory.getLogger(UserDao_JDBCImpl.class);

	@Override
	public User find(String loginName) {
		if (logger.isInfoEnabled()) {
			logger.info("[UserDao_JDBCImpl#find] start.");
		}

		PreparedStatement statement = null;
		try {
			StringBuilder sql = new StringBuilder()
				.append("select \n")
				.append("  login_name, \n")
				.append("  login_password, \n")
				.append("  name, \n")
                .append("  user_group, \n")
				.append("  create_user, \n")
				.append("  create_date, \n")
				.append("  update_user, \n")
				.append("  update_date, \n")
				.append("  delete_flag \n")
				.append("from \n")
				.append("  v_user \n")
				.append("where \n")
				.append("  login_name = ? \n")
                .append("and \n")
                .append("  delete_flag = false \n");
			if (logger.isTraceEnabled()) {
				logger.trace("[UserDao_JDBCImpl#find] sql={}", sql);
			}
			Connection connection = DatabaseSessionManager.getInstance().getSession();
			statement = connection.prepareStatement(sql.toString());
			statement.setString(1, loginName);
			ResultSet resultSet = statement.executeQuery();
			
			ResultSetMapper<User> resultSetMapper = new ResultSetMapper<User>();
			User user = resultSetMapper.mapResultSetToObject(resultSet, User.class);
			return user;
		} catch (SQLException e) {
			if (logger.isTraceEnabled()) {
				logger.trace("[UserDao_JDBCImpl#find] sql error occurred.", e);
			}
			throw new DatabaseException("database error occurred.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[UserDao_JDBCImpl#find] finish.");
			}
			if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
		}
	}

    @Override
    public List<User> findAll() {
        if (logger.isInfoEnabled()) {
            logger.info("[UserDao_JDBCImpl#findAll] start.");
        }

        PreparedStatement statement = null;
        try {
            StringBuilder sql = new StringBuilder()
                .append("select \n")
                .append("  login_name, \n")
                .append("  login_password, \n")
                .append("  name, \n")
                .append("  user_group, \n")
                .append("  user_group_name, \n")
                .append("  create_user, \n")
                .append("  create_date, \n")
                .append("  update_user, \n")
                .append("  update_date, \n")
                .append("  delete_flag \n")
                .append("from \n")
                .append("  v_user \n")
                .append("where \n")
                .append("  delete_flag = false \n");
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#findAll] sql={}", sql);
            }
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            ResultSet resultSet = statement.executeQuery();
            
            ResultSetMapper<User> resultSetMapper = new ResultSetMapper<User>();
            List<User> userList = resultSetMapper.mapResultSetToList(resultSet, User.class);
            return userList;
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#findAll] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[UserDao_JDBCImpl#findAll] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
    }

    @Override
    public List<Group> findAllGroup() {
        if (logger.isInfoEnabled()) {
            logger.info("[UserDao_JDBCImpl#findAllGroup] start.");
        }

        PreparedStatement statement = null;
        try {
            StringBuilder sql = new StringBuilder()
                .append("select \n")
                .append("  group_name, \n")
                .append("  name, \n")
                .append("  group_user, \n")
                .append("  group_user_name, \n")
                .append("  create_user, \n")
                .append("  create_date, \n")
                .append("  update_user, \n")
                .append("  update_date, \n")
                .append("  delete_flag \n")
                .append("from \n")
                .append("  v_group \n")
                .append("where \n")
                .append("  delete_flag = false \n");
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#findAllGroup] sql={}", sql);
            }
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            ResultSet resultSet = statement.executeQuery();
            
            ResultSetMapper<Group> resultSetMapper = new ResultSetMapper<Group>();
            List<Group> groupList = resultSetMapper.mapResultSetToList(resultSet, Group.class);
            return groupList;
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#findAllGroup] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[UserDao_JDBCImpl#findAllGroup] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
    }

    @Override
    public List<User> find(String keyword, String[] groups) {
        if (logger.isInfoEnabled()) {
            logger.info("[UserDao_JDBCImpl#findAll] start.");
        }

        PreparedStatement statement = null;
        try {
            StringBuilder sql = new StringBuilder()
                .append("select \n")
                .append("  login_name, \n")
                .append("  login_password, \n")
                .append("  name, \n")
                .append("  user_group, \n")
                .append("  user_group_name, \n")
                .append("  create_user, \n")
                .append("  create_date, \n")
                .append("  update_user, \n")
                .append("  update_date, \n")
                .append("  delete_flag \n")
                .append("from \n")
                .append("  v_user \n")
                .append("where \n")
                .append("  delete_flag = false \n");
            
            if (StringUtils.isNotEmpty(keyword)) {
                sql.append("and \n");
                sql.append("  (login_name like ? \n");
                sql.append("or \n");
                sql.append("  name like ?) \n");
            }
            
            if (groups != null && groups.length > 0) {
                sql.append("and \n");
                sql.append("  login_name in ( \n");
                sql.append("    select \n");
                sql.append("      login_name \n");
                sql.append("    from \n");
                sql.append("      v_user_group \n");
                sql.append("    where \n");
                sql.append("      delete_flag = false \n");
                sql.append("    and \n");
                sql.append("      group_name in(");
                for (int i = 0; i < groups.length; i++) {
                    sql.append(i == 0 ? "?" : ",?");
                }
                sql.append(")) \n");
            }
            
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#findAll] sql={}", sql);
            }
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            
            int index = 0;
            if (StringUtils.isNotEmpty(keyword)) {
                statement.setString(++index, "%" + keyword + "%");
                statement.setString(++index, "%" + keyword + "%");
            }
            if (groups != null && groups.length > 0) {
                for (String group : groups) {
                    statement.setString(++index, group);
                }
            }
            
            ResultSet resultSet = statement.executeQuery();
            
            ResultSetMapper<User> resultSetMapper = new ResultSetMapper<User>();
            List<User> userList = resultSetMapper.mapResultSetToList(resultSet, User.class);
            return userList;
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#findAll] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[UserDao_JDBCImpl#findAll] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
    }

    @Override
    public List<Group> findGroup(String keyword) {
        if (logger.isInfoEnabled()) {
            logger.info("[UserDao_JDBCImpl#findGroup] start.");
        }

        PreparedStatement statement = null;
        try {
            StringBuilder sql = new StringBuilder()
                .append("select \n")
                .append("  group_name, \n")
                .append("  name, \n")
                .append("  group_user, \n")
                .append("  group_user_name, \n")
                .append("  create_user, \n")
                .append("  create_date, \n")
                .append("  update_user, \n")
                .append("  update_date, \n")
                .append("  delete_flag \n")
                .append("from \n")
                .append("  v_group \n")
                .append("where \n")
                .append("  delete_flag = false \n");
            
            if (StringUtils.isNotEmpty(keyword)) {
                sql.append("and \n");
                sql.append("  (group_name like ? \n");
                sql.append("or \n");
                sql.append("  name like ?) \n");
            }
            
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#findGroup] sql={}", sql);
            }
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            
            if (StringUtils.isNotEmpty(keyword)) {
                statement.setString(1, "%" + keyword + "%");
                statement.setString(2, "%" + keyword + "%");
            }
            
            ResultSet resultSet = statement.executeQuery();
            
            ResultSetMapper<Group> resultSetMapper = new ResultSetMapper<Group>();
            List<Group> groupList = resultSetMapper.mapResultSetToList(resultSet, Group.class);
            return groupList;
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#findGroup] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[UserDao_JDBCImpl#findGroup] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
    }

    @Override
    public Boolean insert(User user) {
        if (logger.isInfoEnabled()) {
            logger.info("[UserDao_JDBCImpl#insert] start.");
        }

        PreparedStatement statement = null;
        Boolean result = false;
        
        try {
            StringBuilder sql = new StringBuilder()
                .append("insert into t_user ( \n")
                .append("  login_name, \n")
                .append("  login_password, \n")
                .append("  name, \n")
                .append("  create_user) \n")
                .append("values ( \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?) \n");
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#insert] sql={}", sql);
            }
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            statement.setString(1, user.getLoginName());
            statement.setString(2, DigestUtils.SHAHashing(user.getLoginPassword()));
            statement.setString(3, user.getName());
            statement.setString(4, user.getCreateUser());
            
            int count = statement.executeUpdate();
            result = (count == 1);
        } catch (SQLException | NoSuchAlgorithmException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#insert] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[UserDao_JDBCImpl#insert] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
        return result;
    }

    @Override
    public Boolean insert(UserGroup userGroup) {
        if (logger.isInfoEnabled()) {
            logger.info("[UserDao_JDBCImpl#insert] start.");
        }

        PreparedStatement statement = null;
        Boolean result = false;
        
        try {
            StringBuilder sql = new StringBuilder()
                .append("insert into t_user_group ( \n")
                .append("  login_name, \n")
                .append("  group_name, \n")
                .append("  create_user) \n")
                .append("values ( \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?) \n");
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#insert] sql={}", sql);
            }
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            statement.setString(1, userGroup.getLoginName());
            statement.setString(2, userGroup.getGroupName());
            statement.setString(3, userGroup.getCreateUser());
            
            int count = statement.executeUpdate();
            result = (count == 1);
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#insert] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[UserDao_JDBCImpl#insert] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
        return result;
    }

    @Override
    public Boolean update(User user) {
        if (logger.isInfoEnabled()) {
            logger.info("[UserDao_JDBCImpl#update] start.");
        }

        PreparedStatement statement = null;
        Boolean result = false;
        
        try {
            StringBuilder sql = new StringBuilder()
                .append("update \n")
                .append("  t_user \n")
                .append("set \n");
            
            if (StringUtils.isNotEmpty(user.getLoginPassword())) {
                sql.append("  login_password = ?, \n");
            }
            
            sql.append("  name = ?, \n");
            sql.append("  update_user = ?, \n");
            sql.append("  update_date = current_timestamp \n");
            sql.append("where \n");
            sql.append("  login_name = ? \n");
            sql.append("and \n");
            sql.append("  delete_flag = false \n");

            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#update] sql={}", sql);
            }
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            
            int index = 0;
            if (StringUtils.isNotEmpty(user.getLoginPassword())) {
                statement.setString(++index, DigestUtils.SHAHashing(user.getLoginPassword()));
            }
            statement.setString(++index, user.getName());
            statement.setString(++index, user.getUpdateUser());
            statement.setString(++index, user.getLoginName());
            
            int count = statement.executeUpdate();
            result = (count == 1);
        } catch (SQLException | NoSuchAlgorithmException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#update] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[UserDao_JDBCImpl#update] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
        return result;
    }

    @Override
    public void deleteUserGroup(String loginName) {
        if (logger.isInfoEnabled()) {
            logger.info("[UserDao_JDBCImpl#deleteUserGroup] start.");
        }

        PreparedStatement statement = null;
        
        try {
            StringBuilder sql = new StringBuilder()
                .append("delete from t_user_group \n")
                .append("where \n")
                .append("  login_name = ? \n");
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#deleteUserGroup] sql={}", sql);
            }
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            statement.setString(1, loginName);
            statement.executeUpdate();
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#deleteUserGroup] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[UserDao_JDBCImpl#deleteUserGroup] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
    }

    @Override
    public Boolean delete(String loginName) {
        if (logger.isInfoEnabled()) {
            logger.info("[UserDao_JDBCImpl#delete] start.");
        }

        PreparedStatement statement = null;
        
        try {
            StringBuilder sql = new StringBuilder()
                .append("delete from t_user \n")
                .append("where \n")
                .append("  login_name = ? \n");
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#delete] sql={}", sql);
            }
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            statement.setString(1, loginName);
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#delete] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[UserDao_JDBCImpl#delete] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
    }

    @Override
    public Group findGroupByGroupName(String groupName) {
        if (logger.isInfoEnabled()) {
            logger.info("[UserDao_JDBCImpl#findAllGroup] start.");
        }

        PreparedStatement statement = null;
        try {
            StringBuilder sql = new StringBuilder()
                .append("select \n")
                .append("  group_name, \n")
                .append("  name, \n")
                .append("  group_user, \n")
                .append("  group_user_name, \n")
                .append("  create_user, \n")
                .append("  create_date, \n")
                .append("  update_user, \n")
                .append("  update_date, \n")
                .append("  delete_flag \n")
                .append("from \n")
                .append("  v_group \n")
                .append("where \n")
                .append("  delete_flag = false \n")
                .append("and \n")
                .append("  group_name = ? \n");
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#findAllGroup] sql={}", sql);
            }
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            statement.setString(1, groupName);
            ResultSet resultSet = statement.executeQuery();
            
            ResultSetMapper<Group> resultSetMapper = new ResultSetMapper<Group>();
            Group group = resultSetMapper.mapResultSetToObject(resultSet, Group.class);
            return group;
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#findAllGroup] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[UserDao_JDBCImpl#findAllGroup] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
    }

    @Override
    public Boolean insert(Group group) {
        if (logger.isInfoEnabled()) {
            logger.info("[UserDao_JDBCImpl#insert] start.");
        }

        PreparedStatement statement = null;
        Boolean result = false;
        
        try {
            StringBuilder sql = new StringBuilder()
                .append("insert into t_group ( \n")
                .append("  group_name, \n")
                .append("  name, \n")
                .append("  create_user) \n")
                .append("values ( \n")
                .append("  ?, \n")
                .append("  ?, \n")
                .append("  ?) \n");
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#insert] sql={}", sql);
            }
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            statement.setString(1, group.getGroupName());
            statement.setString(2, group.getName());
            statement.setString(3, group.getCreateUser());
            
            int count = statement.executeUpdate();
            result = (count == 1);
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#insert] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[UserDao_JDBCImpl#insert] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
        return result;
    }

    @Override
    public Boolean update(Group group) {
        if (logger.isInfoEnabled()) {
            logger.info("[UserDao_JDBCImpl#update] start.");
        }

        PreparedStatement statement = null;
        Boolean result = false;
        
        try {
            StringBuilder sql = new StringBuilder()
                .append("update \n")
                .append("  t_group \n")
                .append("set \n")
                .append("  name = ?, \n")
                .append("  update_user = ?, \n")
                .append("  update_date = current_timestamp \n")
                .append("where \n")
                .append("  group_name = ? \n")
                .append("and \n")
                .append("  delete_flag = false \n");

            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#update] sql={}", sql);
            }
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            statement.setString(1, group.getName());
            statement.setString(2, group.getUpdateUser());
            statement.setString(3, group.getGroupName());
            
            int count = statement.executeUpdate();
            result = (count == 1);
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#update] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[UserDao_JDBCImpl#update] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
        return result;
    }

    @Override
    public Boolean deleteGroup(String groupName) {
        if (logger.isInfoEnabled()) {
            logger.info("[UserDao_JDBCImpl#delete] start.");
        }

        PreparedStatement statement = null;
        
        try {
            StringBuilder sql = new StringBuilder()
                .append("delete from t_group \n")
                .append("where \n")
                .append("  group_name = ? \n");
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#delete] sql={}", sql);
            }
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            statement.setString(1, groupName);
            int count = statement.executeUpdate();
            return count == 1;
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#delete] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[UserDao_JDBCImpl#delete] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
    }

    @Override
    public void deleteGroupUser(String groupName) {
        if (logger.isInfoEnabled()) {
            logger.info("[UserDao_JDBCImpl#deleteGroupUser] start.");
        }

        PreparedStatement statement = null;
        
        try {
            StringBuilder sql = new StringBuilder()
                .append("delete from t_user_group \n")
                .append("where \n")
                .append("  group_name = ? \n");
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#deleteGroupUser] sql={}", sql);
            }
            Connection connection = DatabaseSessionManager.getInstance().getSession();
            statement = connection.prepareStatement(sql.toString());
            statement.setString(1, groupName);
            statement.executeUpdate();
        } catch (SQLException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[UserDao_JDBCImpl#deleteGroupUser] sql error occurred.", e);
            }
            throw new DatabaseException("database error occurred.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[UserDao_JDBCImpl#deleteGroupUser] finish.");
            }
            if (statement != null) { try { statement.close(); } catch (SQLException e) {} }
        }
    }
}
