package com.fenrirtec.aepp.common.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dao.TenderManageDao;
import com.fenrirtec.aepp.common.model.TenderManage;
import com.fenrirtec.core.database.DatabaseSessionManager;
import com.fenrirtec.core.database.ResultSetMapper;
import com.fenrirtec.core.exception.DatabaseException;

public class TenderManageDao_JDBCImpl implements TenderManageDao {
	private static final Logger logger = LoggerFactory
			.getLogger(TenderManageDao_JDBCImpl.class);

	@Override
	public List<TenderManage> findTenderManage(Integer tenderid) {
		if(logger.isInfoEnabled()){
			logger.info("[TenderManageDao_JDBCImpl#findTenderManage] start.");
		}
		PreparedStatement statement=null;
		try{
			StringBuffer sql=new StringBuffer()
			.append("select \n")
			.append("m.enterprise_name, \n")
			.append("m.enterprise_category_name, \n")	
			.append("m.area_code, \n")
			.append("m.rank_name, \n")			
			.append("(SELECT path FROM t_tender_enroll_file WHERE tender_id = t.tender_id ) AS path, \n")
			.append("t.finalist_flag, \n")
			.append("t.member_login_name, \n")
			.append("t.tender_id \n")		
			.append("from \n")
			.append(" v_member m \n")
			.append(" ,t_tender_enroll t \n")			
			.append(" where \n")
			.append(" m.member_login_name=t.member_login_name \n")
			.append(" and t.tender_id= ? \n")
			.append(" and finalist_flag= false \n");
			if (logger.isTraceEnabled()) {
				logger.trace("[TenderManageDao_JDBCImpl#findTenderManage] sql={}", sql);
			}
			
			Connection connection = DatabaseSessionManager.getInstance().getSession();
			statement = connection.prepareStatement(sql.toString());
			statement.setInt(1, tenderid);
			//statement.setInt(2, tenderid);
			ResultSet resultSet = statement.executeQuery();
			ResultSetMapper<TenderManage> resultSetMapper=new ResultSetMapper<TenderManage>();
			List<TenderManage> tenderMangageList=resultSetMapper.mapResultSetToList(resultSet, TenderManage.class);
			return tenderMangageList;
		}catch(SQLException e){
			if (logger.isTraceEnabled()) {
				logger.trace("[TenderManageDao_JDBCImpl#findTenderManage] sql error occurred.", e);
			}
			throw new DatabaseException("database error occurred.", e);
		}finally{
			if (logger.isInfoEnabled()) {
				logger.info("[TenderManageDao_JDBCImpl#findTenderManage] finish.");
			}
			if (statement != null) { 
				try { 
					statement.close(); 
					} catch (SQLException e) {} 
				}
		}
		
	}

}
