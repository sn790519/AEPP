package com.fenrirtec.aepp.common.dao.impl;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dao.FileDao;

public class FileDao_FileImpl implements FileDao {
	
	private static final Logger logger = LoggerFactory.getLogger(FileDao_FileImpl.class);

	@Override
	public void moveFile(String srcFile, String destFile) {
		if (logger.isInfoEnabled()) {
			logger.info("[FileDao_FileImpl#moveFile] start.");
		}
		
		try {
			File src = new File(srcFile);
			File dest = new File(destFile);
			if (src.exists()) {
				if (dest.exists()) {
					FileUtils.forceDelete(dest);
				}
				FileUtils.moveFile(src, dest);
			}
		} catch (IOException e) {
			if (logger.isTraceEnabled()) {
				logger.trace("[FileDao_FileImpl#moveFile] runtime error occurred.", e);
			}
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[FileDao_FileImpl#moveFile] finish.");
			}
		}
	}@Override
    public void copyFile(String srcFile, String destFile) {
        if (logger.isInfoEnabled()) {
            logger.info("[FileDao_FileImpl#copyFile] start.");
        }
        
        try {
            File src = new File(srcFile);
            File dest = new File(destFile);
            if (src.exists()) {
                if (dest.exists()) {
                    FileUtils.forceDelete(dest);
                }
                FileUtils.copyFile(src, dest);
            }
        } catch (IOException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("[FileDao_FileImpl#copyFile] runtime error occurred.", e);
            }
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[FileDao_FileImpl#copyFile] finish.");
            }
        }
    }

	@Override
	public void deleteFile(String path) {
		if (logger.isInfoEnabled()) {
			logger.info("[FileDao_FileImpl#deleteAttachment] start.");
		}
		
		try {
			File file = new File(path);
			if (file.exists()) {
				FileUtils.forceDelete(file);
			}
		} catch (IOException e) {
			if (logger.isTraceEnabled()) {
				logger.trace("[FileDao_FileImpl#deleteAttachment] runtime error occurred.", e);
			}
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[FileDao_FileImpl#deleteAttachment] finish.");
			}
		}
	}

	@Override
	public void renameFolder(String oldPath, String newPath) {
		if (logger.isInfoEnabled()) {
			logger.info("[FileDao_FileImpl#renameFolder] start.");
		}
		
		try {
			File srcFolder = new File(oldPath);
			if (srcFolder.exists() && srcFolder.isDirectory()) {
				File destFolder = new File(newPath);
				FileUtils.moveDirectory(srcFolder, destFolder);
			}
		} catch (IOException e) {
			if (logger.isTraceEnabled()) {
				logger.trace("[FileDao_FileImpl#renameFolder] runtime error occurred.", e);
			}
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[FileDao_FileImpl#renameFolder] finish.");
			}
		}
	}

	@Override
	public void deleteFolder(String path) {
		if (logger.isInfoEnabled()) {
			logger.info("[FileDao_FileImpl#deleteFolder] start.");
		}
		
		try {
			File folder = new File(path);
			if (folder.exists()) {
				FileUtils.forceDelete(folder);
			}
		} catch (IOException e) {
			if (logger.isTraceEnabled()) {
				logger.trace("[FileDao_FileImpl#deleteFolder] runtime error occurred.", e);
			}
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[FileDao_FileImpl#deleteFolder] finish.");
			}
		}
	}

}
