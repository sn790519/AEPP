package com.fenrirtec.aepp.common.dao;

import java.util.List;

import com.fenrirtec.aepp.common.condition.TenderCondition;
import com.fenrirtec.aepp.common.model.TenderDetail;
import com.fenrirtec.aepp.common.model.TenderEnroll;
import com.fenrirtec.aepp.common.model.TenderFinalist;
import com.fenrirtec.aepp.common.model.TenderProduct;

public interface TenderDao {
	Integer countByCnd(TenderCondition condition);
    List<TenderDetail> searchByCnd(TenderCondition condition);
    Boolean insertTenderInformation(TenderDetail tenderDetail, String createUser);
    Boolean insertTenderProduct(TenderProduct tenderProduct, String createUser);
    Boolean insertTenderEnroll(TenderEnroll tenderEnroll, String createUser);
    Boolean insertTenderFinalist(TenderFinalist tenderFinalist, String createUser);
}
