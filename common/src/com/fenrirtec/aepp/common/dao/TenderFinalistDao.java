package com.fenrirtec.aepp.common.dao;

import java.util.List;

import com.fenrirtec.aepp.common.dto.TenderFinalistDto;
import com.fenrirtec.aepp.common.model.TenderEnroll;


public interface TenderFinalistDao {
	void insertTenderFinalist(List<TenderFinalistDto> tenderFinaList);
	//审核通过时更新招标报名表的入围标识为true
    void updateTenderFinalist(Integer tenderid);
    //审核不通过时更新招标报名表的delet_flag为true,并且更新update_user
    void updateTenderFinalistflag(TenderEnroll tenderEnroll);
}