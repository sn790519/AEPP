package com.fenrirtec.aepp.common.dao;

import java.util.List;

import com.fenrirtec.aepp.common.model.AdvImage;

public interface AdvImageDao {
	List<AdvImage> find(Boolean useFlag);
    Boolean insert(AdvImage advImage);
    Boolean update(Integer imageId, Boolean useFlag, String updateUser);
    Boolean delete(Integer imageId);
}
