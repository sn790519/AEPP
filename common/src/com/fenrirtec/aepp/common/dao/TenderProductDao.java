package com.fenrirtec.aepp.common.dao;

import java.util.List;

import com.fenrirtec.aepp.common.dto.TenderProductDto;

public interface TenderProductDao {
	void insertTenderProduct(List<TenderProductDto> tenderproductdtoList,int maxId);
}