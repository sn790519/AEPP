package com.fenrirtec.aepp.common.dao;

import java.util.List;

import com.fenrirtec.aepp.common.condition.MemberCondition;
import com.fenrirtec.aepp.common.model.AuditFlow;
import com.fenrirtec.aepp.common.model.Member;

public interface MemberManageDao {
	Boolean regist(Member member);
	Boolean exists(String memberLoginName);
	List<Member> search(MemberCondition condition);
	Integer count(MemberCondition condition);
	void removeImage(String memberLoginName, Integer attachmentCategory, Integer attachmentId);
	Boolean initAuditFlow(String memberLoginName);
	Boolean audit(String memberLoginName, Integer auditResult, String auditComment, String loginName);
	Member info(String memberLoginName);
	Boolean edit(Member member);
	List<AuditFlow> auditFlowSearch(String memberLoginName);
	void updateAuditResult(AuditFlow auditFlow);
	void insertAuditFlow(AuditFlow auditFlow);
}
