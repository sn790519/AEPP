package com.fenrirtec.aepp.common.dao;

import java.util.List;

import com.fenrirtec.aepp.common.model.TenderManage;


public interface TenderManageDao {
	List<TenderManage> findTenderManage(Integer tenderid);
}