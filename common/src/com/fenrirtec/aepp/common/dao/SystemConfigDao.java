package com.fenrirtec.aepp.common.dao;

import com.fenrirtec.aepp.common.model.SystemConfig;

public interface SystemConfigDao {
	SystemConfig load();
	Boolean changePassword(String loginName, String password);
	Boolean updateSystemConfig(SystemConfig systemConfig);
}
