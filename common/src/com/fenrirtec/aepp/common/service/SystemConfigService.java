package com.fenrirtec.aepp.common.service;

import com.fenrirtec.aepp.common.dto.SystemConfigDto;

public interface SystemConfigService {
	SystemConfigDto loadSystemConfig();
	Boolean changePassword(String loginName, String password);
	Boolean updateSystemConfig(SystemConfigDto systemConfigDto);
}
