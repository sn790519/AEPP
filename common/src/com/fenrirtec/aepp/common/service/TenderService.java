package com.fenrirtec.aepp.common.service;

import java.util.List;

import com.fenrirtec.aepp.common.condition.TenderCondition;
import com.fenrirtec.aepp.common.dto.TenderDetailDto;
import com.fenrirtec.aepp.common.dto.TenderProductDto;
import com.fenrirtec.aepp.common.dto.TenderVenderDto;

public interface TenderService {
    Integer countByCnd(TenderCondition condition);
    List<TenderDetailDto> searchByCnd(TenderCondition condition);
    Boolean publish(TenderDetailDto tenderDetailDto, List<TenderProductDto> tenderProductDtoList, List<TenderVenderDto> tenderVenderDtoList, String createUser);
}
