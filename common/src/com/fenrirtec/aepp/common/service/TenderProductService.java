package com.fenrirtec.aepp.common.service;

import java.util.List;

import com.fenrirtec.aepp.common.dto.TenderProductDto;

public interface TenderProductService {
	void insertTenderProduct(List<TenderProductDto> tenderproductdtoList,int maxId);
}
