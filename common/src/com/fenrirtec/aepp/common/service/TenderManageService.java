package com.fenrirtec.aepp.common.service;

import java.util.List;
import com.fenrirtec.aepp.common.dto.TenderManageDto;


public interface TenderManageService {
	List<TenderManageDto> findTenderManage(Integer tenderid);
}