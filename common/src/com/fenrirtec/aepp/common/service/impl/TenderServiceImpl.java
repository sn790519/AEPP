package com.fenrirtec.aepp.common.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.condition.TenderCondition;
import com.fenrirtec.aepp.common.dao.TenderDao;
import com.fenrirtec.aepp.common.dao.impl.TenderDao_JDBCImpl;
import com.fenrirtec.aepp.common.dto.TenderDetailDto;
import com.fenrirtec.aepp.common.dto.TenderProductDto;
import com.fenrirtec.aepp.common.dto.TenderVenderDto;
import com.fenrirtec.aepp.common.model.TenderDetail;
import com.fenrirtec.aepp.common.model.TenderEnroll;
import com.fenrirtec.aepp.common.model.TenderFinalist;
import com.fenrirtec.aepp.common.model.TenderProduct;
import com.fenrirtec.aepp.common.service.TenderService;
import com.fenrirtec.core.database.DatabaseSessionManager;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.BeanCopierUtils;
import com.fenrirtec.core.utils.DatabaseUtils;

public class TenderServiceImpl implements TenderService {

	private static final Logger logger = LoggerFactory.getLogger(TenderServiceImpl.class);

	@Override
	public Integer countByCnd(TenderCondition condition) {
		if (logger.isInfoEnabled()) {
			logger.info("[TenderServiceImpl#countByCnd] start.");
		}

		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();

		try {
			connection.setAutoCommit(false);
			TenderDao tenderDao = new TenderDao_JDBCImpl();
			Integer count = tenderDao.countByCnd(condition);
			connection.commit();
			return count;
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
                logger.error("[TenderServiceImpl#countByCnd] database error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
                logger.error("[TenderServiceImpl#countByCnd] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[TenderServiceImpl#countByCnd] finish.");
			}
			sessionManager.close();
		}
	}

    @Override
    public List<TenderDetailDto> searchByCnd(TenderCondition condition) {
        if (logger.isInfoEnabled()) {
            logger.info("[TenderServiceImpl#searchByCnd] start.");
        }

        DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
        sessionManager.open();
        Connection connection = sessionManager.getSession();
        List<TenderDetail> tenderDetailList = null;

        try {
            connection.setAutoCommit(false);
            TenderDao tenderDao = new TenderDao_JDBCImpl();
            tenderDetailList = tenderDao.searchByCnd(condition);
            connection.commit();
        } catch (DatabaseException e) {
            // expected
            if (logger.isErrorEnabled()) {
                logger.error("[TenderServiceImpl#searchByCnd] database error occurred.", e);
            }
            try {
                connection.rollback();
            } catch (SQLException e1) {
            }
            throw e;
        } catch (Exception e) {
            // unexpected
            if (logger.isErrorEnabled()) {
                logger.error("[TenderServiceImpl#searchByCnd] unexpected error occurred.", e);
            }
            try {
                connection.rollback();
            } catch (SQLException e1) {
            }
            throw new UnexpectedException("unexpected error.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[TenderServiceImpl#searchByCnd] finish.");
            }
            sessionManager.close();
        }
        
        List<TenderDetailDto> tenderDetailDtoList = null;
        
        if (tenderDetailList != null && tenderDetailList.size() > 0) {
            tenderDetailDtoList = new ArrayList<TenderDetailDto>();
            for (TenderDetail tenderDetail : tenderDetailList) {
                TenderDetailDto tenderDetailDto = new TenderDetailDto();
                BeanCopierUtils.copyProperties(tenderDetail, tenderDetailDto);
                tenderDetailDtoList.add(tenderDetailDto);
            }
        }
        
        return tenderDetailDtoList;
    }

    @Override
    public Boolean publish(TenderDetailDto tenderDetailDto, List<TenderProductDto> tenderProductDtoList, List<TenderVenderDto> tenderVenderDtoList, String createUser) {
        if (logger.isInfoEnabled()) {
            logger.info("[TenderServiceImpl#publish] start.");
        }

        DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
        sessionManager.open();
        Connection connection = sessionManager.getSession();
        Boolean result = false;
        
        try {
            connection.setAutoCommit(false);
            TenderDao tenderDao = new TenderDao_JDBCImpl();
            TenderDetail tenderDetail = new TenderDetail();
            BeanCopierUtils.copyProperties(tenderDetailDto, tenderDetail);
            result = tenderDao.insertTenderInformation(tenderDetail, createUser);
            if (result) {
                Integer tenderId = DatabaseUtils.getLastInsertId(connection);
                if (tenderId > 0) {
                    for (TenderProductDto tenderProductDto : tenderProductDtoList) {
                        TenderProduct tenderProduct = new TenderProduct();
                        BeanCopierUtils.copyProperties(tenderProductDto, tenderProduct);
                        tenderProduct.setTenderId(tenderId);
                        result = tenderDao.insertTenderProduct(tenderProduct, createUser);
                        if (!result) {
                            break;
                        }
                    }
                    
                    if (result) {
                        if (tenderDetailDto.getTenderPattern() == 2 && tenderVenderDtoList != null && !tenderVenderDtoList.isEmpty()) {
                            for (TenderVenderDto tenderVenderDto : tenderVenderDtoList) {
                                TenderEnroll tenderEnroll = new TenderEnroll();
                                tenderEnroll.setTenderId(tenderId);
                                tenderEnroll.setMemberLoginName(tenderVenderDto.getMemberLoginName());
                                tenderEnroll.setFinalistFlag(true);
                                tenderEnroll.setInviteFlag(true);
                                result = tenderDao.insertTenderEnroll(tenderEnroll, createUser);
                                if (!result) {
                                    break;
                                }
                                TenderFinalist tenderFinalist = new TenderFinalist();
                                tenderFinalist.setTenderId(tenderId);
                                tenderFinalist.setMemberLoginName(tenderVenderDto.getMemberLoginName());
                                tenderFinalist.setQuoteFlag(false);
                                tenderFinalist.setInviteFlag(true);
                                result = tenderDao.insertTenderFinalist(tenderFinalist, createUser);
                                if (!result) {
                                    break;
                                }
                            }
                            if (!result) {
                                connection.rollback();
                            }
                        }
                    } else {
                        connection.rollback();
                    }
                } else {
                    result = false;
                    connection.rollback();
                }
            } else {
                connection.rollback();
            }
            
            connection.commit();
        } catch (DatabaseException e) {
            // expected
            if (logger.isErrorEnabled()) {
                logger.error("[TenderServiceImpl#publish] database error occurred.", e);
            }
            try {
                connection.rollback();
            } catch (SQLException e1) {
            }
            throw e;
        } catch (Exception e) {
            // unexpected
            if (logger.isErrorEnabled()) {
                logger.error("[TenderServiceImpl#publish] unexpected error occurred.", e);
            }
            try {
                connection.rollback();
            } catch (SQLException e1) {
            }
            throw new UnexpectedException("unexpected error.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[TenderServiceImpl#publish] finish.");
            }
            sessionManager.close();
        }
        return result;
    }
}
