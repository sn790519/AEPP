package com.fenrirtec.aepp.common.service.impl;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dao.SystemConfigDao;
import com.fenrirtec.aepp.common.dao.impl.SystemConfigDao_JDBCImpl;
import com.fenrirtec.aepp.common.dto.SystemConfigDto;
import com.fenrirtec.aepp.common.model.SystemConfig;
import com.fenrirtec.aepp.common.service.SystemConfigService;
import com.fenrirtec.core.database.DatabaseSessionManager;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.BeanCopierUtils;

public class SystemConfigServiceImpl implements SystemConfigService {
	
	private static final Logger logger = LoggerFactory.getLogger(SystemConfigServiceImpl.class);

	@Override
	public SystemConfigDto loadSystemConfig() {
		
		if (logger.isInfoEnabled()) {
			logger.info("[SystemConfigServiceImpl#loadSystemConfig] start.");
		}
		
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		SystemConfig systemConfig = null;
		
		try {
			connection.setAutoCommit(false);
			SystemConfigDao systemConfigDao = new SystemConfigDao_JDBCImpl();
			systemConfig = systemConfigDao.load();
			connection.commit();
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[SystemConfigServiceImpl#loadSystemConfig] database error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[SystemConfigServiceImpl#loadSystemConfig] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[SystemConfigServiceImpl#loadSystemConfig] finish.");
			}
			sessionManager.close();
		}
		
		SystemConfigDto systemConfigDto = null;
		
		if (systemConfig != null) {
			systemConfigDto = new SystemConfigDto();
//			ConvertUtils.BeanCopy(systemConfig, systemConfigDto);
			BeanCopierUtils.copyProperties(systemConfig, systemConfigDto);
		}
		
		return systemConfigDto;
	}

	@Override
	public Boolean changePassword(String loginName, String password) {
		
		if (logger.isInfoEnabled()) {
			logger.info("[SystemConfigServiceImpl#changePassword] start.");
		}
		
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		Boolean result = false;
		
		try {
			connection.setAutoCommit(false);
			SystemConfigDao systemConfigDao = new SystemConfigDao_JDBCImpl();
			result = systemConfigDao.changePassword(loginName, password);
			connection.commit();
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[SystemConfigServiceImpl#changePassword] database error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[SystemConfigServiceImpl#changePassword] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[SystemConfigServiceImpl#changePassword] finish.");
			}
			sessionManager.close();
		}
		
		return result;
	}

	@Override
	public Boolean updateSystemConfig(SystemConfigDto systemConfigDto) {
		if (logger.isInfoEnabled()) {
			logger.info("[SystemConfigServiceImpl#updateSystemConfig] start.");
		}
		
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		Boolean result = false;
		
		try {
			connection.setAutoCommit(false);
			SystemConfigDao systemConfigDao = new SystemConfigDao_JDBCImpl();
			SystemConfig systemConfig = new SystemConfig();
			BeanCopierUtils.copyProperties(systemConfigDto, systemConfig);
			result = systemConfigDao.updateSystemConfig(systemConfig);
			connection.commit();
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[SystemConfigServiceImpl#updateSystemConfig] database error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[SystemConfigServiceImpl#updateSystemConfig] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[SystemConfigServiceImpl#updateSystemConfig] finish.");
			}
			sessionManager.close();
		}
		
		return result;
	}
	
}
