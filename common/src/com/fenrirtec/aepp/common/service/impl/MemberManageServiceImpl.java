package com.fenrirtec.aepp.common.service.impl;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.condition.MemberCondition;
import com.fenrirtec.aepp.common.dao.MemberManageDao;
import com.fenrirtec.aepp.common.dao.impl.MemberManageDao_JDBCImpl;
import com.fenrirtec.aepp.common.dto.AuditFlowDto;
import com.fenrirtec.aepp.common.dto.MemberDto;
import com.fenrirtec.aepp.common.model.AuditFlow;
import com.fenrirtec.aepp.common.model.Member;
import com.fenrirtec.aepp.common.service.MemberManageService;
import com.fenrirtec.core.database.DatabaseSessionManager;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.BeanCopierUtils;
import com.fenrirtec.core.utils.DigestUtils;

public class MemberManageServiceImpl implements MemberManageService {
	
	private static final Logger logger = LoggerFactory.getLogger(MemberManageServiceImpl.class);

	@Override
	public Boolean memberRegist(MemberDto memberDto) {
		
		if (logger.isInfoEnabled()) {
			logger.info("[MemberManageServiceImpl#memberRegist] start.");
		}
		
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		Boolean result = false;
		
		try {
			connection.setAutoCommit(false);
			MemberManageDao memberManageDao = new MemberManageDao_JDBCImpl();
			
			Member member = new Member();
			BeanCopierUtils.copyProperties(memberDto, member);
			result = memberManageDao.regist(member);
			if (result) {
				result = memberManageDao.initAuditFlow(memberDto.getMemberLoginName());
			}
			connection.commit();
			
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#memberRegist] database error occurred.", e);	
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#memberRegist] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[MemberManageServiceImpl#memberRegist] finish.");
			}
			sessionManager.close();
		}
		
		return result;
	}

	@Override
	public Boolean memberExists(String memberLoginName) {
		if (logger.isInfoEnabled()) {
			logger.info("[MemberManageServiceImpl#memberExists] start.");
		}
		
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		Boolean result = false;
		
		try {
			connection.setAutoCommit(false);
			MemberManageDao memberDao = new MemberManageDao_JDBCImpl();
			result = memberDao.exists(memberLoginName);
			connection.commit();
			
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#memberExists] database error occurred.", e);	
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#memberExists] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[MemberManageServiceImpl#memberExists] finish.");
			}
			sessionManager.close();
		}
		
		return result;
	}

	@Override
	public List<MemberDto> memberSearch(MemberCondition condition) {
		if (logger.isInfoEnabled()) {
			logger.info("[MemberManageServiceImpl#memberSearch] start.");
		}
		
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		List<Member> members = null;
		
		try {
			connection.setAutoCommit(false);
			MemberManageDao memberDao = new MemberManageDao_JDBCImpl();
			members = memberDao.search(condition);
			connection.commit();
			
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#memberSearch] database error occurred.", e);	
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#memberSearch] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[MemberManageServiceImpl#memberSearch] finish.");
			}
			sessionManager.close();
		}
		
		List<MemberDto> memberDtos = null;
		
		if (members != null && members.size() > 0) {
			memberDtos = new ArrayList<MemberDto>();
			for (Member member : members) {
				MemberDto memberDto = new MemberDto();
				BeanCopierUtils.copyProperties(member, memberDto);
				memberDtos.add(memberDto);
			}
		}
		
		return memberDtos;
	}

	@Override
	public MemberDto memberInfo(String memberLoginName) {
		if (logger.isInfoEnabled()) {
			logger.info("[MemberManageServiceImpl#memberInfo] start.");
		}
		
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		Member member = null;
		
		try {
			connection.setAutoCommit(false);
			MemberManageDao memberDao = new MemberManageDao_JDBCImpl();
			member = memberDao.info(memberLoginName);
			connection.commit();
			
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#memberInfo] database error occurred.", e);	
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#memberInfo] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[MemberManageServiceImpl#memberInfo] finish.");
			}
			sessionManager.close();
		}
		
		MemberDto memberDto = null;
		
		if (member != null) {
			memberDto = new MemberDto();
			BeanCopierUtils.copyProperties(member, memberDto);
		}
		
		return memberDto;
	}

	@Override
	public Integer memberCount(MemberCondition condition) {
		if (logger.isInfoEnabled()) {
			logger.info("[MemberManageServiceImpl#memberCount] start.");
		}
		
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		Integer memberCount = 0;
		
		try {
			connection.setAutoCommit(false);
			MemberManageDao memberDao = new MemberManageDao_JDBCImpl();
			memberCount = memberDao.count(condition);
			connection.commit();
			
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#memberCount] database error occurred.", e);	
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#memberCount] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[MemberManageServiceImpl#memberCount] finish.");
			}
			sessionManager.close();
		}
		
		return memberCount;
	}

	@Override
	public void removeImage(String memberLoginName, Integer attachmentCategory, Integer attachmentId) {
		if (logger.isInfoEnabled()) {
			logger.info("[MemberManageServiceImpl#removeImage] start.");
		}
		
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		
		try {
			connection.setAutoCommit(false);
			MemberManageDao memberDao = new MemberManageDao_JDBCImpl();
			memberDao.removeImage(memberLoginName, attachmentCategory, attachmentId);
			connection.commit();
			
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#removeImage] database error occurred.", e);	
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#removeImage] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[MemberManageServiceImpl#removeImage] finish.");
			}
			sessionManager.close();
		}
	}
	
	@Override
	public MemberDto login(String memberLoginName, String memberLoginPassword) {
		if (logger.isInfoEnabled()) {
			logger.info("[MemberManageServiceImpl#login] start.");
		}
		
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();

		List<Member> memberList = null;
		
		try {
			connection.setAutoCommit(false);
			MemberManageDao memberDao = new MemberManageDao_JDBCImpl();
			MemberCondition condition = new MemberCondition();
			condition.setMemberLoginName(memberLoginName);
			memberList = memberDao.search(condition);
			connection.commit();
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#login] database error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#login] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[MemberManageServiceImpl#login] finish.");
			}
			sessionManager.close();
		}

		MemberDto memberDto = null;
		
		try {
			if (memberList != null && memberList.size() > 0) {
				Member member = memberList.get(0);
				if (DigestUtils.SHAHashing(memberLoginPassword).equals(member.getMemberLoginPassword())) {
					memberDto = new MemberDto();
					BeanCopierUtils.copyProperties(member, memberDto);
				}
			}
		} catch (NoSuchAlgorithmException e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#login] unexpected error occurred.", e);
			}
			throw new UnexpectedException("unexpected error.", e);
		}
		return memberDto;
	}

	@Override
	public Boolean memberAudit(String memberLoginName, Integer auditResult, String auditComment, String loginName) {
		if (logger.isInfoEnabled()) {
			logger.info("[MemberManageServiceImpl#memberAudit] start.");
		}
		
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		Boolean result = false;
		
		try {
			connection.setAutoCommit(false);
			MemberManageDao memberDao = new MemberManageDao_JDBCImpl();
			result = memberDao.audit(memberLoginName, auditResult, auditComment, loginName);
			connection.commit();
			
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#memberAudit] database error occurred.", e);	
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#memberAudit] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[MemberManageServiceImpl#memberAudit] finish.");
			}
			sessionManager.close();
		}
		
		return result;
	}
	
	@Override
	public Boolean memberEdit(MemberDto memberDto) {
		
		if (logger.isInfoEnabled()) {
			logger.info("[MemberManageServiceImpl#memberEdit] start.");
		}
		
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		Boolean result = false;
		
		try {
			connection.setAutoCommit(false);
			MemberManageDao memberManageDao = new MemberManageDao_JDBCImpl();
			
			Member member = new Member();
			BeanCopierUtils.copyProperties(memberDto, member);
			result = memberManageDao.edit(member);
			if (result) {
				result = memberManageDao.initAuditFlow(memberDto.getMemberLoginName());
			}
			connection.commit();
			
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#memberEdit] database error occurred.", e);	
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#memberEdit] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[MemberManageServiceImpl#memberEdit] finish.");
			}
			sessionManager.close();
		}
		
		return result;
	}

	@Override
	public List<AuditFlowDto> auditFlowSearch(String memberLoginName) {
		if (logger.isInfoEnabled()) {
			logger.info("[MemberManageServiceImpl#auditFlowSearch] start.");
		}
		
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();

		List<AuditFlow> auditFlowList = null;
		
		try {
			connection.setAutoCommit(false);
			MemberManageDao memberDao = new MemberManageDao_JDBCImpl();
			auditFlowList = memberDao.auditFlowSearch(memberLoginName);
			connection.commit();
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#auditFlowSearch] database error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#auditFlowSearch] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[MemberManageServiceImpl#auditFlowSearch] finish.");
			}
			sessionManager.close();
		}

		List<AuditFlowDto> auditFlowDtos = null;
		if (auditFlowList != null && auditFlowList.size() > 0) {
			auditFlowDtos = new ArrayList<AuditFlowDto>();
			for (AuditFlow auditFlow : auditFlowList) {
				AuditFlowDto auditFlowDto = new AuditFlowDto();
				BeanCopierUtils.copyProperties(auditFlow, auditFlowDto);
				auditFlowDtos.add(auditFlowDto);
			}
		}
		return auditFlowDtos;
	}

	@Override
	public void updateAuditResult(AuditFlowDto auditFlowDto) {
		if (logger.isInfoEnabled()) {
			logger.info("[MemberManageServiceImpl#updateAuditResult] start.");
		}
		
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();

		try {
			connection.setAutoCommit(false);
			MemberManageDao memberDao = new MemberManageDao_JDBCImpl();
			AuditFlow auditFlow = new AuditFlow();
			BeanCopierUtils.copyProperties(auditFlowDto, auditFlow);
			memberDao.updateAuditResult(auditFlow);
			connection.commit();
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#updateAuditResult] database error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#updateAuditResult] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[MemberManageServiceImpl#updateAuditResult] finish.");
			}
			sessionManager.close();
		}
	}

	@Override
	public void insertAuditFlow(AuditFlowDto auditFlowDto) {
		if (logger.isInfoEnabled()) {
			logger.info("[MemberManageServiceImpl#insertAuditFlow] start.");
		}
		
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();

		try {
			connection.setAutoCommit(false);
			MemberManageDao memberDao = new MemberManageDao_JDBCImpl();
			AuditFlow auditFlow = new AuditFlow();
			BeanCopierUtils.copyProperties(auditFlowDto, auditFlow);
			memberDao.insertAuditFlow(auditFlow);
			connection.commit();
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#insertAuditFlow] database error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[MemberManageServiceImpl#insertAuditFlow] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[MemberManageServiceImpl#insertAuditFlow] finish.");
			}
			sessionManager.close();
		}
	}
	
}
