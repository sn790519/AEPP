package com.fenrirtec.aepp.common.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dao.TenderFinalistDao;
import com.fenrirtec.aepp.common.dao.impl.TenderFinalistDao_JDBCImpl;
import com.fenrirtec.aepp.common.dto.TenderEnrollDto;
import com.fenrirtec.aepp.common.dto.TenderFinalistDto;
import com.fenrirtec.aepp.common.model.TenderEnroll;
import com.fenrirtec.aepp.common.service.TenderFinalistService;
import com.fenrirtec.core.database.DatabaseSessionManager;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.BeanCopierUtils;

public class TenderFinalistServiceImpl implements TenderFinalistService {
	private static final Logger logger = LoggerFactory
			.getLogger(TenderFinalistServiceImpl.class);
	@Override
	public void insertTenderFinalist(List<TenderFinalistDto> tenderFinaList) {
		if (logger.isInfoEnabled()) {
			logger.info("[TenderFinalistServiceImpl#insertTenderFinalist] start.");
		}

		DatabaseSessionManager sessionManager = DatabaseSessionManager
				.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		try{
			connection.setAutoCommit(false);
			TenderFinalistDao tenderFinalistDao=new TenderFinalistDao_JDBCImpl();
			tenderFinalistDao.insertTenderFinalist(tenderFinaList);
			connection.commit();
		}catch(DatabaseException e){
			//expected
			if (logger.isErrorEnabled()) {
				logger.error(
						"[TenderFinalistServiceImpl#insertTenderFinalist] database error occurred.",
						e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		}catch(Exception e){
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error(
						"[TenderFinalistServiceImpl#insertTenderFinalist] unexpected error occurred.",
						e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		}finally {
			if (logger.isInfoEnabled()) {
				logger.info("[TenderFinalistServiceImpl#insertTenderFinalist] finish.");
			}
			sessionManager.close();
		}

	}
	@Override
	public void updateTenderFinalist(Integer tenderid) {		
		if (logger.isInfoEnabled()) {
			logger.info("[TenderFinalistServiceImpl#updateTenderFinalist] start.");
		}

		DatabaseSessionManager sessionManager = DatabaseSessionManager
				.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		try{
			connection.setAutoCommit(false);
			TenderFinalistDao tenderFinalistDao=new TenderFinalistDao_JDBCImpl();
			tenderFinalistDao.updateTenderFinalist(tenderid);
			connection.commit();
		}catch(DatabaseException e){
			//expected
			if (logger.isErrorEnabled()) {
				logger.error(
						"[TenderFinalistServiceImpl#updateTenderFinalist] database error occurred.",
						e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		}catch(Exception e){
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error(
						"[TenderFinalistServiceImpl#updateTenderFinalist] unexpected error occurred.",
						e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		}finally {
			if (logger.isInfoEnabled()) {
				logger.info("[TenderFinalistServiceImpl#updateTenderFinalist] finish.");
			}
			sessionManager.close();
		}

	}
	@Override
	public void updateTenderFinalistflag(TenderEnrollDto tenderEnrollDto) {
		if (logger.isInfoEnabled()) {
			logger.info("[TenderFinalistServiceImpl#updateTenderFinalistflag] start.");
		}

		DatabaseSessionManager sessionManager = DatabaseSessionManager
				.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		try{
			connection.setAutoCommit(false);
			TenderFinalistDao tenderFinalistDao=new TenderFinalistDao_JDBCImpl();
			TenderEnroll tenderEnroll= new TenderEnroll();
			BeanCopierUtils.copyProperties(tenderEnrollDto, tenderEnroll);
			tenderFinalistDao.updateTenderFinalistflag(tenderEnroll);
			connection.commit();
		}catch(DatabaseException e){
			//expected
			if (logger.isErrorEnabled()) {
				logger.error(
						"[TenderFinalistServiceImpl#updateTenderFinalistflag] database error occurred.",
						e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		}catch(Exception e){
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error(
						"[TenderFinalistServiceImpl#updateTenderFinalistflag] unexpected error occurred.",
						e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		}finally {
			if (logger.isInfoEnabled()) {
				logger.info("[TenderFinalistServiceImpl#insertTenderFinalist] finish.");
			}
			sessionManager.close();
		}

		
	}
}
