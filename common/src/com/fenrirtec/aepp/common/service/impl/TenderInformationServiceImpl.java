package com.fenrirtec.aepp.common.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fenrirtec.aepp.common.model.TenderInformation;
import com.fenrirtec.aepp.common.dao.TenderInformationDao;
import com.fenrirtec.aepp.common.dao.impl.TenderInformationDao_JDBCImpl;
import com.fenrirtec.aepp.common.dto.TenderInformationDto;
import com.fenrirtec.aepp.common.service.TenderInformationService;
import com.fenrirtec.core.database.DatabaseSessionManager;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.BeanCopierUtils;

public class TenderInformationServiceImpl implements TenderInformationService {

	private static final Logger logger = LoggerFactory
			.getLogger(TenderInformationServiceImpl.class);

	@Override
	public int insertTenderInformation(TenderInformationDto tenderinformationDto) {
		if (logger.isInfoEnabled()) {
			logger.info("[TenderInformationServiceImpl#insertTenderInformation] start.");
		}

		DatabaseSessionManager sessionManager = DatabaseSessionManager
				.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();

		try {
			connection.setAutoCommit(false);
			TenderInformationDao attachmentDao = new TenderInformationDao_JDBCImpl();
			
			TenderInformation tenderInformation = new TenderInformation();
			BeanCopierUtils.copyProperties(tenderinformationDto, tenderInformation);
			
			
			int maxId=attachmentDao.insertTenderInformation(tenderInformation);
			connection.commit();
			return maxId;
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error(
						"[TenderInformationServiceImpl#insertTenderInformation] database error occurred.",
						e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error(
						"[TenderInformationServiceImpl#insertTenderInformation] unexpected error occurred.",
						e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[TenderInformationServiceImpl#insertTenderInformation] finish.");
			}
			sessionManager.close();
		}
	}

	@Override
	public List<TenderInformationDto> findTenderInformation(Integer tenderid) {
		if (logger.isInfoEnabled()) {
			logger.info("[TenderInformationServiceImpl#findTenderInformation] start.");
		}
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		List<TenderInformation> tenderInformation=null;
		try{
			connection.setAutoCommit(false);
			TenderInformationDao tenderInformationDao=new TenderInformationDao_JDBCImpl();
			tenderInformation=tenderInformationDao.findTenderInformation(tenderid);
			//connection.commit();
		}catch(DatabaseException e){
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[TenderInformationServiceImpl#findTenderInformation]", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		}catch(Exception e){
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[TenderInformationServiceImpl#findTenderInformation] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		}finally {
			if (logger.isInfoEnabled()) {
				logger.info("[TenderInformationServiceImpl#findTenderInformation] finish.");
			}
			sessionManager.close();
			
		}
		List<TenderInformationDto> tenderInformationDto=null;
		if(tenderInformation !=null && tenderInformation.size()>0){
			tenderInformationDto=new ArrayList<TenderInformationDto>();
			for(TenderInformation TenderInfo : tenderInformation){
				TenderInformationDto tenderInfoDto=new TenderInformationDto();
				BeanCopierUtils.copyProperties(TenderInfo,tenderInfoDto);
				tenderInformationDto.add(tenderInfoDto);
			}
		}
		return tenderInformationDto;

	}
}
