package com.fenrirtec.aepp.common.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dao.TenderBidDao;
import com.fenrirtec.aepp.common.dao.impl.TenderBidDao_JDBCImpl;
import com.fenrirtec.aepp.common.dto.TenderBidDto;
import com.fenrirtec.aepp.common.dto.TenderBidlistDto;
import com.fenrirtec.aepp.common.model.TenderBidlist;
import com.fenrirtec.aepp.common.service.TenderBidService;
import com.fenrirtec.core.database.DatabaseSessionManager;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.BeanCopierUtils;

public class TenderBidServiceImpl implements TenderBidService {
	private static final Logger logger = LoggerFactory
			.getLogger(TenderBidServiceImpl.class);

	@Override
	public List<TenderBidlistDto> findTenderBidlsit(Integer tenderid) {
		if (logger.isInfoEnabled()) {
			logger.info("[TenderBidServiceImpl#findTenderBidlsit] start.");
		}
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		List<TenderBidlist> tenderBidList=null;
		try{
			connection.setAutoCommit(false);
			TenderBidDao tenderBidDao=new TenderBidDao_JDBCImpl();
			tenderBidList=tenderBidDao.findTenderBidlsit(tenderid);
			//connection.commit();
		}catch(DatabaseException e){
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[TenderBidServiceImpl#findTenderBidlsit]", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		}catch(Exception e){
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[TenderBidServiceImpl#findTenderBidlsit] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		}finally {
			if (logger.isInfoEnabled()) {
				logger.info("[TenderBidServiceImpl#findTenderBidlsit] finish.");
			}
			sessionManager.close();
			
		}
		List<TenderBidlistDto> tenderbidDtoList=null;
		if(tenderBidList !=null && tenderBidList.size()>0){
			tenderbidDtoList=new ArrayList<TenderBidlistDto>();
			for(TenderBidlist tenderBidlist : tenderBidList){
				TenderBidlistDto tenderBidlistDto=new TenderBidlistDto();
				BeanCopierUtils.copyProperties(tenderBidlist,tenderBidlistDto);
				tenderbidDtoList.add(tenderBidlistDto);
			}
		}
		return tenderbidDtoList;
	
	}

	@Override
	public void insertTenderBid(List<TenderBidDto> tenderBid) {
		if (logger.isInfoEnabled()) {
			logger.info("[TenderBidServiceImpl#insertTenderBid] start.");
		}

		DatabaseSessionManager sessionManager = DatabaseSessionManager
				.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		try{
			connection.setAutoCommit(false);
			TenderBidDao tenderBidDao=new TenderBidDao_JDBCImpl();
			tenderBidDao.insertTenderBid(tenderBid);
			connection.commit();
		}catch(DatabaseException e){
			//expected
			if (logger.isErrorEnabled()) {
				logger.error(
						"[TenderBidServiceImpl#insertTenderBid] database error occurred.",
						e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		}catch(Exception e){
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error(
						"[TenderBidServiceImpl#insertTenderBid] unexpected error occurred.",
						e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		}finally {
			if (logger.isInfoEnabled()) {
				logger.info("[TenderBidServiceImpl#insertTenderBid] finish.");
			}
			sessionManager.close();
		}
	}

	@Override
	public void updateTenderFinalist(Integer tenderid) {
		if (logger.isInfoEnabled()) {
			logger.info("[TenderBidServiceImpl#updateTenderFinalist] start.");
		}

		DatabaseSessionManager sessionManager = DatabaseSessionManager
				.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		try{
			connection.setAutoCommit(false);
			TenderBidDao tenderBidDao=new TenderBidDao_JDBCImpl();
			tenderBidDao.updateTenderFinalist(tenderid);
			connection.commit();
		}catch(DatabaseException e){
			//expected
			if (logger.isErrorEnabled()) {
				logger.error(
						"[TenderBidServiceImpl#updateTenderFinalist] database error occurred.",
						e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		}catch(Exception e){
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error(
						"[TenderBidServiceImpl#updateTenderFinalist] unexpected error occurred.",
						e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		}finally {
			if (logger.isInfoEnabled()) {
				logger.info("[TenderBidServiceImpl#updateTenderFinalist] finish.");
			}
			sessionManager.close();
		}

	}

	@Override
	public List<TenderBidlistDto> findTenderBidSuppliers(Integer tenderid) {
		if (logger.isInfoEnabled()) {
			logger.info("[TenderBidServiceImpl#findTenderBidSuppliers] start.");
		}
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		List<TenderBidlist> tenderBidList=null;
		try{
			connection.setAutoCommit(false);
			TenderBidDao tenderBidDao=new TenderBidDao_JDBCImpl();
			tenderBidList=tenderBidDao.findTenderBidSuppliers(tenderid);
			//connection.commit();
		}catch(DatabaseException e){
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[TenderBidServiceImpl#findTenderBidSuppliers]", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		}catch(Exception e){
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[TenderBidServiceImpl#findTenderBidSuppliers] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		}finally {
			if (logger.isInfoEnabled()) {
				logger.info("[TenderBidServiceImpl#findTenderBidSuppliers] finish.");
			}
			sessionManager.close();
			
		}
		List<TenderBidlistDto> tenderbidDtoList=null;
		if(tenderBidList !=null && tenderBidList.size()>0){
			tenderbidDtoList=new ArrayList<TenderBidlistDto>();
			for(TenderBidlist tenderBidlist : tenderBidList){
				TenderBidlistDto tenderBidlistDto=new TenderBidlistDto();
				BeanCopierUtils.copyProperties(tenderBidlist,tenderBidlistDto);
				tenderbidDtoList.add(tenderBidlistDto);
			}
		}
		return tenderbidDtoList;
	}

	@Override
	public void updateTenderInformation(Integer tenderid) {
		if (logger.isInfoEnabled()) {
			logger.info("[TenderBidServiceImpl#updateTenderInformation] start.");
		}

		DatabaseSessionManager sessionManager = DatabaseSessionManager
				.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		try{
			connection.setAutoCommit(false);
			TenderBidDao tenderBidDao=new TenderBidDao_JDBCImpl();
			tenderBidDao.updateTenderInformation(tenderid);
			connection.commit();
		}catch(DatabaseException e){
			//expected
			if (logger.isErrorEnabled()) {
				logger.error(
						"[TenderBidServiceImpl#updateTenderInformation] database error occurred.",
						e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		}catch(Exception e){
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error(
						"[TenderBidServiceImpl#updateTenderInformation] unexpected error occurred.",
						e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		}finally {
			if (logger.isInfoEnabled()) {
				logger.info("[TenderBidServiceImpl#updateTenderInformation] finish.");
			}
			sessionManager.close();
		}
		
	}

}
