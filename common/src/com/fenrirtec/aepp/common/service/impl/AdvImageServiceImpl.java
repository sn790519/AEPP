package com.fenrirtec.aepp.common.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dao.AdvImageDao;
import com.fenrirtec.aepp.common.dao.impl.AdvImageDao_JDBCImpl;
import com.fenrirtec.aepp.common.dto.AdvImageDto;
import com.fenrirtec.aepp.common.model.AdvImage;
import com.fenrirtec.aepp.common.service.AdvImageService;
import com.fenrirtec.core.database.DatabaseSessionManager;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.BeanCopierUtils;

public class AdvImageServiceImpl implements AdvImageService {
	
	private static final Logger logger = LoggerFactory.getLogger(AdvImageServiceImpl.class);

	@Override
	public List<AdvImageDto> find(Boolean useFlag) {
		if (logger.isInfoEnabled()) {
			logger.info("[AttachmentServiceImpl#find] start.");
		}
		
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		List<AdvImage> advImageList = null;
		
		try {
			connection.setAutoCommit(false);
			AdvImageDao advImageDao = new AdvImageDao_JDBCImpl();
			advImageList = advImageDao.find(useFlag);
			connection.commit();
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[AttachmentServiceImpl#find] database error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[AttachmentServiceImpl#find] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[AttachmentServiceImpl#find] finish.");
			}
			sessionManager.close();
		}
		
		List<AdvImageDto> advImageDtoList = null;
		if (advImageList != null && advImageList.size() > 0) {
			advImageDtoList = new ArrayList<AdvImageDto>();
			for (AdvImage advImage : advImageList) {
				AdvImageDto advImageDto = new AdvImageDto();
				BeanCopierUtils.copyProperties(advImage, advImageDto);
				advImageDtoList.add(advImageDto);
			}
		}
		
		return advImageDtoList;
	}

    @Override
    public Boolean insert(AdvImageDto advImageDto) {
        if (logger.isInfoEnabled()) {
            logger.info("[AttachmentServiceImpl#insert] start.");
        }
        
        DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
        sessionManager.open();
        Connection connection = sessionManager.getSession();
        Boolean result = false;
        
        try {
            connection.setAutoCommit(false);
            AdvImageDao advImageDao = new AdvImageDao_JDBCImpl();
            AdvImage advImage = new AdvImage();
            BeanCopierUtils.copyProperties(advImageDto, advImage);
            result = advImageDao.insert(advImage);
            connection.commit();
        } catch (DatabaseException e) {
            // expected
            if (logger.isErrorEnabled()) {
                logger.error("[AttachmentServiceImpl#insert] database error occurred.", e);
            }
            try {
                connection.rollback();
            } catch (SQLException e1) {
            }
            throw e;
        } catch (Exception e) {
            // unexpected
            if (logger.isErrorEnabled()) {
                logger.error("[AttachmentServiceImpl#insert] unexpected error occurred.", e);
            }
            try {
                connection.rollback();
            } catch (SQLException e1) {
            }
            throw new UnexpectedException("unexpected error.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[AttachmentServiceImpl#insert] finish.");
            }
            sessionManager.close();
        }
        return result;
    }

    @Override
    public Boolean update(Integer imageId, Boolean useFlag, String updateUser) {
        if (logger.isInfoEnabled()) {
            logger.info("[AttachmentServiceImpl#update] start.");
        }
        
        DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
        sessionManager.open();
        Connection connection = sessionManager.getSession();
        Boolean result = false;
        
        try {
            connection.setAutoCommit(false);
            AdvImageDao advImageDao = new AdvImageDao_JDBCImpl();
            result = advImageDao.update(imageId, useFlag, updateUser);
            connection.commit();
        } catch (DatabaseException e) {
            // expected
            if (logger.isErrorEnabled()) {
                logger.error("[AttachmentServiceImpl#update] database error occurred.", e);
            }
            try {
                connection.rollback();
            } catch (SQLException e1) {
            }
            throw e;
        } catch (Exception e) {
            // unexpected
            if (logger.isErrorEnabled()) {
                logger.error("[AttachmentServiceImpl#update] unexpected error occurred.", e);
            }
            try {
                connection.rollback();
            } catch (SQLException e1) {
            }
            throw new UnexpectedException("unexpected error.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[AttachmentServiceImpl#update] finish.");
            }
            sessionManager.close();
        }
        return result;
    }

    @Override
    public Boolean delete(Integer imageId) {
        if (logger.isInfoEnabled()) {
            logger.info("[AttachmentServiceImpl#delete] start.");
        }
        
        DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
        sessionManager.open();
        Connection connection = sessionManager.getSession();
        Boolean result = false;
        
        try {
            connection.setAutoCommit(false);
            AdvImageDao advImageDao = new AdvImageDao_JDBCImpl();
            result = advImageDao.delete(imageId);
            connection.commit();
        } catch (DatabaseException e) {
            // expected
            if (logger.isErrorEnabled()) {
                logger.error("[AttachmentServiceImpl#delete] database error occurred.", e);
            }
            try {
                connection.rollback();
            } catch (SQLException e1) {
            }
            throw e;
        } catch (Exception e) {
            // unexpected
            if (logger.isErrorEnabled()) {
                logger.error("[AttachmentServiceImpl#delete] unexpected error occurred.", e);
            }
            try {
                connection.rollback();
            } catch (SQLException e1) {
            }
            throw new UnexpectedException("unexpected error.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[AttachmentServiceImpl#delete] finish.");
            }
            sessionManager.close();
        }
        return result;
    }

}
