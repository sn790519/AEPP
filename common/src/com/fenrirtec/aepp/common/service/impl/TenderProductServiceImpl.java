package com.fenrirtec.aepp.common.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fenrirtec.aepp.common.dao.TenderProductDao;
import com.fenrirtec.aepp.common.dao.impl.TenderProductDao_JDBCImpl;
import com.fenrirtec.aepp.common.dto.TenderProductDto;
import com.fenrirtec.aepp.common.service.TenderProductService;
import com.fenrirtec.core.database.DatabaseSessionManager;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;

public class TenderProductServiceImpl implements TenderProductService {
	private static final Logger logger = LoggerFactory
			.getLogger(TenderProductServiceImpl.class);

	@Override
	public void insertTenderProduct(List<TenderProductDto> tenderproductdtoList,int maxId) {
		if (logger.isInfoEnabled()) {
			logger.info("[TenderProductServiceImpl#insertTenderProduct] start.");
		}

		DatabaseSessionManager sessionManager = DatabaseSessionManager
				.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();

		try {
			connection.setAutoCommit(false);
			TenderProductDao attachmentDao = new TenderProductDao_JDBCImpl();
			attachmentDao.insertTenderProduct(tenderproductdtoList,maxId);
			connection.commit();
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
				logger.error(
						"[TenderProductServiceImpl#insertTenderProduct] database error occurred.",
						e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error(
						"[TenderProductServiceImpl#insertTenderProduct] unexpected error occurred.",
						e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[TenderProductServiceImpl#insertTenderProduct] finish.");
			}
			sessionManager.close();
		}
	}
}
