package com.fenrirtec.aepp.common.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dao.TenderManageDao;
import com.fenrirtec.aepp.common.dao.impl.TenderManageDao_JDBCImpl;
import com.fenrirtec.aepp.common.dto.TenderManageDto;
import com.fenrirtec.aepp.common.model.TenderManage;
import com.fenrirtec.aepp.common.service.TenderManageService;
import com.fenrirtec.core.database.DatabaseSessionManager;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.BeanCopierUtils;

public class TenderManageServiceImpl implements TenderManageService {
	private static final Logger logger = LoggerFactory
			.getLogger(TenderManageServiceImpl.class);

	@Override
	public List<TenderManageDto> findTenderManage(Integer tenderid) {
		if (logger.isInfoEnabled()) {
			logger.info("[TenderManageServiceImpl#findTenderManage] start.");
		}
		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();
		List<TenderManage> tenderManageList=null;
		try{
			connection.setAutoCommit(false);
			TenderManageDao tenderManageDao=new TenderManageDao_JDBCImpl();
			tenderManageList=tenderManageDao.findTenderManage(tenderid);
			//connection.commit();
		}catch(DatabaseException e){
			// expected
			if (logger.isErrorEnabled()) {
				logger.error("[TenderManageServiceImpl#findTenderManage.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		}catch(Exception e){
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[TenderManageServiceImpl#findTenderManage] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		}finally {
			if (logger.isInfoEnabled()) {
				logger.info("[TenderManageServiceImpl#findTenderManage] finish.");
			}
			sessionManager.close();
			
		}
		List<TenderManageDto> tenderManageDtoList=null;
		if(tenderManageList !=null && tenderManageList.size()>0){
			tenderManageDtoList=new ArrayList<TenderManageDto>();
			for(TenderManage tenderManage : tenderManageList){
				TenderManageDto tenderManageDto=new TenderManageDto();
				BeanCopierUtils.copyProperties(tenderManage,tenderManageDto);
				tenderManageDtoList.add(tenderManageDto);
			}
		}
		return tenderManageDtoList;
	}

}
