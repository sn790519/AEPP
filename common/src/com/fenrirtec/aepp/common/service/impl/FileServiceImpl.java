package com.fenrirtec.aepp.common.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dao.FileDao;
import com.fenrirtec.aepp.common.dao.impl.FileDao_FileImpl;
import com.fenrirtec.aepp.common.service.FileService;
import com.fenrirtec.core.exception.UnexpectedException;

public class FileServiceImpl implements FileService {
	
	private static final Logger logger = LoggerFactory.getLogger(FileServiceImpl.class);

	@Override
	public void moveFile(String srcFile, String destFile) {
		if (logger.isInfoEnabled()) {
			logger.info("[FileServiceImpl#moveFile] start.");
		}
		
		try {
			FileDao fileDao = new FileDao_FileImpl();
			fileDao.moveFile(srcFile, destFile);
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[FileServiceImpl#moveFile] unexpected error occurred.", e);
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[FileServiceImpl#moveFile] finish.");
			}
		}
	}
	
	@Override
    public void copyFile(String srcFile, String destFile) {
        if (logger.isInfoEnabled()) {
            logger.info("[FileServiceImpl#copyFile] start.");
        }
        
        try {
            FileDao fileDao = new FileDao_FileImpl();
            fileDao.copyFile(srcFile, destFile);
        } catch (Exception e) {
            // unexpected
            if (logger.isErrorEnabled()) {
                logger.error("[FileServiceImpl#copyFile] unexpected error occurred.", e);
            }
            throw new UnexpectedException("unexpected error.", e);
        } finally {
            if (logger.isInfoEnabled()) {
                logger.info("[FileServiceImpl#copyFile] finish.");
            }
        }
    }

	@Override
	public void deleteFile(String path) {
		if (logger.isInfoEnabled()) {
			logger.info("[FileServiceImpl#deleteFile] start.");
		}
		
		try {
			FileDao fileDao = new FileDao_FileImpl();
			fileDao.deleteFile(path);
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[FileServiceImpl#deleteFile] unexpected error occurred.", e);
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[FileServiceImpl#deleteFile] finish.");
			}
		}
	}

	@Override
	public void renameFolder(String oldPath, String newPath) {
		if (logger.isInfoEnabled()) {
			logger.info("[FileServiceImpl#renameFolder] start.");
		}
		
		try {
			FileDao fileDao = new FileDao_FileImpl();
			fileDao.renameFolder(oldPath, newPath);
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[FileServiceImpl#renameFolder] unexpected error occurred.", e);
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[FileServiceImpl#renameFolder] finish.");
			}
		}
	}

	@Override
	public void deleteFolder(String path) {
		if (logger.isInfoEnabled()) {
			logger.info("[FileServiceImpl#deleteFolder] start.");
		}
		
		try {
			FileDao fileDao = new FileDao_FileImpl();
			fileDao.deleteFolder(path);
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
				logger.error("[FileServiceImpl#deleteFolder] unexpected error occurred.", e);
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[FileServiceImpl#deleteFolder] finish.");
			}
		}
	}

}
