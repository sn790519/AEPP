package com.fenrirtec.aepp.common.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.condition.TenderCondition;
import com.fenrirtec.aepp.common.dao.InformationDao;
import com.fenrirtec.aepp.common.dao.impl.InformationDao_JDBCImpl;
import com.fenrirtec.aepp.common.dto.TenderInformationDto;
import com.fenrirtec.aepp.common.model.TenderInformation;
import com.fenrirtec.aepp.common.service.InformationService;
import com.fenrirtec.core.database.DatabaseSessionManager;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.BeanCopierUtils;

public class InformationServiceImpl implements InformationService{
	private static final Logger logger = LoggerFactory.getLogger(TenderServiceImpl.class);

	@Override
	public Integer countByCnd(TenderCondition condition) {
		if (logger.isInfoEnabled()) {
			logger.info("[InformationServiceImpl#countByCnd] start.");
		}

		DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
		sessionManager.open();
		Connection connection = sessionManager.getSession();

		try {
			connection.setAutoCommit(false);
			InformationDao tenderInformationDao = new InformationDao_JDBCImpl();
			Integer count = tenderInformationDao.countByCnd(condition);
			connection.commit();
			return count;
		} catch (DatabaseException e) {
			// expected
			if (logger.isErrorEnabled()) {
                logger.error("[InformationServiceImpl#countByCnd] database error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw e;
		} catch (Exception e) {
			// unexpected
			if (logger.isErrorEnabled()) {
                logger.error("[InformationServiceImpl#countByCnd] unexpected error occurred.", e);
			}
			try {
				connection.rollback();
			} catch (SQLException e1) {
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[InformationServiceImpl#countByCnd] finish.");
			}
			sessionManager.close();
		}
	}
	
	 @Override
	    public List<TenderInformationDto> searchByCnd(TenderCondition condition) {
	        if (logger.isInfoEnabled()) {
	            logger.info("[InformationServiceImpl#searchByCnd] start.");
	        }

	        DatabaseSessionManager sessionManager = DatabaseSessionManager.getInstance();
	        sessionManager.open();
	        Connection connection = sessionManager.getSession();
	        List<TenderInformation> tenderInformationList = null;

	        try {
	            connection.setAutoCommit(false);
	            InformationDao tenderInformationDao = new InformationDao_JDBCImpl();
	            tenderInformationList = tenderInformationDao.searchByCnd(condition);
	            connection.commit();
	        } catch (DatabaseException e) {
	            // expected
	            if (logger.isErrorEnabled()) {
	                logger.error("[TenderServiceImpl#searchByCnd] database error occurred.", e);
	            }
	            try {
	                connection.rollback();
	            } catch (SQLException e1) {
	            }
	            throw e;
	        } catch (Exception e) {
	            // unexpected
	            if (logger.isErrorEnabled()) {
	                logger.error("[TenderServiceImpl#searchByCnd] unexpected error occurred.", e);
	            }
	            try {
	                connection.rollback();
	            } catch (SQLException e1) {
	            }
	            throw new UnexpectedException("unexpected error.", e);
	        } finally {
	            if (logger.isInfoEnabled()) {
	                logger.info("[TenderServiceImpl#searchByCnd] finish.");
	            }
	            sessionManager.close();
	        }
	        
	        List<TenderInformationDto> tenderInformationDtoList = null;
	        
	        if (tenderInformationList != null && tenderInformationList.size() > 0) {
	            tenderInformationDtoList = new ArrayList<TenderInformationDto>();
	            for (TenderInformation tenderInformation : tenderInformationList) {
	                TenderInformationDto tenderInformationDto = new TenderInformationDto();
	                BeanCopierUtils.copyProperties(tenderInformation, tenderInformationDto);
	                tenderInformationDtoList.add(tenderInformationDto);
	            }
	        }
	        return tenderInformationDtoList;
	    }
}
