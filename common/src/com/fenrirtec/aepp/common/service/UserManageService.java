package com.fenrirtec.aepp.common.service;

import java.util.List;

import com.fenrirtec.aepp.common.dto.GroupDto;
import com.fenrirtec.aepp.common.dto.UserDto;

public interface UserManageService {
	UserDto login(String loginName, String loginPassword);
	List<UserDto> findAll();
    List<GroupDto> findAllGroup();
    List<UserDto> find(String keyword, String[] groups);
    List<GroupDto> findGroup(String keyword);
    UserDto findByLoginName(String loginName);
    Boolean insert(UserDto userDto);
    Boolean update(UserDto userDto);
    Boolean delete(String loginName);
    GroupDto findByGroupName(String groupName);
    Boolean insert(GroupDto groupDto);
    Boolean update(GroupDto groupDto);
    Boolean deleteGroup(String groupName);
}
