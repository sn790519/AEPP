package com.fenrirtec.aepp.common.service;

import java.util.List;

import com.fenrirtec.aepp.common.dto.CodeNameDto;

public interface CodeNameService {
	List<CodeNameDto> findByTypeId(String typeId);
}
