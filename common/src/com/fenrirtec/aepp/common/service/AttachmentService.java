package com.fenrirtec.aepp.common.service;

import com.fenrirtec.aepp.common.dto.AttachmentDto;
import com.fenrirtec.aepp.common.dto.UploadDto;

public interface AttachmentService {
	Integer saveAttachment(Integer fileType, String path, String comment, String uploadUser);
	AttachmentDto searchAttachment(Integer attachmentId);
	void relateMemberMaterial(String memberLoginName, String uploadFileCategory, Integer attachmentId);
	void removeAttachment(Integer attachmentId);
	Integer upload(String person, Integer personType, String purpose, String path, Integer fileType);
	UploadDto uploadInfo(Integer uploadId);
	void removeUpload(Integer uploadId);
}
