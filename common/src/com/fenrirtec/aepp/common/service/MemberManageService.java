package com.fenrirtec.aepp.common.service;

import java.util.List;

import com.fenrirtec.aepp.common.condition.MemberCondition;
import com.fenrirtec.aepp.common.dto.AuditFlowDto;
import com.fenrirtec.aepp.common.dto.MemberDto;

public interface MemberManageService {
	Boolean memberRegist(MemberDto memberDto);
	Boolean memberExists(String memberLoginName);
	List<MemberDto> memberSearch(MemberCondition condition);
	MemberDto memberInfo(String memberLoginName);
	Integer memberCount(MemberCondition condition);
	void removeImage(String memberLoginName, Integer attachmentCategory, Integer attachmentId);
	MemberDto login(String memberLoginName, String memberLoginPassword);
	Boolean memberAudit(String memberLoginName, Integer auditResult, String auditComment, String loginName);
	Boolean memberEdit(MemberDto memberDto);
	List<AuditFlowDto> auditFlowSearch(String memberLoginName);
	void updateAuditResult(AuditFlowDto auditFlowDto);
	void insertAuditFlow(AuditFlowDto auditFlowDto);
}
