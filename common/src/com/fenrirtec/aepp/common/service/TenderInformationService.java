package com.fenrirtec.aepp.common.service;

import java.util.List;

import com.fenrirtec.aepp.common.dto.TenderInformationDto;

public interface TenderInformationService {
	int insertTenderInformation(TenderInformationDto tenderinformationDto);
	List<TenderInformationDto> findTenderInformation(Integer tenderid);
}
