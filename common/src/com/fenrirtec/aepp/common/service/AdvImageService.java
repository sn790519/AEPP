package com.fenrirtec.aepp.common.service;

import java.util.List;

import com.fenrirtec.aepp.common.dto.AdvImageDto;

public interface AdvImageService {
	List<AdvImageDto> find(Boolean useFlag);
	Boolean insert(AdvImageDto advImageDto);
    Boolean update(Integer imageId, Boolean useFlag, String updateUser);
    Boolean delete(Integer imageId);
}
