package com.fenrirtec.aepp.common.service;

import java.util.List;

import com.fenrirtec.aepp.common.dto.TenderEnrollDto;
import com.fenrirtec.aepp.common.dto.TenderFinalistDto;


public interface TenderFinalistService {
	void insertTenderFinalist(List<TenderFinalistDto> tenderFinaList);
	void updateTenderFinalist(Integer tenderid);
	void updateTenderFinalistflag(TenderEnrollDto tenderEnrollDto);
}