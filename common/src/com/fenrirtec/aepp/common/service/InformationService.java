package com.fenrirtec.aepp.common.service;

import java.util.List;

import com.fenrirtec.aepp.common.condition.TenderCondition;
import com.fenrirtec.aepp.common.dto.TenderInformationDto;

public interface InformationService {
	Integer countByCnd(TenderCondition condition);
	List<TenderInformationDto> searchByCnd(TenderCondition condition);
}
