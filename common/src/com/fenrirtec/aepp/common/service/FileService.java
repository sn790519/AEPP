package com.fenrirtec.aepp.common.service;

public interface FileService {
	void moveFile(String srcFile, String destFile);
    void copyFile(String srcFile, String destFile);
	void deleteFile(String path);
	void renameFolder(String oldPath, String newPath);
	void deleteFolder(String path);
}
