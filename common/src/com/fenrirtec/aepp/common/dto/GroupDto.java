package com.fenrirtec.aepp.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fenrirtec.core.dto.BaseDto;

public class GroupDto extends BaseDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("group_name")
	private String groupName;

	@JsonProperty("name")
	private String name;
	
	@JsonProperty("group_user")
	private String groupUser;
    
    @JsonProperty("group_user_name")
    private String groupUserName;

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public String getGroupUser() {
        return groupUser;
    }

    public void setGroupUser(String groupUser) {
        this.groupUser = groupUser;
    }

    public String getGroupUserName() {
        return groupUserName;
    }

    public void setGroupUserName(String groupUserName) {
        this.groupUserName = groupUserName;
    }
}
