package com.fenrirtec.aepp.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fenrirtec.core.dto.BaseDto;

public class AuditFlowDto extends BaseDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("member_login_name")
	private String memberLoginName;
	
	@JsonProperty("audit_sequence")
	private Integer auditSequence;
	
	@JsonProperty("audit_date")
	private String auditDate;
	
	@JsonProperty("audit_phase")
	private Integer auditPhase;
	
	@JsonProperty("audit_result")
	private Integer auditResult;
	
	@JsonProperty("audit_comment")
	private String auditComment;
	
	@JsonProperty("login_name")
	private String loginName;
	
	@JsonProperty("audit_phase_name")
	private String auditPhaseName;
	
	@JsonProperty("audit_result_name")
	private String auditResultName;

	public String getMemberLoginName() {
		return memberLoginName;
	}

	public void setMemberLoginName(String memberLoginName) {
		this.memberLoginName = memberLoginName;
	}

	public Integer getAuditSequence() {
		return auditSequence;
	}

	public void setAuditSequence(Integer auditSequence) {
		this.auditSequence = auditSequence;
	}

	public String getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(String auditDate) {
		this.auditDate = auditDate;
	}

	public Integer getAuditPhase() {
		return auditPhase;
	}

	public void setAuditPhase(Integer auditPhase) {
		this.auditPhase = auditPhase;
	}

	public Integer getAuditResult() {
		return auditResult;
	}

	public void setAuditResult(Integer auditResult) {
		this.auditResult = auditResult;
	}

	public String getAuditComment() {
		return auditComment;
	}

	public void setAuditComment(String auditComment) {
		this.auditComment = auditComment;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getAuditPhaseName() {
		return auditPhaseName;
	}

	public void setAuditPhaseName(String auditPhaseName) {
		this.auditPhaseName = auditPhaseName;
	}

	public String getAuditResultName() {
		return auditResultName;
	}

	public void setAuditResultName(String auditResultName) {
		this.auditResultName = auditResultName;
	}
	
}
