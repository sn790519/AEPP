package com.fenrirtec.aepp.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fenrirtec.core.dto.BaseDto;

public class TenderFinalistDto extends BaseDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("tender_id")
	private Integer tenderId;
	
	@JsonProperty("member_login_name")
	private String memberLoginName;

	@JsonProperty("finalist_date")
	private String finalistDate;

	@JsonProperty("evaluation")
	private String evaluation;

	@JsonProperty("quote_flag")
	private Boolean quoteFlag;

    @JsonProperty("invite_flag")
    private Boolean inviteFlag;

    public Integer getTenderId() {
        return tenderId;
    }

    public void setTenderId(Integer tenderId) {
        this.tenderId = tenderId;
    }

    public String getMemberLoginName() {
        return memberLoginName;
    }

    public void setMemberLoginName(String memberLoginName) {
        this.memberLoginName = memberLoginName;
    }

    public String getFinalistDate() {
        return finalistDate;
    }

    public void setFinalistDate(String finalistDate) {
        this.finalistDate = finalistDate;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    public Boolean getQuoteFlag() {
        return quoteFlag;
    }

    public void setQuoteFlag(Boolean quoteFlag) {
        this.quoteFlag = quoteFlag;
    }

    public Boolean getInviteFlag() {
        return inviteFlag;
    }

    public void setInviteFlag(Boolean inviteFlag) {
        this.inviteFlag = inviteFlag;
    }

}
