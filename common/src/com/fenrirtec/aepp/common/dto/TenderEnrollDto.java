package com.fenrirtec.aepp.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fenrirtec.core.dto.BaseDto;

public class TenderEnrollDto extends BaseDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("tender_id")
	private Integer tenderId;
	
	@JsonProperty("member_login_name")
	private String memberLoginName;

	@JsonProperty("enroll_date")
	private String enrollDate;

	@JsonProperty("description")
	private String description;

	@JsonProperty("finalist_flag")
	private Boolean finalistFlag;

    @JsonProperty("invite_flag")
    private Boolean inviteFlag;

    public Integer getTenderId() {
        return tenderId;
    }

    public void setTenderId(Integer tenderId) {
        this.tenderId = tenderId;
    }

    public String getMemberLoginName() {
        return memberLoginName;
    }

    public void setMemberLoginName(String memberLoginName) {
        this.memberLoginName = memberLoginName;
    }

    public String getEnrollDate() {
        return enrollDate;
    }

    public void setEnrollDate(String enrollDate) {
        this.enrollDate = enrollDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getFinalistFlag() {
        return finalistFlag;
    }

    public void setFinalistFlag(Boolean finalistFlag) {
        this.finalistFlag = finalistFlag;
    }

    public Boolean getInviteFlag() {
        return inviteFlag;
    }

    public void setInviteFlag(Boolean inviteFlag) {
        this.inviteFlag = inviteFlag;
    }

}
