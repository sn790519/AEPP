package com.fenrirtec.aepp.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fenrirtec.core.dto.BaseDto;

public class FunctionDto extends BaseDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("function_name")
	private String functionName;

	@JsonProperty("name")
	private String name;

	@JsonProperty("url")
	private String url;

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
