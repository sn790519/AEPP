package com.fenrirtec.aepp.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fenrirtec.core.dto.BaseDto;

public class UploadDto extends BaseDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("upload_id")
	Integer uploadId;
	
	@JsonProperty("upload_person")
	String uploadPerson;
	
	@JsonProperty("upload_person_type")
	Integer uploadPersonType;
	
	@JsonProperty("upload_purpose")
	String uploadPurpose;
	
	@JsonProperty("upload_datetime")
	String uploadDatetime;
	
	@JsonProperty("upload_file_path")
	String uploadFilePath;
	
	@JsonProperty("upload_file_type")
	Integer uploadFileType;

	public Integer getUploadId() {
		return uploadId;
	}

	public void setUploadId(Integer uploadId) {
		this.uploadId = uploadId;
	}

	public String getUploadPerson() {
		return uploadPerson;
	}

	public void setUploadPerson(String uploadPerson) {
		this.uploadPerson = uploadPerson;
	}

	public Integer getUploadPersonType() {
		return uploadPersonType;
	}

	public void setUploadPersonType(Integer uploadPersonType) {
		this.uploadPersonType = uploadPersonType;
	}

	public String getUploadPurpose() {
		return uploadPurpose;
	}

	public void setUploadPurpose(String uploadPurpose) {
		this.uploadPurpose = uploadPurpose;
	}

	public String getUploadDatetime() {
		return uploadDatetime;
	}

	public void setUploadDatetime(String uploadDatetime) {
		this.uploadDatetime = uploadDatetime;
	}

	public String getUploadFilePath() {
		return uploadFilePath;
	}

	public void setUploadFilePath(String uploadFilePath) {
		this.uploadFilePath = uploadFilePath;
	}

	public Integer getUploadFileType() {
		return uploadFileType;
	}

	public void setUploadFileType(Integer uploadFileType) {
		this.uploadFileType = uploadFileType;
	}
	
}
