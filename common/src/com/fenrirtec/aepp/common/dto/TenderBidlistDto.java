package com.fenrirtec.aepp.common.dto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fenrirtec.core.dto.BaseDto;

public class TenderBidlistDto extends BaseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//招标信息ID 
	@JsonProperty("tender_id")
	private Integer tenderId;
	
    //会员名（登录名）
	@JsonProperty("member_login_name")
	private String memberLoginName;
	
	@JsonProperty("enterprise_name")
	private String enterpriseName;

	//企业类型名称
	@JsonProperty("enterprise_category_name")
	private String enterpriseCategoryName;
	
	@JsonProperty("area_code")
	private String areaCode;

	//会员级别名称
	@JsonProperty("rank_name")
	private String rankName;
	
	//投标总价
	@JsonProperty("total")
	private double total;
	
	//投标文件
	@JsonProperty("path")
	private String path;

	//状态显示文字
	@JsonProperty("status_name")
	private String statusName;

	public Integer getTenderId() {
		return tenderId;
	}

	public void setTenderId(Integer tenderId) {
		this.tenderId = tenderId;
	}

	public String getMemberLoginName() {
		return memberLoginName;
	}

	public void setMemberLoginName(String memberLoginName) {
		this.memberLoginName = memberLoginName;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public String getEnterpriseCategoryName() {
		return enterpriseCategoryName;
	}

	public void setEnterpriseCategoryName(String enterpriseCategoryName) {
		this.enterpriseCategoryName = enterpriseCategoryName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getRankName() {
		return rankName;
	}

	public void setRankName(String rankName) {
		this.rankName = rankName;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
}
