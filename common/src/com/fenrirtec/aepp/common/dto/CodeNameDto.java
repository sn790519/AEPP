package com.fenrirtec.aepp.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fenrirtec.core.dto.BaseDto;

public class CodeNameDto extends BaseDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("type_id")
	private String typeId;
	
	@JsonProperty("code")
	private Integer code;
	
	@JsonProperty("name")
	private String name;

	public CodeNameDto() {
	}

	public String getTypeId() {
		return this.typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public Integer getCode() {
		return this.code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
