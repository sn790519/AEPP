package com.fenrirtec.aepp.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fenrirtec.core.dto.BaseDto;

public class AttachmentDto extends BaseDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("attachment_id")
	private Integer attachmentId;

	@JsonProperty("file_type")
	private Integer fileType;

	@JsonProperty("path")
	private String path;

	@JsonProperty("comment")
	private String comment;

	@JsonProperty("upload_user")
	private String uploadUser;

	@JsonProperty("upload_date")
	private String uploadDate;

	public Integer getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(Integer attachmentId) {
		this.attachmentId = attachmentId;
	}

	public Integer getFileType() {
		return fileType;
	}

	public void setFileType(Integer fileType) {
		this.fileType = fileType;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getUploadUser() {
		return uploadUser;
	}

	public void setUploadUser(String uploadUser) {
		this.uploadUser = uploadUser;
	}

	public String getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}
}
