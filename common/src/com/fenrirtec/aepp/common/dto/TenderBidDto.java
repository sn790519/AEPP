package com.fenrirtec.aepp.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fenrirtec.core.dto.BaseDto;

public class TenderBidDto extends BaseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//招标信息ID 
	@JsonProperty("tender_id")
	private Integer tenderId;
	
    //会员名（登录名）
	@JsonProperty("member_login_name")
	private String memberLoginName;
	
	//中标时间 
	@JsonProperty("bid_date")
	private String bidDate;
    
	//评价说明 
	@JsonProperty("evaluation")
	private String evaluation;

	
	//逻辑删除识别子 
	@JsonProperty("delete_flag")
	private Boolean deleteFlag;

	public Integer getTenderId() {
		return tenderId;
	}

	public void setTenderId(Integer tenderId) {
		this.tenderId = tenderId;
	}

	public String getMemberLoginName() {
		return memberLoginName;
	}

	public void setMemberLoginName(String memberLoginName) {
		this.memberLoginName = memberLoginName;
	}

	public String getBidDate() {
		return bidDate;
	}

	public void setBidDate(String bidDate) {
		this.bidDate = bidDate;
	}

	public String getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}

	public Boolean getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
  
}
