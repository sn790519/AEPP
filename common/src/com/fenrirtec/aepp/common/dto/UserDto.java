package com.fenrirtec.aepp.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fenrirtec.core.dto.BaseDto;

public class UserDto extends BaseDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("login_name")
	private String loginName;

	@JsonProperty("login_password")
	private String loginPassword;

	@JsonProperty("name")
	private String name;
	
	@JsonProperty("user_group")
	private String userGroup;
    
    @JsonProperty("user_group_name")
    private String userGroupName;

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginPassword() {
		return loginPassword;
	}

	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    public String getUserGroupName() {
        return userGroupName;
    }

    public void setUserGroupName(String userGroupName) {
        this.userGroupName = userGroupName;
    }
}
