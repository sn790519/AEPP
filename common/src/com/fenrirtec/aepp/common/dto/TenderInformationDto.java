package com.fenrirtec.aepp.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fenrirtec.core.dto.BaseDto;

public class TenderInformationDto extends BaseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("tender_title")
	private String tenderTitle;
		
	@JsonProperty("enroll_start_date")
	private String enrollStartDate;

	@JsonProperty("enroll_end_date")
	private String enrollEndDate;

	@JsonProperty("tender_end_date")
	private String tenderEndDate;

	@JsonProperty("publish_result_date")
	private String publishResultDate;

	@JsonProperty("content_description")
	private String contentDescription;

	@JsonProperty("vendor_requirement")
	private String vendorRequirement;

	@JsonProperty("tender_pattern")
	private Integer tenderPattern;

	@JsonProperty("tender_template")
	private Integer tenderTemplate;

	@JsonProperty("contact_name")
	private String contactName;

	@JsonProperty("contact_tel")
	private String contactTel;
	
	@JsonProperty("tender_pattern_name")
	private String tenderPatternName;
	
	@JsonProperty("status_name")
	private String statusName;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getTenderTitle() {
		return tenderTitle;
	}

	public void setTenderTitle(String tenderTitle) {
		this.tenderTitle = tenderTitle;
	}

	public String getEnrollStartDate() {
		return enrollStartDate;
	}

	public void setEnrollStartDate(String enrollStartDate) {
		this.enrollStartDate = enrollStartDate;
	}

	public String getContentDescription() {
		return contentDescription;
	}

	public void setContentDescription(String contentDescription) {
		this.contentDescription = contentDescription;
	}

	public String getVendorRequirement() {
		return vendorRequirement;
	}

	public void setVendorRequirement(String vendorRequirement) {
		this.vendorRequirement = vendorRequirement;
	}

	public Integer getTenderPattern() {
		return tenderPattern;
	}

	public void setTenderPattern(Integer tenderPattern) {
		this.tenderPattern = tenderPattern;
	}

	public Integer getTenderTemplate() {
		return tenderTemplate;
	}

	public void setTenderTemplate(Integer tenderTemplate) {
		this.tenderTemplate = tenderTemplate;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactTel() {
		return contactTel;
	}

	public void setContactTel(String contactTel) {
		this.contactTel = contactTel;
	}

	public String getEnrollEndDate() {
		return enrollEndDate;
	}

	public void setEnrollEndDate(String enrollEndDate) {
		this.enrollEndDate = enrollEndDate;
	}

	public String getTenderEndDate() {
		return tenderEndDate;
	}

	public void setTenderEndDate(String tenderEndDate) {
		this.tenderEndDate = tenderEndDate;
	}

	public String getPublishResultDate() {
		return publishResultDate;
	}

	public String getTenderPatternName() {
		return tenderPatternName;
	}

	public void setTenderPatternName(String tenderPatternName) {
		this.tenderPatternName = tenderPatternName;
	}

	public void setPublishResultDate(String publishResultDate) {
		this.publishResultDate = publishResultDate;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	
}
