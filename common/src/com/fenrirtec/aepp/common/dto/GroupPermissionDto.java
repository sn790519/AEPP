package com.fenrirtec.aepp.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fenrirtec.core.dto.BaseDto;

public class GroupPermissionDto extends BaseDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("group_name")
	private String groupName;

	@JsonProperty("function_name")
	private String functionName;

	@JsonProperty("readable")
	private Boolean readable;

    @JsonProperty("addable")
    private Boolean addable;

    @JsonProperty("editable")
    private Boolean editable;

    @JsonProperty("deleteable")
    private Boolean deleteable;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public Boolean getReadable() {
        return readable;
    }

    public void setReadable(Boolean readable) {
        this.readable = readable;
    }

    public Boolean getAddable() {
        return addable;
    }

    public void setAddable(Boolean addable) {
        this.addable = addable;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public Boolean getDeleteable() {
        return deleteable;
    }

    public void setDeleteable(Boolean deleteable) {
        this.deleteable = deleteable;
    }

}
