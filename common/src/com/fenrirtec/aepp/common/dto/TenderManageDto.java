package com.fenrirtec.aepp.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fenrirtec.core.dto.BaseDto;

public class TenderManageDto extends BaseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("tender_id")
	private Integer tenderId;
	
    //会员名（登录名）
	@JsonProperty("member_login_name")
	private String memberLoginName;
	
	@JsonProperty("enterprise_name")
	private String enterpriseName;

	//企业分类名字
	@JsonProperty("enterprise_category_name")
	private String enterpriseCategoryName;

	@JsonProperty("area_code")
	private String areaCode;

	//会员级别名字
	@JsonProperty("rank_name")
	private String rankName;
	
	//报名文件
	@JsonProperty("path")
	private String path;
	
	@JsonProperty("finalist_flag")
	private Boolean finalistFlag;
	
	//是否入围名称
	@JsonProperty("finalistFlagname")
	private String finalistFlagname;
	
	public Integer getTenderId() {
		return tenderId;
	}

	public void setTenderId(Integer tenderId) {
		this.tenderId = tenderId;
	}

	public String getMemberLoginName() {
		return memberLoginName;
	}

	public void setMemberLoginName(String memberLoginName) {
		this.memberLoginName = memberLoginName;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}
    
	
	public String getAreaCode() {
		return areaCode;
	}

	public String getEnterpriseCategoryName() {
		return enterpriseCategoryName;
	}

	public void setEnterpriseCategoryName(String enterpriseCategoryName) {
		this.enterpriseCategoryName = enterpriseCategoryName;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
    
	public String getRankName() {
		return rankName;
	}

	public void setRankName(String rankName) {
		this.rankName = rankName;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Boolean getFinalistFlag() {
		return finalistFlag;
	}

	public void setFinalistFlag(Boolean finalistFlag) {
		this.finalistFlag = finalistFlag;
	}

	public String getFinalistFlagname() {
		return finalistFlagname;
	}

	public void setFinalistFlagname(String finalistFlagname) {
		this.finalistFlagname = finalistFlagname;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
