package com.fenrirtec.aepp.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fenrirtec.core.dto.BaseDto;

public class TenderVenderDto extends BaseDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("tender_id")
	private Integer tenderId;
	
	@JsonProperty("member_login_name")
	private String memberLoginName;

	@JsonProperty("enterprise_name")
	private String enterpriseName;

    public Integer getTenderId() {
        return tenderId;
    }

    public void setTenderId(Integer tenderId) {
        this.tenderId = tenderId;
    }

    public String getMemberLoginName() {
        return memberLoginName;
    }

    public void setMemberLoginName(String memberLoginName) {
        this.memberLoginName = memberLoginName;
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
    }

}
