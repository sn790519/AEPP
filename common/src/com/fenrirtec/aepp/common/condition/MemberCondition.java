package com.fenrirtec.aepp.common.condition;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fenrirtec.core.condition.BaseCondition;

public class MemberCondition extends BaseCondition implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("member_login_name")
	private String memberLoginName;
	
	@JsonProperty("license_number")
	private String licenseNumber;
	
	@JsonProperty("enterprise_name")
	private String enterpriseName;
	
	@JsonProperty("enterprise_property")
	private Integer enterpriseProperty;
	
	@JsonProperty("enterprise_category")
	private Integer enterpriseCategory;
	
	@JsonProperty("contact_name")
	private String contactName;
	
	@JsonProperty("contact_certificates_no")
	private String contactCertificatesNo;
	
	@JsonProperty("telephone")
	private String telephone;
	
	@JsonProperty("mobile")
	private String mobile;
	
	@JsonProperty("email")
	private String email;
	
	@JsonProperty("qq_no")
	private String qqNo;
	
	@JsonProperty("weixin_no")
	private String weixinNo;
	
	@JsonProperty("legal_name")
	private String legalName;
	
	@JsonProperty("legal_certificates_no")
	private String legalCertificatesNo;
	
	@JsonProperty("points_from")
	private Integer pointsFrom;
	
	@JsonProperty("points_to")
	private Integer pointsTo;
	
	@JsonProperty("rank_from")
	private Integer rankFrom;
	
	@JsonProperty("rank_to")
	private Integer rankTo;
	
	@JsonProperty("audit_phase_from")
	private Integer auditPhaseFrom;
	
	@JsonProperty("audit_phase_to")
	private Integer auditPhaseTo;
	
	@JsonProperty("audit_result_from")
	private Integer auditResultFrom;
	
	@JsonProperty("audit_result_to")
	private Integer auditResultTo;
	
	@JsonProperty("certificates_uploaded")
	private Boolean certificatesUploaded;

	public String getMemberLoginName() {
		return memberLoginName;
	}

	public void setMemberLoginName(String memberLoginName) {
		this.memberLoginName = memberLoginName;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public Integer getEnterpriseProperty() {
		return enterpriseProperty;
	}

	public void setEnterpriseProperty(Integer enterpriseProperty) {
		this.enterpriseProperty = enterpriseProperty;
	}

	public Integer getEnterpriseCategory() {
		return enterpriseCategory;
	}

	public void setEnterpriseCategory(Integer enterpriseCategory) {
		this.enterpriseCategory = enterpriseCategory;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactCertificatesNo() {
		return contactCertificatesNo;
	}

	public void setContactCertificatesNo(String contactCertificatesNo) {
		this.contactCertificatesNo = contactCertificatesNo;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getQqNo() {
		return qqNo;
	}

	public void setQqNo(String qqNo) {
		this.qqNo = qqNo;
	}

	public String getWeixinNo() {
		return weixinNo;
	}

	public void setWeixinNo(String weixinNo) {
		this.weixinNo = weixinNo;
	}

	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public String getLegalCertificatesNo() {
		return legalCertificatesNo;
	}

	public void setLegalCertificatesNo(String legalCertificatesNo) {
		this.legalCertificatesNo = legalCertificatesNo;
	}

	public Integer getPointsFrom() {
		return pointsFrom;
	}

	public void setPointsFrom(Integer pointsFrom) {
		this.pointsFrom = pointsFrom;
	}

	public Integer getPointsTo() {
		return pointsTo;
	}

	public void setPointsTo(Integer pointsTo) {
		this.pointsTo = pointsTo;
	}

	public Integer getRankFrom() {
		return rankFrom;
	}

	public void setRankFrom(Integer rankFrom) {
		this.rankFrom = rankFrom;
	}

	public Integer getRankTo() {
		return rankTo;
	}

	public void setRankTo(Integer rankTo) {
		this.rankTo = rankTo;
	}

	public Integer getAuditPhaseFrom() {
		return auditPhaseFrom;
	}

	public void setAuditPhaseFrom(Integer auditPhaseFrom) {
		this.auditPhaseFrom = auditPhaseFrom;
	}

	public Integer getAuditPhaseTo() {
		return auditPhaseTo;
	}

	public void setAuditPhaseTo(Integer auditPhaseTo) {
		this.auditPhaseTo = auditPhaseTo;
	}

	public Integer getAuditResultFrom() {
		return auditResultFrom;
	}

	public void setAuditResultFrom(Integer auditResultFrom) {
		this.auditResultFrom = auditResultFrom;
	}

	public Integer getAuditResultTo() {
		return auditResultTo;
	}

	public void setAuditResultTo(Integer auditResultTo) {
		this.auditResultTo = auditResultTo;
	}

	public Boolean getCertificatesUploaded() {
		return certificatesUploaded;
	}

	public void setCertificatesUploaded(Boolean certificatesUploaded) {
		this.certificatesUploaded = certificatesUploaded;
	}

}
