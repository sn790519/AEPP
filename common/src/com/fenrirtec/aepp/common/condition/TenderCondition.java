package com.fenrirtec.aepp.common.condition;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fenrirtec.core.condition.BaseCondition;

public class TenderCondition extends BaseCondition implements Serializable {

	private static final long serialVersionUID = 1L;

    @JsonProperty("tender_title")
    private String tenderTitle;

    @JsonProperty("enroll_start_date_from")
    private String enrollStartDateFrom;

    @JsonProperty("enroll_start_date_to")
    private String enrollStartDateTo;

    @JsonProperty("enroll_end_date_from")
    private String enrollEndDateFrom;

    @JsonProperty("enroll_end_date_to")
    private String enrollEndDateTo;

    @JsonProperty("tender_end_date_from")
    private String tenderEndDateFrom;

    @JsonProperty("tender_end_date_to")
    private String tenderEndDateTo;

    @JsonProperty("publish_result_date_from")
    private String publishResultDateFrom;

    @JsonProperty("publish_result_date_to")
    private String publishResultDateTo;

    @JsonProperty("tender_pattern")
    private Integer tenderPattern;

    @JsonProperty("status_from")
    private Integer statusFrom;

    @JsonProperty("status_to")
    private Integer statusTo;

    public String getTenderTitle() {
        return tenderTitle;
    }

    public void setTenderTitle(String tenderTitle) {
        this.tenderTitle = tenderTitle;
    }

    public String getEnrollStartDateFrom() {
        return enrollStartDateFrom;
    }

    public void setEnrollStartDateFrom(String enrollStartDateFrom) {
        this.enrollStartDateFrom = enrollStartDateFrom;
    }

    public String getEnrollStartDateTo() {
        return enrollStartDateTo;
    }

    public void setEnrollStartDateTo(String enrollStartDateTo) {
        this.enrollStartDateTo = enrollStartDateTo;
    }

    public String getEnrollEndDateFrom() {
        return enrollEndDateFrom;
    }

    public void setEnrollEndDateFrom(String enrollEndDateFrom) {
        this.enrollEndDateFrom = enrollEndDateFrom;
    }

    public String getEnrollEndDateTo() {
        return enrollEndDateTo;
    }

    public void setEnrollEndDateTo(String enrollEndDateTo) {
        this.enrollEndDateTo = enrollEndDateTo;
    }

    public String getTenderEndDateFrom() {
        return tenderEndDateFrom;
    }

    public void setTenderEndDateFrom(String tenderEndDateFrom) {
        this.tenderEndDateFrom = tenderEndDateFrom;
    }

    public String getTenderEndDateTo() {
        return tenderEndDateTo;
    }

    public void setTenderEndDateTo(String tenderEndDateTo) {
        this.tenderEndDateTo = tenderEndDateTo;
    }

    public String getPublishResultDateFrom() {
        return publishResultDateFrom;
    }

    public void setPublishResultDateFrom(String publishResultDateFrom) {
        this.publishResultDateFrom = publishResultDateFrom;
    }

    public String getPublishResultDateTo() {
        return publishResultDateTo;
    }

    public void setPublishResultDateTo(String publishResultDateTo) {
        this.publishResultDateTo = publishResultDateTo;
    }

    public Integer getTenderPattern() {
        return tenderPattern;
    }

    public void setTenderPattern(Integer tenderPattern) {
        this.tenderPattern = tenderPattern;
    }

    public Integer getStatusFrom() {
        return statusFrom;
    }

    public void setStatusFrom(Integer statusFrom) {
        this.statusFrom = statusFrom;
    }

    public Integer getStatusTo() {
        return statusTo;
    }

    public void setStatusTo(Integer statusTo) {
        this.statusTo = statusTo;
    }
    
}
