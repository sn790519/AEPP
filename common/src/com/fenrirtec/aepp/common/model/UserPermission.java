package com.fenrirtec.aepp.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fenrirtec.core.model.BaseModel;

@Entity
public class UserPermission extends BaseModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name="login_name")
	private String loginName;

	@Column(name="function_name")
	private String functionName;

	@Column(name="readable")
	private Boolean readable;

    @Column(name="addable")
    private Boolean addable;

    @Column(name="editable")
    private Boolean editable;

    @Column(name="deleteable")
    private Boolean deleteable;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public Boolean getReadable() {
        return readable;
    }

    public void setReadable(Boolean readable) {
        this.readable = readable;
    }

    public Boolean getAddable() {
        return addable;
    }

    public void setAddable(Boolean addable) {
        this.addable = addable;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public Boolean getDeleteable() {
        return deleteable;
    }

    public void setDeleteable(Boolean deleteable) {
        this.deleteable = deleteable;
    }

}
