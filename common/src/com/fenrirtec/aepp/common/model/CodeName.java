package com.fenrirtec.aepp.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fenrirtec.core.model.BaseModel;

@Entity
public class CodeName extends BaseModel implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name = "type_id")
	private String typeId;

	@Column(name = "code")
	private Integer code;

	@Column(name = "name")
	private String name;

	public CodeName() {
	}

	public String getTypeId() {
		return this.typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public Integer getCode() {
		return this.code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}