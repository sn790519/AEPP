package com.fenrirtec.aepp.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fenrirtec.core.model.BaseModel;

@Entity
public class AuditFlow extends BaseModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "member_login_name")
	private String memberLoginName;
	
	@Column(name = "audit_sequence")
	private Integer auditSequence;
	
	@Column(name = "audit_date")
	private String auditDate;
	
	@Column(name = "audit_phase")
	private Integer auditPhase;
	
	@Column(name = "audit_result")
	private Integer auditResult;
	
	@Column(name = "audit_comment")
	private String auditComment;
	
	@Column(name = "login_name")
	private String loginName;
	
	@Column(name = "audit_phase_name")
	private String auditPhaseName;
	
	@Column(name = "audit_result_name")
	private String auditResultName;

	public String getMemberLoginName() {
		return memberLoginName;
	}

	public void setMemberLoginName(String memberLoginName) {
		this.memberLoginName = memberLoginName;
	}

	public Integer getAuditSequence() {
		return auditSequence;
	}

	public void setAuditSequence(Integer auditSequence) {
		this.auditSequence = auditSequence;
	}

	public String getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(String auditDate) {
		this.auditDate = auditDate;
	}

	public Integer getAuditPhase() {
		return auditPhase;
	}

	public void setAuditPhase(Integer auditPhase) {
		this.auditPhase = auditPhase;
	}

	public Integer getAuditResult() {
		return auditResult;
	}

	public void setAuditResult(Integer auditResult) {
		this.auditResult = auditResult;
	}

	public String getAuditComment() {
		return auditComment;
	}

	public void setAuditComment(String auditComment) {
		this.auditComment = auditComment;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getAuditPhaseName() {
		return auditPhaseName;
	}

	public void setAuditPhaseName(String auditPhaseName) {
		this.auditPhaseName = auditPhaseName;
	}

	public String getAuditResultName() {
		return auditResultName;
	}

	public void setAuditResultName(String auditResultName) {
		this.auditResultName = auditResultName;
	}
}
