package com.fenrirtec.aepp.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fenrirtec.core.model.BaseModel;

@Entity
public class TenderBidlist extends BaseModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name="tender_id")
	private Integer tenderId;

	@Column(name="member_login_name")
	private String memberLoginName;

	@Column(name="enterprise_name")
	private String enterpriseName;

	//企业类型名称
	@Column(name="enterprise_category_name")
	private String enterpriseCategoryName;

	@Column(name="area_code")
	private String areaCode;

	//会员级别名称
	@Column(name="rank_name")
	private String rankName;
	
	@Column(name="total")
	private double total;
	
	//报名文件
	@Column(name="path")
	private String path;

	//状态显示文字
	@Column(name="status_name")
	private String statusName;

	public Integer getTenderId() {
		return tenderId;
	}

	public void setTenderId(Integer tenderId) {
		this.tenderId = tenderId;
	}

	public String getMemberLoginName() {
		return memberLoginName;
	}

	public void setMemberLoginName(String memberLoginName) {
		this.memberLoginName = memberLoginName;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public String getEnterpriseCategoryName() {
		return enterpriseCategoryName;
	}

	public void setEnterpriseCategoryName(String enterpriseCategoryName) {
		this.enterpriseCategoryName = enterpriseCategoryName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getRankName() {
		return rankName;
	}

	public void setRankName(String rankName) {
		this.rankName = rankName;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
