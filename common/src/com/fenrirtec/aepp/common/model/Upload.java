package com.fenrirtec.aepp.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fenrirtec.core.model.BaseModel;

@Entity
public class Upload extends BaseModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "upload_id")
	Integer uploadId;
	
	@Column(name = "upload_person")
	String uploadPerson;
	
	@Column(name = "upload_person_type")
	Integer uploadPersonType;
	
	@Column(name = "upload_purpose")
	String uploadPurpose;
	
	@Column(name = "upload_datetime")
	String uploadDatetime;
	
	@Column(name = "upload_file_path")
	String uploadFilePath;
	
	@Column(name = "upload_file_type")
	Integer uploadFileType;

	public Integer getUploadId() {
		return uploadId;
	}

	public void setUploadId(Integer uploadId) {
		this.uploadId = uploadId;
	}

	public String getUploadPerson() {
		return uploadPerson;
	}

	public void setUploadPerson(String uploadPerson) {
		this.uploadPerson = uploadPerson;
	}

	public Integer getUploadPersonType() {
		return uploadPersonType;
	}

	public void setUploadPersonType(Integer uploadPersonType) {
		this.uploadPersonType = uploadPersonType;
	}

	public String getUploadPurpose() {
		return uploadPurpose;
	}

	public void setUploadPurpose(String uploadPurpose) {
		this.uploadPurpose = uploadPurpose;
	}

	public String getUploadDatetime() {
		return uploadDatetime;
	}

	public void setUploadDatetime(String uploadDatetime) {
		this.uploadDatetime = uploadDatetime;
	}

	public String getUploadFilePath() {
		return uploadFilePath;
	}

	public void setUploadFilePath(String uploadFilePath) {
		this.uploadFilePath = uploadFilePath;
	}

	public Integer getUploadFileType() {
		return uploadFileType;
	}

	public void setUploadFileType(Integer uploadFileType) {
		this.uploadFileType = uploadFileType;
	}
}
