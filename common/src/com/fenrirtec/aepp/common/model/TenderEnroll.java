package com.fenrirtec.aepp.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fenrirtec.core.model.BaseModel;

@Entity
public class TenderEnroll extends BaseModel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="tender_id")
	private Integer tenderId;
	
	@Column(name="member_login_name")
	private String memberLoginName;

	@Column(name="enroll_date")
	private String enrollDate;

	@Column(name="description")
	private String description;

	@Column(name="finalist_flag")
	private Boolean finalistFlag;

    @Column(name="invite_flag")
    private Boolean inviteFlag;

    public Integer getTenderId() {
        return tenderId;
    }

    public void setTenderId(Integer tenderId) {
        this.tenderId = tenderId;
    }

    public String getMemberLoginName() {
        return memberLoginName;
    }

    public void setMemberLoginName(String memberLoginName) {
        this.memberLoginName = memberLoginName;
    }

    public String getEnrollDate() {
        return enrollDate;
    }

    public void setEnrollDate(String enrollDate) {
        this.enrollDate = enrollDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getFinalistFlag() {
        return finalistFlag;
    }

    public void setFinalistFlag(Boolean finalistFlag) {
        this.finalistFlag = finalistFlag;
    }

    public Boolean getInviteFlag() {
        return inviteFlag;
    }

    public void setInviteFlag(Boolean inviteFlag) {
        this.inviteFlag = inviteFlag;
    }

}
