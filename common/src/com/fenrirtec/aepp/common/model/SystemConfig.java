package com.fenrirtec.aepp.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fenrirtec.core.model.BaseModel;

@Entity
public class SystemConfig extends BaseModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name="password")
	private String password;
	
	@Column(name="change_password")
	private Boolean changePassword;
	
	@Column(name="regist_enabled")
	private Boolean registEnabled;
	
	@Column(name="news_count")
	private Integer newsCount;
	
	@Column(name="tender_count")
	private Integer tenderCount;
	
	@Column(name="tender_sort")
	private Integer tenderSort;
	
	@Column(name="member_count")
	private Integer memberCount;
	
	@Column(name="contact_copy_required")
	private Boolean contactCopyRequired;
	
	@Column(name="license_copy_required")
	private Boolean licenseCopyRequired;
	
	@Column(name="legal_copy_required")
	private Boolean legalCopyRequired;
	
	@Column(name="physical_delete")
	private Boolean physicalDelete;
	
	@Column(name="upload_root_folder")
	private String uploadRootFolder;
	
	@Column(name="qualifications_save_folder")
	private String qualificationsSaveFolder;
	
	@Column(name="images_save_folder")
	private String imagesSaveFolder;
	
	@Column(name="upload_temp_folder")
	private String uploadTempFolder;
	
	@Column(name="allowed_image_extension")
	private String allowedImageExtension;
	
	@Column(name="allowed_image_size")
	private Long allowedImageSize;
	
	@Column(name="allowed_doc_extension")
	private String allowedDocExtension;
	
	@Column(name="allowed_doc_size")
	private Long allowedDocSize;
	
	@Column(name="allowed_compress_extension")
	private String allowedCompressExtension;
	
	@Column(name="allowed_compress_size")
	private Long allowedCompressSize;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getChangePassword() {
		return changePassword;
	}

	public void setChangePassword(Boolean changePassword) {
		this.changePassword = changePassword;
	}

	public Boolean getRegistEnabled() {
		return registEnabled;
	}

	public void setRegistEnabled(Boolean registEnabled) {
		this.registEnabled = registEnabled;
	}

	public Integer getNewsCount() {
		return newsCount;
	}

	public void setNewsCount(Integer newsCount) {
		this.newsCount = newsCount;
	}

	public Integer getTenderCount() {
		return tenderCount;
	}

	public void setTenderCount(Integer tenderCount) {
		this.tenderCount = tenderCount;
	}

	public Integer getTenderSort() {
		return tenderSort;
	}

	public void setTenderSort(Integer tenderSort) {
		this.tenderSort = tenderSort;
	}

	public Integer getMemberCount() {
		return memberCount;
	}

	public void setMemberCount(Integer memberCount) {
		this.memberCount = memberCount;
	}

	public Boolean getContactCopyRequired() {
		return contactCopyRequired;
	}

	public void setContactCopyRequired(Boolean contactCopyRequired) {
		this.contactCopyRequired = contactCopyRequired;
	}

	public Boolean getLicenseCopyRequired() {
		return licenseCopyRequired;
	}

	public void setLicenseCopyRequired(Boolean licenseCopyRequired) {
		this.licenseCopyRequired = licenseCopyRequired;
	}

	public Boolean getLegalCopyRequired() {
		return legalCopyRequired;
	}

	public void setLegalCopyRequired(Boolean legalCopyRequired) {
		this.legalCopyRequired = legalCopyRequired;
	}

	public Boolean getPhysicalDelete() {
		return physicalDelete;
	}

	public void setPhysicalDelete(Boolean physicalDelete) {
		this.physicalDelete = physicalDelete;
	}

	public String getUploadRootFolder() {
		return uploadRootFolder;
	}

	public void setUploadRootFolder(String uploadRootFolder) {
		this.uploadRootFolder = uploadRootFolder;
	}

	public String getQualificationsSaveFolder() {
		return qualificationsSaveFolder;
	}

	public void setQualificationsSaveFolder(String qualificationsSaveFolder) {
		this.qualificationsSaveFolder = qualificationsSaveFolder;
	}

	public String getImagesSaveFolder() {
		return imagesSaveFolder;
	}

	public void setImagesSaveFolder(String imagesSaveFolder) {
		this.imagesSaveFolder = imagesSaveFolder;
	}

	public String getUploadTempFolder() {
		return uploadTempFolder;
	}

	public void setUploadTempFolder(String uploadTempFolder) {
		this.uploadTempFolder = uploadTempFolder;
	}

	public String getAllowedImageExtension() {
		return allowedImageExtension;
	}

	public void setAllowedImageExtension(String allowedImageExtension) {
		this.allowedImageExtension = allowedImageExtension;
	}

	public Long getAllowedImageSize() {
		return allowedImageSize;
	}

	public void setAllowedImageSize(Long allowedImageSize) {
		this.allowedImageSize = allowedImageSize;
	}

	public String getAllowedDocExtension() {
		return allowedDocExtension;
	}

	public void setAllowedDocExtension(String allowedDocExtension) {
		this.allowedDocExtension = allowedDocExtension;
	}

	public Long getAllowedDocSize() {
		return allowedDocSize;
	}

	public void setAllowedDocSize(Long allowedDocSize) {
		this.allowedDocSize = allowedDocSize;
	}

	public String getAllowedCompressExtension() {
		return allowedCompressExtension;
	}

	public void setAllowedCompressExtension(String allowedCompressExtension) {
		this.allowedCompressExtension = allowedCompressExtension;
	}

	public Long getAllowedCompressSize() {
		return allowedCompressSize;
	}

	public void setAllowedCompressSize(Long allowedCompressSize) {
		this.allowedCompressSize = allowedCompressSize;
	}

}
