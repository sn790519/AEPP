package com.fenrirtec.aepp.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fenrirtec.core.model.BaseModel;

@Entity
public class Attachment extends BaseModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "attachment_id")
	private Integer attachmentId;
	
	@Column(name = "file_type")
	private Integer fileType;
	
	@Column(name = "path")
	private String path;
	
	@Column(name = "comment")
	private String comment;
	
	@Column(name = "upload_user")
	private String uploadUser;
	
	@Column(name = "upload_date")
	private String uploadDate;

	public Integer getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(Integer attachmentId) {
		this.attachmentId = attachmentId;
	}

	public Integer getFileType() {
		return fileType;
	}

	public void setFileType(Integer fileType) {
		this.fileType = fileType;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getUploadUser() {
		return uploadUser;
	}

	public void setUploadUser(String uploadUser) {
		this.uploadUser = uploadUser;
	}

	public String getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}
}
