package com.fenrirtec.aepp.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fenrirtec.core.model.BaseModel;

@Entity
public class TenderInformation extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "tender_title")
    private String tenderTitle;
    
    @Column(name = "enroll_start_date")
    private String enrollStartDate;
    
    @Column(name = "enroll_end_date")
    private String enrollEndDate;

    @Column(name = "tender_end_date")
    private String tenderEndDate;

    @Column(name = "publish_result_date")
    private String publishResultDate;

    @Column(name = "content_description")
    private String contentDescription;

    @Column(name = "vendor_requirement")
    private String vendorRequirement;

    @Column(name = "tender_pattern")
    private Integer tenderPattern;

    @Column(name = "tender_template")
    private Integer tenderTemplate;

    @Column(name = "contact_name")
    private String contactName;

    @Column(name = "contact_tel")
    private String contactTel;
    
    @Column(name = "tender_pattern_name")
    private String tenderPatternName;
    
    @Column(name = "status_name")
    private String statusName;

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getTenderTitle() {
        return tenderTitle;
    }

    public void setTenderTitle(String tenderTitle) {
        this.tenderTitle = tenderTitle;
    }

    public String getEnrollStartDate() {
		return enrollStartDate;
	}

	public void setEnrollStartDate(String enrollStartDate) {
		this.enrollStartDate = enrollStartDate;
	}

	public String getEnrollEndDate() {
        return enrollEndDate;
    }

    public void setEnrollEndDate(String enrollEndDate) {
        this.enrollEndDate = enrollEndDate;
    }

    public String getTenderEndDate() {
        return tenderEndDate;
    }

    public void setTenderEndDate(String tenderEndDate) {
        this.tenderEndDate = tenderEndDate;
    }

    public String getPublishResultDate() {
        return publishResultDate;
    }

    public void setPublishResultDate(String publishResultDate) {
        this.publishResultDate = publishResultDate;
    }

    public String getContentDescription() {
        return contentDescription;
    }

    public void setContentDescription(String contentDescription) {
        this.contentDescription = contentDescription;
    }

    public String getVendorRequirement() {
        return vendorRequirement;
    }

    public void setVendorRequirement(String vendorRequirement) {
        this.vendorRequirement = vendorRequirement;
    }

    public Integer getTenderPattern() {
        return tenderPattern;
    }

    public void setTenderPattern(Integer tenderPattern) {
        this.tenderPattern = tenderPattern;
    }

    public Integer getTenderTemplate() {
        return tenderTemplate;
    }

    public void setTenderTemplate(Integer tenderTemplate) {
        this.tenderTemplate = tenderTemplate;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactTel() {
        return contactTel;
    }

    public void setContactTel(String contactTel) {
        this.contactTel = contactTel;
    }

	public String getTenderPatternName() {
		return tenderPatternName;
	}

	public void setTenderPatternName(String tenderPatternName) {
		this.tenderPatternName = tenderPatternName;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
    
}
