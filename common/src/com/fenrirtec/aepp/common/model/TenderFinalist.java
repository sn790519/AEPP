package com.fenrirtec.aepp.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fenrirtec.core.model.BaseModel;

@Entity
public class TenderFinalist extends BaseModel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="tender_id")
	private Integer tenderId;
	
	@Column(name="member_login_name")
	private String memberLoginName;

	@Column(name="finalist_date")
	private String finalistDate;

	@Column(name="evaluation")
	private String evaluation;

	@Column(name="quote_flag")
	private Boolean quoteFlag;

    @Column(name="invite_flag")
    private Boolean inviteFlag;

    public Integer getTenderId() {
        return tenderId;
    }

    public void setTenderId(Integer tenderId) {
        this.tenderId = tenderId;
    }

    public String getMemberLoginName() {
        return memberLoginName;
    }

    public void setMemberLoginName(String memberLoginName) {
        this.memberLoginName = memberLoginName;
    }

    public String getFinalistDate() {
        return finalistDate;
    }

    public void setFinalistDate(String finalistDate) {
        this.finalistDate = finalistDate;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    public Boolean getQuoteFlag() {
        return quoteFlag;
    }

    public void setQuoteFlag(Boolean quoteFlag) {
        this.quoteFlag = quoteFlag;
    }

    public Boolean getInviteFlag() {
        return inviteFlag;
    }

    public void setInviteFlag(Boolean inviteFlag) {
        this.inviteFlag = inviteFlag;
    }

}
