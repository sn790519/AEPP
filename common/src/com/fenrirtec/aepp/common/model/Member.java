package com.fenrirtec.aepp.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fenrirtec.core.model.BaseModel;

@Entity
public class Member extends BaseModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "member_login_name")
	private String memberLoginName;

	@Column(name = "member_login_password")
	private String memberLoginPassword;

	@Column(name = "license_number")
	private String licenseNumber;

	@Column(name = "enterprise_name")
	private String enterpriseName;

	@Column(name = "enterprise_property")
	private Integer enterpriseProperty;

	@Column(name = "enterprise_property_name")
	private String enterprisePropertyName;

	@Column(name = "enterprise_category")
	private Integer enterpriseCategory;

	@Column(name = "enterprise_category_name")
	private String enterpriseCategoryName;

	@Column(name = "contact_name")
	private String contactName;

	@Column(name = "contact_certificates_type")
	private Integer contactCertificatesType;

	@Column(name = "contact_certificates_no")
	private String contactCertificatesNo;

	@Column(name = "area_code")
	private String areaCode;

	@Column(name = "telephone")
	private String telephone;

	@Column(name = "mobile")
	private String mobile;

	@Column(name = "mobile_certificated")
	private Boolean mobileCertificated;

	@Column(name = "email")
	private String email;

	@Column(name = "email_certificated")
	private Boolean emailCertificated;

	@Column(name = "qq_no")
	private String qqNo;

	@Column(name = "weixin_no")
	private String weixinNo;

	@Column(name = "legal_name")
	private String legalName;

	@Column(name = "legal_certificates_type")
	private Integer legalCertificatesType;

	@Column(name = "legal_certificates_no")
	private String legalCertificatesNo;

	@Column(name = "image_contact_certificates_front")
	private Integer imageContactCertificatesFront;

	@Column(name = "image_contact_certificates_back")
	private Integer imageContactCertificatesBack;

	@Column(name = "image_legal_certificates_front")
	private Integer imageLegalCertificatesFront;

	@Column(name = "image_legal_certificates_back")
	private Integer imageLegalCertificatesBack;

	@Column(name = "image_license")
	private Integer imageLicense;

	@Column(name = "points")
	private Integer points;

	@Column(name = "rank")
	private Integer rank;

	@Column(name = "rank_name")
	private String rankName;
	
	@Column(name = "lastest_audit_sequence")
	private Integer lastestAuditSequence;
	
	@Column(name = "lastest_audit_date")
	private String lastestAuditDate;
	
	@Column(name = "lastest_audit_phase")
	private Integer lastestAuditPhase;
	
	@Column(name = "lastest_audit_phase_name")
	private String lastestAuditPhaseName;
	
	@Column(name = "lastest_audit_result")
	private Integer lastestAuditResult;
	
	@Column(name = "lastest_audit_result_name")
	private String lastestAuditResultName;
	
	@Column(name = "lastest_audit_comment")
	private String lastestAuditComment;
	
	@Column(name = "lastest_audit_login_name")
	private String lastestAuditLoginName;

	public String getMemberLoginName() {
		return memberLoginName;
	}

	public void setMemberLoginName(String memberLoginName) {
		this.memberLoginName = memberLoginName;
	}

	public String getMemberLoginPassword() {
		return memberLoginPassword;
	}

	public void setMemberLoginPassword(String memberLoginPassword) {
		this.memberLoginPassword = memberLoginPassword;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public Integer getEnterpriseProperty() {
		return enterpriseProperty;
	}

	public void setEnterpriseProperty(Integer enterpriseProperty) {
		this.enterpriseProperty = enterpriseProperty;
	}

	public String getEnterprisePropertyName() {
		return enterprisePropertyName;
	}

	public void setEnterprisePropertyName(String enterprisePropertyName) {
		this.enterprisePropertyName = enterprisePropertyName;
	}

	public Integer getEnterpriseCategory() {
		return enterpriseCategory;
	}

	public void setEnterpriseCategory(Integer enterpriseCategory) {
		this.enterpriseCategory = enterpriseCategory;
	}

	public String getEnterpriseCategoryName() {
		return enterpriseCategoryName;
	}

	public void setEnterpriseCategoryName(String enterpriseCategoryName) {
		this.enterpriseCategoryName = enterpriseCategoryName;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public Integer getContactCertificatesType() {
		return contactCertificatesType;
	}

	public void setContactCertificatesType(Integer contactCertificatesType) {
		this.contactCertificatesType = contactCertificatesType;
	}

	public String getContactCertificatesNo() {
		return contactCertificatesNo;
	}

	public void setContactCertificatesNo(String contactCertificatesNo) {
		this.contactCertificatesNo = contactCertificatesNo;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Boolean getMobileCertificated() {
		return mobileCertificated;
	}

	public void setMobileCertificated(Boolean mobileCertificated) {
		this.mobileCertificated = mobileCertificated;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getEmailCertificated() {
		return emailCertificated;
	}

	public void setEmailCertificated(Boolean emailCertificated) {
		this.emailCertificated = emailCertificated;
	}

	public String getQqNo() {
		return qqNo;
	}

	public void setQqNo(String qqNo) {
		this.qqNo = qqNo;
	}

	public String getWeixinNo() {
		return weixinNo;
	}

	public void setWeixinNo(String weixinNo) {
		this.weixinNo = weixinNo;
	}

	public String getLegalName() {
		return legalName;
	}

	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	public Integer getLegalCertificatesType() {
		return legalCertificatesType;
	}

	public void setLegalCertificatesType(Integer legalCertificatesType) {
		this.legalCertificatesType = legalCertificatesType;
	}

	public String getLegalCertificatesNo() {
		return legalCertificatesNo;
	}

	public void setLegalCertificatesNo(String legalCertificatesNo) {
		this.legalCertificatesNo = legalCertificatesNo;
	}

	public Integer getImageContactCertificatesFront() {
		return imageContactCertificatesFront;
	}

	public void setImageContactCertificatesFront(
			Integer imageContactCertificatesFront) {
		this.imageContactCertificatesFront = imageContactCertificatesFront;
	}

	public Integer getImageContactCertificatesBack() {
		return imageContactCertificatesBack;
	}

	public void setImageContactCertificatesBack(
			Integer imageContactCertificatesBack) {
		this.imageContactCertificatesBack = imageContactCertificatesBack;
	}

	public Integer getImageLegalCertificatesFront() {
		return imageLegalCertificatesFront;
	}

	public void setImageLegalCertificatesFront(
			Integer imageLegalCertificatesFront) {
		this.imageLegalCertificatesFront = imageLegalCertificatesFront;
	}

	public Integer getImageLegalCertificatesBack() {
		return imageLegalCertificatesBack;
	}

	public void setImageLegalCertificatesBack(Integer imageLegalCertificatesBack) {
		this.imageLegalCertificatesBack = imageLegalCertificatesBack;
	}

	public Integer getImageLicense() {
		return imageLicense;
	}

	public void setImageLicense(Integer imageLicense) {
		this.imageLicense = imageLicense;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getRankName() {
		return rankName;
	}

	public void setRankName(String rankName) {
		this.rankName = rankName;
	}

	public Integer getLastestAuditSequence() {
		return lastestAuditSequence;
	}

	public void setLastestAuditSequence(Integer lastestAuditSequence) {
		this.lastestAuditSequence = lastestAuditSequence;
	}

	public String getLastestAuditDate() {
		return lastestAuditDate;
	}

	public void setLastestAuditDate(String lastestAuditDate) {
		this.lastestAuditDate = lastestAuditDate;
	}

	public Integer getLastestAuditPhase() {
		return lastestAuditPhase;
	}

	public void setLastestAuditPhase(Integer lastestAuditPhase) {
		this.lastestAuditPhase = lastestAuditPhase;
	}

	public String getLastestAuditPhaseName() {
		return lastestAuditPhaseName;
	}

	public void setLastestAuditPhaseName(String lastestAuditPhaseName) {
		this.lastestAuditPhaseName = lastestAuditPhaseName;
	}

	public Integer getLastestAuditResult() {
		return lastestAuditResult;
	}

	public void setLastestAuditResult(Integer lastestAuditResult) {
		this.lastestAuditResult = lastestAuditResult;
	}

	public String getLastestAuditResultName() {
		return lastestAuditResultName;
	}

	public void setLastestAuditResultName(String lastestAuditResultName) {
		this.lastestAuditResultName = lastestAuditResultName;
	}

	public String getLastestAuditComment() {
		return lastestAuditComment;
	}

	public void setLastestAuditComment(String lastestAuditComment) {
		this.lastestAuditComment = lastestAuditComment;
	}

	public String getLastestAuditLoginName() {
		return lastestAuditLoginName;
	}

	public void setLastestAuditLoginName(String lastestAuditLoginName) {
		this.lastestAuditLoginName = lastestAuditLoginName;
	}

}
