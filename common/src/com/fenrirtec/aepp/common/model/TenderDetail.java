package com.fenrirtec.aepp.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fenrirtec.core.model.BaseModel;

@Entity
public class TenderDetail extends BaseModel implements Serializable {
    
	private static final long serialVersionUID = 1L;

    @Column(name="tender_id")
    private Integer tenderId;

    @Column(name="tender_no")
    private String tenderNo;

    @Column(name="status")
    private Integer status;

    @Column(name="status_name")
    private String statusName;

	@Column(name="tender_title")
	private String tenderTitle;

	@Column(name="enroll_start_date")
	private String enrollStartDate;

    @Column(name="enroll_end_date")
    private String enrollEndDate;

	@Column(name="tender_end_date")
	private String tenderEndDate;

	@Column(name="publish_result_date")
	private String publishResultDate;

	@Column(name="content_description")
	private String contentDescription;

	@Column(name="vendor_requirement")
	private String vendorRequirement;

	@Column(name="tender_pattern")
	private Integer tenderPattern;

    @Column(name="tender_pattern_name")
    private String tenderPatternName;

	@Column(name="tender_template")
	private Integer tenderTemplate;

	@Column(name="contact_name")
	private String contactName;

	@Column(name="contact_tel")
	private String contactTel;

	@Column(name="finalist_notice_id")
	private Integer finalistNoticeId;

	@Column(name="bid_notice_id")
	private Integer bidNoticeId;

    @Column(name="enroll_count")
    private Integer enrollCount;

    @Column(name="finalist_count")
    private Integer finalistCount;

    @Column(name="quote_count")
    private Integer quoteCount;

    public Integer getTenderId() {
        return tenderId;
    }

    public void setTenderId(Integer tenderId) {
        this.tenderId = tenderId;
    }

    public String getTenderNo() {
        return tenderNo;
    }

    public void setTenderNo(String tenderNo) {
        this.tenderNo = tenderNo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getTenderTitle() {
        return tenderTitle;
    }

    public void setTenderTitle(String tenderTitle) {
        this.tenderTitle = tenderTitle;
    }

    public String getEnrollEndDate() {
        return enrollEndDate;
    }

    public String getEnrollStartDate() {
        return enrollStartDate;
    }

    public void setEnrollStartDate(String enrollStartDate) {
        this.enrollStartDate = enrollStartDate;
    }

    public void setEnrollEndDate(String enrollEndDate) {
        this.enrollEndDate = enrollEndDate;
    }

    public String getTenderEndDate() {
        return tenderEndDate;
    }

    public void setTenderEndDate(String tenderEndDate) {
        this.tenderEndDate = tenderEndDate;
    }

    public String getPublishResultDate() {
        return publishResultDate;
    }

    public void setPublishResultDate(String publishResultDate) {
        this.publishResultDate = publishResultDate;
    }

    public String getContentDescription() {
        return contentDescription;
    }

    public void setContentDescription(String contentDescription) {
        this.contentDescription = contentDescription;
    }

    public String getVendorRequirement() {
        return vendorRequirement;
    }

    public void setVendorRequirement(String vendorRequirement) {
        this.vendorRequirement = vendorRequirement;
    }

    public Integer getTenderPattern() {
        return tenderPattern;
    }

    public void setTenderPattern(Integer tenderPattern) {
        this.tenderPattern = tenderPattern;
    }

    public String getTenderPatternName() {
        return tenderPatternName;
    }

    public void setTenderPatternName(String tenderPatternName) {
        this.tenderPatternName = tenderPatternName;
    }

    public Integer getTenderTemplate() {
        return tenderTemplate;
    }

    public void setTenderTemplate(Integer tenderTemplate) {
        this.tenderTemplate = tenderTemplate;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactTel() {
        return contactTel;
    }

    public void setContactTel(String contactTel) {
        this.contactTel = contactTel;
    }

    public Integer getFinalistNoticeId() {
		return finalistNoticeId;
	}

	public void setFinalistNoticeId(Integer finalistNoticeId) {
		this.finalistNoticeId = finalistNoticeId;
	}

	public Integer getBidNoticeId() {
		return bidNoticeId;
	}

	public void setBidNoticeId(Integer bidNoticeId) {
		this.bidNoticeId = bidNoticeId;
	}

    public Integer getEnrollCount() {
        return enrollCount;
    }

    public void setEnrollCount(Integer enrollCount) {
        this.enrollCount = enrollCount;
    }

    public Integer getFinalistCount() {
        return finalistCount;
    }

    public void setFinalistCount(Integer finalistCount) {
        this.finalistCount = finalistCount;
    }

    public Integer getQuoteCount() {
        return quoteCount;
    }

    public void setQuoteCount(Integer quoteCount) {
        this.quoteCount = quoteCount;
    }

}
