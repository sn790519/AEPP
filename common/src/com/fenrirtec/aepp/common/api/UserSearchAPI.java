package com.fenrirtec.aepp.common.api;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.UserDto;
import com.fenrirtec.aepp.common.service.UserManageService;
import com.fenrirtec.aepp.common.service.impl.UserManageServiceImpl;
import com.fenrirtec.aepp.common.support.JSONActionSupport;
import com.fenrirtec.core.definition.BaseDefinition.API;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.JsonUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class UserSearchAPI extends JSONActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(UserSearchAPI.class);

	public UserSearchAPI() {
	}
	
	private String param;

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	@Override
	protected List<ErrorInfo> doValidate() {
		return null;
	}

	@Override
	protected JSONObject doExecute() {
		
		UserManageService service = new UserManageServiceImpl();
		List<UserDto> userDtoList = null;
		
		try {
		    JSONObject jsonObj = JSONObject.fromObject(this.param);
		    if (!jsonObj.isNullObject() && !jsonObj.isEmpty()) {
		        String keyword = jsonObj.optString("keyword");
		        String groups = jsonObj.optString("groups");
		        userDtoList = service.find(keyword, StringUtils.isNotEmpty(groups) ? groups.split(",") : null);
		    } else {
		        userDtoList = service.findAll();
		    }
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("database error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("unexpected error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		}
		
		JSONObject root = new JSONObject();
		if (userDtoList != null && userDtoList.size() > 0) {
		    root.put("total", userDtoList.size());
			root.put("users", JsonUtils.fromBean(userDtoList));
		} else {
		    root.put("total", 0);
            root.put("users", new JSONArray());
		}
		return root;
	}
}
