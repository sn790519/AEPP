package com.fenrirtec.aepp.common.api;

import java.util.List;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.CodeNameDto;
import com.fenrirtec.aepp.common.service.CodeNameService;
import com.fenrirtec.aepp.common.service.impl.CodeNameServiceImpl;
import com.fenrirtec.aepp.common.support.JSONActionSupport;
import com.fenrirtec.core.definition.BaseDefinition.API;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.JsonUtils;

public class CodeNameSearchAPI extends JSONActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(CodeNameSearchAPI.class);
	
	private String param;

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	@Override
	protected List<ErrorInfo> doValidate() {
		return null;
	}

	@Override
	protected JSONObject doExecute() {
		CodeNameService service = new CodeNameServiceImpl();
		JSONObject objJson = JSONObject.fromObject(this.param);
		String typeId = objJson.getString("type_id");
		List<CodeNameDto> codeNameDtos = null;
		try {
			codeNameDtos = service.findByTypeId(typeId);
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("database error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("unexpected error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		}
		JSONObject root = new JSONObject();
		if (codeNameDtos != null && !codeNameDtos.isEmpty()) {
			root.put("code_names", JsonUtils.fromBean(codeNameDtos));
		}
		return root;
	}

}
