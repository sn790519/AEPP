package com.fenrirtec.aepp.common.api;

import java.util.List;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.condition.MemberCondition;
import com.fenrirtec.aepp.common.dto.MemberDto;
import com.fenrirtec.aepp.common.service.MemberManageService;
import com.fenrirtec.aepp.common.service.impl.MemberManageServiceImpl;
import com.fenrirtec.aepp.common.support.JSONActionSupport;
import com.fenrirtec.core.definition.BaseDefinition.API;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.JsonUtils;

public class MemberSearchAPI extends JSONActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(MemberSearchAPI.class);

	public MemberSearchAPI() {
	}
	
	private String param;
	
	private int _page;
	
	private int _rows;

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public int getPage() {
		return _page;
	}

	public void setPage(int page) {
		this._page = page;
	}

	public int getRows() {
		return _rows;
	}

	public void setRows(int rows) {
		this._rows = rows;
	}

	@Override
	protected List<ErrorInfo> doValidate() {
		return null;
	}

	@Override
	protected JSONObject doExecute() {
		
		MemberManageService service = new MemberManageServiceImpl();
		List<MemberDto> memberDtos = null;
		Integer memberCount = 0;
		
		try {
			JSONObject jsonObj = JSONObject.fromObject(this.param);
			MemberCondition condition = new MemberCondition();
			
			if (!jsonObj.isNullObject()) {
				condition = JsonUtils.toBean(jsonObj, MemberCondition.class);
			}

			condition.setPage(_page);
			condition.setRows(_rows);
			
			memberCount = service.memberCount(condition);
			memberDtos = service.memberSearch(condition);
			
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("database error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("unexpected error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		}
		
		JSONObject root = new JSONObject();
		if (memberDtos != null && memberDtos.size() > 0) {
			root.put("total", memberCount);
			root.put("members", JsonUtils.fromBean(memberDtos));
		}
		return root;
	}
}
