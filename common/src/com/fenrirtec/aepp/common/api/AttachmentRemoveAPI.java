package com.fenrirtec.aepp.common.api;

import java.util.List;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.AttachmentDto;
import com.fenrirtec.aepp.common.service.AttachmentService;
import com.fenrirtec.aepp.common.service.FileService;
import com.fenrirtec.aepp.common.service.MemberManageService;
import com.fenrirtec.aepp.common.service.impl.AttachmentServiceImpl;
import com.fenrirtec.aepp.common.service.impl.FileServiceImpl;
import com.fenrirtec.aepp.common.service.impl.MemberManageServiceImpl;
import com.fenrirtec.aepp.common.support.JSONActionSupport;
import com.fenrirtec.core.definition.BaseDefinition.API;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;

public class AttachmentRemoveAPI extends JSONActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(AttachmentRemoveAPI.class);

	public AttachmentRemoveAPI() {
	}
	
	private String param;

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	@Override
	protected List<ErrorInfo> doValidate() {
		return null;
	}

	@Override
	protected JSONObject doExecute() {
		AttachmentService attachmentService = new AttachmentServiceImpl();
		FileService fileService = new FileServiceImpl();
		MemberManageService memberService = new MemberManageServiceImpl();
		
		try {
			JSONObject jsonObj = JSONObject.fromObject(this.param);
			Integer attachmentId = jsonObj.getInt("attachment_id");
			Integer attachmentCategory = jsonObj.optInt("attachment_category", 0);
			String memberLoginName = jsonObj.optString("member_login_name", "");
			AttachmentDto attachmentDto = attachmentService.searchAttachment(attachmentId);
			attachmentService.removeAttachment(attachmentId);
			fileService.deleteFile(attachmentDto.getPath());
			
			if (StringUtils.isNotEmpty(memberLoginName) && attachmentCategory > 0) {
				memberService.removeImage(memberLoginName, attachmentCategory, attachmentId);
			}
			
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("database error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("unexpected error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		}

		JSONObject root = new JSONObject();
		root.put(API.Common.Response.STATUS, API.Common.Status.SUCCESS);
		return root;
	}
}
