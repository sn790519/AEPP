package com.fenrirtec.aepp.common.api;

import java.util.List;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.AttachmentDto;
import com.fenrirtec.aepp.common.service.AttachmentService;
import com.fenrirtec.aepp.common.service.impl.AttachmentServiceImpl;
import com.fenrirtec.aepp.common.support.JSONActionSupport;
import com.fenrirtec.core.definition.BaseDefinition.API;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.ImageUtils;

public class ImageAttachmentSearchAPI extends JSONActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(ImageAttachmentSearchAPI.class);

	public ImageAttachmentSearchAPI() {
	}
	
	private String param;

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	@Override
	protected List<ErrorInfo> doValidate() {
		return null;
	}

	@Override
	protected JSONObject doExecute() {
		AttachmentService attachmentService = new AttachmentServiceImpl();
		AttachmentDto attachmentDto = null;
		Integer attachmentId;
		String imageBase64 = "";
		JSONObject root = new JSONObject();
		
		try {
			JSONObject jsonObj = JSONObject.fromObject(this.param);
			attachmentId = jsonObj.getInt("attachment_id");
			attachmentDto = attachmentService.searchAttachment(attachmentId);
			
			root.put("attachment_id", attachmentId);
			if (attachmentDto != null) {
				imageBase64 = ImageUtils.imageToBase64(attachmentDto.getPath());
				root.put("image", "data:image/jpg;base64," + imageBase64);
			}
			
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("database error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("unexpected error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		}
		
		return root;
	}
}
