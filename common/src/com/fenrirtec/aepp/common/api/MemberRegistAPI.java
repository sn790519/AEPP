package com.fenrirtec.aepp.common.api;

import java.util.List;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.MemberDto;
import com.fenrirtec.aepp.common.service.MemberManageService;
import com.fenrirtec.aepp.common.service.impl.MemberManageServiceImpl;
import com.fenrirtec.aepp.common.support.JSONActionSupport;
import com.fenrirtec.core.definition.BaseDefinition.API;
import com.fenrirtec.core.definition.BaseDefinition.API.Common;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.JsonUtils;

public class MemberRegistAPI extends JSONActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(MemberRegistAPI.class);

	public MemberRegistAPI() {
	}
	
	private String param;

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	@Override
	protected List<ErrorInfo> doValidate() {
		return null;
	}

	@Override
	protected JSONObject doExecute() {
		MemberManageService memberManageService = new MemberManageServiceImpl();
		boolean result = false;
		
		try {
			JSONObject jsonObj = JSONObject.fromObject(this.param);
			MemberDto memberDto = JsonUtils.toBean(jsonObj, MemberDto.class);
			result = memberManageService.memberRegist(memberDto);
			getSession().put("regist_name", jsonObj.getString("member_login_name"));
			
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("database error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("unexpected error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		}
		
		JSONObject root = new JSONObject();
		root.put(Common.Response.RESULT, result);
		return root;
	}
}
