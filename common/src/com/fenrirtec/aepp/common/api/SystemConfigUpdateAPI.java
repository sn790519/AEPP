package com.fenrirtec.aepp.common.api;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.SystemConfigDto;
import com.fenrirtec.aepp.common.service.SystemConfigService;
import com.fenrirtec.aepp.common.service.impl.SystemConfigServiceImpl;
import com.fenrirtec.aepp.common.support.JSONActionSupport;
import com.fenrirtec.core.definition.BaseDefinition.API;
import com.fenrirtec.core.definition.BaseDefinition.API.Common;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.DigestUtils;
import com.fenrirtec.core.utils.JsonUtils;
import com.fenrirtec.core.utils.MessageUtils;

public class SystemConfigUpdateAPI extends JSONActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(SystemConfigUpdateAPI.class);

	public SystemConfigUpdateAPI() {
	}
	
	private String param;

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	@Override
	protected List<ErrorInfo> doValidate() {
		return null;
	}

	@Override
	protected JSONObject doExecute() {
		SystemConfigService systemConfigService = new SystemConfigServiceImpl();
		boolean result = false;
		JSONObject root = new JSONObject();
		
		try {
			JSONObject jsonObj = JSONObject.fromObject(this.param);
			String password = jsonObj.getString("old_password");
			if (StringUtils.isNotEmpty(password) && !DigestUtils.SHAHashing(password).equals(systemConfigService.loadSystemConfig().getPassword())) {
				root.put(Common.Response.RESULT, false);
				root.put(Common.Response.MESSAGE, MessageUtils.getLocalizedString(this, "messages.error.1008", null));
				return root;
			}
			jsonObj.remove("old_password");
			SystemConfigDto systemConfigDto = JsonUtils.toBean(jsonObj, SystemConfigDto.class);
			systemConfigDto.setPassword(DigestUtils.SHAHashing(systemConfigDto.getPassword()));
			systemConfigDto.setUpdateUser(getUserInfo().getLoginName());
			result = systemConfigService.updateSystemConfig(systemConfigDto);
			if (result) {
				setSystemConfig(systemConfigService.loadSystemConfig());
			}
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("database error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("unexpected error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} catch (NoSuchAlgorithmException e) {
			if (logger.isErrorEnabled()) {
				logger.error("unexpected error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		}
		
		root.put(Common.Response.RESULT, result);
		return root;
	}
}
