package com.fenrirtec.aepp.manage.action;

import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.UserDto;
import com.fenrirtec.aepp.common.service.UserManageService;
import com.fenrirtec.aepp.common.service.impl.UserManageServiceImpl;
import com.fenrirtec.aepp.manage.common.action.CommonAction;
import com.fenrirtec.aepp.manage.common.definition.Definition;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.DigestUtils;
import com.fenrirtec.core.utils.MessageUtils;

public class LoginAction extends CommonAction {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(LoginAction.class);

	private String mLoginName;

	private String mLoginPassword;

	public String getLoginName() {
		return mLoginName;
	}

	public void setLoginName(String loginName) {
		this.mLoginName = loginName;
	}

	public String getLoginPassword() {
		return mLoginPassword;
	}

	public void setLoginPassword(String loginPassword) {
		this.mLoginPassword = loginPassword;
	}

	public String init() {
		if (logger.isInfoEnabled()) {
			logger.info("[LoginAction#init] start.");
		}
		if (logger.isInfoEnabled()) {
			logger.info("[LoginAction#init] finish.");
		}
		return SUCCESS;
	}

	public String login() {
		try {
			if (logger.isInfoEnabled()) {
				logger.info("[LoginAction#login] start.");
			}
			
			if (mLoginName.equalsIgnoreCase(Definition.ADMIN)) {
				if (!DigestUtils.SHAHashing(mLoginPassword).equals(getSystemConfig().getPassword())) {
					addActionError(MessageUtils.getLocalizedErrorMessage(this, Definition.ErrorCode.ERROR_AUTHORITY, null));
					return INPUT;
				} else {
					UserDto userDto = new UserDto();
					userDto.setLoginName(Definition.ADMIN);
					userDto.setLoginPassword(getSystemConfig().getPassword());
					userDto.setName(Definition.ADMIN);
					getSession().put(Definition.SessionKey.USER_INFO, userDto);
					return SUCCESS;
				}
			} else {
				UserManageService loginService = new UserManageServiceImpl();
				UserDto userDto = loginService.login(mLoginName, mLoginPassword);
				if (userDto == null) {
					addActionError(MessageUtils.getLocalizedErrorMessage(this, Definition.ErrorCode.ERROR_AUTHORITY, null));
					return INPUT;
				} else {
					getSession().put(Definition.SessionKey.USER_INFO, userDto);
					return SUCCESS;
				}
			}
			
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("[LoginAction#login] database error occurred.", e);
			}
			throw e;
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("[LoginAction#login] unexpected error occurred.", e);
			}
			throw e;
		} catch (NoSuchAlgorithmException e) {
			if (logger.isErrorEnabled()) {
				logger.error("[LoginAction#login] unexpected error occurred.", e);
			}
			throw new UnexpectedException("unexpected error.", e);
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[LoginAction#login] finish.");
			}
		}
	}
}
