package com.fenrirtec.aepp.manage.action;


import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.manage.common.action.CommonAction;

public class TenderManageAction extends CommonAction {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(TenderManageAction.class);
	
    private Integer tenderId;
	
	private String tenderTitle;
	
	public String init() {
		if (logger.isInfoEnabled()) {
			logger.info("[TenderManageAction#init] start.");
		}
		
		HttpServletRequest reqeust= ServletActionContext.getRequest();
		String strTenderId = reqeust.getParameter("tenderId");
		if (strTenderId != null){
			tenderId = Integer.valueOf(strTenderId);
		}else{
			tenderId = 0;
		}
		tenderTitle = reqeust.getParameter("tenderTitle");
		try {
			tenderTitle = new String(tenderTitle.getBytes("iso-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}
		if (logger.isInfoEnabled()) {
			logger.info("[TenderManageAction#init] finish.");
		}
		return SUCCESS;
	}

	public Integer getTenderId() {
		return tenderId;
	}

	public void setTenderId(Integer tenderId) {
		this.tenderId = tenderId;
	}

	public String getTenderTitle() {
		return tenderTitle;
	}

	public void setTenderTitle(String tenderTitle) {
		this.tenderTitle = tenderTitle;
	}
	
}