package com.fenrirtec.aepp.manage.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.manage.common.action.CommonAction;

public class SystemConfigAction extends CommonAction {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(SystemConfigAction.class);

	public String init() {
		if (logger.isInfoEnabled()) {
			logger.info("[SystemConfigAction#init] start.");
		}
		if (logger.isInfoEnabled()) {
			logger.info("[SystemConfigAction#init] finish.");
		}
		return SUCCESS;
	}
}
