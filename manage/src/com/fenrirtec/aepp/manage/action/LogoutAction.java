package com.fenrirtec.aepp.manage.action;

import org.apache.struts2.dispatcher.SessionMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.manage.common.action.CommonAction;

public class LogoutAction extends CommonAction {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(LogoutAction.class);

	public String logout() {
		try {
			if (logger.isInfoEnabled()) {
				logger.info("[LogoutAction#logout] start.");
			}
			if (getSession() != null) {
				((SessionMap<String, Object>) getSession()).invalidate();
			}
		} finally {
			if (logger.isInfoEnabled()) {
				logger.info("[LogoutAction#logout] finish.");
			}
		}
		return SUCCESS;
	}
}
