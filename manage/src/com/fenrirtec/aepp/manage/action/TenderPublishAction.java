package com.fenrirtec.aepp.manage.action;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.manage.common.action.CommonAction;

public class TenderPublishAction extends CommonAction {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(TenderPublishAction.class);

	public String init() {
		if (logger.isInfoEnabled()) {
			logger.info("[TenderPublishAction#init] start.");
		}
		if (logger.isInfoEnabled()) {
			logger.info("[TenderPublishAction#init] finish.");
		}
		return SUCCESS;
	}
}