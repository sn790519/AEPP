package com.fenrirtec.aepp.manage.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.manage.common.action.CommonAction;

public class BidInvitationListAction extends CommonAction{
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(BidInvitationListAction.class);

	public String init() {
		if (logger.isInfoEnabled()) {
			logger.info("[BidInvitationListAction#init] start.");
		}
		if (logger.isInfoEnabled()) {
			logger.info("[BidInvitationListAction#init] finish.");
		}
		return SUCCESS;
	}
}
