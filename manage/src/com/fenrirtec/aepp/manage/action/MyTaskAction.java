package com.fenrirtec.aepp.manage.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.manage.common.action.CommonAction;

public class MyTaskAction extends CommonAction {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(MyTaskAction.class);

	public String init() {
		if (logger.isInfoEnabled()) {
			logger.info("[MyTaskAction#init] start.");
		}
		if (logger.isInfoEnabled()) {
			logger.info("[MyTaskAction#init] finish.");
		}
		return SUCCESS;
	}
}
