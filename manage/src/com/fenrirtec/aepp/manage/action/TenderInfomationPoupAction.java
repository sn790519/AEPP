package com.fenrirtec.aepp.manage.action;


import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.TenderInformationDto;
import com.fenrirtec.aepp.common.service.impl.TenderInformationServiceImpl;
import com.fenrirtec.aepp.manage.common.action.CommonAction;

public class TenderInfomationPoupAction extends CommonAction {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(TenderInfomationPoupAction.class);

	private Integer tenderId;
	private TenderInformationDto tenderInformationDto;
	
	public String init() {
		if (logger.isInfoEnabled()) {
			logger.info("[TenderInfomationPoupAction#init] start.");
		}
		HttpServletRequest reqeust= ServletActionContext.getRequest();
		String strTenderId = reqeust.getParameter("tenderId");
		if (strTenderId != null){
			tenderId = Integer.valueOf(strTenderId);
			if(tenderInformationDto == null){
				tenderInformationDto=new TenderInformationServiceImpl().findTenderInformation(tenderId).get(0);
			}
		}else{
			tenderId = 0;
		}
		if (logger.isInfoEnabled()) {
			logger.info("[TenderInfomationPoupAction#init] finish.");
		}
		return SUCCESS;
	}

	public Integer getTenderId() {
		return tenderId;
	}

	public void setTenderId(Integer tenderId) {
		this.tenderId = tenderId;
	}

	public TenderInformationDto getTenderInformationDto() {
		return tenderInformationDto;
	}

	public void setTenderInformationDto(TenderInformationDto tenderInformationDto) {
		this.tenderInformationDto = tenderInformationDto;
	}

	
	
}