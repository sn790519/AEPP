package com.fenrirtec.aepp.manage.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.manage.common.action.CommonAction;

public class AdvImageAction extends CommonAction {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(AdvImageAction.class);

	public String init() {
		if (logger.isInfoEnabled()) {
			logger.info("[AdvImageAction#init] start.");
		}
		if (logger.isInfoEnabled()) {
			logger.info("[AdvImageAction#init] finish.");
		}
		return SUCCESS;
	}
}
