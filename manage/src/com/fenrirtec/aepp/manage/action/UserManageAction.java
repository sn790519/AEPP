package com.fenrirtec.aepp.manage.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.manage.common.action.CommonAction;

public class UserManageAction extends CommonAction {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(UserManageAction.class);

	public String init() {
		if (logger.isInfoEnabled()) {
			logger.info("[UserManageAction#init] start.");
		}
		if (logger.isInfoEnabled()) {
			logger.info("[UserManageAction#init] finish.");
		}
		return SUCCESS;
	}
}
