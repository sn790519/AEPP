package com.fenrirtec.aepp.manage.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.TenderBidDto;
import com.fenrirtec.aepp.common.service.TenderBidService;
import com.fenrirtec.aepp.common.service.impl.TenderBidServiceImpl;
import com.fenrirtec.aepp.common.support.JSONActionSupport;
import com.fenrirtec.core.definition.BaseDefinition.API;
import com.fenrirtec.core.definition.BaseDefinition.API.Common;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;

public class TenderBidAPI extends JSONActionSupport{
	
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(TenderBidAPI.class);

	public TenderBidAPI() {
	}


	private String param;

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	@Override
	protected List<ErrorInfo> doValidate() {
		return null;
	}
	
	@Override
	protected JSONObject doExecute() {
		TenderBidService tenderbidservice = new TenderBidServiceImpl();
		
		try {
			JSONObject jsonObj = JSONObject.fromObject(this.param);					
			JSONObject jst = jsonObj.getJSONObject("listbid");
			JSONArray list = jst.getJSONArray("rows");
			TenderBidDto tenderBidDto=new TenderBidDto();
			List<TenderBidDto> tenderBidDtoList= new ArrayList<TenderBidDto>();
            for(int i=0; i<list.size(); i++){
				JSONObject object = (JSONObject) list.get(i);
				tenderBidDto.setTenderId(object.getInt("tender_id"));				
				tenderBidDto.setMemberLoginName(object.getString("member_login_name"));
			
				Date nowTime = new Date(System.currentTimeMillis());
				SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				String retStrFormatNowDate = sdFormatter.format(nowTime);
				
				tenderBidDto.setBidDate(retStrFormatNowDate);
				//tenderFinalistDto.setEvaluation(object.getString("evaluation"));
				tenderBidDto.setEvaluation("");
				tenderBidDto.setCreateUser("system");
				tenderBidDto.setCreateDate(retStrFormatNowDate);
				tenderBidDto.setUpdateUser("system");
				tenderBidDto.setUpdateDate(retStrFormatNowDate);
				tenderBidDtoList.add(tenderBidDto);
				
			}
            tenderbidservice.insertTenderBid(tenderBidDtoList);
            JSONObject obj = (JSONObject) list.get(0);
            tenderbidservice.updateTenderFinalist(obj.getInt("tender_id"));			
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("database error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("unexpected error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} 
		
		JSONObject root = new JSONObject();
		root.put(Common.Response.RESULT, true);
		return root;
	}	
}
