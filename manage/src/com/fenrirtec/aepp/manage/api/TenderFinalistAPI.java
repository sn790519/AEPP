package com.fenrirtec.aepp.manage.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.TenderEnrollDto;
import com.fenrirtec.aepp.common.dto.TenderFinalistDto;
import com.fenrirtec.aepp.common.service.TenderFinalistService;
import com.fenrirtec.aepp.common.service.impl.TenderFinalistServiceImpl;
import com.fenrirtec.aepp.common.support.JSONActionSupport;
import com.fenrirtec.core.definition.BaseDefinition.API;
import com.fenrirtec.core.definition.BaseDefinition.API.Common;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;

public class TenderFinalistAPI extends JSONActionSupport{
	
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(TenderFinalistAPI.class);

	public TenderFinalistAPI() {
	}


	private String param;

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	@Override
	protected List<ErrorInfo> doValidate() {
		return null;
	}
	
	@Override
	protected JSONObject doExecute() {
		TenderFinalistService tenderFinalistervice = new TenderFinalistServiceImpl();
		
		try {
			JSONObject jsonObj = JSONObject.fromObject(this.param);
					
			JSONObject jst = jsonObj.getJSONObject("list");
			JSONArray list = jst.getJSONArray("rows");
			TenderFinalistDto tenderFinalistDto=new TenderFinalistDto();
			List<TenderFinalistDto> tenderFinalistDtoList= new ArrayList<TenderFinalistDto>();
			String flag=jsonObj.getString("flag"); 
			if (flag.equals("audit")){
				for(int i=0; i<list.size(); i++){
					JSONObject object = (JSONObject) list.get(i);
					tenderFinalistDto.setTenderId(object.getInt("tender_id"));					
					tenderFinalistDto.setMemberLoginName(object.getString("member_login_name"));
				
					Date nowTime = new Date(System.currentTimeMillis());
					SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
					String retStrFormatNowDate = sdFormatter.format(nowTime);
					
					tenderFinalistDto.setFinalistDate(retStrFormatNowDate);
					//tenderFinalistDto.setEvaluation(object.getString("evaluation"));
					tenderFinalistDto.setEvaluation("");
					tenderFinalistDto.setCreateUser("system");
					tenderFinalistDto.setCreateDate(retStrFormatNowDate);
					tenderFinalistDto.setUpdateUser("system");
					tenderFinalistDto.setUpdateDate(retStrFormatNowDate);
					tenderFinalistDtoList.add(tenderFinalistDto);		
				}
				tenderFinalistervice.insertTenderFinalist(tenderFinalistDtoList);
				//tenderFinalistervice.updateTenderFinalist(jsonObj.getInt("tenderId"));
				JSONObject obj = (JSONObject) list.get(0);
				tenderFinalistervice.updateTenderFinalist(obj.getInt("tender_id"));	
			}else{
				TenderEnrollDto tenderEnrollDto=new TenderEnrollDto();
				JSONObject object = (JSONObject) list.get(0);
				tenderEnrollDto.setTenderId(object.getInt("tender_id"));
				tenderEnrollDto.setUpdateUser(getUserInfo().getLoginName());
				tenderFinalistervice.updateTenderFinalistflag(tenderEnrollDto);
			}
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("database error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("unexpected error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} 
		
		JSONObject root = new JSONObject();
		root.put(Common.Response.RESULT, true);
		return root;
	}	
}
