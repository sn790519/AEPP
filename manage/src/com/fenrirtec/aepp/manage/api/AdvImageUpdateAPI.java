package com.fenrirtec.aepp.manage.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.service.AdvImageService;
import com.fenrirtec.aepp.common.service.impl.AdvImageServiceImpl;
import com.fenrirtec.aepp.common.support.JSONActionSupport;
import com.fenrirtec.core.definition.BaseDefinition.API;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;

import net.sf.json.JSONObject;

public class AdvImageUpdateAPI extends JSONActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(AdvImageUpdateAPI.class);

	public AdvImageUpdateAPI() {
	}
	
	private String param;

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	@Override
	protected List<ErrorInfo> doValidate() {
		return null;
	}

	@Override
	protected JSONObject doExecute() {
		AdvImageService advImageService = new AdvImageServiceImpl();
		Boolean result = false;
		
		try {
			JSONObject jsonObj = JSONObject.fromObject(this.param);
			Integer imageId = jsonObj.getInt("image_id");
			Boolean useFlag = jsonObj.getBoolean("use_flag");
			result = advImageService.update(imageId, useFlag, getUserInfo().getLoginName());
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("database error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("unexpected error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		}
		
		JSONObject root = new JSONObject();
		root.put("result", result);
		return root;
	}
}
