package com.fenrirtec.aepp.manage.api;


import java.util.ArrayList;

import java.util.List;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.fenrirtec.aepp.common.dto.TenderManageDto;
import com.fenrirtec.aepp.common.service.TenderManageService;
import com.fenrirtec.aepp.common.service.impl.TenderManageServiceImpl;
import com.fenrirtec.aepp.common.support.JSONActionSupport;
import com.fenrirtec.core.definition.BaseDefinition.API;

import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.JsonUtils;

public class TenderManageAPI extends JSONActionSupport{
	
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(TenderManageAPI.class);

	public TenderManageAPI() {
	}


	private String param;

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	@Override
	protected List<ErrorInfo> doValidate() {
		return null;
	}
	
	@Override
	protected JSONObject doExecute() {
		TenderManageService tenderManageservice = new TenderManageServiceImpl();
		JSONObject root = new JSONObject();
		List<TenderManageDto> tenderManageDtolist = new ArrayList<TenderManageDto>();
		try {
			JSONObject jsonObj = JSONObject.fromObject(this.param);
			Integer tenderid=jsonObj.getInt("tenderId");
			//Integer tenderid = 1;
			tenderManageDtolist=tenderManageservice.findTenderManage(tenderid);
	
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("database error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("unexpected error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} 
		
		if(tenderManageDtolist !=null && tenderManageDtolist.size()>0){
			for(int i=0;i<tenderManageDtolist.size();i++){
				if(tenderManageDtolist.get(i).getFinalistFlag().equals(true)){
					tenderManageDtolist.get(i).setFinalistFlagname("是");
				}else{
					tenderManageDtolist.get(i).setFinalistFlagname("否");
				}
				if(tenderManageDtolist.get(i).getPath() != null){
					tenderManageDtolist.get(i).setPath("资质文件");
				}else{
					tenderManageDtolist.get(i).setPath("无");
				}
			}
			
			root.put("total", tenderManageDtolist.size());
			root.put("tmlist", JsonUtils.fromBean(tenderManageDtolist));
		}
		return root;
	}	
}
