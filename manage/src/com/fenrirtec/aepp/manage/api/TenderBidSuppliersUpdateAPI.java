package com.fenrirtec.aepp.manage.api;


import java.util.List;



import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.fenrirtec.aepp.common.service.TenderBidService;
import com.fenrirtec.aepp.common.service.impl.TenderBidServiceImpl;
import com.fenrirtec.aepp.common.support.JSONActionSupport;
import com.fenrirtec.core.definition.BaseDefinition.API;
import com.fenrirtec.core.definition.BaseDefinition.API.Common;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;

public class TenderBidSuppliersUpdateAPI extends JSONActionSupport{
	
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(TenderBidSuppliersUpdateAPI.class);

	public TenderBidSuppliersUpdateAPI() {
	}


	private String param;

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	@Override
	protected List<ErrorInfo> doValidate() {
		return null;
	}
	
	@Override
	protected JSONObject doExecute() {
		TenderBidService tenderbidservice = new TenderBidServiceImpl();
		
		try {
			JSONObject jsonObj = JSONObject.fromObject(this.param);
			JSONObject jst = jsonObj.getJSONObject("listbids");
			JSONArray list = jst.getJSONArray("rows");
			JSONObject object = (JSONObject) list.get(0);
			tenderbidservice.updateTenderInformation(object.getInt("tender_id"));		
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("database error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("unexpected error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} 
		
		JSONObject root = new JSONObject();
		root.put(Common.Response.RESULT, true);
		return root;
	}	
}
