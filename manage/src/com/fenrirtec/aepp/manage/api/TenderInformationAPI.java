package com.fenrirtec.aepp.manage.api;


import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.TenderInformationDto;
import com.fenrirtec.aepp.common.service.TenderInformationService;
import com.fenrirtec.aepp.common.service.impl.TenderInformationServiceImpl;
import com.fenrirtec.aepp.common.support.JSONActionSupport;
import com.fenrirtec.core.definition.BaseDefinition.API;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.JsonUtils;

public class TenderInformationAPI extends JSONActionSupport{
	
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(TenderInformationAPI.class);

	public TenderInformationAPI() {
	}


	private String param;

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	@Override
	protected List<ErrorInfo> doValidate() {
		return null;
	}
	
	@Override
	protected JSONObject doExecute() {
		TenderInformationService tenderInformationservice = new TenderInformationServiceImpl();
		JSONObject root = new JSONObject();
		List<TenderInformationDto> tenderInformationDtolist = new ArrayList<TenderInformationDto>();
		try {
			JSONObject jsonObj = JSONObject.fromObject(this.param);
			Integer tenderid=jsonObj.getInt("tenderId");
			//Integer tenderid = 1;
			tenderInformationDtolist=tenderInformationservice.findTenderInformation(tenderid);
	
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("database error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("unexpected error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} 
		
		if(tenderInformationDtolist !=null && tenderInformationDtolist.size()>0){
			root.put("total", tenderInformationDtolist.size());
			root.put("tilist", JsonUtils.fromBean(tenderInformationDtolist).get(0));
		}
		return root;
	}	
}
