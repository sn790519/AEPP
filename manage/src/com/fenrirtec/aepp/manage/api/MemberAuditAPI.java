package com.fenrirtec.aepp.manage.api;

import java.util.List;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.AuditFlowDto;
import com.fenrirtec.aepp.common.service.MemberManageService;
import com.fenrirtec.aepp.common.service.impl.MemberManageServiceImpl;
import com.fenrirtec.aepp.common.support.JSONActionSupport;
import com.fenrirtec.core.definition.BaseDefinition.API;
import com.fenrirtec.core.definition.BaseDefinition.API.Common;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;

public class MemberAuditAPI extends JSONActionSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(MemberAuditAPI.class);

	public MemberAuditAPI() {
	}
	
	private String param;

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	@Override
	protected List<ErrorInfo> doValidate() {
		return null;
	}

	@Override
	protected JSONObject doExecute() {
		MemberManageService service = new MemberManageServiceImpl();
		try {
			JSONObject jsonObj = JSONObject.fromObject(this.param);
			String memberLoginName = jsonObj.getString("member_login_name");
			Integer auditSequence = jsonObj.getInt("audit_sequence");
			Integer auditPhase = jsonObj.getInt("audit_phase");
			Integer auditResult = jsonObj.getInt("audit_result");
			String auditComment = jsonObj.getString("audit_comment");
			
			//result = service.memberAudit(memberLoginName, auditResult, auditComment, getUserInfo().getLoginName());
			AuditFlowDto auditFlowDto = new AuditFlowDto();
			auditFlowDto.setMemberLoginName(memberLoginName);
			auditFlowDto.setAuditSequence(auditSequence);
			auditFlowDto.setAuditPhase(auditPhase);
			auditFlowDto.setAuditResult(auditResult);
			auditFlowDto.setAuditComment(auditComment);
			auditFlowDto.setLoginName(getUserInfo().getLoginName());
			auditFlowDto.setUpdateUser(getUserInfo().getLoginName());
			service.updateAuditResult(auditFlowDto);
			
			if (auditResult == 9) {
				auditFlowDto.setAuditSequence(auditSequence + 1);
				auditFlowDto.setAuditPhase(auditPhase == 6 ? 9 : (auditPhase + 1));
				auditFlowDto.setAuditResult(auditPhase == 6 ? 9 : 1);
				service.insertAuditFlow(auditFlowDto);
			}
			
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("database error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("unexpected error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		}
		
		JSONObject root = new JSONObject();
		root.put(Common.Response.RESULT, true);
		return root;
	}
}
