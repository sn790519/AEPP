package com.fenrirtec.aepp.manage.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.TenderDetailDto;
import com.fenrirtec.aepp.common.dto.TenderProductDto;
import com.fenrirtec.aepp.common.dto.TenderVenderDto;
import com.fenrirtec.aepp.common.service.TenderService;
import com.fenrirtec.aepp.common.service.impl.TenderServiceImpl;
import com.fenrirtec.aepp.common.support.JSONActionSupport;
import com.fenrirtec.core.definition.BaseDefinition.API;
import com.fenrirtec.core.definition.BaseDefinition.API.Common;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.JsonUtils;

import net.sf.json.JSONObject;

public class TenderPublishAPI extends JSONActionSupport{
	
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(TenderPublishAPI.class);

	public TenderPublishAPI() {
	}

	private String param;

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	@Override
	protected List<ErrorInfo> doValidate() {
		return null;
	}
	
	@Override
	protected JSONObject doExecute() {
		
	    TenderService tenderService = new TenderServiceImpl();
	    Boolean result = false;
		
		try {
			JSONObject jsonObj = JSONObject.fromObject(this.param);
			
			TenderDetailDto tenderDetailDto = null;
			List<TenderProductDto> tenderProductDtoList = null;
			List<TenderVenderDto> tenderVenderDtoList = null;
			
			tenderDetailDto = JsonUtils.toBean(jsonObj.getJSONObject("tender_information"), TenderDetailDto.class);
			tenderProductDtoList = JsonUtils.toBean(jsonObj.getJSONArray("tender_product"), TenderProductDto.class);
			
			if (tenderDetailDto.getTenderPattern() == 2) {
			    tenderDetailDto.setStatus(3);
			    tenderVenderDtoList = JsonUtils.toBean(jsonObj.getJSONArray("tender_vender"), TenderVenderDto.class);
			} else {
			    tenderDetailDto.setStatus(1);
			}
			
			
			result = tenderService.publish(tenderDetailDto, tenderProductDtoList, tenderVenderDtoList, getUserInfo().getLoginName());
			
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("database error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("unexpected error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} 

		JSONObject root = new JSONObject();
		root.put(Common.Response.RESULT, result);
		return root;
	}	
}
