package com.fenrirtec.aepp.manage.api;


import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.common.dto.TenderBidlistDto;
import com.fenrirtec.aepp.common.service.TenderBidService;
import com.fenrirtec.aepp.common.service.impl.TenderBidServiceImpl;
import com.fenrirtec.aepp.common.support.JSONActionSupport;
import com.fenrirtec.core.definition.BaseDefinition.API;
import com.fenrirtec.core.exception.DatabaseException;
import com.fenrirtec.core.exception.UnexpectedException;
import com.fenrirtec.core.utils.JsonUtils;

public class TenderBidlistAPI extends JSONActionSupport{
	
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(TenderBidlistAPI.class);

	public TenderBidlistAPI() {
	}


	private String param;

	public String getParam() {
		return this.param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	@Override
	protected List<ErrorInfo> doValidate() {
		return null;
	}
	
	@Override
	protected JSONObject doExecute() {
		TenderBidService tenderBidservice = new TenderBidServiceImpl();
		JSONObject root = new JSONObject();
		List<TenderBidlistDto> tenderBidDtolist = new ArrayList<TenderBidlistDto>();
		try {
			JSONObject jsonObj = JSONObject.fromObject(this.param);			
			Integer tenderid=jsonObj.getInt("tenderId");
			//Integer tenderid = 1;
			tenderBidDtolist=tenderBidservice.findTenderBidlsit(tenderid);			
		} catch (DatabaseException e) {
			if (logger.isErrorEnabled()) {
				logger.error("database error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} catch (UnexpectedException e) {
			if (logger.isErrorEnabled()) {
				logger.error("unexpected error occurred.", e);
			}
			throw new JSONActionStatusException(API.Common.Status.DB_ERROR, e);
		} 
		
		if(tenderBidDtolist !=null && tenderBidDtolist.size()>0){
			for(int i=0;i<tenderBidDtolist.size();i++){
				if(tenderBidDtolist.get(i).getPath() != null){
					tenderBidDtolist.get(i).setPath("资质文件");
				}
			}
			root.put("total", tenderBidDtolist.size());
			root.put("tblist", JsonUtils.fromBean(tenderBidDtolist));
		}
		return root;
	}	
}
