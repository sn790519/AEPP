package com.fenrirtec.aepp.manage.interceptor;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fenrirtec.aepp.manage.common.definition.Definition;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class LoginCheckInterceptor extends AbstractInterceptor {
	
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(LoginCheckInterceptor.class);
	
	private static final String TIMEOUT = "timeout";

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		HttpSession session = ServletActionContext.getRequest().getSession();

		if (session == null || session.getAttribute(Definition.SessionKey.USER_INFO) == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("user does not login.");
			}
			return TIMEOUT;
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("user already login. session=" + session);
			}
			return invocation.invoke();
		}
	}
}
