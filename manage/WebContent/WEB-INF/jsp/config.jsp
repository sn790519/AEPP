<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/themes/bootstrap/easyui.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/themes/icon.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/icheck/skins/all.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/api.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/validate.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/icheck/icheck.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
$(function() {
	var urlPrefix = "${pageContext.request.contextPath}/api/";
	
	initialize();
	
    function initialize() {
    	
    	//旧密码
    	$('#old_password').textbox({
    		type: 'password',
    		height: 25,
    		width: 150,
    		validType: {
    			length: [4, 16]
    		},
    		onChange: function(newValue, oldValue) {
    			if (newValue != "") {
    				$('#new_password').textbox({
    					required: true
    				});
    				$('#confirm_password').textbox({
                        required: true
                    });
    			} else {
    				$('#new_password').textbox({
                        required: false
                    });
    				$('#confirm_password').textbox({
                        required: false
                    });
    			}
    			$('#old_password').textbox('validate');
    			$('#new_password').textbox('validate');
    			$('#confirm_password').textbox('validate');
    		}
    	});
    	
    	//新密码
    	$('#new_password').textbox({
            type: 'password',
            height: 25,
            width: 150,
            validType: {
                length: [4, 16],
                same: ["#confirm_password"]
            },
            onChange: function(newValue, oldValue) {
                if (newValue != "") {
                    $('#old_password').textbox({
                        required: true
                    });
                    $('#confirm_password').textbox({
                        required: true
                    });
                } else {
                    $('#old_password').textbox({
                        required: false
                    });
                    $('#confirm_password').textbox({
                        required: false
                    });
                }
                $('#old_password').textbox('validate');
                $('#new_password').textbox('validate');
                $('#confirm_password').textbox('validate');
            }
        });
    	
    	//确认新密码
    	$('#confirm_password').textbox({
            type: 'password',
            height: 25,
            width: 150,
            validType: {
                length: [4, 16],
                same: ["#new_password"]
            },
            onChange: function(newValue, oldValue) {
                if (newValue != "") {
                    $('#old_password').textbox({
                        required: true
                    });
                    $('#new_password').textbox({
                        required: true
                    });
                } else {
                    $('#old_password').textbox({
                        required: false
                    });
                    $('#new_password').textbox({
                        required: false
                    });
                }
                $('#old_password').textbox('validate');
                $('#new_password').textbox('validate');
                $('#confirm_password').textbox('validate');
            }
        });
    	
    	//开放会员注册
    	var registEnabled = <s:property value='systemConfig.registEnabled' />;
    	$('#regist_enabled').iCheck({
    		checkboxClass: 'icheckbox_minimal-blue'
    	}).iCheck(registEnabled ? 'check' : 'uncheck');
    	
    	//新闻表示件数
    	var newsCount = "<s:property value='systemConfig.newsCount' />";
    	$('#news_count').numberspinner({
            width: 80,
            height: 25,
            required: true,
            min: 5,
            max: 20,
            editable: false,
            value: newsCount
        });
    	
    	//招标信息表示件数
        var tenderCount = "<s:property value='systemConfig.tenderCount' />";
        $('#tender_count').numberspinner({
        	width: 80,
        	height: 25,
        	required: true,
        	min: 5,
        	max: 20,
        	editable: false,
        	value: tenderCount
        });
        
        //招标信息表示顺序
        var tenderSort = "<s:property value='systemConfig.tenderSort' />";
        $.codeNameCombo({
            urlPrefix: urlPrefix,
            target: $('#tender_sort'),
            typeId: '07',
            required: true,
            editable: false,
            missingMessage: $.fn.validatebox.defaults.missingMessage,
            errorTitle: '<s:text name="title.error" />',
            errorMessage: '数据取得失败',
            blank: false,
            value: tenderSort
        });
        
        //会员表示件数
        var memberCount = "<s:property value='systemConfig.memberCount' />";
        $('#member_count').numberspinner({
            width: 80,
            height: 25,
            required: true,
            min: 5,
            max: 30,
            editable: false,
            value: memberCount
        });
        
        //联系人证件复印件
        var contactCopyRequired = <s:property value='systemConfig.contactCopyRequired' />;
        $('#contact_copy_required').iCheck({
            checkboxClass: 'icheckbox_minimal-blue'
        }).iCheck(contactCopyRequired ? 'check' : 'uncheck');
        
        //营业执照复印件
        var licenseCopyRequired = <s:property value='systemConfig.licenseCopyRequired' />;
        $('#license_copy_required').iCheck({
            checkboxClass: 'icheckbox_minimal-blue'
        }).iCheck(licenseCopyRequired ? 'check' : 'uncheck');
        
        //法人证件复印件
        var legalCopyRequired = <s:property value='systemConfig.legalCopyRequired' />;
        $('#legal_copy_required').iCheck({
            checkboxClass: 'icheckbox_minimal-blue'
        }).iCheck(legalCopyRequired ? 'check' : 'uncheck');
        
        //记录物理删除
        var physicalDelete = <s:property value='systemConfig.physicalDelete' />;
        $('#physical_delete').iCheck({
            checkboxClass: 'icheckbox_minimal-blue'
        }).iCheck(physicalDelete ? 'check' : 'uncheck');
        
        //上传根目录
        var uploadRootFolder = "<s:property value='systemConfig.uploadRootFolder' />";
        $('#upload_root_folder').textbox({
        	height: 25,
            required: true,
            validType: 'length[1, 255]',
            value: uploadRootFolder,
            disabled: true
        });
        
        //上传临时目录
        var uploadTempFolder = "<s:property value='systemConfig.uploadTempFolder' />";
        $('#upload_temp_folder').textbox({
            height: 25,
            required: true,
            validType: 'length[1, 255]',
            value: uploadTempFolder,
            disabled: true
        });
        
        //资质文件保存目录
        var qualificationsSaveFolder = "<s:property value='systemConfig.qualificationsSaveFolder' />";
        $('#qualifications_save_folder').textbox({
            height: 25,
            required: true,
            validType: 'length[1, 255]',
            value: qualificationsSaveFolder,
            disabled: true
        });
        
        //图片文件保存目录
        var imagesSaveFolder = "<s:property value='systemConfig.imagesSaveFolder' />";
        $('#images_save_folder').textbox({
            height: 25,
            required: true,
            validType: 'length[1, 255]',
            value: imagesSaveFolder,
            disabled: true
        });
        
        //图片文件扩展名
        var allowedImageExtension = "<s:property value='systemConfig.allowedImageExtension' />";
        $('#allowed_image_extension').combobox({
        	valueField: 'ext',
            textField: 'text',
        	multiple: true,
        	editable: false,
            required: true,
            height: 25,
            separator: '|',
            panelHeight: 'auto',
            value: allowedImageExtension,
            data: [
                {ext:'jpg',text:'JPG'},
                {ext:'jpeg',text:'JPEG'},
                {ext:'bmp',text:'BMP'},
                {ext:'png',text:'PNG'},
                {ext:'gif',text:'GIF'},
                {ext:'tiff',text:'TIFF'}
            ]
        });
        
        //图片文件大小
        var allowedImageSize = "<s:property value='systemConfig.allowedImageSize' />";
        $('#allowed_image_size').numberbox({
            height: 25,
            required: true,
            groupSeparator: ',',
            suffix: ' 字节',
        	value: allowedImageSize
        });
        
        //文档文件扩展名
        var allowedDocExtension = "<s:property value='systemConfig.allowedDocExtension' />";
        $('#allowed_doc_extension').combobox({
            valueField: 'ext',
            textField: 'text',
            multiple: true,
            editable: false,
            required: true,
            height: 25,
            separator: '|',
            panelHeight: 'auto',
            value: allowedDocExtension,
            data: [
                {ext:'doc',text:'Word97-2003'},
                {ext:'docx',text:'Word'},
                {ext:'xls',text:'Excel97-2003'},
                {ext:'xlsx',text:'Excel'},
                {ext:'ppt',text:'PowerPoint97-2003'},
                {ext:'pptx',text:'PowerPoint'},
                {ext:'pdf',text:'PDF'},
                {ext:'txt',text:'TXT'}
            ]
        });
        
        //文档文件大小
        var allowedDocSize = "<s:property value='systemConfig.allowedDocSize' />";
        $('#allowed_doc_size').numberbox({
            height: 25,
            required: true,
            groupSeparator: ',',
            suffix: ' 字节',
            value: allowedDocSize
        });
        
        //压缩文件扩展名
        var allowedCompressExtension = "<s:property value='systemConfig.allowedCompressExtension' />";
        $('#allowed_compress_extension').combobox({
            valueField: 'ext',
            textField: 'text',
            multiple: true,
            editable: false,
            required: true,
            height: 25,
            separator: '|',
            panelHeight: 'auto',
            value: allowedCompressExtension,
            data: [
                {ext:'zip',text:'ZIP'},
                {ext:'rar',text:'RAR'},
                {ext:'7z',text:'7-ZIP'}
            ]
        });
        
        //压缩文件大小
        var allowedCompressSize = "<s:property value='systemConfig.allowedCompressSize' />";
        $('#allowed_compress_size').numberbox({
            height: 25,
            required: true,
            groupSeparator: ',',
            suffix: ' 字节',
            value: allowedCompressSize
        });
        
        $('#btn_undo').unbind('click');
        $('#btn_undo').click(function() {
        	$('#config_form').form('reset');
        });
        
        $('#btn_save').unbind('click');
        $('#btn_save').click(function() {
            if ($('#config_form').form('validate')) {
            	$('.tooltip').remove();
            	updateSystemConfig();
            } else {
            	$.messager.alert('<s:text name="title.error" />', '<s:text name="messages.error.1003" />');
            }
        });
    }
    
    function updateSystemConfig() {
    	var systemConfigAPI = new SystemConfigAPI({
            urlPrefix : urlPrefix
        });
    	
    	var paramData = {};
    	paramData.old_password = $('#old_password').textbox('getValue');
    	paramData.password = $('#new_password').textbox('getValue');
    	paramData.regist_enabled = ($('#regist_enabled:checked').length > 0);
    	paramData.news_count = $('#news_count').numberspinner('getValue');
    	paramData.tender_count = $('#tender_count').numberspinner('getValue');
    	paramData.tender_sort = $('#tender_sort').combobox('getValue');
    	paramData.member_count = $('#member_count').numberspinner('getValue');
        paramData.contact_copy_required = ($('#contact_copy_required:checked').length > 0);
        paramData.license_copy_required = ($('#license_copy_required:checked').length > 0);
        paramData.legal_copy_required = ($('#legal_copy_required:checked').length > 0);
        paramData.physical_delete = ($('#physical_delete:checked').length > 0);
        paramData.upload_root_folder = $('#upload_root_folder').textbox('getValue');
        paramData.qualifications_save_folder = $('#qualifications_save_folder').textbox('getValue');
        paramData.images_save_folder = $('#images_save_folder').textbox('getValue');
        paramData.upload_temp_folder = $('#upload_temp_folder').textbox('getValue');
        paramData.allowed_image_extension = $('#allowed_image_extension').combobox('getValues').join('|');
        paramData.allowed_image_size = $('#allowed_image_size').numberbox('getValue');
        paramData.allowed_doc_extension = $('#allowed_doc_extension').combobox('getValues').join('|');
        paramData.allowed_doc_size = $('#allowed_doc_size').numberbox('getValue');
        paramData.allowed_compress_extension = $('#allowed_compress_extension').combobox('getValues').join('|');
        paramData.allowed_compress_size = $('#allowed_compress_size').numberbox('getValue');
    	
        systemConfigAPI.update({
            onSuccess : function(data) {
                if (data && data.result) {
                	$.messager.alert('<s:text name="title.notification" />', $.format('<s:text name="messages.notification.success" />', '系统设定更新'));
                	initialize();
                } else {
                	if (data.msg && data.msg.length > 0) {
                		$.messager.alert('<s:text name="title.error" />', data.msg);
                	} else {
                		$.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.fail" />', '系统设定更新'));
                	}
                }
            },
            onAPIError : function(status, errors) {
                $.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.fail" />', '系统设定更新'));
            },
            onServerError : function(jqXHR, textStatus, errorThrown) {
            	$.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.fail" />', '系统设定更新'));
            }
        },
        {
            param: JSON.stringify(paramData)
        });
    }
});
</script>
<form id="config_form" autocomplete="off">
  <div style="padding:5px 0;">
    <a id="btn_save" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'" style="width:80px">保存</a>
    <a id="btn_undo" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-undo'" style="width:80px">重置</a>
  </div>
  <div class="easyui-panel" title="管理员密码" style="width:420px;padding:10px;margin-bottom: 10px;" data-options="closable:false,collapsible:false,minimizable:false,maximizable:false">
    <table id="password_table" class="edit_table">
      <tr>
        <th width="140">旧密码</th>
        <td width="260"><input id="old_password" autocomplete="off" /></td>
      </tr>
      <tr>
        <th>新密码</th>
        <td><input id="new_password" autocomplete="off" /></td>
      </tr>
      <tr>
        <th>确认密码</th>
        <td><input id="confirm_password" autocomplete="off" /></td>
      </tr>
    </table>
  </div>
  <div class="easyui-panel" title="首页表示设定" style="width:420px;padding:10px;margin-bottom: 10px;" data-options="closable:false,collapsible:false,minimizable:false,maximizable:false">
    <table id="potal_table" class="edit_table">
      <tr>
        <th width="140">开放会员注册</th>
        <td width="260"><input id="regist_enabled" type="checkbox"></td>
      </tr>
      <tr>
        <th>新闻展示表示件数</th>
        <td><input id="news_count"></td>
      </tr>
      <tr>
        <th>招标信息表示件数</th>
        <td><input id="tender_count"></td>
      </tr>
      <tr>
        <th>招标信息表示顺序</th>
        <td><input id="tender_sort" style="width: 100px; height: 25px;"></td>
      </tr>
      <tr>
        <th>会员展示表示件数</th>
        <td><input id="member_count"></td>
      </tr>
    </table>
  </div>
  <div class="easyui-panel" title="会员审核设定" style="width:420px;padding:10px;margin-bottom: 10px;" data-options="closed:true,closable:false,collapsible:false,minimizable:false,maximizable:false">
    <table id="audit_table" class="edit_table">
      <tr>
        <th width="140">联系人证件复印件</th>
        <td width="260"><input id="contact_copy_required" type="checkbox" disabled="disabled"></td>
      </tr>
      <tr>
        <th>营业执照复印件</th>
        <td><input id="license_copy_required" type="checkbox" disabled="disabled"></td>
      </tr>
      <tr>
        <th>法人证件复印件</th>
        <td><input id="legal_copy_required" type="checkbox" disabled="disabled"></td>
      </tr>
    </table>
  </div>
  <div class="easyui-panel" title="系统设定" style="width:420px;padding:10px;margin-bottom: 10px;" data-options="closed:true,closable:false,collapsible:false,minimizable:false,maximizable:false">
    <table id="system_table" class="edit_table">
      <tr>
        <th width="140">记录物理删除</th>
        <td width="260"><input id="physical_delete" type="checkbox" disabled="disabled"></td>
      </tr>
    </table>
  </div>
  <div class="easyui-panel" title="上传设定" style="width:420px;padding:10px;margin-bottom: 10px;" data-options="closed:true,closable:false,collapsible:false,minimizable:false,maximizable:false">
    <table id="upload_table" class="edit_table">
      <tr>
        <th width="140">上传根目录</th>
        <td width="260"><input id="upload_root_folder" type="text" style="width:240px;" /></td>
      </tr>
      <tr>
        <th width="140">上传临时目录</th>
        <td width="260"><input id="upload_temp_folder" type="text" style="width:240px;" /></td>
      </tr>
      <tr>
        <th width="140">资质文件保存目录</th>
        <td><input id="qualifications_save_folder" type="text" style="width:240px;" /></td>
      </tr>
      <tr>
        <th width="140">图片文件保存目录</th>
        <td><input id="images_save_folder" type="text" style="width:240px;" /></td>
      </tr>
    </table>
  </div>
  <div class="easyui-panel" title="文件设定" style="width:420px;padding:10px;margin-bottom: 10px;" data-options="closable:false,collapsible:false,minimizable:false,maximizable:false">
    <table id="file_table" class="edit_table">
      <tr>
        <th width="140">图片文件扩展名</th>
        <td width="260"><input id="allowed_image_extension" style="width:140px;" /></td>
      </tr>
      <tr>
        <th width="140">图片文件大小</th>
        <td width="260"><input id="allowed_image_size" style="width:140px;" /></td>
      </tr>
      <tr>
        <th width="140">文档文件扩展名</th>
        <td width="260"><input id="allowed_doc_extension" style="width:140px;" /></td>
      </tr>
      <tr>
        <th width="140">文档文件大小</th>
        <td width="260"><input id="allowed_doc_size" style="width:140px;" /></td>
      </tr>
      <tr>
        <th width="140">压缩文件扩展名</th>
        <td width="260"><input id="allowed_compress_extension" style="width:140px;" /></td>
      </tr>
      <tr>
        <th width="140">压缩文件大小</th>
        <td width="260"><input id="allowed_compress_size" style="width:140px;" /></td>
      </tr>
    </table>
  </div>
</form>