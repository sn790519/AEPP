<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/themes/bootstrap/easyui.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/themes/icon.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/icheck/skins/all.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/magnific-popup.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/api.js"></script>
<script type="text/javascript">
$(document).ready(function(){	

	var tenderId= ${tenderId};
    $('#product_view_show').datagrid({
    	url: '${pageContext.request.contextPath}/api/TenderProductAPI.json',
	    title: '招标产品',
	    iconCls: 'icon-product',
	    queryParams: {
        	param: JSON.stringify({
        		tenderId:tenderId
        	})
        },
	    columns: [[
            {field:'product_name', title:'产品名称', width:'100px', align:'left', halign:'center', resizable:false},
            {field:'quantity', title:'数量', width:'80px', align:'center', halign:'center', resizable:false},
            {field:'unit', title:'单位', width:'80px', align:'center', halign:'center', resizable:false},
            {field:'product_description', title:'产品描述', width:'100px', align:'left', halign:'center', resizable:false}               
        ]],
        fit: false,
        fitColumns: true,
        striped: true,
        rownumbers: true,
        autoRowHeight: true,
        idField: 'product_id',
        loadFilter: function(data) {
	        var datax = {};	       
            if(data.status!=0){
            	//取得值异常时，制作空结果集返回
            	datax.total = 0;
            	datax.rows = new Array();
            	return datax;
            }          
            //取得值正常时，将数据过滤后返回	       
            if (data.total && data.total > 0) {
	            datax.total = data.total;
	            datax.rows = data.tplist;	           
	        }else{
	        	datax.total = 0;
            	datax.rows = new Array();
	        }
	        return datax;
	    },
	    onLoadSuccess:function(data) {
	    	
	    }
	});
   
}); 

</script>
 <table id="tender_table" class="edit_table" style="margin-left:8px;margin-top: 10px;">
      <tr>
        <th width="77">招标名称</th>
        <td colspan="7" style="width:460px;margin-left:10px;"><label id="tenderTitle" style="width:100;"><s:property value='tenderInformationDto.tenderTitle' /></label> </td>
      </tr>
      <tr>
        <th width="77">招标方式</th>
        <td colspan="7"><label id="tenderPattern" style="width:100;" ><s:property value='tenderInformationDto.tenderPatternName' /></label></td>
      </tr>
      <tr>
        <th width="77">报名开始日</th>
        <td colspan="7"><label id="enrollEndDate" style="width:100;" ><s:property value='tenderInformationDto.enrollEndDate' /></label></td>
      </tr>
      <tr>
        <th width="77">招标截止日</th>
        <td colspan="7"><label id="enrollEndDate" style="width:100;" ><s:property value='tenderInformationDto.enrollEndDate' /></label></td>
      </tr>
      <tr>
        <th width="77">投标截止日</th>
        <td colspan="7"><label id="tenderEndDate" style="width:100;" ><s:property value='tenderInformationDto.tenderEndDate' /></label></td>
      </tr>
      <tr>
        <th width="77">中标公布日</th>
        <td colspan="7"><label id="publishResultDate" style="width:100;" ><s:property value='tenderInformationDto.publishResultDate' /></label></td>
      </tr>
      <tr>
        <th width="77">内容说明</th>
        <td colspan="7"><label id="contentDescription" style="width:100;" ><s:property value='tenderInformationDto.contentDescription' /></label></td>
      </tr>
      <tr>
        <th width="77">供应商要求</th>
        <td colspan="7"><label id="vendorRequirement" style="width:100;" ><s:property value='tenderInformationDto.vendorRequirement' /></label></td>
      </tr>
      <tr>
        <th width="77">联系人</th>
        <td colspan="7"><label id="contactName" style="width:100;" ><s:property value='tenderInformationDto.contactName' /></label></td>
      </tr>
      <tr>
        <th width="77">联系电话</th>
        <td colspan="7"><label id="contactTel" style="width:100;" ><s:property value='tenderInformationDto.contactTel' /></label></td>
      </tr>
     
</table>
 <%-- <div style="margin-left:8px;margin-top:10px;">
    <label style="margin-left:8px;">招标名称：</label>
    <label id="tenderTitle" style="width:100;"><s:property value='tenderInformationDto.tenderTitle' /></label>   
  </div>
  <div style="margin-left:8px;margin-top:10px;">
    <label style="margin-left:8px;">招标方式：</label>
    <label id="tenderPattern" style="width:100;" ><s:property value='tenderInformationDto.tenderPatternName' /></label>
  </div>
  <div style="margin-left:8px;margin-top:10px;">
    <label style="margin-left:8px;">报名开始日：</label>
    <label id="enrollStartDate" style="width:100;" ><s:property value='tenderInformationDto.enrollStartDate' /></label>
  </div>
  <div style="margin-left:8px;margin-top:10px;">
    <label style="margin-left:8px;">报名截止日：</label>
    <label id="enrollEndDate" style="width:100;" ><s:property value='tenderInformationDto.enrollEndDate' /></label>
  </div>
  <div style="margin-left:8px;margin-top:10px;">
    <label style="margin-left:8px;">投标截止日：</label>
    <label id="tenderEndDate" style="width:100;" ><s:property value='tenderInformationDto.tenderEndDate' /></label>
  </div>
  <div style="margin-left:8px;margin-top:10px;">
    <label style="margin-left:8px;">中标公布日：</label>
    <label id="publishResultDate" style="width:100;" ><s:property value='tenderInformationDto.publishResultDate' /></label>
  </div>
  <div style="margin-left:8px;margin-top:10px;">
    <label style="margin-left:8px;">内容说明：</label>
    <label id="contentDescription" style="width:100;" ><s:property value='tenderInformationDto.contentDescription' /></label>
  </div>
  <div style="margin-left:8px;margin-top:10px;">
    <label style="margin-left:8px;">供应商要求：</label>
    <label id="vendorRequirement" style="width:100;" ><s:property value='tenderInformationDto.vendorRequirement' /></label>
  </div>
  <div style="margin-left:8px;margin-top:10px;">
    <label style="margin-left:8px;">联系人：</label>
    <label id="contactName" style="width:100;" ><s:property value='tenderInformationDto.contactName' /></label>
  </div>
  <div style="margin-left:8px;margin-top:10px;">
    <label style="margin-left:8px;">联系电话：</label>
    <label id="contactTel" style="width:100;" ><s:property value='tenderInformationDto.contactTel' /></label>
  </div> --%>
  <div style="margin-left:8px;margin-top:10px;">
    <table id="product_view_show"></table>     
  </div>
