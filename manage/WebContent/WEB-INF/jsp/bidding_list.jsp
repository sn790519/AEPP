<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/magnific-popup.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/api.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	var urlPrefix = "${pageContext.request.contextPath}/api/";
	var images = new Array(5);
	var imageLoadCount = 0;
	var globalTenderId = 0;
	
	var memberAPI = new MemberAPI({urlPrefix : urlPrefix});
       
	$("#datagridList").datagrid({
		iconCls : "icon-add",
		width:950,
		singleSelect: true,
		fitColumns : false, //设置为true将自动使列适应表格宽度以防止出现水平滚动,false则自动匹配大小
		idField : 'id', //标识列，一般设为id，可能会区分大小写，
		url: '${pageContext.request.contextPath}/api/TenderSearchAPI.json',
		loadMsg : "正在努力为您加载数据",
		striped: true,
		pagination:true,
		columns : [ [ {
		    field : 'tender_title',
			title : '产品名称',
			width : 100,
			formatter :rowFormater
		}, {
			field : 'enroll_end_date',
			title : '报名截止日期',
			width : 100,
		}, {
			field : 'tender_end_date',
			title : '投标截止日期',
			width : 100,
		}, {
			field : 'enroll_count',
			title : '报名',
			width : 100,
			size  : 1,	
			
		}, {
			field : 'enroll_count',
			title : '入围',
			width : 100,
			size  : 1,	
			
		}, {
			field : 'enroll_count',
			title : '投标',
			width : 100,
			size  : 1,	
			
		} 
		, {
			field : 'enroll_count',
			title : '中标',
			width : 100,
			size  : 1,	
			
		}, {
			field : 'status_name',
			title : '阶段',
			width : 100,
			size  : 1,	
			
		}, {
		    field : 'tender_id',
			title : '管理',
			width : 100,
			formatter :listFormater
		}
		] ],
		onClickCell : function(rowIndex, field, value) {

			if(field != 'tender_id'){
				return false;
			}
			
			var tenderGrid= $("#datagridList").datagrid('getData');
			var selectTenderId = JSON.stringify(value);
			var selectTendertitle = tenderGrid.rows[rowIndex].tender_title; 
			globalTenderId = selectTenderId;

			//window(弹出对话框)初始化
			newWindowShow(selectTenderId, selectTendertitle);

	    },
        loadFilter: function(data) {
        	var datax = {};
        	if (data.total && data.total > 0) {
    			datax.total = data.total;
    			datax.rows = data.tenders;
        	} else {
        		datax.total = 0;
    			datax.rows = new Array();
        	}
        	return datax;
        }
    });	
	
    function rowFormater(value, row, index) {
        return "<a href='tender_publish.action?id=" + row.tender_title + "' target='_block'>" + row.tender_title + "</a>";
    }
    
    function listFormater(value, row, index) {
        return "<input type='button' value='管理' target='_block'>";
    }

    $('#btn_search').click(function() {
    	var tenderListAPI = new TenderListAPI({
            urlPrefix : urlPrefix
        });
    	var paramData = {};
    	paramData.tender_title = $('#search').textbox('getValue');
    	tenderListAPI.search({
  	        onSuccess : function(data) {
  	        	$('#datagridList').datagrid("loadData",data);//
	        },
	        onAPIError : function(status, errors) {
	        },
	        onServerError : function(jqXHR, textStatus, errorThrown) {
	        }
	    },
	    {
	        param: JSON.stringify(paramData)
	    });
	});
    
    function newWindowShow(selectTenderId, selectTendertitle){
		
		//显示应标管理画面
		var actionUrl= 'tender_manage.action'+'?tenderId=' + selectTenderId + "&&tenderTitle=" + selectTendertitle;
		
		$('#tender_manage_win').window({
		    title: '<s:text name="tab.title.tender_bidding_manage" />',
		    width: 780,
		    height: 400,
		    cache: true,
		    href: actionUrl,
		    draggable: true,
		    modal: true
		}).window('open');
		
    }
});

</script>
<input type="text" class="easyui-textbox" id="search" name="search" style="width:400px" autocomplete="off">
<a id="btn_search" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="width:80px">查找</a>
<form  id="tender_publish" action="tender_publish.action" name="tender_publish" style="margin-top:10px;">
	<table id="datagridList" data-options="method:'post'"></table>
</form>

<div id="tender_manage_win"></div>
