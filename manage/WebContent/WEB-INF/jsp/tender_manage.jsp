<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<link rel="stylesheet" href="${pageContext.request.contextPath}/icheck/skins/all.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/icheck/icheck.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
$(function() {	
	
	var tenderId = ${tenderId};
	var tenderTitle = '${tenderTitle}';

	var urlPrefix = "${pageContext.request.contextPath}/api/";
	
	//刷新显示标题
	$('#tenderTitle').html('<s:text name="招标名称：" />' + tenderTitle);

	//tabs初始化
	tabsInit();
	//刷新显示数据Datagrid
    dataShow();
    
    function dataShow(){
		//刷新显示数据Datagrid
	    tenderEnrollInit(tenderId);
	    tenderQuoteInit(tenderId);
	    tenderBidInit(tenderId);
	    //alert('Data load ok.');
	}
    
	//tabs初始化
    function tabsInit(){
	    $('#tab').tabs({
	    	onSelect:function(title, index){
	    		//alert("title:"+title+ " == index:"+index );
	    		//单击某个TABS时，数据显示刷新
				switch(index)
				{
				case 0://选择[报名供应商]tab
					tenderEnrollInit(tenderId);
					break;
				case 1://选择[投标供应商]tab
					tenderQuoteInit(tenderId);
					break;
				case 2://选择[中标供应商]tab
					tenderBidInit(tenderId);
					break;
				default:
					break;
				}
	    	}
	    });
    }
	
    //报名供应商数据初始化
    function tenderEnrollInit(tenderId){
	
		$("#tenderEnroll").datagrid({
			url: '${pageContext.request.contextPath}/api/TenderManageAPI.json',
			iconCls : "icon-add",
			queryParams: {
	        	param: JSON.stringify({
	        		tenderId : tenderId
	        	})
	        },
	        striped: true,
	        fitColumns : false, //设置为true将自动使列适应表格宽度以防止出现水平滚动,false则自动匹配大小
			idField : 'tender_id', //标识列，一般设为id，可能会区分大小写，
			align : 'center',
			pagination : true,
			rownumbers: true,
	        autoRowHeight: false,
			loadMsg : "正在努力为您加载数据",
			columns : [ [  {
				field : 'ck',
				title : '全选',
				width : 100,
				checkbox : true,
				align : 'center',
			}, {
				field : 'tender_id',
				title : '',
				width : 100,
				align : 'center',
				
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'member_login_name',
				title : '',
				width : 100,
				align : 'center',
				
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'enterprise_name',
				title : '供应商',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'enterprise_category_name',
				title : '企业类型',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'area_code',
				title : '地区',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'rank_name',
				title : '会员级别',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'path',
				title : '报名文件',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'finalistFlagname',
				title : '是否入围',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			} ] ],
			loadFilter: function(data) {
		        var datax = {};
	
	            if(data.status!=0){
	            	//取得值异常时，制作空结果集返回
	            	datax.total = 0;
	            	datax.rows = new Array();
	            	return datax;
	            }
	            
	            //取得值正常时，将数据过滤后返回
		        var count = 0;
	            if (data.total && data.total > 0) {
		            datax.total = data.total;
		            datax.rows = data.tmlist;
		            count = data.total;
		        }else{
		        	datax.total = 0;
	            	datax.rows = new Array();
		        }
	            
	            //更新tab显示名称
		        var strTitle = '报名供应商(' + count + ')';
				var title1 = $('#tab').children().eq(0).children('.tabs-wrap').eq(0).children().eq(0).children().eq(0).children().eq(0).children().eq(0);
		        title1.text(strTitle); 
	
		        return datax;
		    },
		    onLoadSuccess:function(data) {
		    },
		    onLoadError:function(data) {
		    }
		});
		$("#tenderEnroll").datagrid('hideColumn', 'tender_id');
		$("#tenderEnroll").datagrid('hideColumn', 'member_login_name');
		
		$('#btn_audit').click(function(row) {
			var flag="audit";
			var tenderFinalistAPI = new TenderFinalistAPI({
		        urlPrefix : urlPrefix
		    });
			var rowsData = $("#tenderEnroll").datagrid('getData');
	    	var paramData = {};
	    	paramData.member_login_name=$('#member_login_name').text(row.member_login_name);
	    	paramData.tender_id=$('#tender_id').text(row.tender_id);
	    	paramData.flag=flag;
	    	paramData.list=rowsData;
	    	tenderFinalistAPI.insert({
	            onSuccess : function(data) {
	            	alert(JSON.stringify(data));
	                if (data && data.result) {
	              	    $.messager.alert('<s:text name="title.notification" />', $.format('<s:text name="messages.notification.success" />', '系统设定更新'));
	                } else {
	                	if (data.msg && data.msg.length > 0) {
	                	    $.messager.alert('<s:text name="title.error" />', data.msg);
	                	} else {
	                		$.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.fail" />', '系统设定更新'));
	                	}
	                }
	            },
	            onAPIError : function(status, errors) {
	                $.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.fail" />', '系统设定更新'));
	            },
	            onServerError : function(jqXHR, textStatus, errorThrown) {
	            	$.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.fail" />', '系统设定更新'));
	            }
	        },
	        
	        {
	            param: JSON.stringify(paramData)
	        });
	    	   	
		});
	
		$('#btn_unaudit').click(function(row) {
			var flag="unaudit";
			var tenderFinalistAPI = new TenderFinalistAPI({
		        urlPrefix : urlPrefix
		    });
			var rowsData = $("#tenderEnroll").datagrid('getData');
	    	var paramData = {};
	    	paramData.member_login_name=$('#member_login_name').text(row.member_login_name);
	    	paramData.tender_id=$('#tender_id').text(row.tender_id);
	    	paramData.flag=flag;
	    	paramData.list=rowsData;
	    	tenderFinalistAPI.insert({
	            onSuccess : function(data) {
	                if (data && data.result) {
	              	    $.messager.alert('<s:text name="title.notification" />', $.format('<s:text name="messages.notification.success" />', '系统设定更新'));
	                } else {
	                	if (data.msg && data.msg.length > 0) {
	                	    $.messager.alert('<s:text name="title.error" />', data.msg);
	                	} else {
	                		$.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.fail" />', '系统设定更新'));
	                	}
	                }
	            },
	            onAPIError : function(status, errors) {
	                $.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.fail" />', '系统设定更新'));
	            },
	            onServerError : function(jqXHR, textStatus, errorThrown) {
	            	$.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.fail" />', '系统设定更新'));
	            }
	        },
	        
	        {
	            param: JSON.stringify(paramData)
	        });
	    	    	
		});

    }
    
	//投标供应商数据初始化
    function tenderQuoteInit(tenderId){
    
	    $("#tenderQuote").datagrid({
			url: '${pageContext.request.contextPath}/api/TenderBidlistAPI.json',
			iconCls : "icon-add",
			queryParams: {
	        	param: JSON.stringify({
	        		tenderId : tenderId
	        	})
	        },
			fitColumns : false, //设置为true将自动使列适应表格宽度以防止出现水平滚动,false则自动匹配大小
			idField : 'tender_id', //标识列，一般设为id，可能会区分大小写，
			align : 'center',
			pagination : true,
			rownumbers: true,
	        autoRowHeight: false,
			loadMsg : "正在努力为您加载数据",
			columns : [ [  {
				field : 'ck',
				title : '',
				width : 100,
				checkbox : true,
				align : 'center',
				
			
			}, {
				field : 'tender_id',
				title : '',
				width : 100,
				align : 'center',
				
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'member_login_name',
				title : '',
				width : 100,
				align : 'center',
				
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'enterprise_name',
				title : '供应商',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'enterprise_category_name',
				title : '企业类型',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'area_code',
				title : '地区',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'rank_name',
				title : '会员级别',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'total',
				title : '投标总价',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'path',
				title : '投标文件',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'status_name',
				title : '状态',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			} ] ],
			loadFilter: function(data) {
		        var datax = {};
		        if(data.status!=0){
	            	//取得值异常时，制作空结果集返回
	            	datax.total = 0;
	            	datax.rows = new Array();
	            	return datax;
	            }
	            
		        var count = 0;
		        if (data.total && data.total > 0) {
		            datax.total = data.total;
		            datax.rows = data.tblist;
		            count = data.total;
		        }else{
		        	datax.total = 0;
	            	datax.rows = new Array();
		        }
		        var strTitle = '投标供应商(' + count + ')';
		        var title1 = $('#tab').children().eq(0).children('.tabs-wrap').eq(0).children().eq(0).children().eq(1).children().eq(0).children().eq(0);
		        title1.text(strTitle);
		        return datax;
		    }  
		});
		$("#tenderQuote").datagrid('hideColumn', 'tender_id');
		$("#tenderQuote").datagrid('hideColumn', 'member_login_name');
	
		$('#btn_bid').click(function(row) {		
			var tenderBidAPI = new TenderBidAPI({
		        urlPrefix : urlPrefix
		    });
			var rowsData = $("#tenderQuote").datagrid('getData');
	    	var paramData = {};
	    	paramData.member_login_name=$('#member_login_name').text(row.member_login_name);
	    	paramData.tender_id=$('#tender_id').text(row.tender_id);    	
	    	paramData.listbid=rowsData;
	    	tenderBidAPI.insert({
	            onSuccess : function(data) {
	                if (data && data.result) {
	              	    $.messager.alert('<s:text name="title.notification" />', $.format('<s:text name="messages.notification.success" />', '系统设定更新'));
	                } else {
	                	if (data.msg && data.msg.length > 0) {
	                	    $.messager.alert('<s:text name="title.error" />', data.msg);
	                	} else {
	                		$.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.fail" />', '系统设定更新'));
	                	}
	                }
	            },
	            onAPIError : function(status, errors) {
	                $.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.fail" />', '系统设定更新'));
	            },
	            onServerError : function(jqXHR, textStatus, errorThrown) {
	            	$.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.fail" />', '系统设定更新'));
	            }
	        },
	        
	        {
	            param: JSON.stringify(paramData)
	        });
		});

    }
    
	//中标供应商数据初始化
    function tenderBidInit(tenderId){
    
		$("#tenderBid").datagrid({
			url: '${pageContext.request.contextPath}/api/TenderBidSuppliersAPI.json',
			iconCls : "icon-add",
			queryParams: {
	        	param: JSON.stringify({
	        		tenderId : tenderId
	        	})
	        },
			fitColumns : false, //设置为true将自动使列适应表格宽度以防止出现水平滚动,false则自动匹配大小
			idField : 'tender_id', //标识列，一般设为id，可能会区分大小写，
			align : 'center',
			pagination : true,
			rownumbers: true,
	        autoRowHeight: false,
			loadMsg : "正在努力为您加载数据",
			columns : [ [  {
				field : 'ck',
				title : '',
				width : 100,
				checkbox : true,
				align : 'center',
				
			
			}, {
				field : 'tender_id',
				title : '',
				width : 100,
				align : 'center',
				
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'member_login_name',
				title : '',
				width : 100,
				align : 'center',
				
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'enterprise_name',
				title : '供应商',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'enterprise_category_name',
				title : '企业类型',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'area_code',
				title : '地区',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'rank_name',
				title : '会员级别',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'total',
				title : '投标总价',
				width : 100,
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			}, {
				field : 'path',
				title : '投标文件',
				width : 100,
				
				align : 'center',
				editor : {
				    type : 'type',
					options : {}
				}
			}, {
				field : 'status_name',
				title : '状态',
				width : 100,
				
				align : 'center',
				editor : {
				    type : 'text',
					options : {}
				}
			} ] ],
			loadFilter: function(data) {
		        var datax = {};
		        if(data.status!=0){
	            	//取得值异常时，制作空结果集返回
	            	datax.total = 0;
	            	datax.rows = new Array();
	            	return datax;
	            }
	            
		        var count = 0;
		        if (data.total && data.total > 0) {
		            datax.total = data.total;
		            datax.rows = data.tbslist;
		            count = data.total;
		        }else{
		        	datax.total = 0;
	            	datax.rows = new Array();
		        }
		        var strTitle = '中标供应商(' + count + ')';
		        var title1 = $('#tab').children().eq(0).children('.tabs-wrap').eq(0).children().eq(0).children().eq(2).children().eq(0).children().eq(0);
		        title1.text(strTitle);
	
				return datax;
		    }      
			});
		$("#tenderBid").datagrid('hideColumn', 'tender_id');
		$("#tenderBid").datagrid('hideColumn', 'member_login_name');
	
		$('#btn_publicity').click(function(row) {		
			var tenderBidSuppliersUpdateAPI = new TenderBidSuppliersUpdateAPI({
		        urlPrefix : urlPrefix
		    });
			var rowsData = $("#tenderBid").datagrid('getData');
	    	var paramData = {};
	    	
	    	paramData.tender_id=$('#tender_id').text(row.tender_id);    	
	    	paramData.listbids=rowsData;
	    	tenderBidSuppliersUpdateAPI.insert({
	            onSuccess : function(data) {
	                if (data && data.result) {
	              	    $.messager.alert('<s:text name="title.notification" />', $.format('<s:text name="messages.notification.success" />', '系统设定更新'));
	                } else {
	                	if (data.msg && data.msg.length > 0) {
	                	    $.messager.alert('<s:text name="title.error" />', data.msg);
	                	} else {
	                		$.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.fail" />', '系统设定更新'));
	                	}
	                }
	            },
	            onAPIError : function(status, errors) {
	                $.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.fail" />', '系统设定更新'));
	            },
	            onServerError : function(jqXHR, textStatus, errorThrown) {
	            	$.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.fail" />', '系统设定更新'));
	            }
	        },
	        
	        {
	            param: JSON.stringify(paramData)
	        });
		});

    }
    /* 
	//重新载入报名供应商表
	function reloadDataEnroll(tenderId){
		//alert("loadDataEnroll:"+tenderId);
		$("#tenderEnroll").datagrid({
			queryParams: {
	        	param: JSON.stringify({
	        		tenderId : tenderId
	        	})
			}
	    });
		$('#tenderEnroll').datagrid("reload");
	}

	//重新载入投标供应商表
	function reloadDataQuote(tenderId){
		//alert("loadDataQuote:"+tenderId);
		$("#tenderQuote").datagrid({
			queryParams: {
	        	param: JSON.stringify({
	        		tenderId : tenderId
	        	})
			}
	    });
		$('#tenderQuote').datagrid("reload");
	}

	//重新载入中标供应商表
	function reloadDataBid(tenderId){
		//alert("loadDataBid:"+tenderId);
		$("#tenderBid").datagrid({
			queryParams: {
	        	param: JSON.stringify({
	        		tenderId : tenderId
	        	})
			}
	    });
		$('#tenderBid').datagrid("reload");
	}
	 */
});



</script>

<div id="tenderTitle">
    <s:text name="招标名称：" />
</div>
<div id="tab" class="easyui-tabs" data-options="fit:false, border:false, pill:false" style="width: 760px;margin-top:20px;">
	<div title="报名供应商" style="margin-top:10px;">
		<div style="margin-top:10px;">
			<a id="btn_audit" href="#" class="easyui-linkbutton" style="margin-left:10px;">审核通过</a>
			<a id="btn_unaudit" href="#" class="easyui-linkbutton" style="margin-left:5px;">审核不通过</a>
		</div>
		<div style="margin-top:10px;">
			<table id="tenderEnroll" ></table>
	  	</div>
    </div>
		<div title="投标供应商" style="margin-top:10px;">
		<div style="margin-top:10px;">
			<a id="btn_bid" href="#" class="easyui-linkbutton" style="margin-left:10px;">授标</a>
		</div>
		<div style="margin-top:10px;">
			<table id="tenderQuote"></table>
		</div>
	</div>
    <div title="中标供应商" style="margin-top:10px;">  
		<div style="margin-top:10px;">		      
			<a id="btn_publicity" href="#" class="easyui-linkbutton" style="margin-left:10px;">公示</a> 		   
		</div>
		<div style="margin-top:10px;">		      
			<table id="tenderBid" ></table>		      
		</div>
	</div>	  
</div>
