<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/icheck/skins/all.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/icheck/icheck.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/api.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/validate.js"></script>
<script type="text/javascript">
$(function() {
    var urlPrefix = "${pageContext.request.contextPath}/api/";
    var selectedVender = new Array();
    var tenderPattern = 1;
    var tenderAPI = new TenderAPI({urlPrefix : urlPrefix});
    
  	//for debug
	try{throw ''} catch(e){}
	
    initialize();
	
	function initialize() {
	    
	    $('#tender_title').textbox({
    		height: 28,
    		width: 650,
    		required: true,
    		validType: {
    			length: [4, 50]
    		}
    	});
	    
	    $('#content_description').textbox({
    		height: 80,
    		width: 650,
    		multiline: true,
    		required: true,
    		validType: {
    			length: [1, 500]
    		}
    	});
	    
	    $('#vendor_requirement').textbox({
    		height: 80,
    		width: 650,
    		multiline: true,
    		required: true,
    		validType: {
    			length: [1, 200]
    		}
    	});
	    
	    var today = (new Date()).format('yyyy-MM-dd');
	    $('#enroll_start_date').datebox({
	        height: 28,
	        width: 100,
    		required: true,
    		editable: false,
    		value: today,
    		onSelect: function(date) {
    		    if (date.format('yyyy-MM-dd') > $('#enroll_end_date').datebox('getValue')) {
    		        $('#enroll_end_date').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
    		    if (date.format('yyyy-MM-dd') > $('#tender_end_date').datebox('getValue')) {
    		        $('#tender_end_date').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
    		    if (date.format('yyyy-MM-dd') > $('#publish_result_date').datebox('getValue')) {
    		        $('#publish_result_date').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
    		}
	    });
	    
	    $('#enroll_end_date').datebox({
	        height: 28,
	        width: 100,
    		required: true,
    		editable: false,
    		value: today,
    		onSelect: function(date) {
    		    if (date.format('yyyy-MM-dd') < $('#enroll_start_date').datebox('getValue')) {
    		        $('#enroll_end_date').datebox('setValue', '').datebox('showPanel');
    		    } else {
    		        if (date.format('yyyy-MM-dd') > $('#tender_end_date').datebox('getValue')) {
        		        $('#tender_end_date').datebox('setValue', date.format('yyyy-MM-dd'));
        		    }
    		        if (date.format('yyyy-MM-dd') > $('#publish_result_date').datebox('getValue')) {
        		        $('#publish_result_date').datebox('setValue', date.format('yyyy-MM-dd'));
        		    }
    		    }
    		}
	    });
	    
	    $('#tender_end_date').datebox({
	        height: 28,
	        width: 100,
    		required: true,
    		editable: false,
    		value: today,
    		onSelect: function(date) {
    		    if (date.format('yyyy-MM-dd') < $('#enroll_end_date').datebox('getValue')) {
    		        $('#tender_end_date').datebox('setValue', '').datebox('showPanel');
    		    } else {
    		        if (date.format('yyyy-MM-dd') > $('#publish_result_date').datebox('getValue')) {
        		        $('#publish_result_date').datebox('setValue', date.format('yyyy-MM-dd'));
        		    }
    		    }
    		}
	    });
	    
	    $('#publish_result_date').datebox({
	        height: 28,
	        width: 100,
    		required: true,
    		editable: false,
    		value: today,
    		onSelect: function(date) {
    		    if (date.format('yyyy-MM-dd') < $('#tender_end_date').datebox('getValue')) {
    		        $('#publish_result_date').datebox('setValue', '').datebox('showPanel');
    		    }
    		}
	    });
	    
	    $('#contact_name').textbox({
    		height: 28,
    		width: 100,
    		required: true,
    		validType: {
    			length: [1, 20]
    		}
    	});
	    
	    $('#contact_tel').textbox({
    		height: 28,
    		width: 100,
    		required: true,
    		validType: {
    			length: [1, 15],
    			telephoneOrMobile: []
    		}
    	});
	    
	    $('#tender_pattern1').iCheck({
			radioClass: 'iradio_minimal-blue'
		}).iCheck('check').on('ifChecked', function(event) {
		    changeTenderPattern(false);
		});
		
		$('#tender_pattern2').iCheck({
			radioClass: 'iradio_minimal-blue'
		}).iCheck('uncheck').on('ifChecked', function(event) {
		    changeTenderPattern(true);
		});
		
		$('#product_grid').datagrid({
		    title: '招标产品(最多5件)',
		    iconCls: 'icon-product',
		    columns: [[
                {field:'product_name', title:'产品名称', width:'22%', align:'left', halign:'center', resizable:false},
                {field:'quantity', title:'数量', width:'10%', align:'right', halign:'center', resizable:false},
                {field:'unit', title:'单位', width:'10%', align:'center', halign:'center', resizable:false},
                {field:'product_description', title:'产品描述', width:'40%', align:'left', halign:'center', resizable:false},
                {field:'opt', title:'操作', width:'17%', align:'center', halign:'center', resizable:false,
                    formatter: function(value, row, index) {
                        return '<a id="btn_edit" class="button_edit" href="#">编辑</a>&nbsp;<a id="btn_delete" class="button_delete" href="#">删除</a>';
                    }
                }
            ]],
            fit: true,
            fitColumns: true,
            striped: true,
            rownumbers: true,
            singleSelect: true,
            autoRowHeight: false,
            toolbar: '#tender_publish_toolbar',
            idField: 'product_id',
            onLoadSuccess: function(data) {
                if (data.total > 0) {
                	$('.button_edit').linkbutton({
                        iconCls: 'icon-edit',
                        onClick: function() {
                            var index = $(this).index('.button_edit');
                            var productData = $('#product_grid').datagrid('getData').rows[index];
                            $('#win_product').window({
            		            onOpen: function() {
            		                $('#product_form').form('clear');
            		                $('#product_form #product_name').textbox('setValue', productData.product_name);
            			            $('#product_form #quantity').numberbox('setValue', productData.quantity);
            			            $('#product_form #unit').textbox('setValue', productData.unit);
            			            $('#product_form #product_description').textbox('setValue', productData.product_description);
            			            $('#product_form #edit_index').val(index);
            		            },
            		            onClose: function() {
            		                $('#product_form').form('clear');
            		            }
            		        });
            		        $('#win_product').window('open');
                        }
                    });
                    
                    $('.button_delete').linkbutton({
                        iconCls: 'icon-delete',
                        onClick: function() {
                            var index = $(this).index('.button_delete');
                            $('#product_grid').datagrid('deleteRow', index);
                            $('#btn_add').linkbutton('enable');
                        }
                    });
                    
                    $('#product_grid').datagrid('fixRowHeight');
                }
            }
		});
	    
	    $('#product_form #product_name').textbox({
    		height: 28,
    		width: 290,
    		required: true,
    		validType: {
    			length: [1, 100]
    		}
    	});
	    
	    $('#product_form #quantity').numberbox({
    		height: 28,
    		width: 100,
    		required: true,
    		min: 1,
    		max: 99999
    	});
	    
	    $('#product_form #unit').textbox({
    		height: 28,
    		width: 100,
    		required: true,
    		validType: {
    			length: [1, 5]
    		}
    	});
	    
	    $('#product_form #product_description').textbox({
    		height: 80,
    		width: 290,
    		required: true,
    		multiline: true,
    		validType: {
    			length: [1, 100]
    		}
    	});
		
		$('#btn_add').linkbutton({
		    onClick: function() {
		        $('#win_product').window({
		            onOpen: function() {
		                $('#product_form').form('clear');
		            },
		            onClose: function() {
		                $('#product_form').form('clear');
		            }
		        });
		        $('#win_product').window('open');
		    }
		});
		
		$('#product_form #btn_save').linkbutton({
		    onClick: function() {
		        if ($('#product_form').form('validate')) {
		            var productData = {};
		            productData.product_name = $('#product_form #product_name').textbox('getValue');
		            productData.quantity = $('#product_form #quantity').numberbox('getValue');
		            productData.unit = $('#product_form #unit').textbox('getValue');
		            productData.product_description = $('#product_form #product_description').textbox('getValue');
		            
		            var gridData = $('#product_grid').datagrid('getData');
		            if ($('#product_form #edit_index').val() == "") {
			            gridData.total = gridData.rows.push(productData);
		            } else {
		                var index = $('#product_form #edit_index').val();
		                gridData.rows[index] = productData;
		            }
		            $('#product_grid').datagrid('loadData', gridData);
		            
		            if (gridData.total && gridData.total >= 5) {
  	                    $('#btn_add').linkbutton('disable');
  	                } else {
  	                    $('#btn_add').linkbutton('enable');
  	                }
		            
		            $('#win_product').window('close');
		        }
		    }
		});
		
		$('#product_form #btn_cancel').linkbutton({
		    onClick: function() {
		        $('#win_product').window('close');
		    }
		});
		
		$('#btn_submit').linkbutton({
		    text: '保存',
		    iconCls: 'icon-save',
		    width: 80,
		    onClick: function() {
		        if ($('#tender_form').form('validate')) {
		            if ($('#product_grid').datagrid('getData').total == 0) {
		                $.messager.alert('<s:text name="title.error" />', '<s:text name="messages.error.1009" />');
		            } else if (tenderPattern == 2 && selectedVender.length == 0) {
	                    $.messager.alert('<s:text name="title.error" />', '<s:text name="messages.error.1010" />');
		            } else {
		                var paramData = {};
		                paramData.tender_information = {};
		                paramData.tender_product = new Array();
		                paramData.tender_vender = new Array();
                		
		                paramData.tender_information.tender_title = $('#tender_form #tender_title').textbox('getValue');
                		paramData.tender_information.content_description = $('#tender_form #content_description').textbox('getValue');
                		paramData.tender_information.vendor_requirement = $('#tender_form #vendor_requirement').textbox('getValue');
                		paramData.tender_information.enroll_start_date = $('#tender_form #enroll_start_date').datebox('getValue');
                		paramData.tender_information.enroll_end_date = $('#tender_form #enroll_end_date').datebox('getValue');
                		paramData.tender_information.tender_end_date = $('#tender_form #tender_end_date').datebox('getValue');
                		paramData.tender_information.publish_result_date = $('#tender_form #publish_result_date').datebox('getValue');
                		paramData.tender_information.contact_name = $('#tender_form #contact_name').textbox('getValue');
                		paramData.tender_information.contact_tel = $('#tender_form #contact_tel').textbox('getValue');
                		paramData.tender_information.tender_pattern = tenderPattern;
                		
                		paramData.tender_product = $('#product_grid').datagrid('getData').rows;
                		
                		if (tenderPattern == 2) {
                		    $.each(selectedVender, function(index, row) {
                		        var tender_vender = {};
                		        tender_vender.member_login_name = row.member_login_name;
                		        tender_vender.enterprise_name = row.enterprise_name;
                		        paramData.tender_vender.push(tender_vender);
                		    });
                		}
                		
                		tenderAPI.publish({
                	        onSuccess : function(data) {
                	            $.messager.alert('<s:text name="title.notification" />', $.format('<s:text name="messages.notification.success" />', '招标信息发布'));
                	            $('#tender_form').form('clear');
                		        $('#product_grid').datagrid('loadData', {total:0, rows:[]});
                		        selectedVender = new Array();
                		        $('#vender_count').text("0");
                		        $('#tender_pattern1').iCheck('check');
                		        var today = (new Date()).format('yyyy-MM-dd');
                		        $('#enroll_start_date').datebox('setValue', today);
                		        $('#enroll_end_date').datebox('setValue', today);
                		        $('#tender_end_date').datebox('setValue', today);
                		        $('#publish_result_date').datebox('setValue', today);
                	        },
                	        onAPIError : function(status, errors) {
                	            $.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.fail" />', '招标信息发布'));
                	        },
                	        onServerError : function(jqXHR, textStatus, errorThrown) {
                	            $.messager.alert('<s:text name="title.error" />', $.format('<s:text name="messages.notification.fail" />', '招标信息发布'));
                	        }
                	    },
                	    {
                	        param: JSON.stringify(paramData)
                	    });
		            }
		        } else {
		            $.messager.alert('<s:text name="title.error" />', '<s:text name="messages.error.1003" />');
		        }
		    }
		});
		
		$('#btn_reset').linkbutton({
		    text: '重置',
		    iconCls: 'icon-undo',
		    width: 80,
		    onClick: function() {
		        $('#tender_form').form('clear');
		        $('#product_grid').datagrid('loadData', {total:0, rows:[]});
		        selectedVender = new Array();
		        $('#vender_count').text("0");
		        $('#tender_pattern1').iCheck('check');
		        var today = (new Date()).format('yyyy-MM-dd');
		        $('#enroll_start_date').datebox('setValue', today);
		        $('#enroll_end_date').datebox('setValue', today);
		        $('#finalist_end_date').datebox('setValue', today);
		        $('#publish_result_date').datebox('setValue', today);
		    }
		});
		
		$('#btn_select_vender').linkbutton({
		    text: '选择供应商',
		    iconCls: 'icon-vender',
		    disabled: true,
		    onClick: function() {
		        $('#win_vender').window({
		            onOpen: function() {
		                $('#vender_grid').datagrid({
		                    url: '${pageContext.request.contextPath}/api/MemberSearchAPI.json',
		        		    columns: [[
								{field:'selected', checkbox: true, resizable:false},
		                        {field:'enterprise_name', title:'企业名称', width:380, align:'left', halign:'center', resizable:false},
		                        {field:'enterprise_property_name', title:'企业性质', width:150, align:'center', halign:'center', resizable:false},
		                        {field:'enterprise_category_name', title:'行业分类', width:100, align:'center', halign:'center', resizable:false},
		                        {field:'rank_name', title:'会员等级', width:100, align:'center', halign:'center', resizable:false}
		                    ]],
		                    fit: true,
		                    fitColumns: false,
		                    striped: true,
		                    rownumbers: true,
		                    singleSelect: false,
		                    autoRowHeight: false,
		                    idField: 'member_login_name',
		                    pagination: true,
		                    pageSize: 15,
		                    pagePosition: 'bottom',
		                    pageList: [15],
		                    queryParams: {
		                    	param: JSON.stringify({
		                    		audit_phase_from:9
		                    	})
		                    },
		                    loadFilter: function(data) {
		                    	var datax = {};
		                    	if (data.total && data.total > 0) {
		                			datax.total = data.total;
		                			datax.rows = data.members;
		                    	} else {
		                    		datax.total = 0;
		                			datax.rows = new Array();
		                    	}
		                    	return datax;
		                    },
		                    onLoadSuccess: function(data) {
		                        if (data.total > 0) {
		                            $('#vender_grid').datagrid('fixRowHeight');
		                            
		                            if (selectedVender.length > 0) {
		                                $.each(selectedVender, function(index, row) {
		                                    $('#vender_grid').datagrid('selectRecord', row.member_login_name);
		                                });
		                            }
		                        }
		                    }
		        		});
		            }
		        });
		        $('#win_vender').window('open');
		    }
		});
		
		$('#win_vender #btn_cancel').linkbutton({
		    onClick: function() {
		        $('#vender_grid').datagrid('uncheckAll');
		        $('#win_vender').window('close');
		    }
		});
		
		$('#win_vender #btn_select').linkbutton({
		    onClick: function() {
		        selectedVender = new Array();
		        $.each($('#vender_grid').datagrid('getChecked'), function(index, row) {
		            selectedVender.push(row);
		        });
		        $('#vender_count').text(selectedVender.length);
		        $('#vender_grid').datagrid('uncheckAll');
		        $('#win_vender').window('close');
		    }
		});
	}
	
	function changeTenderPattern(inviteFlag) {
	    if (inviteFlag) {
	        $('#btn_select_vender').linkbutton('enable');
	        $('#invited_vender').show();
	        tenderPattern = 2;
	    } else {
	        $('#btn_select_vender').linkbutton('disable');
	        $('#invited_vender').hide();
	        tenderPattern = 1;
	    }
	}
});	
</script>
<div style="margin-bottom: 5px;">
  <a id="btn_submit" href="#"></a>
  <a id="btn_reset" href="#"></a>
</div>
<div class="easyui-panel" data-options="border:0">
  <form id="tender_form">
    <table id="tender_table" class="edit_table" style="margin-bottom: 10px;">
      <tr>
        <th width="77">招标名称</th>
        <td colspan="7"><input type="text" id="tender_title"></td>
      </tr>
      <tr>
        <th width="77">内容说明</th>
        <td colspan="7"><input type="text" id="content_description"></td>
      </tr>
      <tr>
        <th width="77">供应商要求</th>
        <td colspan="7"><input type="text" id="vendor_requirement"></td>
      </tr>
      <tr>
        <th width="77">报名开始日</th>
        <td width="100"><input type="text" id="enroll_start_date"></td>
        <th width="77">报名截止日</th>
        <td width="100"><input type="text" id="enroll_end_date"></td>
        <th width="77">投标截止日</th>
        <td width="100"><input type="text" id="tender_end_date"></td>
        <th width="77">中标公布日</th>
        <td width="100"><input type="text" id="publish_result_date"></td>
      </tr>
      <tr>
        <th width="77">联系人</th>
        <td width="100"><input type="text" id="contact_name"></td>
        <th width="77">联系电话</th>
        <td width="100"><input type="text" id="contact_tel"></td>
        <th width="77">招标方式</th>
        <td colspan="3" valign="middle">
          <input id="tender_pattern1" name="tender_pattern" type="radio"><label for="tender_pattern1">公开招标</label>&nbsp;&nbsp;
          <input id="tender_pattern2" name="tender_pattern" type="radio"><label for="tender_pattern2">定向邀标</label>
        </td>
      </tr>
      <tr id="invited_vender" style="display: none;">
        <th width="77">供应商</th>
        <td colspan="7"><a id="btn_select_vender" href="#"></a>&nbsp;&nbsp;已选择&nbsp;<label id="vender_count">0</label>&nbsp;供应商</td>
      </tr>
    </table>
    <div style="width: 782px; height: 249px;">
      <table id="product_grid"></table>
    </div>
  </form>
</div>
<div id="tender_publish_toolbar">
  <a id="btn_add" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add'">追加</a>
</div>
<div id="win_product" class="easyui-window" title="产品信息" data-options="modal:true, closed:true, collapsible:false, minimizable:false, maximizable:false, resizable:false, iconCls:'icon-product'" style="width:430px; height: auto; padding:10px;">
  <form id="product_form">
    <table id="product_table" class="edit_table">
      <tr>
        <th width="75">产品名称</th>
        <td width="300"><input type="text" id="product_name"></td>
      </tr>
      <tr>
        <th>数量/单位</th>
        <td>
          <input type="text" id="quantity">
          <input type="text" id="unit">
        </td>
      </tr>
      <tr>
        <th>产品描述</th>
        <td><input type="text" id="product_description"></td>
      </tr>
    </table>
    <input id="edit_index" type="hidden">
    <div style="text-align: right; margin-top: 5px;">
      <a id="btn_save" class="easyui-linkbutton" data-options="iconCls:'icon-save'">保存</a>
      <a id="btn_cancel" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'">取消</a>
    </div>
  </form>
</div>
<div id="win_vender" class="easyui-window" title="选择供应商" data-options="modal:true, closed:true, collapsible:false, minimizable:false, maximizable:false, resizable:false, iconCls:'icon-vender'" style="width:800px; height: 625px;">
  <div style="width: 100%; height: 545px;">
    <table id="vender_grid"></table>
  </div>
  <div style="text-align: right; margin-top: 10px; margin-right: 10px;">
    <a id="btn_select" class="easyui-linkbutton" data-options="iconCls:'icon-ok'">选择</a>
    <a id="btn_cancel" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'">取消</a>
  </div>
</div>