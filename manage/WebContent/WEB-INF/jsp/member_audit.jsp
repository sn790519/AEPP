<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/magnific-popup.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript">
$(function() {
	
	var urlPrefix = "${pageContext.request.contextPath}/api/";
	var images = new Array(5);
	var imageLoadCount = 0;
	var selectedIndex = -1;
	
	var memberAPI = new MemberAPI({urlPrefix : urlPrefix});
	
	$('#member_list').datagrid({
		url: '${pageContext.request.contextPath}/api/MemberSearchAPI.json',
	    columns: [[
            {field:'member_login_name', title:'会员登录名', width:120, align:'left', halign:'center', resizable:false},
            {field:'enterprise_name', title:'企业名称', width:202, align:'left', halign:'center', resizable:false},
            {field:'enterprise_property_name', title:'企业性质', width:100, align:'center', halign:'center', resizable:false},
            {field:'enterprise_category_name', title:'行业分类', width:100, align:'center', halign:'center', resizable:false},
            {field:'contact_name', title:'联系人', width:100, align:'center', halign:'center', resizable:false},
            {field:'lastest_audit_phase_name', title:'阶段', width:120, align:'center', halign:'center', resizable:false},
            {field:'lastest_audit_result_name', title:'状态', width:80, align:'center', halign:'center', resizable:false}
        ]],
        fit: true,
        striped: true,
        rownumbers: true,
        singleSelect: true,
        autoRowHeight: false,
        toolbar: '#toolbar_member_audit',
        idField: 'member_login_name',
        pagination: true,
        pageSize: 10,
        pagePosition: 'top',
        pageList: [10],
        queryParams: {
        	param: JSON.stringify({
        		audit_phase_to:6,
        		audit_result_to:2,
        		certificates_uploaded: true
        	})
        },
        loadFilter: function(data) {
        	var datax = {};
        	if (data.total && data.total > 0) {
    			datax.total = data.total;
    			datax.rows = data.members;
        	} else {
        		datax.total = 0;
    			datax.rows = new Array();
        	}
        	return datax;
        },
        onLoadSuccess: function(data) {
            selectedIndex = -1;
            $('#member_list').datagrid('unselectAll');
            $('#btn_audit').linkbutton('disable');
            $('#btn_audit').unbind('click');
            $('#btn_show_image').linkbutton('disable');
        	$('#btn_show_image').unbind('click');
    		$('#member_container').hide();
        },
        onSelect: function(index, row) {
        	if (index >= 0 && selectedIndex != index && row != undefined) {
            	imageLoadCount = 0;
            	images = new Array(5);
            	
        		$('#member_login_name').text(row.member_login_name);
            	$('#enterprise_name').text(row.enterprise_name);
            	$('#enterprise_property').text(row.enterprise_property_name);
            	$('#enterprise_category').text(row.enterprise_category_name);
            	$('#contact_name').text(row.contact_name);
            	$('#contact_certificates_no').text(row.contact_certificates_no);
            	$('#telephone').text(row.telephone);
            	$('#mobile').text(row.mobile);
            	$('#email').text(row.email);
            	$('#qq_no').text(row.qq_no);
            	$('#weixin_no').text(row.weixin_no);
            	$('#legal_name').text(row.legal_name);
            	$('#legal_certificates_no').text(row.legal_certificates_no);
            	
            	$('#btn_show_image').linkbutton({
            		iconCls: 'icon-reload',
            		text: '正在加载图片'
            	});
            	$('#btn_show_image').linkbutton('disable');
            	$('#btn_show_image').unbind('click');
        		$('#member_container').show();
            	
            	var imageInfo1 = {};
            	imageInfo1.image_id = row.image_contact_certificates_front;
            	imageInfo1.loaded = false;
            	imageInfo1.image_data = null;
            	imageInfo1.title = "联系人证件正面";
            	images[0] = imageInfo1;
            	
            	var imageInfo2 = {};
            	imageInfo2.image_id = row.image_contact_certificates_back;
            	imageInfo2.loaded = false;
            	imageInfo2.image_data = null;
            	imageInfo2.title = "联系人证件背面";
            	images[1] = imageInfo2;

            	var imageInfo3 = {};
            	imageInfo3.image_id = row.image_license;
            	imageInfo3.loaded = false;
            	imageInfo3.image_data = null;
            	imageInfo3.title = "工商营业执照";
            	images[2] = imageInfo3;

            	var imageInfo4 = {};
            	imageInfo4.image_id = row.image_legal_certificates_front;
            	imageInfo4.loaded = false;
            	imageInfo4.image_data = null;
            	imageInfo4.title = "法人证件正面";
            	images[3] = imageInfo4;

            	var imageInfo5 = {};
            	imageInfo5.image_id = row.image_legal_certificates_back;
            	imageInfo5.loaded = false;
            	imageInfo5.image_data = null;
            	imageInfo5.title = "法人证件背面";
            	images[4] = imageInfo5;
            	
				$.each(images, function(index, imageInfo) {
					$.loadImage(imageInfo.image_id, urlPrefix, {
						onSuccess: function(image) {
							images[index].image_data = image;
							images[index].loaded = true;
							imageLoadCount = imageLoadCount + 1;
							if (imageLoadCount == 5) {
								initImages();
							}
						},
						onNoImage: function() {
							images[index].image_data = "${pageContext.request.contextPath}/img/no_image.png";
							images[index].loaded = true;
							imageLoadCount = imageLoadCount + 1;
							if (imageLoadCount == 5) {
								initImages();
							}
						},
						onError: function(error) {
							images[index].image_data = "${pageContext.request.contextPath}/img/no_image.png";
							images[index].loaded = true;
							imageLoadCount = imageLoadCount + 1;
							if (imageLoadCount == 5) {
								initImages();
							}
						}
					});
				});
    			
    			selectedIndex = index;
    			
    			$('#btn_audit').linkbutton('enable');
    			$('#btn_audit').unbind('click');
    			$('#btn_audit').bind('click', function() {
    				showAuditWindow(row.member_login_name);
    			});
        	}
        }
	}).datagrid('getPager').pagination({
		buttons:$('#toolbar_member_audit')
	});
	
	function initImages() {
		var imageItems = new Array();
		$.each(images, function(index, image) {
			var imageItem = {};
			imageItem.src = image.image_data;
			imageItem.title = image.title;
			imageItems.push(imageItem);
		});
		
		$('#btn_show_image').linkbutton({
    		iconCls: 'icon-search',
    		text: '点击查看图片'
    	});
		$('#btn_show_image').linkbutton('enable');
		$('#btn_show_image').magnificPopup({
			items: imageItems,
			gallery: {
		        enabled: true
	        },
			type: 'image'
		});
	}
	
	function showAuditWindow(memberLoginName) {
		$('#win_audit').window({
            onOpen: function() {
                $('#btn_cancel').unbind('click');
                $('#btn_cancel').bind('click', function() {
                	$('#win_audit').window('close');
                });
                
                $('#btn_ok').unbind('click');
                $('#btn_ok').bind('click', function() {
                	if ($('#audit_form').form('validate')) {
                		var selectedData = $('#member_list').datagrid('getSelected');
                		var paramData = {};
                		paramData.member_login_name = memberLoginName;
                		paramData.audit_sequence = selectedData.lastest_audit_sequence;
                		paramData.audit_phase = selectedData.lastest_audit_phase;
                		paramData.audit_result = $('#audit_result').combobox('getValue');
                		paramData.audit_comment = $('#audit_comment').val();
                		memberAPI.audit({
                	        onSuccess : function(data) {
                	        	$('#member_list').datagrid('reload');
                	        },
                	        onAPIError : function(status, errors) {
                	        },
                	        onServerError : function(jqXHR, textStatus, errorThrown) {
                	        }
                	    },
                	    {
                	        param: JSON.stringify(paramData)
                	    });
                    	
                    	$('#win_audit').window('close');
                	}
                });
            },
            onClose: function() {
            	$('#audit_form').form('clear');
            	$('#btn_cancel').unbind('click');
            	$('#btn_ok').unbind('click');
            }
        });
        $('#win_audit').window('open');
	}
	
});
</script>
<div style="width: 850px; height: 308px;">
  <table id="member_list"></table>
</div>
<div style="width: 690px; margin-top: 20px; display: none;" id="member_container">
  <table id="member_table" class="edit_table">
    <tr>
      <th width="100" height="24">会员登录名</th>
      <td colspan="3" height="24"><label id="member_login_name"></label></td>
    </tr>
    <tr>
      <th width="100" height="24">企业名称</th>
      <td width="500" colspan="3" height="24"><label id="enterprise_name"></label></td>
    </tr>
    <tr>
      <th width="100" height="24">企业性质</th>
      <td width="200" height="24"><label id="enterprise_property"></label></td>
      <th width="100" height="24">行业分类</th>
      <td width="200" height="24"><label id="enterprise_category"></label></td>
    </tr>
    <tr>
      <th width="100" height="24">联系人</th>
      <td width="200" height="24"><label id="contact_name"></label></td>
      <th width="100" height="24">证件</th>
      <td width="200" height="24"><label id="contact_certificates_no"></label></td>
    </tr>
    <tr>
      <th width="100" height="24">联系电话</th>
      <td width="200" height="24"><label id="telephone"></label></td>
      <th width="100" height="24">手机</th>
      <td width="200" height="24"><label id="mobile"></label></td>
    </tr>
    <tr>
      <th width="100" height="24">电子邮件</th>
      <td colspan="3" height="24"><label id="email"></label></td>
    </tr>
    <tr>
      <th width="100" height="24">QQ号码</th>
      <td width="200" height="24"><label id="qq_no"></label></td>
      <th width="100" height="24">微信号</th>
      <td width="200" height="24"><label id="weixin_no"></label></td>
    </tr>
    <tr>
      <th width="100" height="24">法人姓名</th>
      <td width="200" height="24"><label id="legal_name"></label></td>
      <th width="100" height="24">证件</th>
      <td width="200" height="24"><label id="legal_certificates_no"></label></td>
    </tr>
    <tr>
      <th width="100" height="24">电子资质材料</th>
      <td colspan="3" height="24"><a id="btn_show_image" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload', disabled:true">正在加载图片</a></td>
    </tr>
  </table>
</div>
<div id="toolbar_member_audit">
  <a id="btn_audit" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-ok', disabled:true">审核</a>
</div>
<div id="win_audit" class="easyui-window" title="审核意见" data-options="modal:true, closed:true, collapsible:false, minimizable:false, maximizable:false, resizable:false">
  <div style="padding: 10px; font-size: 13pt;">
    <form id="audit_form">
      <table id="audit_table" class="edit_table">
        <tr>
          <th width="60">审核结果</th>
          <td>
            <select id="audit_result" class="easyui-combobox" name="audit_result" style="width:100px;" data-options="editable:false, required:true">
              <option value="9">通过</option>
              <option value="2">反馈</option>
            </select>
          </td>
        </tr>
        <tr>
          <th>审核意见</th>
          <td><input id="audit_comment" class="easyui-textbox" data-options="width:400,height:100,multiline:true" required></td>
        </tr>
      </table>
      <div style="text-align: right; margin-top: 5px;">
        <a id="btn_ok" class="easyui-linkbutton" data-options="iconCls:'icon-ok', plain:true"><s:text name="button.OK" /></a>
        <a id="btn_cancel" class="easyui-linkbutton" data-options="iconCls:'icon-cancel', plain:true"><s:text name="button.cancel" /></a>
      </div>
    </form>
  </div>
</div>