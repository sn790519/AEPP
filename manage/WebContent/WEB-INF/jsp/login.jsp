<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><s:text name="system_name" /></title>
<link href="<%=request.getContextPath()%>/css/login.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/themes/bootstrap/easyui.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/themes/icon.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
$(function() {
/*     $("#login_name").keypress(function(e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            $('#login_form').submit();
        }
    });

    $("#login_password").keypress(function(e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            $('#login_form').submit();
        }
    }); */
    
    $("body").keypress(function(e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            $('#login_form').submit();
        }
    });

    $("#login_button").click(function() {
        $('#login_form').submit();
    });
});
</script>
</head>
<body>
  <div id="login">
    <div id="login_header">
      <h1 class="login_logo">
        <img src="<%=request.getContextPath()%>/img/logo.gif" />
      </h1>
      <div class="login_headerContent">
        <div class="navList"></div>
        <h2 class="login_title">
          <img src="<%=request.getContextPath()%>/img/login_title.png" />
        </h2>
      </div>
    </div>
    <div id="login_content">
      <div class="loginForm">
        <s:form action="auth" namespace="/forward" id="login_form">
          <p>
            <%-- <label><s:text name="label.login.username" /></label>
            <s:textfield name="loginName" id="login_name" /> --%>
            <input name="loginName" id="login_name" class="easyui-textbox" style="width: 100%; height: 40px; padding: 2px;" data-options="iconCls:'icon-man', iconWidth:38, iconAlign:'left'">
          </p>
          <p>
            <%-- <label><s:text name="label.login.password" /></label>
            <s:password name="loginPassword" id="login_password" /> --%>
            <input name="loginPassword" id=login_password class="easyui-textbox" style="width: 100%; height: 40px; padding: 2px;" data-options="type:'password', iconCls:'icon-lock', iconWidth:38, iconAlign:'left'">
          </p>
          <div class="login_bar">
            <a href="#" id="login_button" class="easyui-linkbutton" data-options="iconCls:'icon-login', plain:false" style="width: 100px;"><s:text name="button.login" /></a>
          </div>
          <div class="error">
            <ul>
              <li><s:actionerror /> <s:fielderror /></li>
            </ul>
          </div>
        </s:form>
      </div>
      <div class="login_banner">
        <img src="<%=request.getContextPath()%>/img/login_banner.jpg" />
      </div>
      <div class="login_main">
        <ul class="helpList"></ul>
        <div class="login_inner"></div>
      </div>
    </div>
    <div id="login_footer">
      <s:text name="copyright" />
    </div>
  </div>
</body>
</html>