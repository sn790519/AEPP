<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/icheck/skins/all.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css" />
<script type="text/javascript">

	var urlPrefix = "${pageContext.request.contextPath}/api/";
	dataInitialize();


function dataInitialize(){
    $('#tender_information_title').textbox({
		height: 28,
		width: 1000,
		required: true,
		validType: {
			length: [4, 50]
		}
	});
    
    var today = (new Date()).format('yyyy-MM-dd');
    $('#enroll_start_date_start').datebox({
        height: 28,
        width: 100,
		required: true,
		editable: false,
		value: today,
		onSelect: function(date) {
		    if (date.format('yyyy-MM-dd') > $('#enroll_start_date_end').datebox('getValue')) {
		        $('#enroll_start_date_end').datebox('setValue', date.format('yyyy-MM-dd'));
		    }
		    if (date.format('yyyy-MM-dd') > $('#enroll_end_date_start').datebox('getValue')) {
		        $('#enroll_end_date_start').datebox('setValue', date.format('yyyy-MM-dd'));
		    }
		    if (date.format('yyyy-MM-dd') > $('#enroll_end_date_end').datebox('getValue')) {
		        $('#enroll_end_date_end').datebox('setValue', date.format('yyyy-MM-dd'));
		    }
		    if (date.format('yyyy-MM-dd') > $('#tender_end_date_start').datebox('getValue')) {
		        $('#tender_end_date_start').datebox('setValue', date.format('yyyy-MM-dd'));
		    }
		    if (date.format('yyyy-MM-dd') > $('#tender_end_date_end').datebox('getValue')) {
		        $('#tender_end_date_end').datebox('setValue', date.format('yyyy-MM-dd'));
		    }
		    if (date.format('yyyy-MM-dd') > $('#publish_result_date_start').datebox('getValue')) {
		        $('#publish_result_date_start').datebox('setValue', date.format('yyyy-MM-dd'));
		    }
		    if (date.format('yyyy-MM-dd') > $('#publish_result_date_end').datebox('getValue')) {
		        $('#publish_result_date_end').datebox('setValue', date.format('yyyy-MM-dd'));
		    }
		}
    });
    
    $('#enroll_start_date_end').datebox({
        height: 28,
        width: 100,
		required: true,
		editable: false,
		value: today,
		onSelect: function(date) {
		    if (date.format('yyyy-MM-dd') < $('#enroll_start_date_start').datebox('getValue')) {
		        $('#enroll_start_date_end').datebox('setValue', '').datebox('showPanel');
		    } else {
		        if (date.format('yyyy-MM-dd') > $('#enroll_end_date_start').datebox('getValue')) {
    		        $('#enroll_end_date_start').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		        if (date.format('yyyy-MM-dd') > $('#enroll_end_date_end').datebox('getValue')) {
    		        $('#enroll_end_date_end').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		        if (date.format('yyyy-MM-dd') > $('#tender_end_date_start').datebox('getValue')) {
    		        $('#tender_end_date_start').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		        if (date.format('yyyy-MM-dd') > $('#tender_end_date_end').datebox('getValue')) {
    		        $('#tender_end_date_end').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		        if (date.format('yyyy-MM-dd') > $('#publish_result_date_start').datebox('getValue')) {
    		        $('#publish_result_date_start').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		        if (date.format('yyyy-MM-dd') > $('#publish_result_date_end').datebox('getValue')) {
    		        $('#publish_result_date_end').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		    }
		}
    });
    
    $('#enroll_end_date_start').datebox({
        height: 28,
        width: 100,
		required: true,
		editable: false,
		value: today,
		onSelect: function(date) {
		    if (date.format('yyyy-MM-dd') < $('#enroll_start_date_end').datebox('getValue')) {
		        $('#enroll_start_date_end').datebox('setValue', '').datebox('showPanel');
		    } else {
		        if (date.format('yyyy-MM-dd') > $('#enroll_end_date_end').datebox('getValue')) {
    		        $('#enroll_end_date_end').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		        if (date.format('yyyy-MM-dd') > $('#tender_end_date_start').datebox('getValue')) {
    		        $('#tender_end_date_start').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		        if (date.format('yyyy-MM-dd') > $('#tender_end_date_end').datebox('getValue')) {
    		        $('#tender_end_date_end').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		        if (date.format('yyyy-MM-dd') > $('#publish_result_date_start').datebox('getValue')) {
    		        $('#publish_result_date_start').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		        if (date.format('yyyy-MM-dd') > $('#publish_result_date_end').datebox('getValue')) {
    		        $('#publish_result_date_end').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		    }
		}
    });
    
    $('#enroll_end_date_end').datebox({
        height: 28,
        width: 100,
		required: true,
		editable: false,
		value: today,
		onSelect: function(date) {
		    if (date.format('yyyy-MM-dd') < $('#enroll_end_date_start').datebox('getValue')) {
		        $('#enroll_end_date_start').datebox('setValue', '').datebox('showPanel');
		    } else {
		        if (date.format('yyyy-MM-dd') > $('#tender_end_date_start').datebox('getValue')) {
    		        $('#tender_end_date_start').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		        if (date.format('yyyy-MM-dd') > $('#tender_end_date_end').datebox('getValue')) {
    		        $('#tender_end_date_end').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		        if (date.format('yyyy-MM-dd') > $('#publish_result_date_start').datebox('getValue')) {
    		        $('#publish_result_date_start').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		        if (date.format('yyyy-MM-dd') > $('#publish_result_date_end').datebox('getValue')) {
    		        $('#publish_result_date_end').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		    }
		}
    });
    
    $('#tender_end_date_start').datebox({
        height: 28,
        width: 100,
		required: true,
		editable: false,
		value: today,
		onSelect: function(date) {
		    if (date.format('yyyy-MM-dd') < $('#enroll_end_date_end').datebox('getValue')) {
		        $('#enroll_end_date_end').datebox('setValue', '').datebox('showPanel');
		    } else {
		        if (date.format('yyyy-MM-dd') > $('#tender_end_date_end').datebox('getValue')) {
    		        $('#tender_end_date_end').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		        if (date.format('yyyy-MM-dd') > $('#publish_result_date_start').datebox('getValue')) {
    		        $('#publish_result_date_start').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		        if (date.format('yyyy-MM-dd') > $('#publish_result_date_end').datebox('getValue')) {
    		        $('#publish_result_date_end').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		    }
		}
    });
    
    $('#tender_end_date_end').datebox({
        height: 28,
        width: 100,
		required: true,
		editable: false,
		value: today,
		onSelect: function(date) {
		    if (date.format('yyyy-MM-dd') < $('#tender_end_date_start').datebox('getValue')) {
		        $('#tender_end_date_start').datebox('setValue', '').datebox('showPanel');
		    } else {
		        if (date.format('yyyy-MM-dd') > $('#publish_result_date_start').datebox('getValue')) {
    		        $('#publish_result_date_start').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		        if (date.format('yyyy-MM-dd') > $('#publish_result_date_end').datebox('getValue')) {
    		        $('#publish_result_date_end').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		    }
		}
    });
    
    $('#publish_result_date_start').datebox({
        height: 28,
        width: 100,
		required: true,
		editable: false,
		value: today,
		onSelect: function(date) {
		    if (date.format('yyyy-MM-dd') < $('#tender_end_date_end').datebox('getValue')) {
		        $('#tender_end_date_end').datebox('setValue', '').datebox('showPanel');
		    } else {
		        if (date.format('yyyy-MM-dd') > $('#publish_result_date_end').datebox('getValue')) {
    		        $('#publish_result_date_end').datebox('setValue', date.format('yyyy-MM-dd'));
    		    }
		    }
		}
    });
    
    $('#publish_result_date_end').datebox({
        height: 28,
        width: 100,
		required: true,
		editable: false,
		value: today,
		onSelect: function(date) {
		    if (date.format('yyyy-MM-dd') < $('#publish_result_date_start').datebox('getValue')) {
		        $('#publish_result_date_start').datebox('setValue', '').datebox('showPanel');
		    } 
		}
    });
    
    
    $('#tender_information_pattern').combobox({
    	valueField: 'ext',
        textField: 'text',
    	editable: false,
        required: true,
        height: 25,
        panelHeight: 'auto',
        value: '定向招标',
        data: [
               {ext:'0',text:'公开招标'},
               {ext:'1',text:'定向招标'}
        ]
    });
    
    $('#information_form #tender_title').textbox({
		height: 28,
		width: 290,
		required: true,
		validType: {
			length: [1, 100]
		}
	});
    
	$('#information_grid').datagrid({
	    title: '招标产品(最多5件)',
	    iconCls: 'icon-information',
	    url: '${pageContext.request.contextPath}/api/InformationSearchAPI.json',
	    columns: [[
            {field:'tender_title', title:'招标名称', width:'22%', align:'left', halign:'center', resizable:false},
            {field:'tender_pattern_name', title:'招标方式', width:'10%', align:'right', halign:'center', resizable:false},
            {field:'enroll_start_date', title:'报名开始日', width:'10%', align:'center', halign:'center', resizable:false},
            {field:'enroll_end_date', title:'报名截止日', width:'40%', align:'left', halign:'center', resizable:false},
            {field:'publish_result_date', title:'中标公布日', width:'40%', align:'left', halign:'center', resizable:false},
            {field:'status_name', title:'状态', width:'40%', align:'left', halign:'center', resizable:false},
            {field:'opt', title:'操作', width:'17%', align:'center', halign:'center', resizable:false,
                formatter: function(value, row, index) {
                    return '<a id="btn_edit" class="button_edit" href="#">编辑</a>&nbsp;<a id="btn_delete" class="button_delete" href="#">删除</a>';
                }
            }
        ]],
        fit: true,
        fitColumns: true,
        striped: true,
        rownumbers: true,
        singleSelect: true,
        autoRowHeight: false,
        toolbar: '#tender_publish_toolbar',
        idField: 'tender_id',
        onLoadSuccess: function(data) {
            if (data.total > 0) {
            	$('.button_edit').linkbutton({
                    iconCls: 'icon-edit',
                    onClick: function() {
                        var index = $(this).index('.button_edit');
                        var informationData = $('#information_grid').datagrid('getData').rows[index];
                        $('#win_information').window({
        		            onOpen: function() {
        		                $('#information_form').form('clear');
        		                $('#information_form #tender_title').textbox('setValue', informationData.tender_title);

        		                
        			            $('#information_form #edit_index').val(index);
        			            
        		            },
        		            onClose: function() {
        		                $('#information_form').form('clear');
        		            }
        		        });
        		        $('#win_information').window('open');
                    }
                });
                
                $('.button_delete').linkbutton({
                    iconCls: 'icon-delete',
                    onClick: function() {
                        var index = $(this).index('.button_delete');
                        $('#information_grid').datagrid('deleteRow', index);
                        $('#btn_add').linkbutton('enable');
                    }
                });
                
                $('#information_grid').datagrid('fixRowHeight');
            }
        },
        loadFilter: function(data) {
        	var datax = {};
        	if (data.total && data.total > 0) {
    			datax.total = data.total;
    			datax.rows = data.tenders;
        	} else {
        		datax.total = 0;
    			datax.rows = new Array();
        	}
        	return datax;
        }
	});
    
	$('#information_form #btn_cancel').linkbutton({
	    onClick: function() {
	        $('#win_information').window('close');
	    }
	});

	$('#information_form #btn_save').linkbutton({
	    onClick: function() {
	        if ($('#information_form').form('validate')) {
	            var informationData = {};
	            informationData.information_tender_title = $('#information_form #tender_title').textbox('getValue');

	            
	            var gridData = $('#information_grid').datagrid('getData');
	            if ($('#information_form #edit_index').val() == "") {
		            gridData.total = gridData.rows.push(informationData);
	            } else {
	                var index = $('#information_form #edit_index').val();
	                gridData.rows[index] = informationData;
	            }
	            $('#information_grid').datagrid('loadData', gridData);
	            
	            if (gridData.total && gridData.total >= 5) {
	                    $('#btn_add').linkbutton('disable');
	                } else {
	                    $('#btn_add').linkbutton('enable');
	                }
	            
	            $('#win_information').window('close');
	        }
	    }
	});
}
</script>

<div class="easyui-panel" data-options="border:0">
  <form id="tender_form">
    <table id="tender_information_table" class="edit_table" style="margin-bottom: 10px;">
      <tr>
        <th width="77">招标名称</th>
        <td colspan="7"><input type="text" id="tender_information_title"></td>
      </tr>
      <tr>
        <th width="77">报名开始日</th>
        <td width="100"><input type="text" id="enroll_start_date_start">~<input type="text" id="enroll_start_date_end"></td>
        <th width="77">报名截止日</th>
        <td width="100"><input type="text" id="enroll_end_date_start">~<input type="text" id="enroll_end_date_end"></td>
        <th width="77">投标截止日</th>
        <td width="100"><input type="text" id="tender_end_date_start">~<input type="text" id="tender_end_date_end"></td>
        <th width="77">中标公布日</th>
        <td width="100"><input type="text" id="publish_result_date_start">~<input type="text" id="publish_result_date_end"></td>
      </tr>
      <tr>
        <th width="77">招标方式</th>
        <td colspan="4" valign="middle">
          <input id="tender_information_pattern" style="width:140px;" />
        </td>
        <td colspan="3">
        	<a id="btn_search" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="width:80px">查询</a>
        </td>
      </tr>
    </table>
    <div style="width: 782px; height: 249px;">
      <table id="information_grid"></table>
    </div>
    
  </form>
</div>
<div id="win_information" class="easyui-window" title="招标信息" data-options="modal:true, closed:true, collapsible:false, minimizable:false, maximizable:false, resizable:false, iconCls:'icon-information'" style="width:430px; height: auto; padding:10px;">
  <form id="information_form">
    <table id="information_table" class="edit_table">
      <tr>
        <th width="75">招标名称</th>
        <td width="300"><input type="text" id="tender_title"></td>
      </tr>
    </table>
    <input id="edit_index" type="hidden">
    <div style="text-align: right; margin-top: 5px;">
      <a id="btn_save" class="easyui-linkbutton" data-options="iconCls:'icon-save'">保存</a>
      <a id="btn_cancel" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'">取消</a>
    </div>
  </form>
</div>