(function($) {
	
	$(document).bind("contextmenu", function(e) {   
    	return false;   
    });

	$.getTopWindow = function() {
		var p = window;
		while (p != p.parent) {
			p = p.parent;
		}
		return p;
	};
	
	$.codeNameCombo = function($option) {
		var codeNameAPI = new CodeNameAPI({
			urlPrefix : $option.urlPrefix
		});

		var paramData = {};
		paramData.type_id = $option.typeId;
		codeNameAPI.search({
			onSuccess : function(codeNameList) {
				if ($option.blank) {
					var blank = {};
					blank.code = "";
					if ($option.blankText) {
						blank.full_name = $option.blankText;
						blank.short_name = $option.blankText;
					} else {
						blank.full_name = "";
						blank.short_name = "";
					}
					codeNameList.unshift(blank);
				}
				
				$option.target.combobox({
					required: $option.required,
	 				editable: $option.editable,
	 	            panelHeight: 'auto',
	 				valueField: 'code',
	 				textField: 'name',
	 				value: $option.value,
	 				data: codeNameList,
	 				missingMessage: $option.missingMessage,
	 				onChange: function(newValue, oldValue) {
						if ($option.onChange && typeof($option.onChange) == "function") {
							$option.onChange(newValue, oldValue);
						}
					}
	 			});
			},
			onAPIError : function(status, errors) {
				$.messager.alert($option.errorTitle, $option.errorMessage);
			},
			onServerError : function(jqXHR, textStatus, errorThrown) {
				$.messager.alert($option.errorTitle, $option.errorMessage);
			}
		},
		{
			param: JSON.stringify(paramData)
		});
	};
	
	$.format = function() {
		var s = arguments[0];
		for (var i = 0; i < arguments.length - 1; i++) {
			var reg = new RegExp("\\{" + i + "\\}", "gm");
			s = s.replace(reg, arguments[i + 1]);
		}
		return s;
	};
	
	$.bytes = function() {
		var str = arguments[0];
		return str.replace(/[^\x00-\xff]/g, "**").length;
	};
	
	$(document).keydown(function(event) {
		//Alt + ←/ →
		if ((event.altKey) && ((event.keyCode == 37) || (event.keyCode == 39))) {
			event.returnValue = false;
			return false;
		}
		//Backspace
		if (event.keyCode == 8 && event.target.tagName.toLowerCase() != "input" && event.target.tagName.toLowerCase() != "textarea") {
			return false;
		}
		//F5
		if (event.keyCode == 116) {
			return false;
		}
		//Alt + R
		if ((event.ctrlKey) && (event.keyCode == 82)) {
			return false;
		}
	});
	
	$.loadImage = function(attachmentId, urlPrefix, callback) {
		var attachmentAPI = new AttachmentAPI({
			urlPrefix : urlPrefix
		});
		
		var paramData = {};
		paramData.attachment_id = attachmentId;
		attachmentAPI.loadImage({
			onSuccess : function(imageData) {
				if (imageData.image) {
					callback.onSuccess(imageData.image);
				} else {
					callback.onNoImage();
				}
			},
			onAPIError : function(status, errors) {
				callback.onError(errors);
			},
			onServerError : function(jqXHR, textStatus, errorThrown) {
				callback.onError(errorThrown);
			}
		},
		{
			param: JSON.stringify(paramData)
		});
	}
	
	// 对Date的扩展，将 Date 转化为指定格式的String
	// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
	// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
	// 例子： 
	// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
	// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
	Date.prototype.format = function (fmt) {
	    var o = {
	        "M+": this.getMonth() + 1, //月份 
	        "d+": this.getDate(), //日 
	        "h+": this.getHours(), //小时 
	        "m+": this.getMinutes(), //分 
	        "s+": this.getSeconds(), //秒 
	        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
	        "S": this.getMilliseconds() //毫秒 
	    };
	    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	    for (var k in o)
	    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	    return fmt;
	}
	
	$.fn.datebox.defaults.formatter = function(date){
	    return date.format('yyyy-MM-dd');
	}
	
})(jQuery);