(function($) {

	$.extend($.fn.validatebox.defaults.rules, {
		same : {
			validator : function(value, param) {
				if ($(param[0]).val() != "" && value != "") {
					return $(param[0]).val() == value;
				} else {
					return true;
				}
			},
			message : '两次输入的密码不一致'
		},
		telephone : {// 验证电话 
	        validator : function(value) { 
	            return /^((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$/i.test(value);
	        }, 
	        message : '格式不正确，请使用下面格式：020-88888888' 
        },
        currency : {// 验证货币 
            validator : function(value) { 
                return /^\d+(\.\d+)?$/i.test(value); 
            }, 
            message : '货币格式不正确' 
        },
        mobile: {// 验证手机
            validator: function (value) {
                return /^(13[0-9]|145|147|15[0-3]|15[5-9]|17[6-8]|18[0-9])\d{8}$/i.test(value);
            },
            message: '格式不正确，请使用下面格式：13088888888'
        },
        telephoneOrMobile: {// 验证手机
            validator: function (value) {
                return /^((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$/i.test(value) | /^(13[0-9]|145|147|15[0-3]|15[5-9]|17[6-8]|18[0-9])\d{8}$/i.test(value);
            },
            message: '格式不正确，请使用下面格式：13088888888或020-88888888'
        }
	});

})(jQuery);