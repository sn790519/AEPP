var SystemConfigAPI = function(option) {
	this.option = $.extend({}, this.option);
	this.setOption(option);
};
SystemConfigAPI.prototype = {
	option: {
		urlPrefix : null
	},
	setOption: function(option) {
		$.extend(this.option, option);
	},
	changePassword: function(handler, data) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/ChangePasswordAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	},
	update: function(handler, data) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/SystemConfigUpdateAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	}
};

var AttachmentAPI = function(option) {
	this.option = $.extend({}, this.option);
	this.setOption(option);
};
AttachmentAPI.prototype = {
	option: {
		urlPrefix : null
	},
	setOption: function(option) {
		$.extend(this.option, option);
	},
	loadImage: function(handler, data) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/ImageAttachmentSearchAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	},
	removeMemberAttachment: function(handler, data) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/AttachmentRemoveAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	}
};

var MemberAPI = function(option) {
	this.option = $.extend({}, this.option);
	this.setOption(option);
};
MemberAPI.prototype = {
	option: {
		urlPrefix : null
	},
	setOption: function(option) {
		$.extend(this.option, option);
	},
	search: function(handler, data) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/MemberSearchAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	},
	audit: function(handler, data) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/MemberAuditAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	}
};

var CodeNameAPI = function(option) {
	this.option = $.extend({}, this.option);
	this.setOption(option);
};
CodeNameAPI.prototype = {
	option: {
		urlPrefix : null
	},
	setOption: function(option) {
		$.extend(this.option, option);
	},
	search: function(handler, data) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/CodeNameSearchAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data.code_names);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	}
};

var TenderAPI = function(option) {
	this.option = $.extend({}, this.option);
	this.setOption(option);
};
TenderAPI.prototype = {
	option: {
		urlPrefix : null
	},
	setOption: function(option) {
		$.extend(this.option, option);
	},
	publish: function(handler, data) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/TenderPublishAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	}
};

var TenderListAPI = function(option) {
	this.option = $.extend({}, this.option);
	this.setOption(option);
};
TenderListAPI.prototype = {
		option: {
			urlPrefix : null
		},
		setOption: function(option) {
			$.extend(this.option, option);
		},
		search: function(handler, data) {
			$.ajax({
				type: "POST",
				dataType: "json",
				url: this.option.urlPrefix + "api/TenderSearchAPI.json",
				param: {},
				data: data,
				cache: false,
				statusCode: {
					404 : function() {
						alert("API not found.");
					}
				},
				success: function(data, textStatus, jqXHR) {
					if (data.status == '0') {
						handler.onSuccess(data);
					} else {
						var status = null;
						if ('status' in data) {
							status = data.status;
						}
						var errors = null;
						if ('errors' in data) {
							errors = data.errors;
						}
						handler.onAPIError(status, errors);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					handler.onServerError(jqXHR, textStatus, errorThrown);
				},
				complete: function() {
				}
			});
		}
	};

var AdvImageAPI = function(option) {
    this.option = $.extend({}, this.option);
    this.setOption(option);
};
AdvImageAPI.prototype = {
    option: {
        urlPrefix : null
    },
    setOption: function(option) {
        $.extend(this.option, option);
    },
    update: function(handler, data) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: this.option.urlPrefix + "api/AdvImageUpdateAPI.json",
            param: {},
            data: data,
            cache: false,
            statusCode: {
                404 : function() {
                    alert("API not found.");
                }
            },
            success: function(data, textStatus, jqXHR) {
                if (data.status == '0') {
                    handler.onSuccess(data);
                } else {
                    var status = null;
                    if ('status' in data) {
                        status = data.status;
                    }
                    var errors = null;
                    if ('errors' in data) {
                        errors = data.errors;
                    }
                    handler.onAPIError(status, errors);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                handler.onServerError(jqXHR, textStatus, errorThrown);
            },
            complete: function() {
            }
        });
    },
    remove: function(handler, data) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: this.option.urlPrefix + "api/AdvImageDeleteAPI.json",
            param: {},
            data: data,
            cache: false,
            statusCode: {
                404 : function() {
                    alert("API not found.");
                }
            },
            success: function(data, textStatus, jqXHR) {
                if (data.status == '0') {
                    handler.onSuccess(data);
                } else {
                    var status = null;
                    if ('status' in data) {
                        status = data.status;
                    }
                    var errors = null;
                    if ('errors' in data) {
                        errors = data.errors;
                    }
                    handler.onAPIError(status, errors);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                handler.onServerError(jqXHR, textStatus, errorThrown);
            },
            complete: function() {
            }
        });
    }
};
var TenderManageAPI = function(option) {
	this.option = $.extend({}, this.option);
	this.setOption(option);
};
TenderManageAPI.prototype = {
	option: {
		urlPrefix : null
	},
	setOption: function(option) {
		$.extend(this.option, option);
	},
	search: function(handler, data) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/TenderManageAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	}

};
var TenderFinalistAPI = function(option) {
	this.option = $.extend({}, this.option);
	this.setOption(option);
};
TenderFinalistAPI.prototype = {
	option: {
		urlPrefix : null
	},
	setOption: function(option) {
		$.extend(this.option, option);
	},
	insert: function(handler, data) {		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/TenderFinalistAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	
	}

};
var TenderBidlistAPI = function(option) {
	this.option = $.extend({}, this.option);
	this.setOption(option);
};
TenderBidlistAPI.prototype = {
	option: {
		urlPrefix : null
	},
	setOption: function(option) {
		$.extend(this.option, option);
	},
	search: function(handler, data) {
		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/TenderBidlistAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	
	}

};
var TenderBidAPI = function(option) {
	this.option = $.extend({}, this.option);
	this.setOption(option);
};
TenderBidAPI.prototype = {
	option: {
		urlPrefix : null
	},
	setOption: function(option) {
		$.extend(this.option, option);
	},
	insert: function(handler, data) {
		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/TenderBidAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	
	}

};
var TenderBidSuppliersAPI = function(option) {
	this.option = $.extend({}, this.option);
	this.setOption(option);
};
TenderBidSuppliersAPI.prototype = {
	option: {
		urlPrefix : null
	},
	setOption: function(option) {
		$.extend(this.option, option);
	},
	search: function(handler, data) {
		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/TenderBidSuppliersAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	
	}

};

var TenderBidSuppliersUpdateAPI = function(option) {
	this.option = $.extend({}, this.option);
	this.setOption(option);
};
TenderBidSuppliersUpdateAPI.prototype = {
	option: {
		urlPrefix : null
	},
	setOption: function(option) {
		$.extend(this.option, option);
	},
	insert: function(handler, data) {
		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: this.option.urlPrefix + "api/TenderBidSuppliersUpdateAPI.json",
			param: {},
			data: data,
			cache: false,
			statusCode: {
				404 : function() {
					alert("API not found.");
				}
			},
			success: function(data, textStatus, jqXHR) {
				if (data.status == '0') {
					handler.onSuccess(data);
				} else {
					var status = null;
					if ('status' in data) {
						status = data.status;
					}
					var errors = null;
					if ('errors' in data) {
						errors = data.errors;
					}
					handler.onAPIError(status, errors);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				handler.onServerError(jqXHR, textStatus, errorThrown);
			},
			complete: function() {
			}
		});
	
	}

};



var UserAPI = function(option) {
    this.option = $.extend({}, this.option);
    this.setOption(option);
};
UserAPI.prototype = {
    option: {
        urlPrefix : null
    },
    setOption: function(option) {
        $.extend(this.option, option);
    },
    insert: function(handler, data) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: this.option.urlPrefix + "api/UserInsertAPI.json",
            param: {},
            data: data,
            cache: false,
            statusCode: {
                404 : function() {
                    alert("API not found.");
                }
            },
            success: function(data, textStatus, jqXHR) {
                if (data.status == '0') {
                    handler.onSuccess(data);
                } else {
                    var status = null;
                    if ('status' in data) {
                        status = data.status;
                    }
                    var errors = null;
                    if ('errors' in data) {
                        errors = data.errors;
                    }
                    handler.onAPIError(status, errors);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                handler.onServerError(jqXHR, textStatus, errorThrown);
            },
            complete: function() {
            }
        });
    },
    update: function(handler, data) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: this.option.urlPrefix + "api/UserUpdateAPI.json",
            param: {},
            data: data,
            cache: false,
            statusCode: {
                404 : function() {
                    alert("API not found.");
                }
            },
            success: function(data, textStatus, jqXHR) {
                if (data.status == '0') {
                    handler.onSuccess(data);
                } else {
                    var status = null;
                    if ('status' in data) {
                        status = data.status;
                    }
                    var errors = null;
                    if ('errors' in data) {
                        errors = data.errors;
                    }
                    handler.onAPIError(status, errors);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                handler.onServerError(jqXHR, textStatus, errorThrown);
            },
            complete: function() {
            }
        });
    },
    remove: function(handler, data) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: this.option.urlPrefix + "api/UserDeleteAPI.json",
            param: {},
            data: data,
            cache: false,
            statusCode: {
                404 : function() {
                    alert("API not found.");
                }
            },
            success: function(data, textStatus, jqXHR) {
                if (data.status == '0') {
                    handler.onSuccess(data);
                } else {
                    var status = null;
                    if ('status' in data) {
                        status = data.status;
                    }
                    var errors = null;
                    if ('errors' in data) {
                        errors = data.errors;
                    }
                    handler.onAPIError(status, errors);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                handler.onServerError(jqXHR, textStatus, errorThrown);
            },
            complete: function() {
            }
        });
    },
    insertGroup: function(handler, data) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: this.option.urlPrefix + "api/GroupInsertAPI.json",
            param: {},
            data: data,
            cache: false,
            statusCode: {
                404 : function() {
                    alert("API not found.");
                }
            },
            success: function(data, textStatus, jqXHR) {
                if (data.status == '0') {
                    handler.onSuccess(data);
                } else {
                    var status = null;
                    if ('status' in data) {
                        status = data.status;
                    }
                    var errors = null;
                    if ('errors' in data) {
                        errors = data.errors;
                    }
                    handler.onAPIError(status, errors);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                handler.onServerError(jqXHR, textStatus, errorThrown);
            },
            complete: function() {
            }
        });
    },
    updateGroup: function(handler, data) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: this.option.urlPrefix + "api/GroupUpdateAPI.json",
            param: {},
            data: data,
            cache: false,
            statusCode: {
                404 : function() {
                    alert("API not found.");
                }
            },
            success: function(data, textStatus, jqXHR) {
                if (data.status == '0') {
                    handler.onSuccess(data);
                } else {
                    var status = null;
                    if ('status' in data) {
                        status = data.status;
                    }
                    var errors = null;
                    if ('errors' in data) {
                        errors = data.errors;
                    }
                    handler.onAPIError(status, errors);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                handler.onServerError(jqXHR, textStatus, errorThrown);
            },
            complete: function() {
            }
        });
    },
    removeGroup: function(handler, data) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: this.option.urlPrefix + "api/GroupDeleteAPI.json",
            param: {},
            data: data,
            cache: false,
            statusCode: {
                404 : function() {
                    alert("API not found.");
                }
            },
            success: function(data, textStatus, jqXHR) {
                if (data.status == '0') {
                    handler.onSuccess(data);
                } else {
                    var status = null;
                    if ('status' in data) {
                        status = data.status;
                    }
                    var errors = null;
                    if ('errors' in data) {
                        errors = data.errors;
                    }
                    handler.onAPIError(status, errors);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                handler.onServerError(jqXHR, textStatus, errorThrown);
            },
            complete: function() {
            }
        });
    }
};